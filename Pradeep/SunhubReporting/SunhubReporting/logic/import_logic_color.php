<?php
include '../config/database.php';
include '../includes/PHPExcel/Classes/PHPExcel/IOFactory.php';
include '../includes/PHPExcel/Classes/PHPExcel.php';
ob_start();
error_reporting(E_ALL);
set_time_limit(0);
class Import
{
	function upload_recruit_data($filNam,$created,$mysqli)
	{
		$uploaded_dir = $_SERVER['DOCUMENT_ROOT'].'/eliteRecruit/uploadReports';
		$file_path = $uploaded_dir.'/'.$filNam;
		 if(preg_match('/.csv/', $filNam))
		{
			$objReader = new PHPExcel_Reader_Excel2007();
			$objReader->setReadDataOnly(true);
			$objReader = new PHPExcel_Reader_CSV();
			$objPHPExcel = $objReader->load($file_path);
			$allSheetName=$objPHPExcel->getSheetNames();
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			$highestRow = $objWorksheet->getHighestRow();
		    $highestColumn = $objWorksheet->getHighestColumn();
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);	
			 for ($column = 0; $column <= $highestColumnIndex; $column++) 
			   {
					$columnNamez[$column] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column,1)->getValue();
					//var_dump($columnNamez[$column]);
						
			   }
			    foreach($columnNamez as $c)
				{
					$columnName[]=trim($c);
					//var_dump($columnName);
				} 
				$arrayColumn = array("Number","Title","Preliminary Program","Program","Market","Status","First Name","Last Name","Email Address","Home Phone #","Cell Phone #","Other Phone #","Job Posting Site","Employee Referral","Date Applied","Date Contacted","Contacted By","Contact Notes","Comments","Interview Scheduled","Interviewing Manager","Interview Date","Interview Time","Interview Outcome","Rescheduled Date","New Hire Link Sent","Start Date","End Date","Production");
				$columnIndex = array_keys(array_intersect($columnName, $arrayColumn));
				$cellValue = array();
				$i=0;
				
				 for ($row = 2; $row <= $highestRow; $row++) 
					{
						for($column = 0; $column < count($columnIndex); $column++) 
						{
							$cellValue[$i][$column] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columnIndex[$column],$row)->getValue();
							//var_dump($cellValue[$i][$column]);
						}
						$i++;
					
				   }
				   $uniq_pointerarray=array();					
					$duplicate_location=array();
				   for($i=0;$i<count($cellValue);$i++)
					{						
						$uniqstatus	=0;
						for($j=0; $j<count($cellValue); $j++)
						{							
							if($i != $j)
							{
								if($cellValue[$i][0] == $cellValue[$j][0])
								{
								   $uniqstatus=1;								   
																								
								}						
							}
								
						}
						if($uniqstatus == 0)
						{
							array_push($uniq_pointerarray, $i);							
						}
					}
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->setActiveSheetIndex(0);
					
					$default_border = array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb'=>'000000'));
					$style_header = array('borders' => array('bottom' => $default_border,'left' => $default_border,'top' => $default_border,'right' => $default_border),
					'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'92D050')),
					'font' => array('bold' => true));
					
					$style_error = array('borders' => array('bottom' => $default_border,'left' => $default_border,'top' => $default_border,'right' => $default_border),
					'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'ffcccc')),
					'font' => array('bold' => false));
					
					 $style_text = array('borders' => array('bottom' => $default_border,'left' => $default_border,'top' => $default_border,'right' => $default_border),
					'font' => array('bold' => false));
					
					 $style_exist_text = array('borders' => array('bottom' => $default_border,'left' => $default_border,'top' => $default_border,'right' => $default_border),
					'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'ffcccc')),
					'font' => array('bold' => false));
					
					$objPHPExcel->getActiveSheet()->getStyle('A1:AC1')->applyFromArray( $style_header );
					
					$cells_border = array('A1','B1','C1','D1','E1','F1','G1','H1','I1','J1','K1','L1','M1','N1','O1','P1','Q1','R1','S1','T1','U1','V1','W1','X1','Y1','Z1','AA1','AB1','AC1');
					foreach($cells_border as $cell_brd)
					{
						$objPHPExcel->getActiveSheet()->getStyle($cell_brd)->applyFromArray($style_header);
					}

					
					$cells = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC');

					foreach($cells as $cell)
					{
						//$objPHPExcel->getActiveSheet()->getStyle($cell.'3')->applyFromArray( $style_header );
						$objPHPExcel->getActiveSheet()->getColumnDimension($cell)->setAutoSize(true);
						
					} 
					
					$objPHPExcel->getDefaultStyle()
								->getAlignment()
								->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER) 
								->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER ); 
								
					$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Number');
					$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Title');
					$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Preliminary Program');
					$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Program');
					$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Market');
					$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'First Name');
					$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Last Name');
					$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Email Address');
					$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Home Phone #');
					$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Cell Phone #');
					$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Phone #');
					$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Job Posting Site');
					$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Employee Referral');
					$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Date Applied');
					$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Date Contacted');
					$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Contacted By');
					$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Contact Notes');
					$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Comments');
					$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Interview Scheduled');
					$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Interviewing Manager');
					$objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Interview Date');
					$objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Interview Time');
					$objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Interview Outcome');
					$objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Rescheduled Date');
					$objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'New Hire Link Sent');
					$objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Start Date');
					$objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'End Date');
					$objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'Production');
					
					
					//$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'DUPLICATES RECORDS');
							//$j=4;
							$j=2;
					for($i=0;$i<count($cellValue);$i++)//getting duplicates
					{
						if(!in_array($i, $uniq_pointerarray))
						{
								$dup_status = 3;
								$dup_stat = 1;
								$number_unique =$cellValue[$i][0];
								$number_unique_dup = $number_unique.' (Number is duplicated)';
								$number_uniqueArray[] = $number_unique;
								$title =$cellValue[$i][1];
								//$titleArray[] =  $title;
								$preliminary_program =$cellValue[$i][2];
								$program =$cellValue[$i][3];
								$market =$cellValue[$i][4];
								$status =$cellValue[$i][5];
								$firstname =$cellValue[$i][6];
								$lastname =$cellValue[$i][7];
								$emailaddress =$cellValue[$i][8];
								$homephone =$cellValue[$i][9];
								$cellphone =$cellValue[$i][10];
								$otherphone =$cellValue[$i][11];
								$jobPostingSite =$cellValue[$i][12];
								$employeeReferral =$cellValue[$i][13];
								$dateApplied =$cellValue[$i][14];
								$dateContacted =$cellValue[$i][15];
								$contactedBy =$cellValue[$i][16];
								$contactNotes =$cellValue[$i][17];
								$comments =$cellValue[$i][18];
								$interviewScheduled =$cellValue[$i][19];
								$interviewingManager =$cellValue[$i][20];
								$interviewDate = $cellValue[$i][21];
								$interviewTime =$cellValue[$i][22];
								$interviewOutcome =$cellValue[$i][23];
								$rescheduledDate =$cellValue[$i][24];
								$newHireLinkSent =$cellValue[$i][25];
								$startDate =$cellValue[$i][26];
								$enddate =$cellValue[$i][27];
								$production =$cellValue[$i][28];
								
								$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray( $style_error );
								if (!filter_var($emailaddress, FILTER_VALIDATE_EMAIL)) {
									  $emailshow = $emailaddress." (Invalid email format)"; 
									  $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray( $style_error );
									  
									}
									else{
										$objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray( $style_text );
										$emailshow = $emailaddress; 
									}
									if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $homephone)) {
									
										$homephoneshow = $homephone." (Homephone Format is not valid)";
										$objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray( $style_error );
									}
									else{ 
										$objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray( $style_text );
										$homephoneshow =  $homephone;
										//echo $homephone;
									}
									if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $cellphone)) {
									
										$cellphoneshow = $cellphone." (Cellphone Format is not valid)";
										$objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray( $style_error );
									}
									else{
										$objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray( $style_text );
										$cellphoneshow =  $cellphone;
										//echo $homephone;
									}
									if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $otherphone)) {
									
										$otherphoneshow = $otherphone."( Otherphone Format is not valid)";
										$objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray( $style_error );
									}
									else{
										$objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray( $style_text );
										$otherphoneshow =  $otherphone;
									//echo $homephone;
										}
									if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $dateApplied))
										{
										    $dateappliedshow = $dateApplied." (Date Applied Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('O'.$j)->applyFromArray( $style_error );
										}
										else
										{
											$objPHPExcel->getActiveSheet()->getStyle('O'.$j)->applyFromArray( $style_text );
										     $dateappliedshow =  $dateApplied;
										}
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $dateContacted))
										{
										    $dateContactedshow = $dateContacted." (Date Contacted Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('P'.$j)->applyFromArray( $style_error );
										}
										else
										{
											$objPHPExcel->getActiveSheet()->getStyle('P'.$j)->applyFromArray( $style_text );
										     $dateContactedshow =  $dateContacted;
										}
										//echo $all_interviewDate;
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $interviewDate))
										{
										    $interviewDateshow = $interviewDate." (Interview Date Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('V'.$j)->applyFromArray( $style_error );
										}
										else
										{
											$objPHPExcel->getActiveSheet()->getStyle('V'.$j)->applyFromArray( $style_text );
										     $interviewDateshow =  $interviewDate;
										}										
										
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $rescheduledDate))
										{
										    $rescheduledDateshow = $rescheduledDate." (Rescheduled Date Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('Y'.$j)->applyFromArray( $style_error );
										}
										else
										{
											$objPHPExcel->getActiveSheet()->getStyle('Y'.$j)->applyFromArray( $style_text );
										     $rescheduledDateshow =  $rescheduledDate;
										}
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $newHireLinkSent))
										//if(!preg_match("^d{1,2}/d{2}/d{4}^", $all_newHireLinkSent))
										{
										    $newHireLinkSentshow = $newHireLinkSent." (New Hire Link Sent Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('Z'.$j)->applyFromArray( $style_error );
										}
										else
										{
											$objPHPExcel->getActiveSheet()->getStyle('Z'.$j)->applyFromArray( $style_text );
										     $newHireLinkSentshow =  $newHireLinkSent;
										}
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $startDate))
										{
										    $startDateshow = $startDate." (Start Date Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('AA'.$j)->applyFromArray( $style_error );
										}
										else
										{
											$objPHPExcel->getActiveSheet()->getStyle('AA'.$j)->applyFromArray( $style_text );
										     $startDateshow =  $startDate;
										}
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $enddate))
										{
										    $enddateshow = $enddate." (End Date Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('AB'.$j)->applyFromArray( $style_error );
										}
										else
										{
											$objPHPExcel->getActiveSheet()->getStyle('AB'.$j)->applyFromArray( $style_text );
										     $enddateshow =  $enddate;
										}	
								$objPHPExcel->getActiveSheet()->SetCellValue('A'.$j, $number_unique_dup);
							    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$j, $title);
							    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$j, $preliminary_program);
								$objPHPExcel->getActiveSheet()->SetCellValue('D'.$j, $program);
								$objPHPExcel->getActiveSheet()->SetCellValue('E'.$j, $market);
								$objPHPExcel->getActiveSheet()->SetCellValue('F'.$j, $status);
								$objPHPExcel->getActiveSheet()->SetCellValue('G'.$j, $firstname);
								$objPHPExcel->getActiveSheet()->SetCellValue('H'.$j, $lastname);
								
							    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$j, $emailshow);
							    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$j, $homephoneshow);
								$objPHPExcel->getActiveSheet()->SetCellValue('K'.$j, $cellphoneshow );
								$objPHPExcel->getActiveSheet()->SetCellValue('L'.$j, $otherphoneshow );
								
								$objPHPExcel->getActiveSheet()->SetCellValue('M'.$j, $jobPostingSite);
								$objPHPExcel->getActiveSheet()->SetCellValue('N'.$j, $employeeReferral);
								$objPHPExcel->getActiveSheet()->SetCellValue('O'.$j, $dateappliedshow);
								$objPHPExcel->getActiveSheet()->SetCellValue('P'.$j, $dateContactedshow);
								$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$j, $contactedBy);
								$objPHPExcel->getActiveSheet()->SetCellValue('R'.$j, $contactNotes);
								$objPHPExcel->getActiveSheet()->SetCellValue('S'.$j, $comments);
								$objPHPExcel->getActiveSheet()->SetCellValue('T'.$j, $interviewScheduled);
								$objPHPExcel->getActiveSheet()->SetCellValue('U'.$j, $interviewingManager);
								
								$objPHPExcel->getActiveSheet()->SetCellValue('V'.$j, $interviewDateshow);
								
								$objPHPExcel->getActiveSheet()->SetCellValue('W'.$j, $interviewTime);
								$objPHPExcel->getActiveSheet()->SetCellValue('X'.$j, $interviewOutcome);
								
								$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$j, $rescheduledDateshow);
								$objPHPExcel->getActiveSheet()->SetCellValue('Z'.$j, $newHireLinkSentshow);
								$objPHPExcel->getActiveSheet()->SetCellValue('AA'.$j, $startDateshow);
								$objPHPExcel->getActiveSheet()->SetCellValue('AB'.$j, $enddateshow);
								
								$objPHPExcel->getActiveSheet()->SetCellValue('AC'.$j, $production);
								
								$objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('N'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('Q'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('R'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('S'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('T'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('U'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('W'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('X'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('AC'.$j)->applyFromArray( $style_text );
							 $j++;
								
							
					    }
					}
					//$k = $j+1;
					//$objPHPExcel->getActiveSheet()->SetCellValue('A'.$k, 'UNIQUE ERROR RECORDS');
					//$j = $k+1;
					//$dbinsertedvalue=0;
					
					if($dup_status!='')
					{
					  //echo "val".$k = $j+1;
					  $k = $j;
					  $j = $k;
					}
					//echo $j;
						//echo 'ewr'.$j;
					$al_number_unique  = array();
					$al_titleArray  = array();
					$al_preliminary_programArray  = array();
					$al_programArray  = array();
					$al_marketArray  = array();
					$al_statusArray  = array();
					$al_firstnameArray  = array();
					$al_lastnameArray  = array();
					$al_emailaddressArray  = array();
					$al_homephoneArray  = array();
					$al_cellphoneArray  = array();
					$al_otherphoneArray  = array();
					$al_jobPostingSiteArray  = array();
					$al_employeeReferralArray  = array();
					$al_dateAppliedArray  = array();
					$al_dateContactedArray  = array();
					$al_contactedByArray  = array();
					$al_contactNotesArray  = array();
					$al_commentsArray  = array();
					$al_interviewScheduledArray  = array();
					$al_interviewingManagerArray  = array();
					$al_interviewDateArray  = array();
					$al_interviewTimeArray  = array();
					$al_interviewOutcomeArray  = array();
					$al_rescheduledDateArray  = array();
					$al_newHireLinkSentArray  = array();
					$al_startDateArray  = array();
					$al_enddateArray  = array();
					$al_production  = array();

					for($i=0;$i<count($cellValue);$i++)//unique records
					{
						if(in_array($i, $uniq_pointerarray))
						{
								
								$number_unique =$cellValue[$i][0];
								//$number_uniqueArray[] = $number_unique;
								$title =$cellValue[$i][1];
								//$titleArray[] =  $title;
								$preliminary_program =$cellValue[$i][2];
								$program =$cellValue[$i][3];
								$market =$cellValue[$i][4];
								$status =$cellValue[$i][5];
								$firstname =$cellValue[$i][6];
								$lastname =$cellValue[$i][7];
								$emailaddress =$cellValue[$i][8];
								$homephone =$cellValue[$i][9];
								$cellphone =$cellValue[$i][10];
								$otherphone =$cellValue[$i][11];
								$jobPostingSite =$cellValue[$i][12];
								$employeeReferral =$cellValue[$i][13];
								$all_dateApplied =$cellValue[$i][14];
								$dateApplied = date('Y-m-d',strtotime($cellValue[$i][14]));
								$all_dateContacted =$cellValue[$i][15];
								$dateContacted = date('Y-m-d',strtotime($cellValue[$i][15]));
								$contactedBy =$cellValue[$i][16];
								$contactNotes =$cellValue[$i][17];
								$comments =$cellValue[$i][18];
								$interviewScheduled =$cellValue[$i][19];
								$interviewingManager =$cellValue[$i][20];
								$all_interviewDate =$cellValue[$i][21];
								$interviewDate = $cellValue[$i][21];
								
								if($interviewDate=='')
								{
									$interviewDate = '';
								}
								else
								{
									$interviewDate = date('Y-m-d',strtotime($cellValue[$i][21]));
								}
								$interviewTime =$cellValue[$i][22];
								$interviewOutcome =$cellValue[$i][23];
								$all_rescheduledDate =$cellValue[$i][24];
								$rescheduledDate =$cellValue[$i][24];
								if($rescheduledDate=='')
								{
									$rescheduledDate = '';
								}
								else
								{
								  $rescheduledDate = date('Y-m-d',strtotime($cellValue[$i][24]));
								}
								$all_newHireLinkSent =$cellValue[$i][25];
								$newHireLinkSent =$cellValue[$i][25];
								
								if($newHireLinkSent=='')
								{
									$newHireLinkSent = '';
								}
								else
								{
								  $newHireLinkSent = date('Y-m-d',strtotime($cellValue[$i][25]));
								}
								$all_startDate =$cellValue[$i][26];
								$startDate =$cellValue[$i][26];
								if($startDate=='')
								{
									$startDate = '';
								}
								else
								{
									 $startDate = date('Y-m-d',strtotime($cellValue[$i][26]));
								}
								$all_enddate =$cellValue[$i][27];
								$enddate =$cellValue[$i][27];
								if($enddate=='')
								{
									$enddate = '';
								}
								else
								{
									 $enddate = date('Y-m-d',strtotime($cellValue[$i][27]));
								}
								$production =$cellValue[$i][28];
								$errorlog_id = 0;
								$createdDate = date('Y-m-d H:i:s');
								
								//if(($number_unique!=0 && filter_var($emailaddress, FILTER_VALIDATE_EMAIL) && $number_unique!='' && preg_match("/^[1-9][0-9]*$/",$number_unique]) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $homephone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $cellphone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $otherphone)))
								if($number_unique!=0 && filter_var($emailaddress, FILTER_VALIDATE_EMAIL) && $number_unique!='' && 
							    preg_match("/^[1-9][0-9]*$/",$number_unique) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $homephone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $cellphone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $otherphone) && (preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_dateApplied) || $all_dateApplied=='') && (preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_dateContacted) || $all_dateContacted=='') && ( preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_interviewDate) || $all_interviewDate=='' ) && (preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_rescheduledDate) || $all_rescheduledDate=='') && (preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_newHireLinkSent) || $all_newHireLinkSent=='') && (preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_startDate) || $all_startDate=='') && (preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_enddate) || $all_enddate==''))
								{
										
									 $select_number = $mysqli->prepare("select id  from recruitData where number=?");
									 $select_number->bind_param("s",$number_unique);
										$select_number->execute();
										$select_number->store_result();
										$select_number->bind_result($insid);
										$select_number->fetch();
										$rowcount = $select_number->num_rows;
										if($rowcount!=0)
										{
											$status_exi=1;
											array_push($al_number_unique,$number_unique);
											array_push($al_titleArray,$title);
											array_push($al_preliminary_programArray,$preliminary_program);
											array_push($al_programArray,$program);
											array_push($al_marketArray,$market);
											array_push($al_statusArray,$status);
											array_push($al_firstnameArray,$firstname);
											array_push($al_lastnameArray,$lastname);
											array_push($al_emailaddressArray,$emailaddress);
											array_push($al_homephoneArray,$homephone);
											array_push($al_cellphoneArray,$cellphone);
											array_push($al_otherphoneArray,$otherphone);
											array_push($al_jobPostingSiteArray,$jobPostingSite);
											array_push($al_employeeReferralArray,$employeeReferral);
											array_push($al_dateAppliedArray,$all_dateApplied);
											array_push($al_dateContactedArray,$all_dateContacted);
											array_push($al_contactedByArray,$contactedBy);
											array_push($al_contactNotesArray,$contactNotes);
											array_push($al_commentsArray,$comments);
											array_push($al_interviewScheduledArray,$interviewScheduled);
											array_push($al_interviewingManagerArray,$interviewingManager);
											array_push($al_interviewDateArray,$all_interviewDate);
											array_push($al_interviewTimeArray,$interviewTime);
											array_push($al_interviewOutcomeArray,$interviewOutcome);
											array_push($al_rescheduledDateArray,$all_rescheduledDate);
											array_push($al_newHireLinkSentArray,$all_newHireLinkSent);
											array_push($al_startDateArray,$all_startDate);
											array_push($al_enddateArray,$all_enddate);
											array_push($al_production,$production);
											
											/*$stmt_update = $mysqli->prepare("UPDATE `recruitData_pra` SET `title`=?,`preliminary_program`=?,`program`=?,`market`=?,`status`=?,`firstname`=?,`lastname`=?,`email_address`=?,`homephone`=?,`cellphone`=?,`otherphone`=?,`jobPostingSite`=?,`employee_referral`=?,`date_applied`=?,`date_contacted`=?,`contactedby`=?,`contactnotes`=?,`comments`=?,`interviewScheduled`=?,`interviewingManager`=?,`interview_date`=?,`interview_time`=?,`interview_outcome`=?,`rescheduled_date`=?,`newHireLinkSent`=?,`startDate`=?,`endDate`=?,`production`=?,`errorlog_id`=?,`createdDate`=? WHERE id=$insid");
											$stmt_update->bind_param('ssssssssssssssssssssssssssssss',trim($title),trim($preliminary_program),trim($program),trim($market),trim($status),trim($firstname),trim($lastname),trim($emailaddress),trim($homephone),trim($cellphone),trim($otherphone),trim($jobPostingSite),trim($employeeReferral),trim($dateApplied),trim($dateContacted),trim($contactedBy),trim($contactNotes),trim($comments),trim($interviewScheduled),trim($interviewingManager),trim($interviewDate),trim($interviewTime),trim($interviewOutcome),trim($rescheduledDate),trim($newHireLinkSent),trim($startDate),trim($enddate),trim($production),$errorlog_id,trim($createdDate));
											$stmt_update->execute();
											$stmt_update->close();*/
											
											
										} 
										else
										{
											if($all_dateApplied=='')
											{
												$dateApplied_ins = $all_dateApplied;
											}
											else{
												$dateApplied_ins = $dateApplied;
											}
											if($all_dateContacted=='')
											{
												$dateContacted_ins = $all_dateContacted;
											}
											else{
												$dateContacted_ins = $dateContacted;
											}
											if($all_interviewDate=='')
											{
												$interviewDate_ins = $all_interviewDate;
											}
											else{
												$interviewDate_ins = $interviewDate;
											}
											if($all_rescheduledDate=='')
											{
												$rescheduledDate_ins = $all_rescheduledDate;
											}
											else{
												$rescheduledDate_ins = $rescheduledDate;
											}
											if($all_newHireLinkSent=='')
											{
												$newHireLinkSent_ins = $all_newHireLinkSent;
											}
											else{
												$newHireLinkSent_ins = $newHireLinkSent;
											}
											if($all_startDate=='')
											{
												$startDate_ins = $all_startDate;
											}
											else{
												$startDate_ins = $startDate;
											}
											if($all_enddate=='')
											{
												$enddate_ins = $all_enddate;
											}
											else{
												$enddate_ins = $enddate;
											}
											
											$status_ins=1;
											$fname = $filNam;
											 $stmt_insert = $mysqli->prepare("INSERT INTO `recruitData`(`number`, `title`, `preliminary_program`, `program`, `market`, `status`, `firstname`, `lastname`, `email_address`, `homephone`, `cellphone`, `otherphone`, `jobPostingSite`, `employee_referral`, `date_applied`, `date_contacted`, `contactedby`, `contactnotes`, `comments`, `interviewScheduled`, `interviewingManager`, `interview_date`, `interview_time`, `interview_outcome`, `rescheduled_date`, `newHireLinkSent`, `startDate`, `endDate`, `production`,`errorlog_id`, `createdDate`,`original_file`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
											$stmt_insert->bind_param('issssssssssssssssssssssssssssiss',trim($number_unique),trim($title),trim($preliminary_program),trim($program),trim($market),trim($status),trim($firstname),trim($lastname),trim($emailaddress),trim($homephone),trim($cellphone),trim($otherphone),trim($jobPostingSite),trim($employeeReferral),trim($dateApplied_ins),trim($dateContacted_ins),trim($contactedBy),trim($contactNotes),trim($comments),trim($interviewScheduled),trim($interviewingManager),trim($interviewDate_ins),trim($interviewTime),trim($interviewOutcome),trim($rescheduledDate_ins),trim($newHireLinkSent_ins),trim($startDate_ins),trim($enddate_ins),trim($production),$errorlog_id,trim($createdDate),$fname); 
											$stmt_insert->execute();
											$stmt_insert->close(); 
											//$dbinsertedvalue++;
										}
										
									//$dbinsertedvalue++;
									
									
								}
								else
								{
										$status_err_un = 1;
										
										 if (!preg_match("/^[1-9][0-9]*$/",$number_unique)) {
										$numbershow = $number_unique." (Number Format is not valid)"; 
										$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray( $style_error );
										}
										
										else{
											$numbershow = $number_unique;
											$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray( $style_text );
										}
										if (!filter_var($emailaddress, FILTER_VALIDATE_EMAIL)) {
										  $emailshow = $emailaddress." (Invalid email format)"; 
										  $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray( $style_error );
										  
										}
										else{
											$emailshow = $emailaddress; 
											$objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray( $style_text );
										}
										if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $homephone)) {
										
											$homephoneshow = $homephone." (Homephone Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray( $style_error );
										}
										else{ 
										   
											$homephoneshow =  $homephone;
											$objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray( $style_text );
											//echo $homephone;
										}
										if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $cellphone)) {
										
											$cellphoneshow = $cellphone." (Cellphone Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray( $style_error );
										}
										else{
											$cellphoneshow =  $cellphone;
											$objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray( $style_text );
											//echo $homephone;
										}
										if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $otherphone)) {
										
											$otherphoneshow = $otherphone." (Otherphone Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray( $style_error );
										}
										else{
											$otherphoneshow =  $otherphone;
											$objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray( $style_text );
										//echo $homephone;
											}
										//if(!preg_match("^d{1,2}/d{2}/d{4}^", $all_dateApplied))
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_dateApplied))
										{
										    $dateappliedshow = $all_dateApplied." (Date Applied Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('O'.$j)->applyFromArray( $style_error );
										}
										else
										{
										     $dateappliedshow =  $all_dateApplied;
											 $objPHPExcel->getActiveSheet()->getStyle('O'.$j)->applyFromArray( $style_text );
										}
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_dateContacted))
										{
										    $dateContactedshow = $all_dateContacted." (Date Contacted Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('P'.$j)->applyFromArray( $style_error );
										}
										else
										{
										     $dateContactedshow =  $all_dateContacted;
											 $objPHPExcel->getActiveSheet()->getStyle('P'.$j)->applyFromArray( $style_text );
										}
										//echo $all_interviewDate;
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_interviewDate) || $all_interviewDate=='')
										{
											
										    $interviewDateshow = $all_interviewDate." (Interview Date Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('V'.$j)->applyFromArray( $style_error );
										}
										else
										{
										     $interviewDateshow =  $all_interviewDate;
											 $objPHPExcel->getActiveSheet()->getStyle('V'.$j)->applyFromArray( $style_text );
										}										
										
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_rescheduledDate) || $all_rescheduledDate=='')
										{
										    $rescheduledDateshow = $all_rescheduledDate." (Rescheduled Date Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('Y'.$j)->applyFromArray( $style_error );
										}
										else
										{
										     $rescheduledDateshow =  $all_rescheduledDate;
											 $objPHPExcel->getActiveSheet()->getStyle('Y'.$j)->applyFromArray( $style_text );
										}
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_newHireLinkSent) || $all_newHireLinkSent=='')
										//if(!preg_match("^d{1,2}/d{2}/d{4}^", $all_newHireLinkSent))
										{
										    $newHireLinkSentshow = $all_newHireLinkSent." (New Hire Link Sent Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('Z'.$j)->applyFromArray( $style_error );
										}
										else
										{
										     $newHireLinkSentshow =  $all_newHireLinkSent;
											 $objPHPExcel->getActiveSheet()->getStyle('Z'.$j)->applyFromArray( $style_text );
										}
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_startDate) || $all_startDate=='')
										{
										    $startDateshow = $all_startDate." (Start Date Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('AA'.$j)->applyFromArray( $style_error );
										}
										else
										{
										     $startDateshow =  $all_startDate;
											 $objPHPExcel->getActiveSheet()->getStyle('AA'.$j)->applyFromArray( $style_text );
										}
										if(!preg_match("/[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}/", $all_enddate) || $all_enddate=='')
										{
										    $enddateshow = $all_enddate." (End Date Format is not valid)";
											$objPHPExcel->getActiveSheet()->getStyle('AB'.$j)->applyFromArray( $style_error );
										}
										else
										{
										     $enddateshow =  $all_enddate;
											  $objPHPExcel->getActiveSheet()->getStyle('AB'.$j)->applyFromArray( $style_text );
										}
										//echo "<br/>";
										//echo "unique error".$j;
									 $objPHPExcel->getActiveSheet()->SetCellValue('A'.$j, $numbershow);
									 
									$objPHPExcel->getActiveSheet()->SetCellValue('B'.$j, $title);
									$objPHPExcel->getActiveSheet()->SetCellValue('C'.$j, $preliminary_program);
									$objPHPExcel->getActiveSheet()->SetCellValue('D'.$j, $program);
									$objPHPExcel->getActiveSheet()->SetCellValue('E'.$j, $market);
									$objPHPExcel->getActiveSheet()->SetCellValue('F'.$j, $status);
									$objPHPExcel->getActiveSheet()->SetCellValue('G'.$j, $firstname);
									$objPHPExcel->getActiveSheet()->SetCellValue('H'.$j, $lastname); 
									
									 $objPHPExcel->getActiveSheet()->SetCellValue('I'.$j, $emailshow);
									$objPHPExcel->getActiveSheet()->SetCellValue('J'.$j, $homephoneshow);
									
									$objPHPExcel->getActiveSheet()->SetCellValue('K'.$j, $cellphoneshow );
									
									$objPHPExcel->getActiveSheet()->SetCellValue('L'.$j, $otherphoneshow ); 
									
									
									$objPHPExcel->getActiveSheet()->SetCellValue('M'.$j, $jobPostingSite );
									$objPHPExcel->getActiveSheet()->SetCellValue('N'.$j, $employeeReferral);
									
									$objPHPExcel->getActiveSheet()->SetCellValue('O'.$j, $dateappliedshow);
									
									$objPHPExcel->getActiveSheet()->SetCellValue('P'.$j, $dateContactedshow);
									
									
									$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$j, $contactedBy);
									$objPHPExcel->getActiveSheet()->SetCellValue('R'.$j, $contactNotes);
									$objPHPExcel->getActiveSheet()->SetCellValue('S'.$j, $comments);
									$objPHPExcel->getActiveSheet()->SetCellValue('T'.$j, $interviewScheduled);
									$objPHPExcel->getActiveSheet()->SetCellValue('U'.$j, $interviewingManager);
									
									$objPHPExcel->getActiveSheet()->SetCellValue('V'.$j, $interviewDateshow);
									
									
									$objPHPExcel->getActiveSheet()->SetCellValue('W'.$j, $interviewTime);
									$objPHPExcel->getActiveSheet()->SetCellValue('X'.$j, $interviewOutcome);
									
									$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$j, $rescheduledDateshow);
									
									$objPHPExcel->getActiveSheet()->SetCellValue('Z'.$j, $newHireLinkSentshow);
									
									$objPHPExcel->getActiveSheet()->SetCellValue('AA'.$j, $startDateshow);
									
									$objPHPExcel->getActiveSheet()->SetCellValue('AB'.$j, $enddateshow);
									
									
									$objPHPExcel->getActiveSheet()->SetCellValue('AC'.$j, $production);
									
									$objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('N'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('Q'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('R'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('S'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('T'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('U'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('W'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('X'.$j)->applyFromArray( $style_text );
									$objPHPExcel->getActiveSheet()->getStyle('AC'.$j)->applyFromArray( $style_text );
								    $j++;
									
									
									
								}	
								
							
					    }
					} 

					//print_r($al_number_unique);
					//print_r($al_titleArray);
					//$k = $j+1;
					//$objPHPExcel->getActiveSheet()->SetCellValue('A'.$k, 'Existed RECORDS');
					//$j = $k+1;
					//echo $status_exi;
					// $k = $j+1;
					//	$j = $k;
					//echo $j;
					 if($status_exi!=1)
					{
					    $k = $j+1;
						$j = $k;
					}	
					 for($p=0; $p<count($al_number_unique); $p++)
							{ 
									$numbershow = $al_number_unique[$p]. '(Number Already Existed)';
								 /* if (!filter_var($al_emailaddressArray[$p], FILTER_VALIDATE_EMAIL)) {
									  $emailshow = $al_emailaddressArray[$p]."(Number Already Existed)"; 
									  
									}
									else{
										$emailshow = $emailaddress; 
									}
									if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $al_homephoneArray[$p])) {
									
										$homephoneshow = $al_homephoneArray[$p]." (Homephone Format is not valid)";
									}
									else{ 
										$homephoneshow =  $homephone;
										//echo $homephone;
									}
									if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $al_cellphoneArray[$p])) {
									
										$cellphoneshow = $al_cellphoneArray[$p]." (Cellphone Format is not valid)";
									}
									else{
										$cellphoneshow =  $cellphone;
										//echo $homephone;
									}
									if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $al_otherphoneArray[$p])) {
									
										$otherphoneshow = $al_otherphoneArray[$p]." (Otherphone Format is not valid)";
									}
									else{
										$otherphoneshow =  $otherphone;
									//echo $homephone;
										}  */
						       $objPHPExcel->getActiveSheet()->SetCellValue('A'.$j, $numbershow);
							   $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray( $style_exist_text );
							   $objPHPExcel->getActiveSheet()->SetCellValue('B'.$j, $al_titleArray[$p]);
							   $objPHPExcel->getActiveSheet()->SetCellValue('C'.$j, $al_preliminary_programArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('D'.$j, $al_programArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('E'.$j, $al_marketArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('F'.$j, $al_statusArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('G'.$j, $al_firstnameArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('H'.$j, $al_lastnameArray[$p]); 
								
								 $objPHPExcel->getActiveSheet()->SetCellValue('I'.$j, $al_emailaddressArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('J'.$j, $al_homephoneArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('K'.$j,  $al_cellphoneArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('L'.$j, $al_otherphoneArray[$p] ); 
								
								$objPHPExcel->getActiveSheet()->SetCellValue('M'.$j, $al_jobPostingSiteArray[$p] );
								$objPHPExcel->getActiveSheet()->SetCellValue('N'.$j, $al_employeeReferralArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('O'.$j, $al_dateAppliedArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('P'.$j, $al_dateContactedArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$j, $al_contactedByArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('R'.$j, $al_contactNotesArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('S'.$j, $al_commentsArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('T'.$j, $al_interviewScheduledArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('U'.$j, $al_interviewingManagerArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('V'.$j, $al_interviewDateArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('W'.$j, $al_interviewTimeArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('X'.$j, $al_interviewOutcomeArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$j, $al_rescheduledDateArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('Z'.$j, $al_newHireLinkSentArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('AA'.$j, $al_startDateArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('AB'.$j, $al_enddateArray[$p]);
								$objPHPExcel->getActiveSheet()->SetCellValue('AC'.$j, $al_production[$p]);
								
								$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('N'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('O'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('P'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('Q'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('R'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('S'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('T'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('U'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('V'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('W'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('X'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('Y'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('Z'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('AA'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('AB'.$j)->applyFromArray( $style_text );
								$objPHPExcel->getActiveSheet()->getStyle('AC'.$j)->applyFromArray( $style_text );
								//$number_unique1 =$al_number_unique[$p];
								/* foreach($al_number_unique as $key=>$value)
								{
									echo $value;
								} */
								$j++;
							}	
							
					//echo 'status_exi'.$status_exi;
					// echo "<br/>";
					// echo 'status_ins'.$status_ins;
					// echo "<br/>";
					// echo 'status_err_un'.$status_err_un;
					//  echo "<br/>";
					// echo 'status_duplicate'.$dup_stat;
					//echo 'records'.count($cellValue).'<br>';
					//echo $dbinsertedvalue;
					
					
						
					// if($status_ins==1 && $status_exi!=1)
					 if($status_ins==1 && $status_exi!=1 && $status_err_un!=1 && $dup_stat!=1)
					{
						//echo "<div style='aign:center;font-size:20px;font-color:green'><b>successfully inserted</b></div>";
						echo '<div class="alert alert-success" align="center" id="success">
						  <strong> Successfully Imported. </strong>
						</div>';
					}	
					
					else
					{
						
						//echo "partially inserted";
						//echo "<div style='aign:center;font-size:20px;font-color:red'><b>partially inserted</b></div>";
						
						$timestampdate = date("Y-m-d H_i_s");
						$fileName = 'ErroLog'.'_'.$timestampdate.'.xlsx';
						//$fpath = $_SERVER['DOCUMENT_ROOT'].'/eliteRecruit/errorlogFiles/'.$fileName;
						$error_dir = 'http://123.176.43.85'.'/eliteRecruit/errorlogFiles';
		                $errorfile_path = $error_dir.'/'.$fileName;
						 $stmt_insert = $mysqli->prepare("INSERT INTO `errorlog_details`(`login_id`,`errorlog_file`,`original_file`,`errorlog_filePath`) VALUES (1,'$fileName','$filNam','$errorfile_path')");
						//$stmt_insert->bind_param('is',1,trim($fileName)); 
						$stmt_insert->execute();
						$stmt_insert->close();
						
								
						$objPHPExcel->getActiveSheet()->setTitle('Sheet1');
						//$objPHPExcel->getSheetByName('Sheet1')->getStyle("A1")->getFont()->getBold();
						
						 $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
						 //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
						 //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'XLSX');
						//header('Content-Type: application/vnd.ms-excel');
						//header('Content-Disposition: attachment;filename="' . $fileName . '"');
						//header('Cache-Control: max-age=0');
						//ob_clean(); 
						$downloadL = str_replace( __FILE__,$_SERVER['DOCUMENT_ROOT'].'/eliteRecruit/errorlogFiles/'.$fileName, __FILE__);
						 $objWriter->save($downloadL); 
						//exit; 
						//echo '<div class="alert alert-danger" align="center"><strong>Partially Imported. </strong> Please find the below error log file path.<a href="'.$errorfile_path.'"><br/>'.$errorfile_path.'</a></div>';
						
						if(($status_ins!=1 && $status_exi==1 && $status_err_un==1))
						{
						echo '<div class="alert alert-danger" align="center" id="partial"><strong> Import Falied. </strong><br/> Please <a href="'.$errorfile_path.'">Click here</a> for error log file</div>';
						}
						else if($status_ins!=1 && $status_exi==1 && $status_err_un!=1)
						{
							echo '<div class="alert alert-danger" align="center" id="partial"><strong> Import Falied. </strong><br/> Please <a href="'.$errorfile_path.'">Click here</a> for error log file</div>';
						}	
						else if($status_ins!=1 && $status_exi!=1 && $status_err_un!=1)
						{
							echo '<div class="alert alert-danger" align="center" id="partial"><strong> Import Falied. </strong><br/> Please <a href="'.$errorfile_path.'">Click here</a> for error log file</div>';
						}
						else if($status_ins==1 && $status_exi!=1 && $status_err_un!=1 && $dup_stat==1)
						{
							echo '<div class="alert alert-danger" align="center" id="partial"><strong> Partially Imported. </strong><br/> Please <a href="'.$errorfile_path.'">Click here</a> for error log file</div>';
						}
						else{
							echo '<div class="alert alert-danger" align="center" id="partial"><strong> Partially Imported. </strong><br/> Please <a href="'.$errorfile_path.'">Click here</a> for error log file</div>';
						}	
						//exit;
						
					} 					
						
					/* $objPHPExcel->getActiveSheet()->setTitle('Sheet1');
					$timestampdate = date("Y-m-d H_i_s");
						$fileName = 'ErroLog'.'_'.$timestampdate.'.xlsx';
						//$objPHPExcel->getSheetByName('Sheet1')->getStyle("A1")->getFont()->getBold();
						
						 $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
						 //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
						 //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'XLSX');
						//header('Content-Type: application/vnd.ms-excel');
						//header('Content-Disposition: attachment;filename="' . $fileName . '"');
						//header('Cache-Control: max-age=0');
						//ob_clean(); 
						$downloadL = str_replace( __FILE__,$_SERVER['DOCUMENT_ROOT'].'/eliteRecruit/errorlogFiles/'.$fileName, __FILE__);
						 $objWriter->save($downloadL); */ 
										
						
		}	
		
	
	}
}
$importValues	= new Import();
//$filNam = "template (1)_2016-01-27 19_56_46.csv";
//$filNam = "template (1)_2016-01-27 19_58_12.csv";
//$filNam = "template (1)_2016-01-27 19_58_12_2.csv";
//$created = "2016-01-27";
//$importValues->upload_recruitdata($filNam,$created,$mysqli);
?>