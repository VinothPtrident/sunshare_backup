<?php
class Charts
{
	function currentmonthContracts($partnermonthvalue,$mysqliportal)
	{	
		$current_startdate = date('Y-m-01 00:00:00');
		$current_enddate = date('Y-m-t 23:59:59');
		
		$leadCurrentMonthSavedArray = array();
		$contractCurrentMonthUnsignedArray = array();
		$contractCurrentMonthSignedArray = array();
		$contractCurrentMonthDeclinedArray = array();
		$orgnameArray = array();
		$fetch_orgname = $mysqliportal->prepare("select distinct org_name from manage_salesrep");
		$fetch_orgname->execute();
		$fetch_orgname->bind_result($orgname);
		while($fetch_orgname->fetch())
		{
			array_push($orgnameArray,$orgname);
		}
		$fetch_orgname->close();
		
		$salesrepRidArray = array();
		$salesrepPidArray = array();
		
		for($i=0; $i<count($orgnameArray); $i++)
		{
			$org_name =  $orgnameArray[$i];
			
			$fetch_salesrepids = $mysqliportal->prepare("select id as salesrepid,org_name from manage_salesrep where org_name='$org_name' and org_name!=''");
			$fetch_salesrepids->execute();
			$fetch_salesrepids->bind_result($salesrepid,$orname);
			while($fetch_salesrepids->fetch())
			{
				
				if($orname == 'RESULTS')
				{	
				  array_push($salesrepRidArray,$salesrepid);
				}
				else if($orname == 'PCCW')
				{
				 array_push($salesrepPidArray,$salesrepid);
				}
			}
			$fetch_salesrepids->close();
			
			
		}
		$salesrepRids = implode(",",$salesrepRidArray);
		$salesrepPids = implode(",",$salesrepPidArray);
		
		    //total lead saved count
			if($partnermonthvalue == "all")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus!=0 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			}
			else if($partnermonthvalue == "results")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus!=0 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "pccw")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus!=0 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			 $fetch_leadsaved = $mysqliportal->prepare($query_leadsaved);
			$fetch_leadsaved->execute();
			$fetch_leadsaved->bind_result($leadsavedCount);
			while($fetch_leadsaved->fetch())
			{
				array_push($leadCurrentMonthSavedArray,$leadsavedCount);
			}
			$fetch_leadsaved->close(); 
			
		   //total unsigned/contract generated count
			if($partnermonthvalue == "all")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			}
			else if($partnermonthvalue == "results")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "pccw")
			{ 
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			$fetch_contractunsigned = $mysqliportal->prepare($query_contractunsigned);
			$fetch_contractunsigned->execute();
			$fetch_contractunsigned->bind_result($contractUnsignedCount);
			while($fetch_contractunsigned->fetch())
			{
				array_push($contractCurrentMonthUnsignedArray,$contractUnsignedCount);
			}
			$fetch_contractunsigned->close();
			
			//total signed/contract generated and signed count
			if($partnermonthvalue == "all")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			}
			else if($partnermonthvalue == "results")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthvalue == "pccw")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			
			$fetch_contractsigned = $mysqliportal->prepare($query_contractsigned);
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($contractSignedCount);
			while($fetch_contractsigned->fetch())
			{
				array_push($contractCurrentMonthSignedArray,$contractSignedCount);
			}
			$fetch_contractsigned->close();

            //total Declined count
			if($partnermonthvalue == "all")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			}
			else if($partnermonthvalue == "results")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthvalue == "pccw")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			
			$fetch_contractdeclined = $mysqliportal->prepare($query_contractdeclined);
			$fetch_contractdeclined->execute();
			$fetch_contractdeclined->bind_result($contractDeclinedCount);
			while($fetch_contractdeclined->fetch())
			{
				array_push($contractCurrentMonthDeclinedArray,$contractDeclinedCount);
			}
			$fetch_contractdeclined->close();			
			
			$total_currentmonth_leadcount=implode(',', $leadCurrentMonthSavedArray);
			$total_currentmonth_contractUnsignedCount=implode(',', $contractCurrentMonthUnsignedArray);
			$total_currentmonth_contractSignedCount=implode(',', $contractCurrentMonthSignedArray);	
			$total_currentmonth_contractDeclinedCount=implode(',', $contractCurrentMonthDeclinedArray);
			$total_currentmonth_leadcounts=	$total_currentmonth_leadcount-$total_currentmonth_contractSignedCount;
			$overallOutcomeValues_currentMonth = array($total_currentmonth_leadcounts,$total_currentmonth_contractUnsignedCount, $total_currentmonth_contractSignedCount,$total_currentmonth_contractDeclinedCount);
				
			return $overallOutcomeValues_currentMonth;
		 
			//print_r($overallOutcomeValues_currentMonth);
	}
	
	function monthlySummary_yearContracts($partnermonthlyvalue,$mysqliportal)
	{
		
		$months_display = array();
		$leadmonnthlySavedArray = array();
		$contractmonnthlyUnsignedArray = array();
		$contractmonnthlySignedArray = array();
		$contractmonnthlyDeclinedArray = array();
		//leadsaved-contractsigned array
		$leadmonnthlySavedsubstractArray = array();
		$orgnameArray = array();
		$fetch_orgname = $mysqliportal->prepare("select distinct org_name from manage_salesrep");
		$fetch_orgname->execute();
		$fetch_orgname->bind_result($orgname);
		while($fetch_orgname->fetch())
		{
			array_push($orgnameArray,$orgname);
		}
		$fetch_orgname->close();
		
		$salesrepRidArray = array();
		$salesrepPidArray = array();
		
		for($i=0; $i<count($orgnameArray); $i++)
		{
			$org_name =  $orgnameArray[$i];
			
			$fetch_salesrepids = $mysqliportal->prepare("select id as salesrepid,org_name from manage_salesrep where org_name='$org_name' and org_name!=''");
			$fetch_salesrepids->execute();
			$fetch_salesrepids->bind_result($salesrepid,$orname);
			while($fetch_salesrepids->fetch())
			{
				
				if($orname == 'RESULTS')
				{	
				  array_push($salesrepRidArray,$salesrepid);
				}
				else if($orname == 'PCCW')
				{
				 array_push($salesrepPidArray,$salesrepid);
				}
			}
			$fetch_salesrepids->close();
			
			
		}
		$salesrepRids = implode(",",$salesrepRidArray);
		$salesrepPids = implode(",",$salesrepPidArray);
		for ($m=1; $m<=12; $m++) 
		{
			$month = date('m', mktime(0,0,0,$m, 1, date('Y')));
			$months = date('M', mktime(0,0,0,$m, 1, date('Y')));
			array_push($months_display,$months);
			$first_day_this_month = date('Y-'.$month.'-01'.' 00:00:00'); 
			$last_day_this_month = date('Y-m-t 23:59:59', strtotime($first_day_this_month));
			//echo "<br/>";	
			//total lead saved count
			if($partnermonthlyvalue == "all")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus!=0 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			}
			else if($partnermonthlyvalue == "results")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus!=0 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "pccw")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus!=0 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			 $fetch_leadsaved = $mysqliportal->prepare($query_leadsaved);
			$fetch_leadsaved->execute();
			$fetch_leadsaved->bind_result($leadsavedCount);
			while($fetch_leadsaved->fetch())
			{
				array_push($leadmonnthlySavedArray,$leadsavedCount);
				$leadsavedsubtract=$leadsavedCount;
			}
			$fetch_leadsaved->close(); 
			
			//total unsigned/contract generated count
			if($partnermonthlyvalue == "all")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			}
			else if($partnermonthlyvalue == "results")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "pccw")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			$fetch_contractunsigned = $mysqliportal->prepare($query_contractunsigned);
			$fetch_contractunsigned->execute();
			$fetch_contractunsigned->bind_result($contractUnsignedCount);
			while($fetch_contractunsigned->fetch())
			{
				array_push($contractmonnthlyUnsignedArray,$contractUnsignedCount);
			}
			$fetch_contractunsigned->close();
			
			//total signed/contract generated and signed count
			//$fetch_contractsigned = $mysqliportal->prepare("select count(l.lstatus) as contractsignedcount from leads l,saleshub_import si where  l.lstatus=3 and l.email=si.email");
			
			if($partnermonthlyvalue == "all")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			}
			else if($partnermonthlyvalue == "results")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthlyvalue == "pccw")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			
			$fetch_contractsigned = $mysqliportal->prepare($query_contractsigned);
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($contractSignedCount);
			while($fetch_contractsigned->fetch())
			{
				array_push($contractmonnthlySignedArray,$contractSignedCount);
				$contractsignedsubtract=$contractSignedCount;
			}
			$fetch_contractsigned->close();
			$leadcountafter_substraction=$leadsavedsubtract-$contractsignedsubtract;
			array_push($leadmonnthlySavedsubstractArray,$leadcountafter_substraction);
			//for declined 
			if($partnermonthlyvalue == "all")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractdeclinedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			}
			else if($partnermonthlyvalue == "results")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractdeclinedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthlyvalue == "pccw")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractdeclinedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			
			$fetch_contractdeclined = $mysqliportal->prepare($query_contractdeclined);
			$fetch_contractdeclined->execute();
			$fetch_contractdeclined->bind_result($contractDeclinedCount);
			while($fetch_contractdeclined->fetch())
			{
				array_push($contractmonnthlyDeclinedArray,$contractDeclinedCount);
			}
			$fetch_contractdeclined->close();
			
			
			
		}
		$total_monthly_leadcount=implode(',', $leadmonnthlySavedArray);
		$total_monthly_contractUnsignedCount=implode(',', $contractmonnthlyUnsignedArray);
		$total_monthly_contractSignedCount=implode(',', $contractmonnthlySignedArray);
        $total_monthly_contractDeclinedCount=implode(',', $contractmonnthlyDeclinedArray);			
		$total_monthly_months=implode(',', $months_display);
		$total_monthly_leadcounts=implode(',', $leadmonnthlySavedsubstractArray);
		
		$overallOutcomeValues_monthly = array($total_monthly_months,$total_monthly_leadcounts, $total_monthly_contractUnsignedCount, $total_monthly_contractSignedCount,$total_monthly_contractDeclinedCount);
				//print_r($overallOutcomeValues_monthly);
			return $overallOutcomeValues_monthly;
		 
		
	}
	function dailyContracts($cmonth,$mysqliportal)
	{
		$list = array();
		$month_year = explode(" ",$cmonth);
		$cmonthstr = $month_year[0];
		$cyear = $month_year[1];
		$nmonth = date("m", strtotime($cmonth));
		$start_date = date($cyear.'-'.$nmonth.'-01'); 
		$end_date = date('Y-m-t', strtotime($start_date));
		$showMonth=$cmonthstr.' - '.$cyear;
		$list=array();
		$datesArray = array();
		$contractDailyUnsignedrArray = array();
		$contractDailySignedrArray = array();
		/* $date_from = strtotime($start_date); 
		$date_to = strtotime($end_date); // Convert date to a UNIX timestamp  
		$date_to = $date_to+86400;
		
		// Loop from the start date to end date and output all dates inbetween  
		for ($i=$date_from; $i<=$date_to; $i+=86400) 
		{  
			 $list[] = date('Y-m-d', $i);
		}   */
		$begin = new DateTime($start_date);
		$todated = date('Y-m-d', strtotime($end_date . ' +1 day')); //Add one day because of fullcalendar doesnot include
		$end = new DateTime($todated);
		$dateranges = new DatePeriod($begin, new DateInterval('P1D'), $end);
		foreach($dateranges as $date){
		   $list[]=$date->format("Y-m-d");// all dates b/w start&end dates
		   
		}
		for($i=0 ; $i<count($list); $i++)
		{
			$date_month = $list[$i];
			$dates_show = date('d', strtotime($list[$i]));
			array_push($datesArray,$dates_show);
			
			//total unsigned/contract generated count
			$query_daily = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and DATE_FORMAT(l.createdlead_time,'%Y-%m-%d') = '$date_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			$fetch_contractunsigned = $mysqliportal->prepare($query_daily);
			$fetch_contractunsigned->execute();
			$fetch_contractunsigned->bind_result($contractunsignedcount);
			while($fetch_contractunsigned->fetch())
			{
				array_push($contractDailyUnsignedrArray,$contractunsignedcount);
			}
			$fetch_contractunsigned->close(); 
			
			//total signed/contract generated and signed count
			
			$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and DATE_FORMAT(l.createdlead_time,'%Y-%m-%d') = '$date_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id not in (50,59,60,61,62,63,64,65,66,67,68,69,70)";
			
			$fetch_contractsigned = $mysqliportal->prepare($query_contractsigned);
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($contractSignedCount);
			while($fetch_contractsigned->fetch())
			{
				array_push($contractDailySignedrArray,$contractSignedCount);
			}
			$fetch_contractsigned->close();	
			
		}	
		
		$total_monthDates=implode(',', $datesArray);
		$total_daily_contractUnsignedCount=implode(',', $contractDailyUnsignedrArray);
	    $total_daily_contractSignedCount=implode(',', $contractDailySignedrArray);	
		$dateMonthValues = array($total_monthDates, $total_daily_contractUnsignedCount, $total_daily_contractSignedCount, $showMonth);
					//print_r($dateMonthValues);
					return $dateMonthValues; 
	}	

	
}
$chartValues = new Charts();


?>