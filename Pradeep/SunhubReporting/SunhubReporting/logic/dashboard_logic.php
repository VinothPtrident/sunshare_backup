<?php
//include ("../config/database.php");
class SunshareDashboard
{
	function overallSummary_contractsign_Unsign_LeadSaved($mysqliportal)
	{
		//dashboard 1st Chart
		$salesrepidArray = array();
		$fetch_orgname = $mysqliportal->prepare("select distinct `id` from manage_salesrep where `partner_id` in('10001','10002','10003','10007')");
		$fetch_orgname->execute();
		$fetch_orgname->bind_result($salesrep_id);
		while($fetch_orgname->fetch())
		{
			array_push($salesrepidArray,$salesrep_id);
		}
		$fetch_orgname->close();
		$salesrepids=implode(',', $salesrepidArray);
		/* echo($salesrepids);
		exit; */
		$leadSavedArray = array();
		$contractUnsignedArray = array();
		$contractSignedArray = array();
		$contractDeclinedArray = array();
		
		//total lead saved count
		$fetch_leadsaved = $mysqliportal->prepare("select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)");
		$fetch_leadsaved->execute();
		$fetch_leadsaved->bind_result($leadsavedCount);
		while($fetch_leadsaved->fetch())
		{
			array_push($leadSavedArray,$leadsavedCount);
		}
		$fetch_leadsaved->close();
		
		//total unsigned/contract generated count
		$fetch_contractunsigned = $mysqliportal->prepare("select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)");
		$fetch_contractunsigned->execute();
		$fetch_contractunsigned->bind_result($contractUnsignedCount);
		while($fetch_contractunsigned->fetch())
		{
			array_push($contractUnsignedArray,$contractUnsignedCount);
		}
		$fetch_contractunsigned->close();
		
		//total signed/contract generated and signed count
		//$fetch_contractsigned = $mysqliportal->prepare("select count(l.lstatus) as contractsignedcount from leads l,saleshub_import si where  l.lstatus=3 and l.email=si.email");
		$fetch_contractsigned = $mysqliportal->prepare("select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)");
		$fetch_contractsigned->execute();
		$fetch_contractsigned->bind_result($contractSignedCount);
		while($fetch_contractsigned->fetch())
		{
			array_push($contractSignedArray,$contractSignedCount);
		}
		$fetch_contractsigned->close();
		//total Declined contract count
		$fetch_contractdeclined = $mysqliportal->prepare("select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)");
		$fetch_contractdeclined->execute();
		$fetch_contractdeclined->bind_result($contractDeclinedCount);
		while($fetch_contractdeclined->fetch())
		{
			array_push($contractDeclinedArray,$contractDeclinedCount);
		}
		$fetch_contractdeclined->close();
		
		//total Declined contract count
		$fetch_minmaxdate = $mysqliportal->prepare("select max(DATE_FORMAT(createdlead_time,'%Y-%m-%d')),min(DATE_FORMAT(createdlead_time,'%Y-%m-%d')) from leads where createdlead_time!='0000-00-00 00:00:00'");
		$fetch_minmaxdate->execute();
		$fetch_minmaxdate->bind_result($maxDates,$minDates);
		while($fetch_minmaxdate->fetch())
		{
			$totalmaxdate=$maxDates;
			$totalmindate=$minDates;
		}
		$fetch_minmaxdate->close();
		
		$total_leadcount=implode(',', $leadSavedArray);
		$total_contractUnsignedCount=implode(',', $contractUnsignedArray);
		$total_contractSignedCount=implode(',', $contractSignedArray);
		$total_contractDeclinedCount=implode(',', $contractDeclinedArray);
		
		$overallOutcomeValues = array($total_leadcount, $total_contractUnsignedCount, $total_contractSignedCount,$total_contractDeclinedCount,$totalmaxdate,$totalmindate);
			//print_r($overallOutcomeValues);
			return $overallOutcomeValues;
			
	}
	function overallSummary_Contracts($mysqliportal)
	{
		//dashboard 2nd chart(table)
		$reqsalesrepsarray=array('10001','10002','10003','10007');
		$salesrepRidArray = array();
		$salesrepPidArray = array();
		$salesrepSunidArray = array();
		$salesrepWebidArray = array();
		
		for($i=0; $i<count($reqsalesrepsarray); $i++)
		{
			$patnerid =  $reqsalesrepsarray[$i];
			
			$fetch_salesrepids = $mysqliportal->prepare("select id as salesrepid from manage_salesrep where partner_id='$patnerid'");
			$fetch_salesrepids->execute();
			$fetch_salesrepids->bind_result($salesrepid);
			while($fetch_salesrepids->fetch())
			{
				
				if($patnerid == '10001')
				{	
				  array_push($salesrepRidArray,$salesrepid);
				}
				else if($patnerid == '10002')
				{
				 array_push($salesrepPidArray,$salesrepid);
				}
				else if($patnerid == '10003')
				{
				 array_push($salesrepSunidArray,$salesrepid);
				}
				else if($patnerid == '10007')
				{
				 array_push($salesrepWebidArray,$salesrepid);
				}
			}
			$fetch_salesrepids->close();
			
			//echo "<br/>";
		} 
		/* $total_monthly_leadcount=implode(',', $salesrepWebidArray);
		echo($total_monthly_leadcount);
		exit; */
		$Rleadsavesum = 0;
		$RContractUnsignedsum = 0;
		$RContractsignedsum = 0;
		$RContractdeclinedsum = 0;
		for($j=0; $j<count($salesrepRidArray); $j++)
		{
			$salesRid = $salesrepRidArray[$j];
		
			//total Results lead saved count
			 $fetch_leadsaved= $mysqliportal->prepare("select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.salesrep_id in ($salesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_leadsaved->execute();
			$fetch_leadsaved->bind_result($leadsavedCount);
			while($fetch_leadsaved->fetch())
			{
				$Rleadsavesum += $leadsavedCount; 
				//array_push($RleadSavedArray,$leadsavedCount);
			}
			$fetch_leadsaved->close(); 
			
		    //total Results contract sent/contract unsigned count
			$fetch_contractsent = $mysqliportal->prepare("select count(l.lstatus) as contractunsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.salesrep_id in ($salesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractsent->execute();
			$fetch_contractsent->bind_result($contractunsigned);
			while($fetch_contractsent->fetch())
			{
				$RContractUnsignedsum += $contractunsigned; 
			}
			$fetch_contractsent->close(); 
			
			//total Results contract Signed count
			
			
			// $fetch_contractsigned = $mysqliportal->prepare("select count(l.lstatus) as contractsigned from leads l,saleshub_import si where  l.lstatus=3 and l.email=si.email and l.salesrep_id in ($salesRid)");
			$fetch_contractsigned = $mysqliportal->prepare("select count(l.lstatus) as contractsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.salesrep_id in ($salesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($contractsigned);
			while($fetch_contractsigned->fetch())
			{
				$RContractsignedsum += $contractsigned; 
			}
			$fetch_contractsigned->close();
            //total Results contract Declined count 
            $fetch_contractdeclined = $mysqliportal->prepare("select count(l.lstatus) as contractsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.salesrep_id in ($salesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractdeclined->execute();
			$fetch_contractdeclined->bind_result($contractdeclined);
			while($fetch_contractdeclined->fetch())
			{
				$RContractdeclinedsum += $contractdeclined; 
			}
			$fetch_contractdeclined->close();			
		}
		
		$Pleadsavesum = 0;
		$PContractUnsignedsum = 0;
		$PContractsignedsum = 0;
		$PContractdeclinedsum = 0;
		for($j=0; $j<count($salesrepPidArray); $j++)
		{
			$psalesRid = $salesrepPidArray[$j];
		
			
			//total PCCW lead saved count
			 $fetch_leadsaved= $mysqliportal->prepare("select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.salesrep_id in ($psalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_leadsaved->execute();
			$fetch_leadsaved->bind_result($pleadsavedCount);
			while($fetch_leadsaved->fetch())
			{
				$Pleadsavesum += $pleadsavedCount; 
				//array_push($RleadSavedArray,$leadsavedCount);
			}
			$fetch_leadsaved->close(); 
			
			//total PCCW contract sent/contract unsigned count
			$fetch_contractsent = $mysqliportal->prepare("select count(l.lstatus) as contractunsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.salesrep_id in ($psalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractsent->execute();
			$fetch_contractsent->bind_result($pcontractunsigned);
			while($fetch_contractsent->fetch())
			{
				$PContractUnsignedsum += $pcontractunsigned; 
			}
			$fetch_contractsent->close(); 
			
			//totalPCCW  contract Signed count
			 $fetch_contractsigned = $mysqliportal->prepare("select count(l.lstatus) as contractsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.salesrep_id in ($psalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($pcontractsigned);
			while($fetch_contractsigned->fetch())
			{
				$PContractsignedsum += $pcontractsigned; 
			}
			$fetch_contractsigned->close();
            //total Results contract Declined count 
            $fetch_contractdeclined = $mysqliportal->prepare("select count(l.lstatus) as contractsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.salesrep_id in ($psalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractdeclined->execute();
			$fetch_contractdeclined->bind_result($pcontractdeclined);
			while($fetch_contractdeclined->fetch())
			{
				$PContractdeclinedsum += $pcontractdeclined; 
			}
			$fetch_contractdeclined->close();			
		}
		$Sunleadsavesum = 0;
		$SunContractUnsignedsum = 0;
		$SunContractsignedsum = 0;
		$SunContractdeclinedsum = 0;
		for($j=0; $j<count($salesrepSunidArray); $j++)
		{
			$sunsalesRid = $salesrepSunidArray[$j];
		
			
			//total PCCW lead saved count
			 $fetch_leadsaved= $mysqliportal->prepare("select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.salesrep_id in ($sunsalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_leadsaved->execute();
			$fetch_leadsaved->bind_result($sunleadsavedCount);
			while($fetch_leadsaved->fetch())
			{
				$Sunleadsavesum += $sunleadsavedCount; 
				//array_push($RleadSavedArray,$leadsavedCount);
			}
			$fetch_leadsaved->close(); 
			
			//total PCCW contract sent/contract unsigned count
			$fetch_contractsent = $mysqliportal->prepare("select count(l.lstatus) as contractunsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.salesrep_id in ($sunsalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractsent->execute();
			$fetch_contractsent->bind_result($suncontractunsigned);
			while($fetch_contractsent->fetch())
			{
				$SunContractUnsignedsum += $suncontractunsigned; 
			}
			$fetch_contractsent->close(); 
			
			//totalPCCW  contract Signed count
			 $fetch_contractsigned = $mysqliportal->prepare("select count(l.lstatus) as contractsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.salesrep_id in ($sunsalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($suncontractsigned);
			while($fetch_contractsigned->fetch())
			{
				$SunContractsignedsum += $suncontractsigned; 
			}
			$fetch_contractsigned->close();
            //total Results contract Declined count 
            $fetch_contractdeclined = $mysqliportal->prepare("select count(l.lstatus) as contractsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.salesrep_id in ($sunsalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractdeclined->execute();
			$fetch_contractdeclined->bind_result($suncontractdeclined);
			while($fetch_contractdeclined->fetch())
			{
				$SunContractdeclinedsum += $suncontractdeclined; 
			}
			$fetch_contractdeclined->close();			
		}
		$Webleadsavesum = 0;
		$WebContractUnsignedsum = 0;
		$WebContractsignedsum = 0;
		$WebContractdeclinedsum = 0;
		for($j=0; $j<count($salesrepWebidArray); $j++)
		{
			$websalesRid = $salesrepWebidArray[$j];
		
			
			//total PCCW lead saved count
			 $fetch_leadsaved= $mysqliportal->prepare("select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.salesrep_id in ($websalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_leadsaved->execute();
			$fetch_leadsaved->bind_result($webleadsavedCount);
			while($fetch_leadsaved->fetch())
			{
				$Webleadsavesum += $webleadsavedCount; 
				//array_push($RleadSavedArray,$leadsavedCount);
			}
			$fetch_leadsaved->close(); 
			
			//total PCCW contract sent/contract unsigned count
			$fetch_contractsent = $mysqliportal->prepare("select count(l.lstatus) as contractunsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.salesrep_id in ($websalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractsent->execute();
			$fetch_contractsent->bind_result($webcontractunsigned);
			while($fetch_contractsent->fetch())
			{
				$WebContractUnsignedsum += $webcontractunsigned; 
			}
			$fetch_contractsent->close(); 
			
			//totalPCCW  contract Signed count
			 $fetch_contractsigned = $mysqliportal->prepare("select count(l.lstatus) as contractsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.salesrep_id in ($websalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($webcontractsigned);
			while($fetch_contractsigned->fetch())
			{
				$WebContractsignedsum += $webcontractsigned; 
			}
			$fetch_contractsigned->close();
            //total Results contract Declined count 
            $fetch_contractdeclined = $mysqliportal->prepare("select count(l.lstatus) as contractsigned from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.salesrep_id in ($websalesRid) and l.createdlead_time!='0000-00-00 00:00:00'");
			$fetch_contractdeclined->execute();
			$fetch_contractdeclined->bind_result($webcontractdeclined);
			while($fetch_contractdeclined->fetch())
			{
				$WebContractdeclinedsum += $webcontractdeclined; 
			}
			$fetch_contractdeclined->close();			
		}
		
		$re_results_leadsavedsum = $Rleadsavesum;	
		$re_results_contractUnsignedsum = $RContractUnsignedsum;	
		$re_results_contractSignedsum = $RContractsignedsum;
		$re_results_contractDeclinedsum = $RContractdeclinedsum;
		
		$pccw_results_leadsavedsum = $Pleadsavesum;	
		$pccw_results_contractUnsignedsum = $PContractUnsignedsum;	
		$pccw_results_contractSignedsum = $PContractsignedsum;
		$pccw_results_contractDeclinedsum = $PContractdeclinedsum;
		
		$sun_results_leadsavedsum = $Sunleadsavesum;	
		$sun_results_contractUnsignedsum = $SunContractUnsignedsum;	
		$sun_results_contractSignedsum = $SunContractsignedsum;
		$sun_results_contractDeclinedsum = $SunContractdeclinedsum;
		
		$web_results_leadsavedsum = $Webleadsavesum;	
		$web_results_contractUnsignedsum = $WebContractUnsignedsum;	
		$web_results_contractSignedsum = $WebContractsignedsum;
		$web_results_contractDeclinedsum = $WebContractdeclinedsum;
			
        $overallContracts = array($re_results_leadsavedsum, $re_results_contractUnsignedsum, $re_results_contractSignedsum,$re_results_contractDeclinedsum, $pccw_results_leadsavedsum, $pccw_results_contractUnsignedsum, $pccw_results_contractSignedsum,$pccw_results_contractDeclinedsum,$sun_results_leadsavedsum,$sun_results_contractUnsignedsum,$sun_results_contractSignedsum,$sun_results_contractDeclinedsum,$web_results_leadsavedsum,$web_results_contractUnsignedsum,$web_results_contractSignedsum,$web_results_contractDeclinedsum);
			//print_r($overallContracts);
			return $overallContracts;
		//echo $Rsum;	
	}	
	function monthlySummary_yearContracts($partnermonthlyvalue,$mysqliportal)
	{
		
		$reqsalesrepsarray=array('10001','10002','10003','10007');
		$allreps = array();
		$fetch_orgname = $mysqliportal->prepare("select distinct `id` from manage_salesrep where `partner_id` in('10001','10002','10003','10007')");
		$fetch_orgname->execute();
		$fetch_orgname->bind_result($salesrep_id);
		while($fetch_orgname->fetch())
		{
			array_push($allreps,$salesrep_id);
		}
		$fetch_orgname->close();
		$salesrepids=implode(',', $allreps);
		//$allreps=array($salesrepids);
		$months_display = array();
		$leadmonnthlySavedArray = array();
		$contractmonnthlyUnsignedArray = array();
		$contractmonnthlySignedArray = array();
		$contractmonnthlyDeclinedArray = array();
		//leadsaved-contractsigned array
		//$leadmonnthlySavedsubstractArray = array();
		$salesrepRidArray = array();
		$salesrepPidArray = array();
		$salesrepSunidArray = array();
		$salesrepWebidArray = array();
		for($i=0; $i<count($reqsalesrepsarray); $i++)
		{
			$patnerid =  $reqsalesrepsarray[$i];
			
			$fetch_salesrepids = $mysqliportal->prepare("select id as salesrepid from manage_salesrep where partner_id='$patnerid'");
			$fetch_salesrepids->execute();
			$fetch_salesrepids->bind_result($salesrepid);
			while($fetch_salesrepids->fetch())
			{
				
				if($patnerid == '10001')
				{	
				  array_push($salesrepRidArray,$salesrepid);
				}
				else if($patnerid == '10002')
				{
				 array_push($salesrepPidArray,$salesrepid);
				}
				else if($patnerid == '10003')
				{
				 array_push($salesrepSunidArray,$salesrepid);
				}
				else if($patnerid == '10007')
				{
				 array_push($salesrepWebidArray,$salesrepid);
				}
			}
			$fetch_salesrepids->close();
			
			//echo "<br/>";
		} 
		
		$salesrepRids = implode(",",$salesrepRidArray);
		$salesrepPids = implode(",",$salesrepPidArray);
		$salesrepSunids = implode(",",$salesrepSunidArray);
		$salesrepWebids = implode(",",$salesrepWebidArray);
		/* echo $salesrepRids;
		exit; */
		for ($m=1; $m<=12; $m++) 
		{
			$month = date('m', mktime(0,0,0,$m, 1, date('Y')));
			$months = date('M', mktime(0,0,0,$m, 1, date('Y')));
			array_push($months_display,$months);
			$first_day_this_month = date('Y-'.$month.'-01'.' 00:00:00'); 
			$last_day_this_month = date('Y-m-t 23:59:59', strtotime($first_day_this_month));
			//echo "<br/>";	
			//total lead saved count
			if($partnermonthlyvalue == "all")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			}
			else if($partnermonthlyvalue == "10001")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "10002")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "10003")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepSunids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "10007")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepWebids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			$fetch_leadsaved = $mysqliportal->prepare($query_leadsaved);
			$fetch_leadsaved->execute();
			$fetch_leadsaved->bind_result($leadsavedCount);
			while($fetch_leadsaved->fetch())
			{
				array_push($leadmonnthlySavedArray,$leadsavedCount);
				$leadsavedsubtract=$leadsavedCount;
			}
			$fetch_leadsaved->close(); 
			
			//total unsigned/contract generated count
			if($partnermonthlyvalue == "all")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			}
			else if($partnermonthlyvalue == "10001")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "10002")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "10003")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepSunids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "10007")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepWebids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			$fetch_contractunsigned = $mysqliportal->prepare($query_contractunsigned);
			$fetch_contractunsigned->execute();
			$fetch_contractunsigned->bind_result($contractUnsignedCount);
			while($fetch_contractunsigned->fetch())
			{
				array_push($contractmonnthlyUnsignedArray,$contractUnsignedCount);
			}
			$fetch_contractunsigned->close();
			
			//total signed/contract generated and signed count
			//$fetch_contractsigned = $mysqliportal->prepare("select count(l.lstatus) as contractsignedcount from leads l,saleshub_import si where  l.lstatus=3 and l.email=si.email");
			
			if($partnermonthlyvalue == "all")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			}
			else if($partnermonthlyvalue == "10001")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthlyvalue == "10002")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "10003")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepSunids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthlyvalue == "10007")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepWebids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			
			$fetch_contractsigned = $mysqliportal->prepare($query_contractsigned);
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($contractSignedCount);
			while($fetch_contractsigned->fetch())
			{
				array_push($contractmonnthlySignedArray,$contractSignedCount);
				$contractsignedsubtract=$contractSignedCount;
			}
			$fetch_contractsigned->close();
			/* $leadcountafter_substraction=$leadsavedsubtract-$contractsignedsubtract;
			array_push($leadmonnthlySavedsubstractArray,$leadcountafter_substraction); */
			//for declined 
			if($partnermonthlyvalue == "all")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractdeclinedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			}
			else if($partnermonthlyvalue == "10001")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractdeclinedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthlyvalue == "10002")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractdeclinedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthlyvalue == "10003")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractdeclinedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepSunids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthlyvalue == "10007")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractdeclinedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$first_day_this_month' and '$last_day_this_month' and l.salesrep_id in ($salesrepWebids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			
			$fetch_contractdeclined = $mysqliportal->prepare($query_contractdeclined);
			$fetch_contractdeclined->execute();
			$fetch_contractdeclined->bind_result($contractDeclinedCount);
			while($fetch_contractdeclined->fetch())
			{
				array_push($contractmonnthlyDeclinedArray,$contractDeclinedCount);
			}
			$fetch_contractdeclined->close();
			
			
			
		}
		$total_monthly_leadcount=implode(',', $leadmonnthlySavedArray);
		$total_monthly_contractUnsignedCount=implode(',', $contractmonnthlyUnsignedArray);
		$total_monthly_contractSignedCount=implode(',', $contractmonnthlySignedArray);
        $total_monthly_contractDeclinedCount=implode(',', $contractmonnthlyDeclinedArray);			
		$total_monthly_months=implode(',', $months_display);
		//$total_monthly_leadcounts=implode(',', $leadmonnthlySavedsubstractArray);
		
		$overallOutcomeValues_monthly = array($total_monthly_months,$total_monthly_leadcount, $total_monthly_contractUnsignedCount, $total_monthly_contractSignedCount,$total_monthly_contractDeclinedCount);
				//print_r($overallOutcomeValues_monthly);
			return $overallOutcomeValues_monthly;
		 
		
	}
	function currentmonthContracts($partnermonthvalue,$mysqliportal)
	{	
		$allreps = array();
		$fetch_orgname = $mysqliportal->prepare("select distinct `id` from manage_salesrep where `partner_id` in('10001','10002','10003','10007')");
		$fetch_orgname->execute();
		$fetch_orgname->bind_result($salesrep_id);
		while($fetch_orgname->fetch())
		{
			array_push($allreps,$salesrep_id);
		}
		$fetch_orgname->close();
		$salesrepids=implode(',', $allreps);
		$current_startdate = date('Y-m-01 00:00:00');
		$current_enddate = date('Y-m-t 23:59:59');
		$reqsalesrepsarray=array('10001','10002','10003','10007');
		$leadCurrentMonthSavedArray = array();
		$contractCurrentMonthUnsignedArray = array();
		$contractCurrentMonthSignedArray = array();
		$contractCurrentMonthDeclinedArray = array();
		/* $orgnameArray = array();
		$fetch_orgname = $mysqliportal->prepare("select distinct org_name from manage_salesrep");
		$fetch_orgname->execute();
		$fetch_orgname->bind_result($orgname);
		while($fetch_orgname->fetch())
		{
			array_push($orgnameArray,$orgname);
		}
		$fetch_orgname->close(); */
		
		$salesrepRidArray = array();
		$salesrepPidArray = array();
		$salesrepSunidArray = array();
		$salesrepWebidArray = array();
		
		for($i=0; $i<count($reqsalesrepsarray); $i++)
		{
			$patnerid =  $reqsalesrepsarray[$i];
			
			$fetch_salesrepids = $mysqliportal->prepare("select id as salesrepid from manage_salesrep where partner_id='$patnerid'");
			$fetch_salesrepids->execute();
			$fetch_salesrepids->bind_result($salesrepid);
			while($fetch_salesrepids->fetch())
			{
				
				if($patnerid == '10001')
				{	
				  array_push($salesrepRidArray,$salesrepid);
				}
				else if($patnerid == '10002')
				{
				 array_push($salesrepPidArray,$salesrepid);
				}
				else if($patnerid == '10003')
				{
				 array_push($salesrepSunidArray,$salesrepid);
				}
				else if($patnerid == '10007')
				{
				 array_push($salesrepWebidArray,$salesrepid);
				}
			}
			$fetch_salesrepids->close();
			
			//echo "<br/>";
		} 
		$salesrepRids = implode(",",$salesrepRidArray);
		$salesrepPids = implode(",",$salesrepPidArray);
		$salesrepSunids = implode(",",$salesrepSunidArray);
		$salesrepWebids = implode(",",$salesrepWebidArray);
		
		    //total lead saved count
			if($partnermonthvalue == "all")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			}
			else if($partnermonthvalue == "10001")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "10002")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "10003")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepSunids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "10007")
			{
				$query_leadsaved = "select count(l.lstatus) as leadsavedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=1 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepWebids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			 $fetch_leadsaved = $mysqliportal->prepare($query_leadsaved);
			$fetch_leadsaved->execute();
			$fetch_leadsaved->bind_result($leadsavedCount);
			while($fetch_leadsaved->fetch())
			{
				array_push($leadCurrentMonthSavedArray,$leadsavedCount);
			}
			$fetch_leadsaved->close(); 
			
		   //total unsigned/contract generated count
			if($partnermonthvalue == "all")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			}
			else if($partnermonthvalue == "10001")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "10002")
			{ 
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "10003")
			{
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepSunids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "10007")
			{ 
				$query_contractunsigned = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepWebids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			$fetch_contractunsigned = $mysqliportal->prepare($query_contractunsigned);
			$fetch_contractunsigned->execute();
			$fetch_contractunsigned->bind_result($contractUnsignedCount);
			while($fetch_contractunsigned->fetch())
			{
				array_push($contractCurrentMonthUnsignedArray,$contractUnsignedCount);
			}
			$fetch_contractunsigned->close();
			
			//total signed/contract generated and signed count
			if($partnermonthvalue == "all")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			}
			else if($partnermonthvalue == "10001")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthvalue == "10002")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "10003")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepSunids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthvalue == "10007")
			{
				$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepWebids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			
			$fetch_contractsigned = $mysqliportal->prepare($query_contractsigned);
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($contractSignedCount);
			while($fetch_contractsigned->fetch())
			{
				array_push($contractCurrentMonthSignedArray,$contractSignedCount);
			}
			$fetch_contractsigned->close();

            //total Declined count
			if($partnermonthvalue == "all")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			}
			else if($partnermonthvalue == "10001")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepRids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthvalue == "10002")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepPids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			else if($partnermonthvalue == "10003")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepSunids) and l.createdlead_time!='0000-00-00 00:00:00'";
				
			}
			else if($partnermonthvalue == "10007")
			{
				$query_contractdeclined = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=5 and l.createdlead_time between '$current_startdate' and '$current_enddate' and l.salesrep_id in ($salesrepWebids) and l.createdlead_time!='0000-00-00 00:00:00'";
			}
			
			$fetch_contractdeclined = $mysqliportal->prepare($query_contractdeclined);
			$fetch_contractdeclined->execute();
			$fetch_contractdeclined->bind_result($contractDeclinedCount);
			while($fetch_contractdeclined->fetch())
			{
				array_push($contractCurrentMonthDeclinedArray,$contractDeclinedCount);
			}
			$fetch_contractdeclined->close();			
			
			$total_currentmonth_leadcount=implode(',', $leadCurrentMonthSavedArray);
			$total_currentmonth_contractUnsignedCount=implode(',', $contractCurrentMonthUnsignedArray);
			$total_currentmonth_contractSignedCount=implode(',', $contractCurrentMonthSignedArray);	
			$total_currentmonth_contractDeclinedCount=implode(',', $contractCurrentMonthDeclinedArray);
			//$total_currentmonth_leadcounts=	$total_currentmonth_leadcount-$total_currentmonth_contractSignedCount;
			
			$overallOutcomeValues_currentMonth = array($total_currentmonth_leadcount,$total_currentmonth_contractUnsignedCount, $total_currentmonth_contractSignedCount,$total_currentmonth_contractDeclinedCount);
				
			return $overallOutcomeValues_currentMonth;
		 
			//print_r($overallOutcomeValues_currentMonth);
	}
	function dailyContracts($cmonth,$mysqliportal)
	{
		$allreps = array();
		$fetch_orgname = $mysqliportal->prepare("select distinct `id` from manage_salesrep where `partner_id` in('10001','10002','10003','10007')");
		$fetch_orgname->execute();
		$fetch_orgname->bind_result($salesrep_id);
		while($fetch_orgname->fetch())
		{
			array_push($allreps,$salesrep_id);
		}
		$fetch_orgname->close();
		$salesrepids=implode(',', $allreps);
		$month_year = explode(" ",$cmonth);
		$cmonthstr = $month_year[0];
		$cyear = $month_year[1];
		$nmonth = date("m", strtotime($cmonth));
		$start_date = date($cyear.'-'.$nmonth.'-01'); 
		$end_date = date('Y-m-t', strtotime($start_date));
		$showMonth=$cmonthstr.' - '.$cyear;
		$list=array();
		$datesArray = array();
		$contractDailyUnsignedrArray = array();
		$contractDailySignedrArray = array();
		/* $date_from = strtotime($start_date); 
		$date_to = strtotime($end_date); // Convert date to a UNIX timestamp 
        $date_to = $date_to+86400;
		
	 	
		// Loop from the start date to end date and output all dates inbetween  
		for ($i=$date_from; $i<=$date_to; $i+=86400) 
		{  
			 $list[] = date('Y-m-d', $i);
			 
		}  */
		$begin = new DateTime($start_date);
		$todated = date('Y-m-d', strtotime($end_date . ' +1 day')); //Add one day because of fullcalendar doesnot include
		$end = new DateTime($todated);
		$dateranges = new DatePeriod($begin, new DateInterval('P1D'), $end);
		foreach($dateranges as $date){
		   $list[]=$date->format("Y-m-d");// all dates b/w start&end dates
		   
		}
		/* echo "<pre>";
		
		echo $list[0];
		print_r($list);
		echo count($list);
		exit; */
		for($i=0 ; $i<count($list); $i++)
		{
			$date_month = $list[$i];
			$dates_show = date('d', strtotime($list[$i]));
			array_push($datesArray,$dates_show);
			
		    //total unsigned/contract generated count
			$query_daily = "select count(l.lstatus) as contractunsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=3 and DATE_FORMAT(l.createdlead_time,'%Y-%m-%d') = '$date_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			$fetch_contractunsigned = $mysqliportal->prepare($query_daily);
			$fetch_contractunsigned->execute();
			$fetch_contractunsigned->bind_result($contractunsignedcount);
			while($fetch_contractunsigned->fetch())
			{
				array_push($contractDailyUnsignedrArray,$contractunsignedcount);
			}
			$fetch_contractunsigned->close(); 
			
			//total signed/contract generated and signed count
			
			$query_contractsigned = "select count(l.lstatus) as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4 and DATE_FORMAT(l.createdlead_time,'%Y-%m-%d') = '$date_month' and l.createdlead_time!='0000-00-00 00:00:00' and l.salesrep_id in ($salesrepids)";
			
			$fetch_contractsigned = $mysqliportal->prepare($query_contractsigned);
			$fetch_contractsigned->execute();
			$fetch_contractsigned->bind_result($contractSignedCount);
			while($fetch_contractsigned->fetch())
			{
				array_push($contractDailySignedrArray,$contractSignedCount);
			}
			$fetch_contractsigned->close();	
			
		}	
		
		$total_monthDates=implode(',', $datesArray);
		$total_daily_contractUnsignedCount=implode(',', $contractDailyUnsignedrArray);
	    $total_daily_contractSignedCount=implode(',', $contractDailySignedrArray);	
		$dateMonthValues = array($total_monthDates, $total_daily_contractUnsignedCount, $total_daily_contractSignedCount, $showMonth);
					//echo "<pre>";
				//print_r($dateMonthValues);
					return $dateMonthValues; 
	}	
}	
$dashboardValues = new SunshareDashboard(); 
//$mysqliportal = new mysqli("localhost","root","phpils135$","telesales_final"); 
//$cmonth = 'March 2016'   
//$partnermonthvalue = 'all';
//$partnermonthlyvalue = 'all';
//$dashboardValues->overallSummary_contractsign_Unsign_LeadSaved($mysqliportal); 
//$dashboardValues->overallSummary_Contracts($mysqliportal); 
//$dashboardValues->monthlySummary_yearContracts($partnermonthlyvalue,$mysqliportal); 
//$dashboardValues->currentmonthContracts($partnermonthvalue,$mysqliportal); 
//$dashboardValues->dailyContracts($cmonth,$mysqliportal); 


?>