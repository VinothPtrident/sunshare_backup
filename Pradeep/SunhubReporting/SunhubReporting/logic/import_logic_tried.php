<?php
include '../config/database.php';
include '../includes/PHPExcel/Classes/PHPExcel/IOFactory.php';
include '../includes/PHPExcel/Classes/PHPExcel.php';
error_reporting(E_ALL);
set_time_limit(0);
class Import
{
	function upload_recruitdata($filNam,$created,$mysqli)
	{
		$uploaded_dir = $_SERVER['DOCUMENT_ROOT'].'/eliteRecruit/uploadReports';
		$file_path = $uploaded_dir.'/'.$filNam;
		 if(preg_match('/.csv/', $filNam))
		{
			
			$objReader = new PHPExcel_Reader_Excel2007();
			$objReader->setReadDataOnly(true);
			$objReader = new PHPExcel_Reader_CSV();
			$objPHPExcel = $objReader->load($file_path);
			$allSheetName=$objPHPExcel->getSheetNames();
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			$highestRow = $objWorksheet->getHighestRow();
		    $highestColumn = $objWorksheet->getHighestColumn();
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);	
			 for ($column = 0; $column <= $highestColumnIndex; $column++) 
			   {
					$columnNamez[$column] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column,1)->getValue();
					//var_dump($columnNamez[$column]);
						
			   }
			    foreach($columnNamez as $c)
				{
					$columnName[]=trim($c);
					//var_dump($columnName);
				} 
				$arrayColumn = array("Number","Title","Preliminary Program","Program","Market","Status","First Name","Last Name","Email Address","Home Phone #","Cell Phone #","Other Phone #","Job Posting Site","Employee Referral","Date Applied","Date Contacted","Contacted By","Contact Notes","Comments","Interview Scheduled","Interviewing Manager","Interview Date","Interview Time","Interview Outcome","Rescheduled Date","New Hire Link Sent","Start Date","End Date","Production");
				$columnIndex = array_keys(array_intersect($columnName, $arrayColumn));
				$cellValue = array();
				$i=0;
				
				 for ($row = 2; $row <= $highestRow; $row++) 
					{
						for($column = 0; $column < count($columnIndex); $column++) 
						{
							$cellValue[$i][$column] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columnIndex[$column],$row)->getValue();
							//var_dump($cellValue[$i][$column]);
						}
						$i++;
					
				   }
					$numcheckArray = array();
					$duplicate_data_storage=array();
					$data=array();
					$duplicate_location=array();
				   for($i=0;$i<count($cellValue);$i++)
					{						
						$status=0;
						for($j=$i+1; $j<count($cellValue); $j++)
						{
							if($cellValue[$i][0] == $cellValue[$j][0])
							{								
								array_push($duplicate_data_storage,$cellValue[$j][0]);	
								array_push($duplicate_location,$i);	
								$status=1;			
							}	
						}
						$number_unique =$cellValue[$i][0];						
						/* if(!in_array($number_unique, $numcheckArray))
						{
							array_push($numcheckArray,$number_unique);
						}
						else{							
							array_push($duplicate_data_storage,$number_unique);							
						} */
						$data[]=array("location" => $i, "value"=> $number_unique);

					}	
					echo "<pre>";
				
					//arsort($data);
					uasort($data['value'], 'SORT_ASC');
					//array_multisort('value', SORT_ASC, $data);
						print_r($data);
					exit;
					 for($i=0;$i<count($cellValue);$i++)
					{
						$number_unique =$cellValue[$i][0];	
						$dup_status=0;
						if(!in_array($number_unique, $duplicate_data_storage))
						{							
							$dup_status=1;						
						}
						if($dup_status == 1)
						{
							print_r($duplicate_data_storage);
						}
						else
						{
							//insertion
						}
											
						$number_uniqueArray[] = $number_unique;
						$title =$cellValue[$i][1];
						//$titleArray[] =  $title;
						$preliminary_program =$cellValue[$i][2];
						$program =$cellValue[$i][3];
						$market =$cellValue[$i][4];
						$status =$cellValue[$i][5];
						$firstname =$cellValue[$i][6];
						$lastname =$cellValue[$i][7];
						$emailaddress =$cellValue[$i][8];
						$homephone =$cellValue[$i][9];
						$cellphone =$cellValue[$i][10];
						$otherphone =$cellValue[$i][11];
						$jobPostingSite =$cellValue[$i][12];
						$employeeReferral =$cellValue[$i][13];
						//$dateApplied =$cellValue[$i][14];
					    $dateApplied = date('Y-m-d',strtotime($cellValue[$i][14]));
						//$dateContacted =$cellValue[$i][15];
						$dateContacted = date('Y-m-d',strtotime($cellValue[$i][15]));
						$contactedBy =$cellValue[$i][16];
						$contactNotes =$cellValue[$i][17];
						$comments =$cellValue[$i][18];
						$interviewScheduled =$cellValue[$i][19];
						$interviewingManager =$cellValue[$i][20];
						//$interviewDate =$cellValue[$i][21];
					    $interviewDate = $cellValue[$i][21];
						
						if($interviewDate=='')
						{
							$interviewDate = '';
						}
						else
						{
							$interviewDate = date('Y-m-d',strtotime($cellValue[$i][21]));
						}
						$interviewTime =$cellValue[$i][22];
						$interviewOutcome =$cellValue[$i][23];
						$rescheduledDate =$cellValue[$i][24];
						if($rescheduledDate=='')
						{
							$rescheduledDate = '';
						}
						else
						{
						  $rescheduledDate = date('Y-m-d',strtotime($cellValue[$i][24]));
						}
						$newHireLinkSent =$cellValue[$i][25];
						
						if($newHireLinkSent=='')
						{
							$newHireLinkSent = '';
						}
						else
						{
						  $newHireLinkSent = date('Y-m-d',strtotime($cellValue[$i][25]));
						}
						$startDate =$cellValue[$i][26];
						if($startDate=='')
						{
							$startDate = '';
						}
						else
						{
						     $startDate = date('Y-m-d',strtotime($cellValue[$i][26]));
						}
						
						$enddate =$cellValue[$i][27];
						if($enddate=='')
						{
							$enddate = '';
						}
						else
						{
						     $enddate = date('Y-m-d',strtotime($cellValue[$i][27]));
						}
						$production =$cellValue[$i][28];
						$errorlog_id = 0;
						$createdDate = date('Y-m-d H:i:s');
						//if(filter_var($emailaddress, FILTER_VALIDATE_EMAIL))
						//{
							//if($number_uniqueArray[$i]!=0 || $number_uniqueArray[$i]!='')
							if(($number_uniqueArray[$i]!=0 && filter_var($emailaddress, FILTER_VALIDATE_EMAIL) && $number_uniqueArray[$i]!='' && preg_match("/^[1-9][0-9]*$/",$number_uniqueArray[$i]) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $homephone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $cellphone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $otherphone)))
							{
							 $select_number = $mysqli->prepare("select id  from recruitData_pra where number=?");
							$select_number->bind_param("s",$number_uniqueArray[$i]);
							$select_number->execute();
							$select_number->store_result();
							$select_number->bind_result($insid);
							$select_number->fetch();
						    $rowcount = $select_number->num_rows;
							if($rowcount!=0)
							{
								
								/*$stmt_update = $mysqli->prepare("UPDATE `recruitData_pra` SET `title`=?,`preliminary_program`=?,`program`=?,`market`=?,`status`=?,`firstname`=?,`lastname`=?,`email_address`=?,`homephone`=?,`cellphone`=?,`otherphone`=?,`jobPostingSite`=?,`employee_referral`=?,`date_applied`=?,`date_contacted`=?,`contactedby`=?,`contactnotes`=?,`comments`=?,`interviewScheduled`=?,`interviewingManager`=?,`interview_date`=?,`interview_time`=?,`interview_outcome`=?,`rescheduled_date`=?,`newHireLinkSent`=?,`startDate`=?,`endDate`=?,`production`=?,`errorlog_id`=?,`createdDate`=? WHERE id=$insid");
								$stmt_update->bind_param('ssssssssssssssssssssssssssssss',trim($title),trim($preliminary_program),trim($program),trim($market),trim($status),trim($firstname),trim($lastname),trim($emailaddress),trim($homephone),trim($cellphone),trim($otherphone),trim($jobPostingSite),trim($employeeReferral),trim($dateApplied),trim($dateContacted),trim($contactedBy),trim($contactNotes),trim($comments),trim($interviewScheduled),trim($interviewingManager),trim($interviewDate),trim($interviewTime),trim($interviewOutcome),trim($rescheduledDate),trim($newHireLinkSent),trim($startDate),trim($enddate),trim($production),$errorlog_id,trim($createdDate));
								$stmt_update->execute();
								$stmt_update->close();*/
								
								
							} 
							else
							{
								
								$stmt_insert = $mysqli->prepare("INSERT INTO `recruitData_pra`(`number`, `title`, `preliminary_program`, `program`, `market`, `status`, `firstname`, `lastname`, `email_address`, `homephone`, `cellphone`, `otherphone`, `jobPostingSite`, `employee_referral`, `date_applied`, `date_contacted`, `contactedby`, `contactnotes`, `comments`, `interviewScheduled`, `interviewingManager`, `interview_date`, `interview_time`, `interview_outcome`, `rescheduled_date`, `newHireLinkSent`, `startDate`, `endDate`, `production`, `errorlog_id`, `createdDate`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
								$stmt_insert->bind_param('issssssssssssssssssssssssssssis',trim($number_uniqueArray[$i]),trim($title),trim($preliminary_program),trim($program),trim($market),trim($status),trim($firstname),trim($lastname),trim($emailaddress),trim($homephone),trim($cellphone),trim($otherphone),trim($jobPostingSite),trim($employeeReferral),trim($dateApplied),trim($dateContacted),trim($contactedBy),trim($contactNotes),trim($comments),trim($interviewScheduled),trim($interviewingManager),trim($interviewDate),trim($interviewTime),trim($interviewOutcome),trim($rescheduledDate),trim($newHireLinkSent),trim($startDate),trim($enddate),trim($production),$errorlog_id,trim($createdDate)); 
								$stmt_insert->execute();
								$stmt_insert->close();
							}
							
							}
						//}
					 else
						{
							
							 //$blankarray[]=$titleArray[$i];
							 $numberErrorArray[]=$number_uniqueArray[$i];
							 
							 $titleArray[]=$title;
							 $preliminary_programArray[]=$preliminary_program;
							 $programArray[]=$program;
							 $marketArray[]=$market;
							 $statusArray[] =  $status;
							 $firstnameArray[] =  $firstname;
							 $lastnameArray[] =  $lastname;
							 $jobPostingSiteArray[] =  $jobPostingSite;
							 $employeeReferralArray[] =  $employeeReferral;
							 $dateAppliedArray[] =  $dateApplied;
							 $dateContactedArray[] =  $dateContacted;
							 $contactedByArray[] =  $contactedBy;
							 $contactNotesArray[] =  $contactNotes;
							 $commentsArray[] =  $comments;
							 $interviewScheduledArray[] =  $interviewScheduled;
							 $interviewingManagerArray[] =  $interviewingManager;
							 $interviewDateArray[] = $interviewDate;
							 $interviewTimeArray[] = $interviewTime; 
							 $interviewOutcomeArray[] = $interviewOutcome; 
							 $rescheduledDateArray[] = $rescheduledDate; 
							 $newHireLinkSentArray[] = $newHireLinkSent; 
							 $startDateArray[] = $startDate; 
							 $enddateArray[] = $enddate; 
							 $productionArray[] = $production; 
							 
							 $emailaddressErrorArray[] = $emailaddress;
							 $homephoneErrorArray[] =  $homephone;
							 $cellphoneErrorArray[] =  $cellphone;
							 $otherphoneErrorArray[] =  $otherphone;
							 
							 
							
							//echo $title;
							//print_r($number_uniqueArray);
							$objPHPExcel = new PHPExcel();
							$objPHPExcel->setActiveSheetIndex(0);
							
							
							$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Number');
							$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Title');
							$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Preliminary Program');
							$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Program');
							$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Market');
							$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Status');
							$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'First Name');
							$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Last Name');
							$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Email Address');
							$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Home Phone #');
							$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Cell Phone #');
							$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Phone #');
							$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Job Posting Site');
							$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Employee Referral');
							$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Date Applied');
							$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Date Contacted');
							$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Contacted By');
							$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Contact Notes');
							$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Comments');
							$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Interview Scheduled');
							$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Interviewing Manager');
							$objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Interview Date');
							$objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Interview Time');
							$objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Interview Outcome');
							$objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Rescheduled Date');
							$objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'New Hire Link Sent');
							$objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Start Date');
							$objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'End Date');
							$objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'Production');
							//echo $number_unique.'+++++'.$title;
							//echo $title;
							//echo 'B'.$j.$titleArray[$i];
							 //$titleError = $title;
							// echo $title;
							$j=2;
							for($k=0;$k<count($numberErrorArray);$k++)
							{
								if (!preg_match("/^[1-9][0-9]*$/",$numberErrorArray[$k])) {
								    $numbershow = "Number Format is not valid"; 
								}
								else{
									$numbershow = $numberErrorArray[$k];
								}
								if (!filter_var($emailaddressErrorArray[$k], FILTER_VALIDATE_EMAIL)) {
								  $emailshow = "Invalid email format"; 
								  
								}
								else{
									$emailshow = $emailaddressErrorArray[$k]; 
								}
								if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $homephoneErrorArray[$k])) {
								
								    $homephoneshow = "Homephone Format is not valid";
								}
								else{
									$homephoneshow =  $homephoneErrorArray[$k];
									//echo $homephone;
								}
								if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $cellphoneErrorArray[$k])) {
								
								    $cellphoneshow = "Homephone Format is not valid";
								}
								else{
									$cellphoneshow =  $cellphoneErrorArray[$k];
									//echo $homephone;
								}
								if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $otherphoneErrorArray[$k])) {
								
								    $otherphoneshow = "Homephone Format is not valid";
								}
								else{
									$otherphoneshow =  $otherphoneErrorArray[$k];
									//echo $homephone;
								}
							    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$j, $numbershow);
							    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$j, $titleArray[$k]);
							    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$j, $preliminary_programArray[$k]);
								$objPHPExcel->getActiveSheet()->SetCellValue('D'.$j, $programArray[$k]);
								$objPHPExcel->getActiveSheet()->SetCellValue('E'.$j, $marketArray[$k]);
								$objPHPExcel->getActiveSheet()->SetCellValue('F'.$j, $statusArray[$k]);
								$objPHPExcel->getActiveSheet()->SetCellValue('G'.$j, $firstnameArray[$k]);
								$objPHPExcel->getActiveSheet()->SetCellValue('H'.$j, $lastnameArray[$k]);
								
							    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$j, $emailshow);
							    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$j, $homephoneshow);
								$objPHPExcel->getActiveSheet()->SetCellValue('K'.$j, $cellphoneshow );
								$objPHPExcel->getActiveSheet()->SetCellValue('L'.$j, $otherphoneshow );
								
								$objPHPExcel->getActiveSheet()->SetCellValue('M'.$j, $jobPostingSiteArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('N'.$j, $employeeReferralArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('O'.$j, $dateAppliedArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('P'.$j, $dateContactedArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$j, $contactedByArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('R'.$j, $contactNotesArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('S'.$j, $commentsArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('T'.$j, $interviewScheduledArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('U'.$j, $interviewingManagerArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('V'.$j, $interviewDateArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('W'.$j, $interviewTimeArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('X'.$j, $interviewOutcomeArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$j, $rescheduledDateArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('Z'.$j, $newHireLinkSentArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('AA'.$j, $startDateArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('AB'.$j, $enddateArray[$k] );
								$objPHPExcel->getActiveSheet()->SetCellValue('AC'.$j, $productionArray[$k] );
							 $j++;
							}
							
							
							//$objPHPExcel->getActiveSheet()->SetCellValue('B3', trim($title));
							//echo '---'.$j.'--';
						
							//echo "---";
							//echo $titleError;
							//$title = $titleError;
							
							
							 $fileName = 'ErroLog.csv';
							
							$objPHPExcel->getActiveSheet()->setTitle('Sheet1');
							//$objPHPExcel->getSheetByName('Sheet1')->getStyle("A1")->getFont()->getBold();
							
							 //$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
							 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
							//header('Content-Type: application/vnd.ms-excel');
							//header('Content-Disposition: attachment;filename="' . $fileName . '"');
							//header('Cache-Control: max-age=0');
							//ob_clean(); 
							$downloadL = str_replace( __FILE__,$_SERVER['DOCUMENT_ROOT'].'/eliteRecruit/errorlogFiles/'.$fileName, __FILE__);
		                     $objWriter->save($downloadL); 
							//exit;  
							
						
						  
						 
						} 
						

					}
					//print_r($numcheckArray);
				
					
					    //unset($number_uniqueArray);
			
		}
		else
		{
			echo "no";
		}			
	
	}
}
$importValues	= new Import();
$filNam = "template (1)_2016-01-27 19_56_46.csv";
$created = "2016-01-27";
$importValues->upload_recruitdata($filNam,$created,$mysqli);
?>