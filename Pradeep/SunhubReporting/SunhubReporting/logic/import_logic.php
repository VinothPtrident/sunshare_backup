<?php
include '../config/database.php';
include 'PHPExcel/Classes/PHPExcel/IOFactory.php';
include 'PHPExcel/Classes/PHPExcel.php';
error_reporting(E_ALL);
//set_time_limit(0);
class Import
{
	function upload_recruit_data($filNam,$created,$mysqli)
	{
		$uploaded_dir = $_SERVER['DOCUMENT_ROOT'].'/saleshubdashboard/uploadReports';
		$file_path = $uploaded_dir.'/'.$filNam;
		 if(preg_match('/.csv/', $filNam))
		{
			$objReader = new PHPExcel_Reader_Excel2007();
			$objReader->setReadDataOnly(true);
			$objReader = new PHPExcel_Reader_CSV();
			$objPHPExcel = $objReader->load($file_path);
			$allSheetName=$objPHPExcel->getSheetNames();
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			$highestRow = $objWorksheet->getHighestRow();
		    $highestColumn = $objWorksheet->getHighestColumn();
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);	
			
			for ($column = 0; $column <= $highestColumnIndex; $column++) 
			   {
					$columnNamez[$column] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column,1)->getValue();
					//var_dump($columnNamez[$column]);
						
			   }
			   foreach($columnNamez as $c)
				{
					$columnName[]=trim($c);
					//var_dump($columnName);
				} 
			   $arrayColumn = array("Signer List","Completed On");
			   $columnIndex = array_keys(array_intersect($columnName, $arrayColumn));
			   $i=0;
			   for ($row = 2; $row <= $highestRow; $row++) 
					{
						for($column = 0; $column < count($columnIndex); $column++) 
						{
							$cellValue[$i][$column] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columnIndex[$column],$row)->getValue();
							//var_dump($cellValue[$i][$column]);
						}
						$i++;
					
				   }
				     for($m=0;$m<count($cellValue);$m++)
					{    
						
						
						$arr=explode('[',$cellValue[$m][0]);
						$names=$arr[0];
						$emails=$arr[1];
						$correctemails=trim($emails, "]");
						$dates=explode(' ',$cellValue[$m][1]);
						$datescor=date('Y-m-d',strtotime($dates[0]));
						//var_dump($datescor);
						 $datetime=$dates[1];
					    $totaltime=$datescor.' '.$datetime;
						//date("g:i a", strtotime("13:30"));
						
						$select_number = $mysqli->prepare("select email from saleshub_import where email=?");
						$select_number->bind_param("s",$correctemails);
						$select_number->execute();
						$select_number->bind_result($insid);
						$select_number->store_result();
						 $rowcount = $select_number->num_rows;
					    $select_number->fetch();
						$select_number->close();
						 if($rowcount!=0)
						{
							$status_err=1;
							
						}
						else
						{ 
							$status_err=2;
							$stmt_insert = $mysqli->prepare("INSERT INTO `saleshub_import`(`fname`,`email`,`completed_by`) VALUES ('$names','$correctemails','$totaltime')");
							//$stmt_insert->bind_param('sss',trim($number_unique),trim($title),trim($preliminary_program)); 
							$stmt_insert->execute();
							$stmt_insert->close(); 
											
						} 
					}
					 if($status_err==1)
					{
						echo '<div class="alert alert-danger" align="center" id="partial"><strong>Import Failed...Please Try again </strong></div>';
					}	
					else 
					{
						echo '<div class="alert alert-success" align="center" id="success">
													  <strong> Successfully Imported </strong>
													</div>';			
					}	 
		}
	}
				   
}											
$importValues	= new Import();	
  //$filNam = "template (12)_2016-02-24 17_38_36.csv";
  /* $filNam = "sample prasu test.csv";
$created = "2016-01-27 23:12:00";
$mysqli = new mysqli("localhost","root","phpils135$","sunshare_saleshub");
$importValues->upload_recruit_data($filNam,$created,$mysqli);  */ 

	
?>