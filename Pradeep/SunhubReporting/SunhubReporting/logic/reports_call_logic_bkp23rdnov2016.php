<?php
//include '../config/database.php';
set_time_limit(0);
class Report
{
	function reports_call_testing($mysqliportal)
	{
		$partneridArray =array();
		$fetch_partnerid = $mysqliportal->prepare("select distinct partner_id from manage_salesrep ");
		$fetch_partnerid->execute();
		$fetch_partnerid->bind_result($partner_id);
		while($fetch_partnerid->fetch())
		{
			array_push($partneridArray, $partner_id);
		}
		$fetch_partnerid->close();
		
		$resultsRepidArray =array();
		$pccwRepidArray =array();
		$sunshareSalesRepidArray =array();
		$webRepidArray =array();
		$callcenterRepidArray =array();
		for($i=0; $i<count($partneridArray); $i++)
		{
			
			$partnerid = $partneridArray[$i];
			
			if($partnerid == '10001' || $partnerid == '10002')
			{
				$fetch_pccwrep = $mysqliportal->prepare("select id as pccwrepid from manage_salesrep where partner_id=$partnerid");
				$fetch_pccwrep->execute();
				$fetch_pccwrep->bind_result($pccwrepid);
				while($fetch_pccwrep->fetch())
				{
					array_push($callcenterRepidArray, $pccwrepid);
				}
				$fetch_pccwrep->close();
				
			}
			
		}
		//echo "<pre>";
		//print_r($callcenterRepidArray);
		 $callcenter_repids = implode(',',$callcenterRepidArray); 
	
		$leadidArray = array();
		$salesrepidArray = array();
		$salesrepnameArray = array();
		$loginidArray = array();
		$callcenterArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$xcelaccountnoArray = array();
		$campaigntypefromArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		$leadsourceArray = array();
		$lastmodifieddateArray = array();
		$lastmodifiedsalespersonArray = array();
		$lastmodifiedsalesrepidArray = array();
		$signeddateArray=array();
		$lastmodifiedcallcenterArray=array();
	
		$fetch_salesid = $mysqliportal->prepare("select ld.lid as leadid,ld.salesrep_id as salesrepid,ms.email as loginid,ms.name as salesrepname,ms.partner_id as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus,lf.leadsource as leadsource from leadinfo lf,leads ld,manage_salesrep ms where lf.leadid=ld.lid and ld.salesrep_id in ($callcenter_repids) and ld.salesrep_id=ms.id and ld.createdlead_time!='0000-00-00 00:00:00' order by ld.lid DESC");
		$fetch_salesid->execute();
		$fetch_salesid->bind_result($leadid,$salesrepid,$loginid,$salesrepname,$callcenter,$firstname,$lastname,$emailid,$address1,$xcelaccountno,$campaigntypefrom,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus,$leadsource);
		$fetch_salesid->store_result();
		$rows = $fetch_salesid->num_rows;
		 while($fetch_salesid->fetch())
		{
			array_push($leadidArray,$leadid);
			array_push($salesrepidArray,$salesrepid);
			array_push($salesrepnameArray,$salesrepname);
			array_push($loginidArray,$loginid);
			array_push($callcenterArray,$callcenter);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($xcelaccountnoArray,$xcelaccountno);
			array_push($campaigntypefromArray,$campaigntypefrom);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			array_push($leadsourceArray,$leadsource);
			//array_push($lastmodifieddateArray,$lastmodifieddate);
			//array_push($lastmodifiessalespersonArray,$lastmodifiessalesperson);
			
			
		}
		$fetch_salesid->close();
		
	
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			
			$lead_id = $leadidArray[$i];
			$fetch_callss= $mysqliportal->prepare("SELECT * FROM `calls`WHERE lead_id = $lead_id");
			$fetch_callss->execute();
			$fetch_callss->store_result();
			$checkingrow=$fetch_callss->num_rows;
			if($checkingrow>0){
				$fetch_calls= $mysqliportal->prepare("SELECT  C.name as lastmodifiedsalesrep,date_format(L.time,'%m/%d/%Y %H:%i:%S') as lastmodifieddate,L.call_centerid as repid,C.partner_id FROM `calls` AS L,manage_salesrep as C WHERE C.id=L.call_centerid  and L.lead_id = $lead_id ORDER BY time desc limit 1");
				$fetch_calls->execute();
				$fetch_calls->bind_result($lastmodifiedsalesperson,$lastmodifieddate,$lastmodifiedsalesrepid,$lastmodifiedcallcenter);
				while($fetch_calls->fetch())
				{
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$lastmodifiedsalesperson);
					 array_push($lastmodifiedsalesrepidArray,$lastmodifiedsalesrepid);
					 array_push($lastmodifiedcallcenterArray,$lastmodifiedcallcenter);
				} 
			}
			else{
				   $lastmodifieddate=date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$salesrepnameArray[$i]);
					array_push($lastmodifiedcallcenterArray,$callcenterArray[$i]);
					
			}
			
		}
		
		$tablereport = '<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">  
					<thead>
						<!--<tr>
							<th style="font-size:13px;">Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Lead Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Last Modified</th>
							<th style="font-size:13px;">Last Modified Date</th>
							
						</tr>-->
						<tr>
						    <th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Created Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">Updated Date</th>
							<th style="font-size:13px;">Last Updated By</th>
							<th style="font-size:13px;">Updated Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Xcel Account No</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Contract Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Envelope ID</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Referred By</th>
						</tr>
						
					</thead>
       
				<tbody>';
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			$city_sh = $cityArray[$i];
			if($city_sh=='.' || $city_sh=='....' || $city_sh=='0' || $city_sh=='00' || $city_sh=='000' || $city_sh=='0000' || $city_sh=='?')
			{
				$city_show = 'NA';
			}	
			else
			{
				$city_show = ucwords($city_sh);
			}	
			$state_sh = $stateArray[$i];	
			if($state_sh=='0')
			{
				$state_show = 'NA';
			}	
			else
			{
				$state_show = ucwords($state_sh);
			}	
			$country_sh = $countryArray[$i];	
			if($country_sh=='0')
			{
				$country_show = 'NA';
			}	
			else
			{
				$country_show = ucwords($country_sh);
			}
			$campaigntypefromstatus='';
			$campaigntype_from = $campaigntypefromArray[$i];
			if($campaigntype_from == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaigntype_from == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaigntype_from == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else if($campaigntype_from == 4)
			{
				$campaigntypefromstatus = '$25 Amazon.com Gift Card';
			}
			else if($campaigntype_from == 5)
			{
				$campaigntypefromstatus = '$50 Amazon.com Gift Card';
			}
			$lead_status = $leadstatusArray[$i];
			if($lead_status == 1)
			{
				$leadstatus_show = 'Lead Saved';
				$signeddateArray='';
				$envelopeid='';
			}
			else if($lead_status == 3)
			{
				$leadstatus_show = 'Contract Generated';
				$signeddateArray='';
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
			}
			else if($lead_status == 4)
			{
				$leadstatus_show = 'Contract Signed';
				$lead_id = $leadidArray[$i];
				$fetch_calls= $mysqliportal->prepare("SELECT date_format(enp.signedtime,'%m/%d/%Y %H:%i:%s') as signedtime FROM `envelope` AS enp,leads as L WHERE L.lid=enp.leadID and enp.leadID = $lead_id and enp.Envelope_Status='completed'");
				$fetch_calls->execute();
				$fetch_calls->bind_result($signedtimedate);
				while($fetch_calls->fetch())
				{
					//array_push($signeddateArray,$signedtimedate);
					$signeddateArray=$signedtimedate;
				   
				}
				$fetch_calls->close();
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
				
			}
			else if($lead_status == 5)
			{
				$leadstatus_show = 'Contract Declined';
				$signeddateArray='';
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
			}
			$emailcheck = $emailidArray[$i];
			
			$excelexp =$xcelaccountnoArray[$i];
			if($excelexp=='0' || $excelexp==''){
				$excelexp='';
			}
			$call_center = $callcenterArray[$i];
			if($call_center=='10001')
			{
				$callcenterShow = 'The Results';
			}
			else if($call_center=='10002')
			{
				$callcenterShow = 'PCCW';
			}
			$lastcall_center = $lastmodifiedcallcenterArray[$i];
			if($lastcall_center=='10001')
			{
				$lastcall_centerShow = 'The Results';
			}
			else if($lastcall_center=='10002')
			{
				$lastcall_centerShow = 'PCCW';
			}
			else if($lastcall_center=='10003')
			{
				$lastcall_centerShow = 'Internal Sales Team';
			}
			else if($lastcall_center=='10007')
			{
				$lastcall_centerShow = 'Web';
			}
			$leadsourceShow='';
			$leadsource =$leadsourceArray[$i];
			if($leadsource==1)
			{
				$leadsourceShow = 'Direct Outreach';
			}
			else if($leadsource==2)
			{
				$leadsourceShow = 'Partner Generated';
			}
			else if($leadsource==3)
			{
				$leadsourceShow = 'Event';
			}
			else if($leadsource==4)
			{
				$leadsourceShow = 'Media';
			}
			else if($leadsource==5)
			{
				$leadsourceShow = 'Digital Promotion';
			}
			else if($leadsource==6)
			{
				$leadsourceShow = 'Social Media - Organic';
			}
			else if($leadsource==7)
			{
				$leadsourceShow = 'Referral - Word of Mouth';
			}
			else if($leadsource==8)
			{
				$leadsourceShow = 'Web Search';
			}
			else if($leadsource==9)
			{
				$leadsourceShow = 'External Website';
			}
			else if($leadsource==10)
			{
				$leadsourceShow = 'Out Of Home';
			}
			else if($leadsource==11)
			{
				$leadsourceShow = 'Garden - Drive By';
			}
			else if($leadsource==12)
			{
				$leadsourceShow = "Competitor's Marketing  Material";
			}
			else if($leadsource==13)
			{
				$leadsourceShow = 'UNKNOWN';
			}
			else if($leadsource==14)
			{
				$leadsourceShow = "Other's";
			}
			$lastmodified_date =$lastmodifieddateArray[$i];
			$lastmodified_salesperson =$lastmodifiedsalespersonArray[$i];
			$referredby='';
			$fetch_referredby= $mysqliportal->prepare("SELECT generalcomments FROM `general_comments` WHERE leadid=$leadidArray[$i]");
			$fetch_referredby->execute();
			$fetch_referredby->bind_result($rreferredby);
			while($fetch_referredby->fetch())
			{
				$referredby=$rreferredby;
			   
			}
			$fetch_referredby->close();
			
			$tablereport .= '<tr>
			 <td>'.date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i])).'</td>
			 <td>'.ucwords(strtolower($salesrepnameArray[$i])).'</td>
			 <td>'.$callcenterShow.'</td>
			 <td>'.$lastmodified_date.'</td>
			 <td>'.$lastmodified_salesperson.'</td>
			 <td>'.$lastcall_centerShow.'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			 <td>'.$excelexp.'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$city_show.'</td>
			 <td>'.$state_show.'</td>
			 <td>'.$country_show.'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.$leadstatus_show.'</td>
			 <td>'.$signeddateArray.'</td>
			 <td>'.$envelopeid.'</td>
			 <td>'.$leadsourceShow.'</td>
			 <td>'.$referredby.'</td>
			</tr>';
			 //$v--; 
			 
			unset($envelopeid);  
			
		}	
		$tablereport .= '</tbody>
				</table>';
		echo $tablereport; 
		//$tablereport = '<table>';
	}
	function reports_call_twoweeks($mysqliportal)
	{
		$fromdate = date('Y-m-d');
        $weekdate = date('Y-m-d', strtotime("-2 week")); //2 week ago
		$partneridArray =array();
		$fetch_partnerid = $mysqliportal->prepare("select distinct partner_id from manage_salesrep ");
		$fetch_partnerid->execute();
		$fetch_partnerid->bind_result($partner_id);
		while($fetch_partnerid->fetch())
		{
			array_push($partneridArray, $partner_id);
		}
		$fetch_partnerid->close();
		
		$resultsRepidArray =array();
		$pccwRepidArray =array();
		$sunshareSalesRepidArray =array();
		$webRepidArray =array();
		$callcenterRepidArray =array();
		for($i=0; $i<count($partneridArray); $i++)
		{
			
			$partnerid = $partneridArray[$i];
			
			if($partnerid == '10001' || $partnerid == '10002')
			{
				$fetch_pccwrep = $mysqliportal->prepare("select id as pccwrepid from manage_salesrep where partner_id=$partnerid");
				$fetch_pccwrep->execute();
				$fetch_pccwrep->bind_result($pccwrepid);
				while($fetch_pccwrep->fetch())
				{
					array_push($callcenterRepidArray, $pccwrepid);
				}
				$fetch_pccwrep->close();
				
			}
			
		}
		//echo "<pre>";
		//print_r($callcenterRepidArray);
		 $callcenter_repids = implode(',',$callcenterRepidArray); 
	
		$leadidArray = array();
		$salesrepidArray = array();
		$salesrepnameArray = array();
		$loginidArray = array();
		$callcenterArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$xcelaccountnoArray = array();
		$campaigntypefromArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		$leadsourceArray = array();
		$lastmodifieddateArray = array();
		$lastmodifiedsalespersonArray = array();
		$lastmodifiedsalesrepidArray = array();
		$signeddateArray=array();
		$lastmodifiedcallcenterArray=array();
	
		$fetch_salesid = $mysqliportal->prepare("select ld.lid as leadid,ld.salesrep_id as salesrepid,ms.email as loginid,ms.name as salesrepname,ms.partner_id as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus,lf.leadsource as leadsource from leadinfo lf,leads ld,manage_salesrep ms where lf.leadid=ld.lid and ld.salesrep_id in ($callcenter_repids) and ld.salesrep_id=ms.id and DATE_FORMAT(ld.createdlead_time,'%Y-%m-%d') BETWEEN '$weekdate' and '$fromdate' order by ld.lid DESC");
		$fetch_salesid->execute();
		$fetch_salesid->bind_result($leadid,$salesrepid,$loginid,$salesrepname,$callcenter,$firstname,$lastname,$emailid,$address1,$xcelaccountno,$campaigntypefrom,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus,$leadsource);
		$fetch_salesid->store_result();
		$rows = $fetch_salesid->num_rows;
		 while($fetch_salesid->fetch())
		{
			array_push($leadidArray,$leadid);
			array_push($salesrepidArray,$salesrepid);
			array_push($salesrepnameArray,$salesrepname);
			array_push($loginidArray,$loginid);
			array_push($callcenterArray,$callcenter);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($xcelaccountnoArray,$xcelaccountno);
			array_push($campaigntypefromArray,$campaigntypefrom);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			array_push($leadsourceArray,$leadsource);
			//array_push($lastmodifieddateArray,$lastmodifieddate);
			//array_push($lastmodifiessalespersonArray,$lastmodifiessalesperson);
			
			
		}
		$fetch_salesid->close();
		
	
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			
			$lead_id = $leadidArray[$i];
			$fetch_callss= $mysqliportal->prepare("SELECT * FROM `calls`WHERE lead_id = $lead_id");
			$fetch_callss->execute();
			$fetch_callss->store_result();
			$checkingrow=$fetch_callss->num_rows;
			if($checkingrow>0){
				$fetch_calls= $mysqliportal->prepare("SELECT  C.name as lastmodifiedsalesrep,date_format(L.time,'%m/%d/%Y %H:%i:%S') as lastmodifieddate,L.call_centerid as repid,C.partner_id FROM `calls` AS L,manage_salesrep as C WHERE C.id=L.call_centerid  and L.lead_id = $lead_id ORDER BY time desc limit 1");
				$fetch_calls->execute();
				$fetch_calls->bind_result($lastmodifiedsalesperson,$lastmodifieddate,$lastmodifiedsalesrepid,$lastmodifiedcallcenter);
				while($fetch_calls->fetch())
				{
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$lastmodifiedsalesperson);
					 array_push($lastmodifiedsalesrepidArray,$lastmodifiedsalesrepid);
					 array_push($lastmodifiedcallcenterArray,$lastmodifiedcallcenter);
				} 
			}
			else{
				   $lastmodifieddate=date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$salesrepnameArray[$i]);
					array_push($lastmodifiedcallcenterArray,$callcenterArray[$i]);
					
			}
			
		}
		
		$tablereport = '<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">  
					<thead>
						<!--<tr>
							<th style="font-size:13px;">Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Lead Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Last Modified</th>
							<th style="font-size:13px;">Last Modified Date</th>
							
						</tr>-->
						<tr>
						    <th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Created Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">Updated Date</th>
							<th style="font-size:13px;">Last Updated By</th>
							<th style="font-size:13px;">Updated Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Xcel Account No</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Contract Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Envelope ID</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Referred By</th>
						</tr>
						
					</thead>
       
				<tbody>';
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			$city_sh = $cityArray[$i];
			if($city_sh=='.' || $city_sh=='....' || $city_sh=='0' || $city_sh=='00' || $city_sh=='000' || $city_sh=='0000' || $city_sh=='?')
			{
				$city_show = 'NA';
			}	
			else
			{
				$city_show = ucwords($city_sh);
			}	
			$state_sh = $stateArray[$i];	
			if($state_sh=='0')
			{
				$state_show = 'NA';
			}	
			else
			{
				$state_show = ucwords($state_sh);
			}	
			$country_sh = $countryArray[$i];	
			if($country_sh=='0')
			{
				$country_show = 'NA';
			}	
			else
			{
				$country_show = ucwords($country_sh);
			}	
			$campaigntype_from = $campaigntypefromArray[$i];
			$campaigntypefromstatus='';
			if($campaigntype_from == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaigntype_from == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaigntype_from == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else if($campaigntype_from == 4)
			{
				$campaigntypefromstatus = '$25 Amazon.com Gift Card';
			}
			else if($campaigntype_from == 5)
			{
				$campaigntypefromstatus = '$50 Amazon.com Gift Card';
			}
			$lead_status = $leadstatusArray[$i];
			if($lead_status == 1)
			{
				$leadstatus_show = 'Lead Saved';
				$signeddateArray='';
				$envelopeid='';
			}
			else if($lead_status == 3)
			{
				$leadstatus_show = 'Contract Generated';
				$signeddateArray='';
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
			}
			else if($lead_status == 4)
			{
				$leadstatus_show = 'Contract Signed';
				$lead_id = $leadidArray[$i];
				$fetch_calls= $mysqliportal->prepare("SELECT date_format(enp.signedtime,'%m/%d/%Y %H:%i:%s') as signedtime FROM `envelope` AS enp,leads as L WHERE L.lid=enp.leadID and enp.leadID = $lead_id and enp.Envelope_Status='completed'");
				$fetch_calls->execute();
				$fetch_calls->bind_result($signedtimedate);
				while($fetch_calls->fetch())
				{
					//array_push($signeddateArray,$signedtimedate);
					$signeddateArray=$signedtimedate;
				   
				}
				$fetch_calls->close();
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
				
			}
			else if($lead_status == 5)
			{
				$leadstatus_show = 'Contract Declined';
				$signeddateArray='';
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
			}
			$emailcheck = $emailidArray[$i];
			
			$excelexp =$xcelaccountnoArray[$i];
			if($excelexp=='0' || $excelexp==''){
				$excelexp='';
			}
			$call_center = $callcenterArray[$i];
			if($call_center=='10001')
			{
				$callcenterShow = 'The Results';
			}
			else if($call_center=='10002')
			{
				$callcenterShow = 'PCCW';
			}
			$lastcall_center = $lastmodifiedcallcenterArray[$i];
			if($lastcall_center=='10001')
			{
				$lastcall_centerShow = 'The Results';
			}
			else if($lastcall_center=='10002')
			{
				$lastcall_centerShow = 'PCCW';
			}
			else if($lastcall_center=='10003')
			{
				$lastcall_centerShow = 'Internal Sales Team';
			}
			else if($lastcall_center=='10007')
			{
				$lastcall_centerShow = 'Web';
			}
			$leadsourceShow='';
			$leadsource =$leadsourceArray[$i];
			if($leadsource==1)
			{
				$leadsourceShow = 'Direct Outreach';
			}
			else if($leadsource==2)
			{
				$leadsourceShow = 'Partner Generated';
			}
			else if($leadsource==3)
			{
				$leadsourceShow = 'Event';
			}
			else if($leadsource==4)
			{
				$leadsourceShow = 'Media';
			}
			else if($leadsource==5)
			{
				$leadsourceShow = 'Digital Promotion';
			}
			else if($leadsource==6)
			{
				$leadsourceShow = 'Social Media - Organic';
			}
			else if($leadsource==7)
			{
				$leadsourceShow = 'Referral - Word of Mouth';
			}
			else if($leadsource==8)
			{
				$leadsourceShow = 'Web Search';
			}
			else if($leadsource==9)
			{
				$leadsourceShow = 'External Website';
			}
			else if($leadsource==10)
			{
				$leadsourceShow = 'Out Of Home';
			}
			else if($leadsource==11)
			{
				$leadsourceShow = 'Garden - Drive By';
			}
			else if($leadsource==12)
			{
				$leadsourceShow = "Competitor's Marketing  Material";
			}
			else if($leadsource==13)
			{
				$leadsourceShow = 'UNKNOWN';
			}
			else if($leadsource==14)
			{
				$leadsourceShow = "Other's";
			}
			$lastmodified_date =$lastmodifieddateArray[$i];
			$lastmodified_salesperson =$lastmodifiedsalespersonArray[$i];
			$referredby='';
			$fetch_referredby= $mysqliportal->prepare("SELECT generalcomments FROM `general_comments` WHERE leadid=$leadidArray[$i]");
			$fetch_referredby->execute();
			$fetch_referredby->bind_result($rreferredby);
			while($fetch_referredby->fetch())
			{
				$referredby=$rreferredby;
			   
			}
			$fetch_referredby->close();
			
			$tablereport .= '<tr>
			 <td>'.date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i])).'</td>
			 <td>'.ucwords(strtolower($salesrepnameArray[$i])).'</td>
			 <td>'.$callcenterShow.'</td>
			 <td>'.$lastmodified_date.'</td>
			 <td>'.$lastmodified_salesperson.'</td>
			 <td>'.$lastcall_centerShow.'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			 <td>'.$excelexp.'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$city_show.'</td>
			 <td>'.$state_show.'</td>
			 <td>'.$country_show.'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.$leadstatus_show.'</td>
			 <td>'.$signeddateArray.'</td>
			 <td>'.$envelopeid.'</td>
			 <td>'.$leadsourceShow.'</td>
			 <td>'.$referredby.'</td>
			</tr>';
			 //$v--; 
			 
			unset($envelopeid);  
			
		}	
		$tablereport .= '</tbody>
				</table>';
		echo $tablereport; 
		//$tablereport = '<table>';
	}
	
	function reports_webenroll_testing($mysqliportal)
	{
		$partneridArray =array();
		$fetch_partnerid = $mysqliportal->prepare("select distinct partner_id from manage_salesrep where status=1");
		$fetch_partnerid->execute();
		$fetch_partnerid->bind_result($partner_id);
		while($fetch_partnerid->fetch())
		{
			array_push($partneridArray, $partner_id);
		}
		$fetch_partnerid->close();
		
		
		$webRepidArray =array();
		for($i=0; $i<count($partneridArray); $i++)
		{
			
			$partnerid = $partneridArray[$i];
			
			if($partnerid == '10007')
			{
				$fetch_pccwrep = $mysqliportal->prepare("select id as webrepid from manage_salesrep where partner_id=$partnerid");
				$fetch_pccwrep->execute();
				$fetch_pccwrep->bind_result($webrepid);
				while($fetch_pccwrep->fetch())
				{
					array_push($webRepidArray, $webrepid);
				}
				$fetch_pccwrep->close();
				
			}
			
		}
		
		$web_repids = implode(',',$webRepidArray); 
		
		//$sourceArray = array();
		$leadidArray = array();
		$salesrepidArray = array();
		$salesrepnameArray = array();
		$loginidArray = array();
		$callcenterArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$xcelaccountnoArray = array();
		$campaigntypefromArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		$leadsourceArray = array();
		$lastmodifieddateArray = array();
		$lastmodifiedsalespersonArray = array();
		$lastmodifiedsalesrepidArray = array();
		$lastcallcenterArray = array();
		$lastloginidArray = array();
		
		$fetch_webenroll = $mysqliportal->prepare("select ld.lid as leadid,ld.salesrep_id as salesrepid,ms.email as loginid,ms.name as salesrepname,ms.partner_id as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus,lf.leadsource as leadsource from leadinfo lf,leads ld,manage_salesrep ms where lf.leadid=ld.lid and ld.salesrep_id in ($web_repids) and ld.salesrep_id=ms.id and ld.createdlead_time!='0000-00-00 00:00:00' order by ld.lid DESC");
		$fetch_webenroll->execute();
		$fetch_webenroll->bind_result($leadid,$salesrepid,$loginid,$salesrepname,$callcenter,$firstname,$lastname,$emailid,$address1,$xcelaccountno,$campaigntypefrom,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus,$leadsource);
		$fetch_webenroll->store_result();
		$rows = $fetch_webenroll->num_rows;
		
		//$tablereport = '<table>';
		 while($fetch_webenroll->fetch())
		{
			//array_push($sourceArray,$source);
			array_push($leadidArray,$leadid);
			array_push($salesrepidArray,$salesrepid);
			array_push($salesrepnameArray,$salesrepname);
			array_push($loginidArray,$loginid);
			array_push($callcenterArray,$callcenter);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($xcelaccountnoArray,$xcelaccountno);
			array_push($campaigntypefromArray,$campaigntypefrom);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			array_push($leadsourceArray,$leadsource);
			
			
			
		}
		$fetch_webenroll->close();
		
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			
			$lead_id = $leadidArray[$i];
			$fetch_callss= $mysqliportal->prepare("SELECT * FROM `calls`WHERE lead_id = $lead_id");
			$fetch_callss->execute();
			$fetch_callss->store_result();
			$checkingrow=$fetch_callss->num_rows;
			if($checkingrow>0){
				$fetch_calls= $mysqliportal->prepare("SELECT  C.name as lastmodifiedsalesrep,date_format(L.time,'%m/%d/%Y %H:%i:%S') as lastmodifieddate,L.call_centerid as repid,C.partner_id,C.email FROM `calls` AS L,manage_salesrep as C WHERE C.id=L.call_centerid  and L.lead_id = $lead_id ORDER BY time desc limit 1");
				$fetch_calls->execute();
				$fetch_calls->bind_result($lastmodifiedsalesperson,$lastmodifieddate,$lastmodifiedsalesrepid,$lastcallcentername,$lastloginid);
				while($fetch_calls->fetch())
				{
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$lastmodifiedsalesperson);
					array_push($lastmodifiedsalesrepidArray,$lastmodifiedsalesrepid);
					array_push($lastcallcenterArray,$lastcallcentername);
					array_push($lastloginidArray,$lastloginid);
					
					 
				} 
			}
			else{
				    $lastmodifieddate=date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$salesrepnameArray[$i]);
					array_push($lastmodifiedsalesrepidArray,$salesrepidArray[$i]);
					array_push($lastcallcenterArray,$callcenterArray[$i]);
					array_push($lastloginidArray,$loginidArray[$i]);
			}
			
		} 
		/* print_r($lastmodifieddateArray);
		exit; */
		$tablereport = '<table id="example" class="table table-striped table-bordered display" cellspacing="0" width="100%">  
					<thead>
						<!--<tr>
							<th style="font-size:13px;">Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Lead Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Last Modified</th>
							<th style="font-size:13px;">Last Modified Date</th>
						</tr>-->
						<tr>
							<th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Source</th>
							<th style="font-size:13px;">Updated Date</th>
							<th style="font-size:13px;">Sales Rep ID</th>
							<th style="font-size:13px;">Last Updated By</th>
							<th style="font-size:13px;">Login ID</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Excel Acc #</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Contract Status</th>
							<th style="font-size:13px;">Signed Date</th>
						</tr>
					</thead>
					
       
				<tbody>';
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			$city_sh = $cityArray[$i];
			if($city_sh=='.' || $city_sh=='....' || $city_sh=='0' || $city_sh=='00' || $city_sh=='000' || $city_sh=='0000' || $city_sh=='?')
			{
				$city_show = 'NA';
			}	
			else
			{
				$city_show = ucwords($city_sh);
			}	
			$state_sh = $stateArray[$i];	
			if($state_sh=='0')
			{
				$state_show = 'NA';
			}	
			else
			{
				$state_show = ucwords($state_sh);
			}	
			$country_sh = $countryArray[$i];	
			if($country_sh=='0')
			{
				$country_show = 'NA';
			}	
			else
			{
				$country_show = ucwords($country_sh);
			}	
			$campaigntype_from = $campaigntypefromArray[$i];
			$campaigntypefromstatus='';
			if($campaigntype_from == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaigntype_from == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaigntype_from == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else if($campaigntype_from == 4)
			{
				$campaigntypefromstatus = '$25 Amazon.com Gift Card';
			}
			else if($campaigntype_from == 5)
			{
				$campaigntypefromstatus = '$50 Amazon.com Gift Card';
			}
			$lead_status = $leadstatusArray[$i];
			if($lead_status == 1)
			{
				$leadstatus_show = 'Lead Saved';
				$signeddateArray='';
			}
			else if($lead_status == 3)
			{
				$leadstatus_show = 'Contract Generated';
				$signeddateArray='';
			}
			else if($lead_status == 4)
			{
				$leadstatus_show = 'Contract Signed';
				$lead_id = $leadidArray[$i];
				$fetch_calls= $mysqliportal->prepare("SELECT date_format(enp.signedtime,'%m/%d/%Y %H:%i:%s') as signedtime FROM `envelope` AS enp,leads as L WHERE L.lid=enp.leadID and enp.leadID = $lead_id and enp.Envelope_Status='completed'");
				$fetch_calls->execute();
				$fetch_calls->bind_result($signedtimedate);
				while($fetch_calls->fetch())
				{
					//array_push($signeddateArray,$signedtimedate);
					$signeddateArray=$signedtimedate;
				   
				}
			}
			else if($lead_status == 5)
			{
				$leadstatus_show = 'Contract Declined';
				$signeddateArray='';
			}
			$emailcheck = $emailidArray[$i];
			
			$excelexp =$xcelaccountnoArray[$i];
			if($excelexp=='0' || $excelexp==''){
				$excelexp='';
			}
			$call_center = $callcenterArray[$i];
			if($call_center=='10007')
			{
				$callcenterShow = 'Web';
			}
			$lastcall_center = $lastcallcenterArray[$i];
			if($lastcall_center=='10007')
			{
				$lastcallcenterShow = 'Web';
			}
			else if($lastcall_center=='10001'){
				$lastcallcenterShow = 'The Results';
			}
			else if($lastcall_center=='10002'){
				$lastcallcenterShow = 'PCCW';
			}
			else if($lastcall_center=='10003'){
				$lastcallcenterShow = 'Internal Sales Team';
			}
			$leadsource =$leadsourceArray[$i];
			if($leadsource==1)
			{
				$leadsourceShow = 'Direct Outreach';
			}
			else if($leadsource==2)
			{
				$leadsourceShow = 'Partner Generated';
			}
			else if($leadsource==3)
			{
				$leadsourceShow = 'Event';
			}
			else if($leadsource==4)
			{
				$leadsourceShow = 'Media';
			}
			else if($leadsource==5)
			{
				$leadsourceShow = 'Digital Promotion';
			}
			else if($leadsource==6)
			{
				$leadsourceShow = 'Social Media - Organic';
			}
			else if($leadsource==7)
			{
				$leadsourceShow = 'Referral - Word of Mouth';
			}
			else if($leadsource==8)
			{
				$leadsourceShow = 'Web Search';
			}
			else if($leadsource==9)
			{
				$leadsourceShow = 'External Website';
			}
			else if($leadsource==10)
			{
				$leadsourceShow = 'Out Of Home';
			}
			else if($leadsource==11)
			{
				$leadsourceShow = 'Garden - Drive By';
			}
			else if($leadsource==12)
			{
				$leadsourceShow = "Competitor's Marketing  Material";
			}
			else if($leadsource==13)
			{
				$leadsourceShow = 'UNKNOWN';
			}
			else if($leadsource==14)
			{
				$leadsourceShow = "Other's";
			}
			
			$lastmodified_date =$lastmodifieddateArray[$i];
			/* if($lastmodified_date == '')
			{
				$lastmodified_date = date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
			} */
			$lastmodified_salesperson =$lastmodifiedsalespersonArray[$i];
			if($lastmodified_salesperson == '' || $lastmodified_salesperson == 'Infinite Linked')
			{
				$lastmodified_salesperson = 'Web';
			}
		
			$tablereport .= '<tr>
			<td>'.date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i])).'</td>
			 <td>Web</td>
			  <td>'.$lastmodified_date.'</td>
			  <td>'.$lastmodifiedsalesrepidArray[$i].'</td>
			 <td>'.$lastmodified_salesperson.'</td>
			 <td>'.$lastloginidArray[$i].'</td>
			 <td>'.$lastcallcenterShow.'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			 <td>'.$excelexp.'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$city_show.'</td>
			 <td>'.$state_show.'</td>
			 <td>'.$country_show.'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.$leadstatus_show.'</td>
			 <td>'.$signeddateArray.'</td>
			</tr>';
			 //$v--; 
			 
			 
			
		}	
		$tablereport .= '</tbody>
				</table>';
		echo $tablereport; 
		//$tablereport = '<table>';
		
	}	
	function reports_webenroll_twoweeks($mysqliportal)
	{
		$fromdate = date('Y-m-d');
        $weekdate = date('Y-m-d', strtotime("-2 week")); //2 week ago
		$partneridArray =array();
		$fetch_partnerid = $mysqliportal->prepare("select distinct partner_id from manage_salesrep where status=1");
		$fetch_partnerid->execute();
		$fetch_partnerid->bind_result($partner_id);
		while($fetch_partnerid->fetch())
		{
			array_push($partneridArray, $partner_id);
		}
		$fetch_partnerid->close();
		
		
		$webRepidArray =array();
		for($i=0; $i<count($partneridArray); $i++)
		{
			
			$partnerid = $partneridArray[$i];
			
			if($partnerid == '10007')
			{
				$fetch_pccwrep = $mysqliportal->prepare("select id as webrepid from manage_salesrep where partner_id=$partnerid");
				$fetch_pccwrep->execute();
				$fetch_pccwrep->bind_result($webrepid);
				while($fetch_pccwrep->fetch())
				{
					array_push($webRepidArray, $webrepid);
				}
				$fetch_pccwrep->close();
				
			}
			
		}
		
		$web_repids = implode(',',$webRepidArray); 
		
		//$sourceArray = array();
		$leadidArray = array();
		$salesrepidArray = array();
		$salesrepnameArray = array();
		$loginidArray = array();
		$callcenterArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$xcelaccountnoArray = array();
		$campaigntypefromArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		$leadsourceArray = array();
		$lastmodifieddateArray = array();
		$lastmodifiedsalespersonArray = array();
		$lastmodifiedsalesrepidArray = array();
		$lastcallcenterArray = array();
		$lastloginidArray = array();
		
		
		$fetch_webenroll = $mysqliportal->prepare("select ld.lid as leadid,ld.salesrep_id as salesrepid,ms.email as loginid,ms.name as salesrepname,ms.partner_id as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus,lf.leadsource as leadsource from leadinfo lf,leads ld,manage_salesrep ms where lf.leadid=ld.lid and ld.salesrep_id in ($web_repids) and ld.salesrep_id=ms.id and DATE_FORMAT(ld.createdlead_time,'%Y-%m-%d') BETWEEN '$weekdate' and '$fromdate' order by ld.lid DESC");
		$fetch_webenroll->execute();
		$fetch_webenroll->bind_result($leadid,$salesrepid,$loginid,$salesrepname,$callcenter,$firstname,$lastname,$emailid,$address1,$xcelaccountno,$campaigntypefrom,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus,$leadsource);
		$fetch_webenroll->store_result();
		$rows = $fetch_webenroll->num_rows;
		
		//$tablereport = '<table>';
		 while($fetch_webenroll->fetch())
		{
			//array_push($sourceArray,$source);
			array_push($leadidArray,$leadid);
			array_push($salesrepidArray,$salesrepid);
			array_push($salesrepnameArray,$salesrepname);
			array_push($loginidArray,$loginid);
			array_push($callcenterArray,$callcenter);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($xcelaccountnoArray,$xcelaccountno);
			array_push($campaigntypefromArray,$campaigntypefrom);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			array_push($leadsourceArray,$leadsource);
			
			
			
		}
		$fetch_webenroll->close();
		
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			
			$lead_id = $leadidArray[$i];
			$fetch_callss= $mysqliportal->prepare("SELECT * FROM `calls`WHERE lead_id = $lead_id");
			$fetch_callss->execute();
			$fetch_callss->store_result();
			$checkingrow=$fetch_callss->num_rows;
			if($checkingrow>0){
				$fetch_calls= $mysqliportal->prepare("SELECT  C.name as lastmodifiedsalesrep,date_format(L.time,'%m/%d/%Y %H:%i:%S') as lastmodifieddate,L.call_centerid as repid,C.partner_id,C.email FROM `calls` AS L,manage_salesrep as C WHERE C.id=L.call_centerid  and L.lead_id = $lead_id ORDER BY time desc limit 1");
				$fetch_calls->execute();
				$fetch_calls->bind_result($lastmodifiedsalesperson,$lastmodifieddate,$lastmodifiedsalesrepid,$lastcallcentername,$lastloginid);
				while($fetch_calls->fetch())
				{
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$lastmodifiedsalesperson);
					array_push($lastmodifiedsalesrepidArray,$lastmodifiedsalesrepid);
					array_push($lastcallcenterArray,$lastcallcentername);
					array_push($lastloginidArray,$lastloginid);
					
					 
				} 
			}
			else{
				    $lastmodifieddate=date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$salesrepnameArray[$i]);
					array_push($lastmodifiedsalesrepidArray,$salesrepidArray[$i]);
					array_push($lastcallcenterArray,$callcenterArray[$i]);
					array_push($lastloginidArray,$loginidArray[$i]);
			}
			
		} 
		/* print_r($lastmodifieddateArray);
		exit; */
		$tablereport = '<table id="example" class="table table-striped table-bordered display" cellspacing="0" width="100%">  
					<thead>
						<!--<tr>
							<th style="font-size:13px;">Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Lead Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Last Modified</th>
							<th style="font-size:13px;">Last Modified Date</th>
						</tr>-->
						<tr>
							<th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Source</th>
							<th style="font-size:13px;">Updated Date</th>
							<th style="font-size:13px;">Sales Rep ID</th>
							<th style="font-size:13px;">Last Updated By</th>
							<th style="font-size:13px;">Login ID</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Excel Acc #</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Contract Status</th>
							<th style="font-size:13px;">Signed Date</th>
						</tr>
					</thead>
					
       
				<tbody>';
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			$city_sh = $cityArray[$i];
			if($city_sh=='.' || $city_sh=='....' || $city_sh=='0' || $city_sh=='00' || $city_sh=='000' || $city_sh=='0000' || $city_sh=='?')
			{
				$city_show = 'NA';
			}	
			else
			{
				$city_show = ucwords($city_sh);
			}	
			$state_sh = $stateArray[$i];	
			if($state_sh=='0')
			{
				$state_show = 'NA';
			}	
			else
			{
				$state_show = ucwords($state_sh);
			}	
			$country_sh = $countryArray[$i];	
			if($country_sh=='0')
			{
				$country_show = 'NA';
			}	
			else
			{
				$country_show = ucwords($country_sh);
			}	
			$campaigntype_from = $campaigntypefromArray[$i];
			$campaigntypefromstatus = '';
			if($campaigntype_from == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaigntype_from == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaigntype_from == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else if($campaigntype_from == 4)
			{
				$campaigntypefromstatus = '$25 Amazon.com Gift Card';
			}
			else if($campaigntype_from == 5)
			{
				$campaigntypefromstatus = '$50 Amazon.com Gift Card';
			}
			$lead_status = $leadstatusArray[$i];
			if($lead_status == 1)
			{
				$leadstatus_show = 'Lead Saved';
				$signeddateArray='';
			}
			else if($lead_status == 3)
			{
				$leadstatus_show = 'Contract Generated';
				$signeddateArray='';
			}
			else if($lead_status == 4)
			{
				$leadstatus_show = 'Contract Signed';
				$lead_id = $leadidArray[$i];
				$fetch_calls= $mysqliportal->prepare("SELECT date_format(enp.signedtime,'%m/%d/%Y %H:%i:%s') as signedtime FROM `envelope` AS enp,leads as L WHERE L.lid=enp.leadID and enp.leadID = $lead_id and enp.Envelope_Status='completed'");
				$fetch_calls->execute();
				$fetch_calls->bind_result($signedtimedate);
				while($fetch_calls->fetch())
				{
					//array_push($signeddateArray,$signedtimedate);
					$signeddateArray=$signedtimedate;
				   
				}
			}
			else if($lead_status == 5)
			{
				$leadstatus_show = 'Contract Declined';
				$signeddateArray='';
			}
			$emailcheck = $emailidArray[$i];
			
			$excelexp =$xcelaccountnoArray[$i];
			if($excelexp=='0' || $excelexp==''){
				$excelexp='';
			}
			$call_center = $callcenterArray[$i];
			if($call_center=='10007')
			{
				$callcenterShow = 'Web';
			}
			$lastcall_center = $lastcallcenterArray[$i];
			if($lastcall_center=='10007')
			{
				$lastcallcenterShow = 'Web';
			}
			else if($lastcall_center=='10001'){
				$lastcallcenterShow = 'The Results';
			}
			else if($lastcall_center=='10002'){
				$lastcallcenterShow = 'PCCW';
			}
			else if($lastcall_center=='10003'){
				$lastcallcenterShow = 'Internal Sales Team';
			}
			$leadsource =$leadsourceArray[$i];
			if($leadsource==1)
			{
				$leadsourceShow = 'Direct Outreach';
			}
			else if($leadsource==2)
			{
				$leadsourceShow = 'Partner Generated';
			}
			else if($leadsource==3)
			{
				$leadsourceShow = 'Event';
			}
			else if($leadsource==4)
			{
				$leadsourceShow = 'Media';
			}
			else if($leadsource==5)
			{
				$leadsourceShow = 'Digital Promotion';
			}
			else if($leadsource==6)
			{
				$leadsourceShow = 'Social Media - Organic';
			}
			else if($leadsource==7)
			{
				$leadsourceShow = 'Referral - Word of Mouth';
			}
			else if($leadsource==8)
			{
				$leadsourceShow = 'Web Search';
			}
			else if($leadsource==9)
			{
				$leadsourceShow = 'External Website';
			}
			else if($leadsource==10)
			{
				$leadsourceShow = 'Out Of Home';
			}
			else if($leadsource==11)
			{
				$leadsourceShow = 'Garden - Drive By';
			}
			else if($leadsource==12)
			{
				$leadsourceShow = "Competitor's Marketing  Material";
			}
			else if($leadsource==13)
			{
				$leadsourceShow = 'UNKNOWN';
			}
			else if($leadsource==14)
			{
				$leadsourceShow = "Other's";
			}
			
			$lastmodified_date =$lastmodifieddateArray[$i];
			/* if($lastmodified_date == '')
			{
				$lastmodified_date = date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
			} */
			$lastmodified_salesperson =$lastmodifiedsalespersonArray[$i];
			if($lastmodified_salesperson == '' || $lastmodified_salesperson == 'Infinite Linked')
			{
				$lastmodified_salesperson = 'Web';
			}
		
			$tablereport .= '<tr>
			<td>'.date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i])).'</td>
			 <td>Web</td>
			  <td>'.$lastmodified_date.'</td>
			  <td>'.$lastmodifiedsalesrepidArray[$i].'</td>
			 <td>'.$lastmodified_salesperson.'</td>
			 <td>'.$lastloginidArray[$i].'</td>
			 <td>'.$lastcallcenterShow.'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			 <td>'.$excelexp.'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$city_show.'</td>
			 <td>'.$state_show.'</td>
			 <td>'.$country_show.'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.$leadstatus_show.'</td>
			 <td>'.$signeddateArray.'</td>
			</tr>';
			 //$v--; 
			 
			 
			
		}	
		$tablereport .= '</tbody>
				</table>';
		echo $tablereport; 
		//$tablereport = '<table>';
		
	}	
	
	function reports_all($mysqliportal)
	{
		$partneridArray =array();
		$fetch_partnerid = $mysqliportal->prepare("select distinct partner_id from manage_salesrep ");
		$fetch_partnerid->execute();
		$fetch_partnerid->bind_result($partner_id);
		while($fetch_partnerid->fetch())
		{
			array_push($partneridArray, $partner_id);
		}
		$fetch_partnerid->close();
		
		$resultsRepidArray =array();
		$pccwRepidArray =array();
		$sunshareSalesRepidArray =array();
		$webRepidArray =array();
		$allRepidArray =array();
		for($i=0; $i<count($partneridArray); $i++)
		{
			
			$partnerid = $partneridArray[$i];
			
			if($partnerid == '10001' || $partnerid == '10002' || $partnerid == '10003' || $partnerid == '10007')
			{
				$fetch_pccwrep = $mysqliportal->prepare("select id as allrepid from manage_salesrep where partner_id=$partnerid");
				$fetch_pccwrep->execute();
				$fetch_pccwrep->bind_result($allrepid);
				while($fetch_pccwrep->fetch())
				{
					array_push($allRepidArray, $allrepid);
				}
				$fetch_pccwrep->close();
				
			}
			
		}
		//echo "<pre>";
		//print_r($callcenterRepidArray);
		 $all_repids = implode(',',$allRepidArray); 
		
		/* echo $all_repids;
		exit; */
		$leadidArray = array();
		$salesrepidArray = array();
		$salesrepnameArray = array();
		$loginidArray = array();
		$callcenterArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$xcelaccountnoArray = array();
		$campaigntypefromArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		$leadsourceArray = array();
		$lastmodifieddateArray = array();
		$lastmodifiedsalespersonArray = array();
		$lastmodifiedsalesrepidArray = array();
		$lastmodifiedcallcenterArray = array();
		
		$fetch_salesid = $mysqliportal->prepare("select ld.lid as leadid,ld.salesrep_id as salesrepid,ms.email as loginid,ms.name as salesrepname,ms.partner_id as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus,lf.leadsource as leadsource from leadinfo lf,leads ld,manage_salesrep ms where lf.leadid=ld.lid and ld.salesrep_id in ($all_repids) and ld.salesrep_id=ms.id and ld.createdlead_time!='0000-00-00 00:00:00'order by ld.lid DESC ");
		$fetch_salesid->execute();
		$fetch_salesid->bind_result($leadid,$salesrepid,$loginid,$salesrepname,$callcenter,$firstname,$lastname,$emailid,$address1,$xcelaccountno,$campaigntypefrom,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus,$leadsource);
		$fetch_salesid->store_result();
		$rows = $fetch_salesid->num_rows;
		 while($fetch_salesid->fetch())
		{
			array_push($leadidArray,$leadid);
			array_push($salesrepidArray,$salesrepid);
			array_push($salesrepnameArray,$salesrepname);
			array_push($loginidArray,$loginid);
			array_push($callcenterArray,$callcenter);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($xcelaccountnoArray,$xcelaccountno);
			array_push($campaigntypefromArray,$campaigntypefrom);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			array_push($leadsourceArray,$leadsource);
			//array_push($lastmodifieddateArray,$lastmodifieddate);
			//array_push($lastmodifiessalespersonArray,$lastmodifiessalesperson);
			
			
		}
		$fetch_salesid->close();
		
	
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			
			$lead_id = $leadidArray[$i];
			$fetch_callss= $mysqliportal->prepare("SELECT * FROM `calls`WHERE lead_id = $lead_id");
			$fetch_callss->execute();
			$fetch_callss->store_result();
			$checkingrow=$fetch_callss->num_rows;
			if($checkingrow>0){
				$fetch_calls= $mysqliportal->prepare("SELECT  C.name as lastmodifiedsalesrep,date_format(L.time,'%m/%d/%Y %H:%i:%S') as lastmodifieddate,L.call_centerid as repid,C.partner_id FROM `calls` AS L,manage_salesrep as C WHERE C.id=L.call_centerid  and L.lead_id = $lead_id ORDER BY time desc limit 1");
				$fetch_calls->execute();
				$fetch_calls->bind_result($lastmodifiedsalesperson,$lastmodifieddate,$lastmodifiedsalesrepid,$lastmodifiedcallcenter);
				while($fetch_calls->fetch())
				{
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$lastmodifiedsalesperson);
					 array_push($lastmodifiedsalesrepidArray,$lastmodifiedsalesrepid);
					 array_push($lastmodifiedcallcenterArray,$lastmodifiedcallcenter);
				} 
			}
			else{
				   $lastmodifieddate=date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$salesrepnameArray[$i]);
					array_push($lastmodifiedcallcenterArray,$callcenterArray[$i]);
					
			}
			
		}
		/* print_r($lastmodifieddateArray);
		exit; */
				$tablereport1 = '<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">  
					<thead>
						<!--<tr>
							<th style="font-size:13px;">Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Lead Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Last Modified</th>
							<th style="font-size:13px;">Last Modified Date</th>
							
						</tr>-->
						<tr>
						    <th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Created Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">Updated Date</th>
							<th style="font-size:13px;">Last Updated By</th>
							<th style="font-size:13px;">Updated Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Xcel Account No</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Contract Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Envelope ID</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Referred By</th>
						</tr>
						
					</thead>
       
				<tbody>';
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			$city_sh = $cityArray[$i];
			if($city_sh=='.' || $city_sh=='....' || $city_sh=='0' || $city_sh=='00' || $city_sh=='000' || $city_sh=='0000' || $city_sh=='?')
			{
				$city_show = 'NA';
			}	
			else
			{
				$city_show = ucwords($city_sh);
			}	
			$state_sh = $stateArray[$i];	
			if($state_sh=='0')
			{
				$state_show = 'NA';
			}	
			else
			{
				$state_show = ucwords($state_sh);
			}	
			$country_sh = $countryArray[$i];	
			if($country_sh=='0')
			{
				$country_show = 'NA';
			}	
			else
			{
				$country_show = ucwords($country_sh);
			}	
			$campaigntype_from = $campaigntypefromArray[$i];
			$campaigntypefromstatus = '';
			if($campaigntype_from == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaigntype_from == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaigntype_from == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else if($campaigntype_from == 4)
			{
				$campaigntypefromstatus = '$25 Amazon.com Gift Card';
			}
			else if($campaigntype_from == 5)
			{
				$campaigntypefromstatus = '$50 Amazon.com Gift Card';
			}
			$lead_status = $leadstatusArray[$i];
			if($lead_status == 1)
			{
				$leadstatus_show = 'Lead Saved';
				$signeddateArray='';
				$envelopeid='';
			}
			else if($lead_status == 3)
			{
				$leadstatus_show = 'Contract Generated';
				$signeddateArray='';
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
			}
			else if($lead_status == 4)
			{
				$leadstatus_show = 'Contract Signed';
				$lead_id = $leadidArray[$i];
				$fetch_calls= $mysqliportal->prepare("SELECT date_format(enp.signedtime,'%m/%d/%Y %H:%i:%s') as signedtime FROM `envelope` AS enp,leads as L WHERE L.lid=enp.leadID and enp.leadID = $lead_id and enp.Envelope_Status='completed'");
				$fetch_calls->execute();
				$fetch_calls->bind_result($signedtimedate);
				while($fetch_calls->fetch())
				{
					//array_push($signeddateArray,$signedtimedate);
					$signeddateArray=$signedtimedate;
				   
				}
				$fetch_calls->close();
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
				
			}
			else if($lead_status == 5)
			{
				$leadstatus_show = 'Contract Declined';
				$signeddateArray='';
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
			}
			$emailcheck = $emailidArray[$i];
			
			$excelexp =$xcelaccountnoArray[$i];
			if($excelexp=='0' || $excelexp==''){
				$excelexp='';
			}
			$call_center = $callcenterArray[$i];
			if($call_center=='10001')
			{
				$callcenterShow = 'THE RESULTS';
			}
			else if($call_center=='10002')
			{
				$callcenterShow = 'PCCW';
			}
			else if($call_center=='10003')
			{
				$callcenterShow = 'SUNSHARE SALES';
			}
			else if($call_center=='10007')
			{
				$callcenterShow = 'WEB';
			}
			$lastcall_center = $lastmodifiedcallcenterArray[$i];
			if($lastcall_center=='10001')
			{
				$lastcall_centerShow = 'The Results';
			}
			else if($lastcall_center=='10002')
			{
				$lastcall_centerShow = 'PCCW';
			}
			else if($lastcall_center=='10003')
			{
				$lastcall_centerShow = 'Internal Sales Team';
			}
			else if($lastcall_center=='10007')
			{
				$lastcall_centerShow = 'Web';
			}
			$leadsourceShow='';
			$leadsource =$leadsourceArray[$i];
			if($leadsource==1)
			{
				$leadsourceShow = 'Direct Outreach';
			}
			else if($leadsource==2)
			{
				$leadsourceShow = 'Partner Generated';
			}
			else if($leadsource==3)
			{
				$leadsourceShow = 'Event';
			}
			else if($leadsource==4)
			{
				$leadsourceShow = 'Media';
			}
			else if($leadsource==5)
			{
				$leadsourceShow = 'Digital Promotion';
			}
			else if($leadsource==6)
			{
				$leadsourceShow = 'Social Media - Organic';
			}
			else if($leadsource==7)
			{
				$leadsourceShow = 'Referral - Word of Mouth';
			}
			else if($leadsource==8)
			{
				$leadsourceShow = 'Web Search';
			}
			else if($leadsource==9)
			{
				$leadsourceShow = 'External Website';
			}
			else if($leadsource==10)
			{
				$leadsourceShow = 'Out Of Home';
			}
			else if($leadsource==11)
			{
				$leadsourceShow = 'Garden - Drive By';
			}
			else if($leadsource==12)
			{
				$leadsourceShow = "Competitor's Marketing  Material";
			}
			else if($leadsource==13)
			{
				$leadsourceShow = 'UNKNOWN';
			}
			else if($leadsource==14)
			{
				$leadsourceShow = "Other's";
			}
			$lastmodified_date =$lastmodifieddateArray[$i];
			$lastmodified_salesperson =$lastmodifiedsalespersonArray[$i];
			
			/* if($lastmodified_date == '')
			{
				$lastmodified_date = date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
			} */
			$lastmodified_salesperson =$lastmodifiedsalespersonArray[$i];
			if($lastmodified_salesperson == '' || $lastmodified_salesperson == 'Infinite Linked')
			{
				$lastmodified_salesperson = 'Web';
			}
			$salesrepnameArrays=$salesrepnameArray[$i];
			if($salesrepnameArrays == 'Infinite Linked')
			{
				$salesrepnameArrays = 'Web';
			}
			else{
				$salesrepnameArrays = $salesrepnameArrays;
			}
			
			$referredby='';
			$fetch_referredby= $mysqliportal->prepare("SELECT generalcomments FROM `general_comments` WHERE leadid=$leadidArray[$i]");
			$fetch_referredby->execute();
			$fetch_referredby->bind_result($rreferredby);
			while($fetch_referredby->fetch())
			{
				$referredby=$rreferredby;
			   
			}
			$fetch_referredby->close();
			$tablereport1 .= '<tr>
			<td>'.date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i])).'</td>
			 <td>'.ucwords(strtolower($salesrepnameArrays)).'</td>
			 <td>'.$callcenterShow.'</td>
			 <td>'.$lastmodified_date.'</td>
			 <td>'.$lastmodified_salesperson.'</td>
			 <td>'.$lastcall_centerShow.'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			  <td>'.$excelexp.'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$city_show.'</td>
			 <td>'.$state_show.'</td>
			 <td>'.$country_show.'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.$leadstatus_show.'</td>
			 <td>'.$signeddateArray.'</td>
			 <td>'.$envelopeid.'</td>
			 <td>'.$leadsourceShow.'</td>
			 <td>'.$referredby.'</td>
			</tr>';
			 //$v--; 
			 
			unset($envelopeid); 
			
		}	
		$tablereport1 .= '</tbody>
				</table>';
		echo $tablereport1; 
		//$tablereport = '<table>';
	}
	function reports_all_twoweeks($mysqliportal)
	{
		$fromdate = date('Y-m-d');
        $weekdate = date('Y-m-d', strtotime("-2 week")); //1 week ago
		$partneridArray =array();
		$fetch_partnerid = $mysqliportal->prepare("select distinct partner_id from manage_salesrep ");
		$fetch_partnerid->execute();
		$fetch_partnerid->bind_result($partner_id);
		while($fetch_partnerid->fetch())
		{
			array_push($partneridArray, $partner_id);
		}
		$fetch_partnerid->close();
		
		$resultsRepidArray =array();
		$pccwRepidArray =array();
		$sunshareSalesRepidArray =array();
		$webRepidArray =array();
		$allRepidArray =array();
		for($i=0; $i<count($partneridArray); $i++)
		{
			
			$partnerid = $partneridArray[$i];
			
			if($partnerid == '10001' || $partnerid == '10002' || $partnerid == '10003' || $partnerid == '10007')
			{
				$fetch_pccwrep = $mysqliportal->prepare("select id as allrepid from manage_salesrep where partner_id=$partnerid");
				$fetch_pccwrep->execute();
				$fetch_pccwrep->bind_result($allrepid);
				while($fetch_pccwrep->fetch())
				{
					array_push($allRepidArray, $allrepid);
				}
				$fetch_pccwrep->close();
				
			}
			
		}
		//echo "<pre>";
		//print_r($callcenterRepidArray);
		 $all_repids = implode(',',$allRepidArray); 
		
		/* echo $all_repids;
		exit; */
		$leadidArray = array();
		$salesrepidArray = array();
		$salesrepnameArray = array();
		$loginidArray = array();
		$callcenterArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$xcelaccountnoArray = array();
		$campaigntypefromArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		$leadsourceArray = array();
		$lastmodifieddateArray = array();
		$lastmodifiedsalespersonArray = array();
		$lastmodifiedsalesrepidArray = array();
		$lastmodifiedcallcenterArray = array();
		
		$fetch_salesid = $mysqliportal->prepare("select ld.lid as leadid,ld.salesrep_id as salesrepid,ms.email as loginid,ms.name as salesrepname,ms.partner_id as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus,lf.leadsource as leadsource from leadinfo lf,leads ld,manage_salesrep ms where lf.leadid=ld.lid and ld.salesrep_id in ($all_repids) and ld.salesrep_id=ms.id and DATE_FORMAT(ld.createdlead_time,'%Y-%m-%d') BETWEEN '$weekdate' and '$fromdate' order by ld.lid DESC ");
		$fetch_salesid->execute();
		$fetch_salesid->bind_result($leadid,$salesrepid,$loginid,$salesrepname,$callcenter,$firstname,$lastname,$emailid,$address1,$xcelaccountno,$campaigntypefrom,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus,$leadsource);
		$fetch_salesid->store_result();
		$rows = $fetch_salesid->num_rows;
		 while($fetch_salesid->fetch())
		{
			array_push($leadidArray,$leadid);
			array_push($salesrepidArray,$salesrepid);
			array_push($salesrepnameArray,$salesrepname);
			array_push($loginidArray,$loginid);
			array_push($callcenterArray,$callcenter);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($xcelaccountnoArray,$xcelaccountno);
			array_push($campaigntypefromArray,$campaigntypefrom);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			array_push($leadsourceArray,$leadsource);
			//array_push($lastmodifieddateArray,$lastmodifieddate);
			//array_push($lastmodifiessalespersonArray,$lastmodifiessalesperson);
			
			
		}
		$fetch_salesid->close();
		
	
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			
			$lead_id = $leadidArray[$i];
			$fetch_callss= $mysqliportal->prepare("SELECT * FROM `calls`WHERE lead_id = $lead_id");
			$fetch_callss->execute();
			$fetch_callss->store_result();
			$checkingrow=$fetch_callss->num_rows;
			if($checkingrow>0){
				$fetch_calls= $mysqliportal->prepare("SELECT  C.name as lastmodifiedsalesrep,date_format(L.time,'%m/%d/%Y %H:%i:%S') as lastmodifieddate,L.call_centerid as repid,C.partner_id FROM `calls` AS L,manage_salesrep as C WHERE C.id=L.call_centerid  and L.lead_id = $lead_id ORDER BY time desc limit 1");
				$fetch_calls->execute();
				$fetch_calls->bind_result($lastmodifiedsalesperson,$lastmodifieddate,$lastmodifiedsalesrepid,$lastmodifiedcallcenter);
				while($fetch_calls->fetch())
				{
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$lastmodifiedsalesperson);
					 array_push($lastmodifiedsalesrepidArray,$lastmodifiedsalesrepid);
					 array_push($lastmodifiedcallcenterArray,$lastmodifiedcallcenter);
				} 
			}
			else{
				   $lastmodifieddate=date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
					array_push($lastmodifieddateArray,$lastmodifieddate);
					array_push($lastmodifiedsalespersonArray,$salesrepnameArray[$i]);
					array_push($lastmodifiedcallcenterArray,$callcenterArray[$i]);
					
			}
			
		}
		/* print_r($lastmodifieddateArray);
		exit; */
				$tablereport1 = '<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">  
					<thead>
						<!--<tr>
							<th style="font-size:13px;">Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Lead Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Last Modified</th>
							<th style="font-size:13px;">Last Modified Date</th>
							
						</tr>-->
						<tr>
						    <th style="font-size:13px;">Created Date</th>
							<th style="font-size:13px;">Created Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">Updated Date</th>
							<th style="font-size:13px;">Last Updated By</th>
							<th style="font-size:13px;">Updated Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Xcel Account No</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">County</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Contract Status</th>
							<th style="font-size:13px;">Signed Date</th>
							<th style="font-size:13px;">Envelope ID</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Referred By</th>
						</tr>
						
					</thead>
       
				<tbody>';
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			$city_sh = $cityArray[$i];
			if($city_sh=='.' || $city_sh=='....' || $city_sh=='0' || $city_sh=='00' || $city_sh=='000' || $city_sh=='0000' || $city_sh=='?')
			{
				$city_show = 'NA';
			}	
			else
			{
				$city_show = ucwords($city_sh);
			}	
			$state_sh = $stateArray[$i];	
			if($state_sh=='0')
			{
				$state_show = 'NA';
			}	
			else
			{
				$state_show = ucwords($state_sh);
			}	
			$country_sh = $countryArray[$i];	
			if($country_sh=='0')
			{
				$country_show = 'NA';
			}	
			else
			{
				$country_show = ucwords($country_sh);
			}	
			$campaigntype_from = $campaigntypefromArray[$i];
			$campaigntypefromstatus = '';
			if($campaigntype_from == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaigntype_from == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaigntype_from == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else if($campaigntype_from == 4)
			{
				$campaigntypefromstatus = '$25 Amazon.com Gift Card';
			}
			else if($campaigntype_from == 5)
			{
				$campaigntypefromstatus = '$50 Amazon.com Gift Card';
			}
			$lead_status = $leadstatusArray[$i];
			if($lead_status == 1)
			{
				$leadstatus_show = 'Lead Saved';
				$signeddateArray='';
				$envelopeid='';
			}
			else if($lead_status == 3)
			{
				$leadstatus_show = 'Contract Generated';
				$signeddateArray='';
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
			}
			else if($lead_status == 4)
			{
				$leadstatus_show = 'Contract Signed';
				$lead_id = $leadidArray[$i];
				$fetch_calls= $mysqliportal->prepare("SELECT date_format(enp.signedtime,'%m/%d/%Y %H:%i:%s') as signedtime FROM `envelope` AS enp,leads as L WHERE L.lid=enp.leadID and enp.leadID = $lead_id and enp.Envelope_Status='completed'");
				$fetch_calls->execute();
				$fetch_calls->bind_result($signedtimedate);
				while($fetch_calls->fetch())
				{
					//array_push($signeddateArray,$signedtimedate);
					$signeddateArray=$signedtimedate;
				   
				}
				$fetch_calls->close();
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
				
			}
			else if($lead_status == 5)
			{
				$leadstatus_show = 'Contract Declined';
				$signeddateArray='';
				$lead_ids = $leadidArray[$i];
				$fetch_envelpoe= $mysqliportal->prepare("SELECT Envelope_ID FROM `envelope` WHERE leadID = $lead_ids");
				$fetch_envelpoe->execute();
				$fetch_envelpoe->bind_result($envelopeids);
				while($fetch_envelpoe->fetch())
				{
					$envelopeid=$envelopeids;
				   
				}
				$fetch_envelpoe->close();
			}
			$emailcheck = $emailidArray[$i];
			
			$excelexp =$xcelaccountnoArray[$i];
			if($excelexp=='0' || $excelexp==''){
				$excelexp='';
			}
			$call_center = $callcenterArray[$i];
			if($call_center=='10001')
			{
				$callcenterShow = 'THE RESULTS';
			}
			else if($call_center=='10002')
			{
				$callcenterShow = 'PCCW';
			}
			else if($call_center=='10003')
			{
				$callcenterShow = 'SUNSHARE SALES';
			}
			else if($call_center=='10007')
			{
				$callcenterShow = 'WEB';
			}
			$lastcall_center = $lastmodifiedcallcenterArray[$i];
			if($lastcall_center=='10001')
			{
				$lastcall_centerShow = 'The Results';
			}
			else if($lastcall_center=='10002')
			{
				$lastcall_centerShow = 'PCCW';
			}
			else if($lastcall_center=='10003')
			{
				$lastcall_centerShow = 'Internal Sales Team';
			}
			else if($lastcall_center=='10007')
			{
				$lastcall_centerShow = 'Web';
			}
			$leadsourceShow='';
			$leadsource =$leadsourceArray[$i];
			if($leadsource==1)
			{
				$leadsourceShow = 'Direct Outreach';
			}
			else if($leadsource==2)
			{
				$leadsourceShow = 'Partner Generated';
			}
			else if($leadsource==3)
			{
				$leadsourceShow = 'Event';
			}
			else if($leadsource==4)
			{
				$leadsourceShow = 'Media';
			}
			else if($leadsource==5)
			{
				$leadsourceShow = 'Digital Promotion';
			}
			else if($leadsource==6)
			{
				$leadsourceShow = 'Social Media - Organic';
			}
			else if($leadsource==7)
			{
				$leadsourceShow = 'Referral - Word of Mouth';
			}
			else if($leadsource==8)
			{
				$leadsourceShow = 'Web Search';
			}
			else if($leadsource==9)
			{
				$leadsourceShow = 'External Website';
			}
			else if($leadsource==10)
			{
				$leadsourceShow = 'Out Of Home';
			}
			else if($leadsource==11)
			{
				$leadsourceShow = 'Garden - Drive By';
			}
			else if($leadsource==12)
			{
				$leadsourceShow = "Competitor's Marketing  Material";
			}
			else if($leadsource==13)
			{
				$leadsourceShow = 'UNKNOWN';
			}
			else if($leadsource==14)
			{
				$leadsourceShow = "Other's";
			}
			$lastmodified_date =$lastmodifieddateArray[$i];
			$lastmodified_salesperson =$lastmodifiedsalespersonArray[$i];
			
			/* if($lastmodified_date == '')
			{
				$lastmodified_date = date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i]));
			} */
			$lastmodified_salesperson =$lastmodifiedsalespersonArray[$i];
			if($lastmodified_salesperson == '' || $lastmodified_salesperson == 'Infinite Linked')
			{
				$lastmodified_salesperson = 'Web';
			}
			$salesrepnameArrays=$salesrepnameArray[$i];
			if($salesrepnameArrays == 'Infinite Linked')
			{
				$salesrepnameArrays = 'Web';
			}
			else{
				$salesrepnameArrays = $salesrepnameArrays;
			}
			$referredby='';
			$fetch_referredby= $mysqliportal->prepare("SELECT generalcomments FROM `general_comments` WHERE leadid=$leadidArray[$i]");
			$fetch_referredby->execute();
			$fetch_referredby->bind_result($rreferredby);
			while($fetch_referredby->fetch())
			{
				$referredby=$rreferredby;
			   
			}
			$fetch_referredby->close();
			$tablereport1 .= '<tr>
			<td>'.date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i])).'</td>
			 <td>'.ucwords(strtolower($salesrepnameArrays)).'</td>
			 <td>'.$callcenterShow.'</td>
			 <td>'.$lastmodified_date.'</td>
			 <td>'.$lastmodified_salesperson.'</td>
			 <td>'.$lastcall_centerShow.'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			  <td>'.$excelexp.'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$city_show.'</td>
			 <td>'.$state_show.'</td>
			 <td>'.$country_show.'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.$leadstatus_show.'</td>
			 <td>'.$signeddateArray.'</td>
			 <td>'.$envelopeid.'</td>
			 <td>'.$leadsourceShow.'</td>
			 <td>'.$referredby.'</td>
			</tr>';
			 //$v--; 
			 unset($envelopeid); 
			 
			
		}	
		$tablereport1 .= '</tbody>
				</table>';
		echo $tablereport1; 
		//$tablereport = '<table>';
	}
	
	
	
}
$reportValues = new Report();
//$mysqli = new mysqli("localhost","root","phpils135$","sunshare_saleshub");
//$reportValues->reports_call($mysqli); 
//$mysqliportal = new mysqli("localhost","root","SunShare@123","telesales_final");
//$reportValues->reports_call_testing($mysqliportal); 
//$reportValues->reports_all($mysqliportal); 
//$reportValues->reports_all_twoweeks($mysqliportal); 
//$reportValues->reports_webenroll_testing($mysqliportal); 
?>