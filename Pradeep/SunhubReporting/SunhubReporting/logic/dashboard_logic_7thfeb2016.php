<?php
class RecruitDashboard
{
	function interview_scheduledBy_program($mysqli)
	{
		$programArray=array();
		$yesArray=array();
		$noArray=array();
		$intSch_qry = $mysqli->prepare("SELECT  distinct `program` from recruitData");
		$intSch_qry->execute();
		$intSch_qry->bind_result($program);
		while($intSch_qry->fetch())
			{
				array_push($programArray,$program);
			}	
			$intSch_qry->close();
			for($i=0; $i<count($programArray); $i++)
			{
				$programname = $programArray[$i];
				$yes_qry = $mysqli->prepare("SELECT count(`interviewScheduled`) as interviewScheduledYes FROM `recruitData` WHERE `interviewScheduled`='Yes' and program='$programname'");
				$yes_qry->execute();
		        $yes_qry->bind_result($yes_interviewScheduled);
				while($yes_qry->fetch())
				{
					array_push($yesArray,$yes_interviewScheduled);
				}
				$yes_qry->close();
				
				$no_qry = $mysqli->prepare("SELECT count(`interviewScheduled`) as interviewScheduledNo FROM `recruitData` WHERE `interviewScheduled`='No' and program='$programname'");
				$no_qry->execute();
		        $no_qry->bind_result($no_interviewScheduled);
				while($no_qry->fetch())
				{
					array_push($noArray,$no_interviewScheduled);
				}
				$no_qry->close();
				$programs=implode(',', $programArray);
				$yesvalues=implode(',', $yesArray);	
				$novalues=implode(',', $noArray);
			 }
			$interviewScheduledValues = array($programs, $yesvalues, $novalues);
			//print_r($interviewScheduledValues);
			return $interviewScheduledValues;
				
	}
	function jobpostings($mysqli)
	{
		
		
		$jobpost_qry = $mysqli->prepare("SELECT  distinct `jobPostingSite`,count(`number`) as jobpostcount  FROM `recruitData` group by jobPostingSite");
		$jobpost_qry->execute();
		$jobpost_qry->bind_result($jobpostsite,$jobpostcount);
		while($jobpost_qry->fetch())
			{
				$recruitJobpostArray[] = array($jobpostsite=>$jobpostcount); 
			}
			$jobpost_qry->close(); 
			$result_rjobpost= call_user_func_array('array_merge',$recruitJobpostArray);
			//asort($result_rjobpost);
			//ksort($result_tmarket);
			$result_jobpostsitenames = implode(',',array_keys($result_rjobpost));
			$result_jobpostvalues = implode(',',$result_rjobpost);
			$result_jobpostings = array($result_jobpostsitenames,$result_jobpostvalues);
			//var_dump($result_jobpostings);
			return $result_jobpostings; 
	}	
	function jobtitles($mysqli)
	{
		
		$jobtitles_qry = $mysqli->prepare("SELECT  distinct `title`,count(`number`) as titlecount  FROM `recruitData` group by title");
		$jobtitles_qry->execute();
		$jobtitles_qry->bind_result($title,$titlecount);
		while($jobtitles_qry->fetch())
			{
				$recruitTitleArray[] = array($title=>$titlecount); 
			}
			$jobtitles_qry->close(); 
			$result_rtitles= call_user_func_array('array_merge',$recruitTitleArray);
			$result_titlenames = implode(',',array_keys($result_rtitles));
			$result_titlevalues = implode(',',$result_rtitles);
			$result_titles = array($result_titlenames,$result_titlevalues);
			//var_dump($result_titles); 
			return $result_titles;
		
	}
	
}	
$dashboardValues = new RecruitDashboard(); 
//$mysqli = new mysqli("localhost","root","phpils135$","eliterecruitment");    
//$dashboardValues->jobpostings($mysqli); 
//$dashboardValues ->jobtitles($mysqli);
//$dashboardValues ->interview_scheduledBy_program($mysqli);
?>