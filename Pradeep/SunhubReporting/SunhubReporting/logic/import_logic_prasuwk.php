<?php
include '../config/database.php';
include '../includes/PHPExcel/Classes/PHPExcel/IOFactory.php';
include '../includes/PHPExcel/Classes/PHPExcel.php';
error_reporting(E_ALL);
set_time_limit(0);
class Import
{
	function upload_recruit_data($filNam,$created,$mysqli)
	{
		$uploaded_dir = $_SERVER['DOCUMENT_ROOT'].'/eliteRecruit/uploadReports';
		$file_path = $uploaded_dir.'/'.$filNam;
		 if(preg_match('/.csv/', $filNam))
		{
			
			$objReader = new PHPExcel_Reader_Excel2007();
			$objReader->setReadDataOnly(true);
			$objReader = new PHPExcel_Reader_CSV();
			$objPHPExcel = $objReader->load($file_path);
			$allSheetName=$objPHPExcel->getSheetNames();
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			$highestRow = $objWorksheet->getHighestRow();
		    $highestColumn = $objWorksheet->getHighestColumn();
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);	
			 for ($column = 0; $column <= $highestColumnIndex; $column++) 
			   {
					$columnNamez[$column] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column,1)->getValue();
					//var_dump($columnNamez[$column]);
						
			   }
			    foreach($columnNamez as $c)
				{
					$columnName[]=trim($c);
					//var_dump($columnName);
				} 
				$arrayColumn = array("Number","Title","Preliminary Program","Program","Market","Status","First Name","Last Name","Email Address","Home Phone #","Cell Phone #","Other Phone #","Job Posting Site","Employee Referral","Date Applied","Date Contacted","Contacted By","Contact Notes","Comments","Interview Scheduled","Interviewing Manager","Interview Date","Interview Time","Interview Outcome","Rescheduled Date","New Hire Link Sent","Start Date","End Date","Production");
				$columnIndex = array_keys(array_intersect($columnName, $arrayColumn));
				$cellValue = array();
				$i=0;
				
				 for ($row = 2; $row <= $highestRow; $row++) 
					{
						for($column = 0; $column < count($columnIndex); $column++) 
						{
							$cellValue[$i][$column] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columnIndex[$column],$row)->getValue();
							//var_dump($cellValue[$i][$column]);
						}
						$i++;
					
				   }
					
					$uniq_pointerarray=array();					
					$duplicate_location=array();
				   for($i=0;$i<count($cellValue);$i++)
					{						
						$uniqstatus	=0;
						for($j=0; $j<count($cellValue); $j++)
						{							
							if($i != $j)
							{
								if($cellValue[$i][0] == $cellValue[$j][0])
								{
								   $uniqstatus=1;								   
																								
								}						
							}
								
						}
						if($uniqstatus == 0)
						{
							array_push($uniq_pointerarray, $i);							
						}
					}
					        $objPHPExcel = new PHPExcel();
							$objPHPExcel->setActiveSheetIndex(0);
							$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Number');
							$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Title');
							$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Preliminary Program');
							$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Program');
							$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Market');
							$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Status');
							$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'First Name');
							$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Last Name');
							$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Email Address');
							$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Home Phone #');
							$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Cell Phone #');
							$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Phone #');
							$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Job Posting Site');
							$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Employee Referral');
							$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Date Applied');
							$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Date Contacted');
							$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Contacted By');
							$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Contact Notes');
							$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Comments');
							$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Interview Scheduled');
							$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Interviewing Manager');
							$objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Interview Date');
							$objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Interview Time');
							$objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Interview Outcome');
							$objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Rescheduled Date');
							$objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'New Hire Link Sent');
							$objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Start Date');
							$objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'End Date');
							$objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'Production');
							
							$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'DUPLICATES RECORDS');
							$j=4;
					for($i=0;$i<count($cellValue);$i++)//getting duplicates
					{
						if(!in_array($i, $uniq_pointerarray))
						{
								$number_unique =$cellValue[$i][0];
								$number_uniqueArray[] = $number_unique;
								$title =$cellValue[$i][1];
								//$titleArray[] =  $title;
								$preliminary_program =$cellValue[$i][2];
								$program =$cellValue[$i][3];
								$market =$cellValue[$i][4];
								$status =$cellValue[$i][5];
								$firstname =$cellValue[$i][6];
								$lastname =$cellValue[$i][7];
								$emailaddress =$cellValue[$i][8];
								$homephone =$cellValue[$i][9];
								$cellphone =$cellValue[$i][10];
								$otherphone =$cellValue[$i][11];
								$jobPostingSite =$cellValue[$i][12];
								$employeeReferral =$cellValue[$i][13];
								$dateApplied =$cellValue[$i][14];
								$dateContacted =$cellValue[$i][15];
								$contactedBy =$cellValue[$i][16];
								$contactNotes =$cellValue[$i][17];
								$comments =$cellValue[$i][18];
								$interviewScheduled =$cellValue[$i][19];
								$interviewingManager =$cellValue[$i][20];
								$interviewDate = $cellValue[$i][21];
								$interviewTime =$cellValue[$i][22];
								$interviewOutcome =$cellValue[$i][23];
								$rescheduledDate =$cellValue[$i][24];
								$newHireLinkSent =$cellValue[$i][25];
								$startDate =$cellValue[$i][26];
								$enddate =$cellValue[$i][27];
								$production =$cellValue[$i][28];
								
								$objPHPExcel->getActiveSheet()->SetCellValue('A'.$j, $number_unique);
							    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$j, $title);
							    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$j, $preliminary_program);
								$objPHPExcel->getActiveSheet()->SetCellValue('D'.$j, $program);
								$objPHPExcel->getActiveSheet()->SetCellValue('E'.$j, $market);
								$objPHPExcel->getActiveSheet()->SetCellValue('F'.$j, $status);
								$objPHPExcel->getActiveSheet()->SetCellValue('G'.$j, $firstname);
								$objPHPExcel->getActiveSheet()->SetCellValue('H'.$j, $lastname);
								
							    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$j, $emailaddress);
							    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$j, $homephone);
								$objPHPExcel->getActiveSheet()->SetCellValue('K'.$j, $cellphone );
								$objPHPExcel->getActiveSheet()->SetCellValue('L'.$j, $otherphone );
								
								$objPHPExcel->getActiveSheet()->SetCellValue('M'.$j, $jobPostingSite);
								$objPHPExcel->getActiveSheet()->SetCellValue('N'.$j, $employeeReferral);
								$objPHPExcel->getActiveSheet()->SetCellValue('O'.$j, $dateApplied);
								$objPHPExcel->getActiveSheet()->SetCellValue('P'.$j, $dateContacted);
								$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$j, $contactedBy);
								$objPHPExcel->getActiveSheet()->SetCellValue('R'.$j, $contactNotes);
								$objPHPExcel->getActiveSheet()->SetCellValue('S'.$j, $comments);
								$objPHPExcel->getActiveSheet()->SetCellValue('T'.$j, $interviewScheduled);
								$objPHPExcel->getActiveSheet()->SetCellValue('U'.$j, $interviewingManager);
								$objPHPExcel->getActiveSheet()->SetCellValue('V'.$j, $interviewDate);
								$objPHPExcel->getActiveSheet()->SetCellValue('W'.$j, $interviewTime);
								$objPHPExcel->getActiveSheet()->SetCellValue('X'.$j, $interviewOutcome);
								$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$j, $rescheduledDate);
								$objPHPExcel->getActiveSheet()->SetCellValue('Z'.$j, $newHireLinkSent);
								$objPHPExcel->getActiveSheet()->SetCellValue('AA'.$j, $startDate);
								$objPHPExcel->getActiveSheet()->SetCellValue('AB'.$j, $enddate);
								$objPHPExcel->getActiveSheet()->SetCellValue('AC'.$j, $production);
							 $j++;
								
								
					    }
					}
					$k = $j+1;
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$k, 'UNIQUE ERROR RECORDS');
					$j = $k+1;
					$dbinsertedvalue=0;
					
					$al_number_unique  = array();
					$al_titleArray  = array();
				   for($i=0;$i<count($cellValue);$i++)//unique records
					{
						if(in_array($i, $uniq_pointerarray))
						{
								
								$number_unique =$cellValue[$i][0];
								//$number_uniqueArray[] = $number_unique;
								$title =$cellValue[$i][1];
								//$titleArray[] =  $title;
								$preliminary_program =$cellValue[$i][2];
								$program =$cellValue[$i][3];
								$market =$cellValue[$i][4];
								$status =$cellValue[$i][5];
								$firstname =$cellValue[$i][6];
								$lastname =$cellValue[$i][7];
								$emailaddress =$cellValue[$i][8];
								$homephone =$cellValue[$i][9];
								$cellphone =$cellValue[$i][10];
								$otherphone =$cellValue[$i][11];
								$jobPostingSite =$cellValue[$i][12];
								$employeeReferral =$cellValue[$i][13];
								//$dateApplied =$cellValue[$i][14];
								$dateApplied = date('Y-m-d',strtotime($cellValue[$i][14]));
								//$dateContacted =$cellValue[$i][15];
								$dateContacted = date('Y-m-d',strtotime($cellValue[$i][15]));
								$contactedBy =$cellValue[$i][16];
								$contactNotes =$cellValue[$i][17];
								$comments =$cellValue[$i][18];
								$interviewScheduled =$cellValue[$i][19];
								$interviewingManager =$cellValue[$i][20];
								//$interviewDate =$cellValue[$i][21];
								$interviewDate = $cellValue[$i][21];
								
								if($interviewDate=='')
								{
									$interviewDate = '';
								}
								else
								{
									$interviewDate = date('Y-m-d',strtotime($cellValue[$i][21]));
								}
								$interviewTime =$cellValue[$i][22];
								$interviewOutcome =$cellValue[$i][23];
								$rescheduledDate =$cellValue[$i][24];
								if($rescheduledDate=='')
								{
									$rescheduledDate = '';
								}
								else
								{
								  $rescheduledDate = date('Y-m-d',strtotime($cellValue[$i][24]));
								}
								$newHireLinkSent =$cellValue[$i][25];
								
								if($newHireLinkSent=='')
								{
									$newHireLinkSent = '';
								}
								else
								{
								  $newHireLinkSent = date('Y-m-d',strtotime($cellValue[$i][25]));
								}
								$startDate =$cellValue[$i][26];
								if($startDate=='')
								{
									$startDate = '';
								}
								else
								{
									 $startDate = date('Y-m-d',strtotime($cellValue[$i][26]));
								}
								
								$enddate =$cellValue[$i][27];
								if($enddate=='')
								{
									$enddate = '';
								}
								else
								{
									 $enddate = date('Y-m-d',strtotime($cellValue[$i][27]));
								}
								$production =$cellValue[$i][28];
								$errorlog_id = 0;
								$createdDate = date('Y-m-d H:i:s');
								
								//if(($number_unique!=0 && filter_var($emailaddress, FILTER_VALIDATE_EMAIL) && $number_unique!='' && preg_match("/^[1-9][0-9]*$/",$number_unique]) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $homephone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $cellphone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $otherphone)))
								if($number_unique!=0 && filter_var($emailaddress, FILTER_VALIDATE_EMAIL) && $number_unique!='' && 
							    preg_match("/^[1-9][0-9]*$/",$number_unique) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $homephone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $cellphone) && preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $otherphone))
								{
									 $select_number = $mysqli->prepare("select id  from recruitData where number=?");
									 $select_number->bind_param("s",$number_unique);
										$select_number->execute();
										$select_number->store_result();
										$select_number->bind_result($insid);
										$select_number->fetch();
										$rowcount = $select_number->num_rows;
										if($rowcount!=0)
										{
											
											array_push($al_number_unique,$number_unique);
											array_push($al_titleArray,$title);
											
											/*$stmt_update = $mysqli->prepare("UPDATE `recruitData_pra` SET `title`=?,`preliminary_program`=?,`program`=?,`market`=?,`status`=?,`firstname`=?,`lastname`=?,`email_address`=?,`homephone`=?,`cellphone`=?,`otherphone`=?,`jobPostingSite`=?,`employee_referral`=?,`date_applied`=?,`date_contacted`=?,`contactedby`=?,`contactnotes`=?,`comments`=?,`interviewScheduled`=?,`interviewingManager`=?,`interview_date`=?,`interview_time`=?,`interview_outcome`=?,`rescheduled_date`=?,`newHireLinkSent`=?,`startDate`=?,`endDate`=?,`production`=?,`errorlog_id`=?,`createdDate`=? WHERE id=$insid");
											$stmt_update->bind_param('ssssssssssssssssssssssssssssss',trim($title),trim($preliminary_program),trim($program),trim($market),trim($status),trim($firstname),trim($lastname),trim($emailaddress),trim($homephone),trim($cellphone),trim($otherphone),trim($jobPostingSite),trim($employeeReferral),trim($dateApplied),trim($dateContacted),trim($contactedBy),trim($contactNotes),trim($comments),trim($interviewScheduled),trim($interviewingManager),trim($interviewDate),trim($interviewTime),trim($interviewOutcome),trim($rescheduledDate),trim($newHireLinkSent),trim($startDate),trim($enddate),trim($production),$errorlog_id,trim($createdDate));
											$stmt_update->execute();
											$stmt_update->close();*/
											
											
										} 
										else
										{
											$fname = $filNam;
											 $stmt_insert = $mysqli->prepare("INSERT INTO `recruitData`(`number`, `title`, `preliminary_program`, `program`, `market`, `status`, `firstname`, `lastname`, `email_address`, `homephone`, `cellphone`, `otherphone`, `jobPostingSite`, `employee_referral`, `date_applied`, `date_contacted`, `contactedby`, `contactnotes`, `comments`, `interviewScheduled`, `interviewingManager`, `interview_date`, `interview_time`, `interview_outcome`, `rescheduled_date`, `newHireLinkSent`, `startDate`, `endDate`, `production`,`errorlog_id`, `createdDate`,`original_file`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
											$stmt_insert->bind_param('issssssssssssssssssssssssssssiss',trim($number_unique),trim($title),trim($preliminary_program),trim($program),trim($market),trim($status),trim($firstname),trim($lastname),trim($emailaddress),trim($homephone),trim($cellphone),trim($otherphone),trim($jobPostingSite),trim($employeeReferral),trim($dateApplied),trim($dateContacted),trim($contactedBy),trim($contactNotes),trim($comments),trim($interviewScheduled),trim($interviewingManager),trim($interviewDate),trim($interviewTime),trim($interviewOutcome),trim($rescheduledDate),trim($newHireLinkSent),trim($startDate),trim($enddate),trim($production),$errorlog_id,trim($createdDate),$fname); 
											$stmt_insert->execute();
											$stmt_insert->close(); 
											//$dbinsertedvalue++;
										}
										
									$dbinsertedvalue++;
								}
								else
								{
									 if (!preg_match("/^[1-9][0-9]*$/",$number_unique)) {
								    $numbershow = "Number Format is not valid"; 
									}
									else{
										$numbershow = $number_unique;
									}
									if (!filter_var($emailaddress, FILTER_VALIDATE_EMAIL)) {
									  $emailshow = "Invalid email format"; 
									  
									}
									else{
										$emailshow = $emailaddress; 
									}
									if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $homephone)) {
									
										$homephoneshow = "Homephone Format is not valid";
									}
									else{ 
										$homephoneshow =  $homephone;
										//echo $homephone;
									}
									if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $cellphone)) {
									
										$cellphoneshow = "Cellphone Format is not valid";
									}
									else{
										$cellphoneshow =  $cellphone;
										//echo $homephone;
									}
									if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $otherphone)) {
									
										$otherphoneshow = "Otherphone Format is not valid";
									}
									else{
										$otherphoneshow =  $otherphone;
									//echo $homephone;
										}
									 $objPHPExcel->getActiveSheet()->SetCellValue('A'.$j, $numbershow);
									$objPHPExcel->getActiveSheet()->SetCellValue('B'.$j, $title);
									$objPHPExcel->getActiveSheet()->SetCellValue('C'.$j, $preliminary_program);
									$objPHPExcel->getActiveSheet()->SetCellValue('D'.$j, $program);
									$objPHPExcel->getActiveSheet()->SetCellValue('E'.$j, $market);
									$objPHPExcel->getActiveSheet()->SetCellValue('F'.$j, $status);
									$objPHPExcel->getActiveSheet()->SetCellValue('G'.$j, $firstname);
									$objPHPExcel->getActiveSheet()->SetCellValue('H'.$j, $lastname); 
									
									 $objPHPExcel->getActiveSheet()->SetCellValue('I'.$j, $emailshow);
									$objPHPExcel->getActiveSheet()->SetCellValue('J'.$j, $homephoneshow);
									$objPHPExcel->getActiveSheet()->SetCellValue('K'.$j, $cellphoneshow );
									$objPHPExcel->getActiveSheet()->SetCellValue('L'.$j, $otherphoneshow ); 
									
									$objPHPExcel->getActiveSheet()->SetCellValue('M'.$j, $jobPostingSite );
									$objPHPExcel->getActiveSheet()->SetCellValue('N'.$j, $employeeReferral);
									$objPHPExcel->getActiveSheet()->SetCellValue('O'.$j, $dateApplied);
									$objPHPExcel->getActiveSheet()->SetCellValue('P'.$j, $dateContacted);
									$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$j, $contactedBy);
									$objPHPExcel->getActiveSheet()->SetCellValue('R'.$j, $contactNotes);
									$objPHPExcel->getActiveSheet()->SetCellValue('S'.$j, $comments);
									$objPHPExcel->getActiveSheet()->SetCellValue('T'.$j, $interviewScheduled);
									$objPHPExcel->getActiveSheet()->SetCellValue('U'.$j, $interviewingManager);
									$objPHPExcel->getActiveSheet()->SetCellValue('V'.$j, $interviewDate);
									$objPHPExcel->getActiveSheet()->SetCellValue('W'.$j, $interviewTime);
									$objPHPExcel->getActiveSheet()->SetCellValue('X'.$j, $interviewOutcome);
									$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$j, $rescheduledDate);
									$objPHPExcel->getActiveSheet()->SetCellValue('Z'.$j, $newHireLinkSent);
									$objPHPExcel->getActiveSheet()->SetCellValue('AA'.$j, $startDate);
									$objPHPExcel->getActiveSheet()->SetCellValue('AB'.$j, $enddate);
									$objPHPExcel->getActiveSheet()->SetCellValue('AC'.$j, $production);
								   $j++;
									
									
									
								}	
								
							
					    }
					} 
					//print_r($al_number_unique);
					//print_r($al_titleArray);
					$k = $j+1;
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$k, 'Existed RECORDS');
					$j = $k+1;	
							 for($p=0; $p<count($al_number_unique); $p++)
							{ 
						      $objPHPExcel->getActiveSheet()->SetCellValue('A'.$j, $al_number_unique[$p]);
								//$number_unique1 =$al_number_unique[$p];
								/* foreach($al_number_unique as $key=>$value)
								{
									echo $value;
								} */
								$j++;
							}
							
						
					 
					echo 'boops'.count($cellValue).'<br>';
					
					echo $dbinsertedvalue;
					//exit;
					if(count($cellValue) == $dbinsertedvalue)
					{
						//echo "<div style='aign:center;font-size:20px;font-color:green'><b>successfully inserted</b></div>";
						echo '<div class="alert alert-success" align="center" id="partial">
						  <strong> Successfully Imported. </strong>
						</div>';
					}
					
					else
					{
						//echo "partially inserted";
						//echo "<div style='aign:center;font-size:20px;font-color:red'><b>partially inserted</b></div>";
						
						$timestampdate = date("Y-m-d H_i_s");
						$fileName = 'ErroLog'.'_'.$timestampdate.'.csv';
						//$fpath = $_SERVER['DOCUMENT_ROOT'].'/eliteRecruit/errorlogFiles/'.$fileName;
						$error_dir = 'http://123.176.43.85'.'/eliteRecruit/errorlogFiles';
		                $errorfile_path = $error_dir.'/'.$fileName;
						 $stmt_insert = $mysqli->prepare("INSERT INTO `errorlog_details`(`login_id`,`errorlog_file`,`original_file`,`errorlog_filePath`) VALUES (1,'$fileName','$filNam','$errorfile_path')");
						//$stmt_insert->bind_param('is',1,trim($fileName)); 
						$stmt_insert->execute();
						$stmt_insert->close();
						
								
						$objPHPExcel->getActiveSheet()->setTitle('Sheet1');
						//$objPHPExcel->getSheetByName('Sheet1')->getStyle("A1")->getFont()->getBold();
						
						 //$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
						 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
						//header('Content-Type: application/vnd.ms-excel');
						//header('Content-Disposition: attachment;filename="' . $fileName . '"');
						//header('Cache-Control: max-age=0');
						//ob_clean(); 
						$downloadL = str_replace( __FILE__,$_SERVER['DOCUMENT_ROOT'].'/eliteRecruit/errorlogFiles/'.$fileName, __FILE__);
						 $objWriter->save($downloadL); 
						//exit; 
						//echo '<div class="alert alert-danger" align="center"><strong>Partially Imported. </strong> Please find the below error log file path.<a href="'.$errorfile_path.'"><br/>'.$errorfile_path.'</a></div>';
						echo '<div class="alert alert-danger" align="center" id="partial"><strong>Partially Imported. </strong><br/> Please <a href="'.$errorfile_path.'">Click here</a> for error log file</div>';						
						exit;
						
					}
			
		}
		/* else
		{
			echo "no";
		} */			
	
	}
}
$importValues	= new Import();
//$filNam = "template (1)_2016-01-27 19_56_46.csv";
//$filNam = "template (1)_2016-01-27 19_58_12.csv";
//$filNam = "template (1)_2016-01-27 19_58_12_2.csv";
//$created = "2016-01-27";
//$importValues->upload_recruitdata($filNam,$created,$mysqli);
?>