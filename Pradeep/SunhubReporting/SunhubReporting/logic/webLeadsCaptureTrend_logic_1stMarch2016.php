<?php
//include '../config/database.php';
//$month = "Jan 2016";

class SunsharehubDashboard
{
	function totalleadsCaptureTrend($mysqli)
	{
		$list=array();


		$month = "01";
		$year = "2016";

		//$start_date = "01-".$month."-".$year;
		$start_date = $year."-".$month."-01";
		$start_time = strtotime($start_date);

		$end_time = strtotime("+1 month", $start_time);

		for($i=$start_time; $i<$end_time; $i+=86400)
		{
		   //$list[] = date('Y-m-d-D', $i);
		   $list[] = date('Y-m-d', $i);
		}
		/*  echo "<pre>";
		print_r($list);
		echo "</pre>";  */
		$datesArray = array();
		$newletterArray = array();
		$enjoy50Array = array();
		$enjoy200Array = array();
		$enjoysolarArray = array();
		$enjoyArray = array();
		$giftArray = array();
		$communitysolarArray = array();
		$digital_1_Array = array();
		$digital_2_Array = array();
		$digital_3_Array = array();
		$digital_4_Array = array();
		$digital_5_Array = array();
		$digital_6_Array = array();
		$wowArray = array();
		$facebookArray = array();
		$twitterArray = array();
		$radioArray = array();
		  for($i=0 ; $i<count($list); $i++)
		{
			
			$date_newsletter = $list[$i];
			$dates_show = date('d', strtotime($list[$i]));
			array_push($datesArray,$dates_show);
			$fetch_newsletter = $mysqli->prepare("SELECT count(*) as newslettercount FROM `newsletter` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_newsletter->execute();
			$fetch_newsletter->bind_result($newslettercount);
			while($fetch_newsletter->fetch())
			{
				array_push($newletterArray,$newslettercount);
			}
			$fetch_newsletter->close();
			
			$fetch_enjoy50 = $mysqli->prepare("SELECT count(*) as enjoy50count FROM `enjoy50` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_enjoy50->execute();
			$fetch_enjoy50->bind_result($enjoy50count);
			while($fetch_enjoy50->fetch())
			{
				array_push($enjoy50Array,$enjoy50count);
			}
			$fetch_enjoy50->close();
			
			$fetch_enjoy200 = $mysqli->prepare("SELECT count(*) as enjoy200count FROM `enjoy200` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_enjoy200->execute();
			$fetch_enjoy200->bind_result($enjoy200count);
			while($fetch_enjoy200->fetch())
			{
				array_push($enjoy200Array,$enjoy200count);
			}
			$fetch_enjoy200->close();
			
			$fetch_enjoysolar = $mysqli->prepare("SELECT count(*) as enjoysolarcount FROM `enjoysolar` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_enjoysolar->execute();
			$fetch_enjoysolar->bind_result($enjoysolarcount);
			while($fetch_enjoysolar->fetch())
			{
				array_push($enjoysolarArray,$enjoysolarcount);
			}
			$fetch_enjoysolar->close();
			
			$fetch_enjoy = $mysqli->prepare("SELECT count(*) as enjoycount FROM `enoy` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_enjoy->execute();
			$fetch_enjoy->bind_result($enjoycount);
			while($fetch_enjoy->fetch())
			{
				array_push($enjoyArray,$enjoycount);
			}
			$fetch_enjoy->close();
			
			$fetch_gift = $mysqli->prepare("SELECT count(*) as giftcount FROM `gift` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_gift->execute();
			$fetch_gift->bind_result($giftcount);
			while($fetch_gift->fetch())
			{
				array_push($giftArray,$giftcount);
			}
			$fetch_gift->close();
			
			//community solar
			 $fetch_communitysolar = $mysqli->prepare("SELECT count(*) as communitysolarcount FROM `communitysolar` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_communitysolar->execute();
			$fetch_communitysolar->bind_result($communitysolarcount);
			while($fetch_communitysolar->fetch())
			{
				array_push($communitysolarArray,$communitysolarcount);
			}
			$fetch_communitysolar->close(); 
			
			//digital_1
			$fetch_digital1 = $mysqli->prepare("SELECT count(*) as digital1count FROM `digital1` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital1->execute();
			$fetch_digital1->bind_result($digital1count);
			while($fetch_digital1->fetch())
			{
				array_push($digital_1_Array,$digital1count);
			}
			$fetch_digital1->close(); 
			
			//digital_2
			$fetch_digital2 = $mysqli->prepare("SELECT count(*) as digital2count FROM `digital2` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital2->execute();
			$fetch_digital2->bind_result($digital2count);
			while($fetch_digital2->fetch())
			{
				array_push($digital_2_Array,$digital2count);
			}
			$fetch_digital2->close(); 
			
			//digital_3
			$fetch_digital3 = $mysqli->prepare("SELECT count(*) as digital3count FROM `digital3` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital3->execute();
			$fetch_digital3->bind_result($digital3count);
			while($fetch_digital3->fetch())
			{
				array_push($digital_3_Array,$digital3count);
			}
			$fetch_digital3->close(); 
			
			//digital_4
			$fetch_digital4 = $mysqli->prepare("SELECT count(*) as digital4count FROM `digital4` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital4->execute();
			$fetch_digital4->bind_result($digital4count);
			while($fetch_digital4->fetch())
			{
				array_push($digital_4_Array,$digital4count);
			}
			$fetch_digital4->close(); 
			
			//digital_5
			$fetch_digital5 = $mysqli->prepare("SELECT count(*) as digital5count FROM `digital5` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital5->execute();
			$fetch_digital5->bind_result($digital5count);
			while($fetch_digital5->fetch())
			{
				array_push($digital_5_Array,$digital5count);
			}
			$fetch_digital5->close(); 
			
			//digital_6
			$fetch_digital6 = $mysqli->prepare("SELECT count(*) as digital6count FROM `digital6` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital6->execute();
			$fetch_digital6->bind_result($digital6count);
			while($fetch_digital6->fetch())
			{
				array_push($digital_6_Array,$digital6count);
			}
			$fetch_digital6->close();
			
			//wow
			$fetch_wow = $mysqli->prepare("SELECT count(*) as wowcount FROM `wow` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_wow->execute();
			$fetch_wow->bind_result($wowcount);
			while($fetch_wow->fetch())
			{
				array_push($wowArray,$wowcount);
			}
			$fetch_wow->close();
			
			//facebook
			$fetch_facebook = $mysqli->prepare("SELECT count(*) as  facebookcount FROM `facebook` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_facebook->execute();
			$fetch_facebook->bind_result($facebookcount);
			while($fetch_facebook->fetch())
			{
				array_push($facebookArray,$facebookcount);
			}
			$fetch_facebook->close();
			
			//twitter
			$fetch_twitter = $mysqli->prepare("SELECT count(*) as  twittercount FROM `twitter` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_twitter->execute();
			$fetch_twitter->bind_result($twittercount);
			while($fetch_twitter->fetch())
			{
				array_push($twitterArray,$twittercount);
			}
			$fetch_twitter->close();
			
			//twitter
			$fetch_radio = $mysqli->prepare("SELECT count(*) as  radiocount FROM `radio` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_radio->execute();
			$fetch_radio->bind_result($radiocount);
			while($fetch_radio->fetch())
			{
				array_push($radioArray,$radiocount);
			}
			$fetch_radio->close();
			
			$dateValues=implode(',', $datesArray);
			$newletterValues=implode(',', $newletterArray);
			$enjoy50Values=implode(',', $enjoy50Array);	
			$enjoy200Values=implode(',', $enjoy200Array);
			$enjoysolarValues=implode(',', $enjoysolarArray);
			$enjoyValues=implode(',', $enjoyArray);
			$giftValues=implode(',', $giftArray);
			$communitysolaValues=implode(',', $communitysolarArray);
			$digital_1_Values=implode(',', $digital_1_Array);
			$digital_2_Values=implode(',', $digital_2_Array);
			$digital_3_Values=implode(',', $digital_3_Array);
			$digital_4_Values=implode(',', $digital_4_Array);
			$digital_5_Values=implode(',', $digital_6_Array);
			$digital_6_Values=implode(',', $digital_6_Array);
			$wowValues=implode(',', $wowArray);
			$facebookValues=implode(',', $facebookArray);
			$twitterValues=implode(',', $twitterArray);
			$radioValues=implode(',', $radioArray); 
			
		}  
		$leadsCaptureValues = array($dateValues, $newletterValues, $enjoy50Values, $enjoy200Values, $enjoysolarValues, $enjoyValues, $giftValues, $communitysolaValues, $digital_1_Values, $digital_2_Values, $digital_3_Values, $digital_4_Values, $digital_5_Values, $digital_6_Values, $wowValues, $facebookValues, $twitterValues, $radioValues);
					//print_r($leadsCaptureValues);
					return $leadsCaptureValues; 
		  //echo "<pre>";
			//print_r($list);
			//print_r($leadsCaptureValues);   
	}
	function totalleadsCaptureTrend_1($date_trend,$mysqli)
	{
		
		 $dateconvert=strtotime($date_trend);
		 $month=date("m",$dateconvert);
         $year=date("Y",$dateconvert);
		 $monthshow=date("F Y",$dateconvert);
		
		$list=array();


		//$month = "01";
		//$year = "2016";

		//$start_date = "01-".$month."-".$year;
		$start_date = $year."-".$month."-01";
		
		$start_time = strtotime($start_date);

		$end_time = strtotime("+1 month", $start_time);

		for($i=$start_time; $i<$end_time; $i+=86400)
		{
		   //$list[] = date('Y-m-d-D', $i);
		   $list[] = date('Y-m-d', $i);
		}
		/*  echo "<pre>";
		print_r($list);
		echo "</pre>";  */
		$datesArray = array();
		$newletterArray = array();
		$enjoy50Array = array();
		$enjoy200Array = array();
		$enjoysolarArray = array();
		$enjoyArray = array();
		$giftArray = array();
		$communitysolarArray = array();
		$digital_1_Array = array();
		$digital_2_Array = array();
		$digital_3_Array = array();
		$digital_4_Array = array();
		$digital_5_Array = array();
		$digital_6_Array = array();
		$wowArray = array();
		$facebookArray = array();
		$twitterArray = array();
		$radioArray = array();
		  for($i=0 ; $i<count($list); $i++)
		{
			
			$date_newsletter = $list[$i];
			$dates_show = date('d', strtotime($list[$i]));
			array_push($datesArray,$dates_show);
			$fetch_newsletter = $mysqli->prepare("SELECT count(*) as newslettercount FROM `newsletter` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_newsletter->execute();
			$fetch_newsletter->bind_result($newslettercount);
			while($fetch_newsletter->fetch())
			{
				array_push($newletterArray,$newslettercount);
			}
			$fetch_newsletter->close();
			
			$fetch_enjoy50 = $mysqli->prepare("SELECT count(*) as enjoy50count FROM `enjoy50` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_enjoy50->execute();
			$fetch_enjoy50->bind_result($enjoy50count);
			while($fetch_enjoy50->fetch())
			{
				array_push($enjoy50Array,$enjoy50count);
			}
			$fetch_enjoy50->close();
			
			$fetch_enjoy200 = $mysqli->prepare("SELECT count(*) as enjoy200count FROM `enjoy200` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_enjoy200->execute();
			$fetch_enjoy200->bind_result($enjoy200count);
			while($fetch_enjoy200->fetch())
			{
				array_push($enjoy200Array,$enjoy200count);
			}
			$fetch_enjoy200->close();
			
			$fetch_enjoysolar = $mysqli->prepare("SELECT count(*) as enjoysolarcount FROM `enjoysolar` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_enjoysolar->execute();
			$fetch_enjoysolar->bind_result($enjoysolarcount);
			while($fetch_enjoysolar->fetch())
			{
				array_push($enjoysolarArray,$enjoysolarcount);
			}
			$fetch_enjoysolar->close();
			
			$fetch_enjoy = $mysqli->prepare("SELECT count(*) as enjoycount FROM `enoy` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_enjoy->execute();
			$fetch_enjoy->bind_result($enjoycount);
			while($fetch_enjoy->fetch())
			{
				array_push($enjoyArray,$enjoycount);
			}
			$fetch_enjoy->close();
			
			$fetch_gift = $mysqli->prepare("SELECT count(*) as giftcount FROM `gift` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_gift->execute();
			$fetch_gift->bind_result($giftcount);
			while($fetch_gift->fetch())
			{
				array_push($giftArray,$giftcount);
			}
			$fetch_gift->close();
			
			//community solar
			 $fetch_communitysolar = $mysqli->prepare("SELECT count(*) as communitysolarcount FROM `communitysolar` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_communitysolar->execute();
			$fetch_communitysolar->bind_result($communitysolarcount);
			while($fetch_communitysolar->fetch())
			{
				array_push($communitysolarArray,$communitysolarcount);
			}
			$fetch_communitysolar->close(); 
			
			//digital_1
			$fetch_digital1 = $mysqli->prepare("SELECT count(*) as digital1count FROM `digital1` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital1->execute();
			$fetch_digital1->bind_result($digital1count);
			while($fetch_digital1->fetch())
			{
				array_push($digital_1_Array,$digital1count);
			}
			$fetch_digital1->close(); 
			
			//digital_2
			$fetch_digital2 = $mysqli->prepare("SELECT count(*) as digital2count FROM `digital2` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital2->execute();
			$fetch_digital2->bind_result($digital2count);
			while($fetch_digital2->fetch())
			{
				array_push($digital_2_Array,$digital2count);
			}
			$fetch_digital2->close(); 
			
			//digital_3
			$fetch_digital3 = $mysqli->prepare("SELECT count(*) as digital3count FROM `digital3` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital3->execute();
			$fetch_digital3->bind_result($digital3count);
			while($fetch_digital3->fetch())
			{
				array_push($digital_3_Array,$digital3count);
			}
			$fetch_digital3->close(); 
			
			//digital_4
			$fetch_digital4 = $mysqli->prepare("SELECT count(*) as digital4count FROM `digital4` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital4->execute();
			$fetch_digital4->bind_result($digital4count);
			while($fetch_digital4->fetch())
			{
				array_push($digital_4_Array,$digital4count);
			}
			$fetch_digital4->close(); 
			
			//digital_5
			$fetch_digital5 = $mysqli->prepare("SELECT count(*) as digital5count FROM `digital5` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital5->execute();
			$fetch_digital5->bind_result($digital5count);
			while($fetch_digital5->fetch())
			{
				array_push($digital_5_Array,$digital5count);
			}
			$fetch_digital5->close(); 
			
			//digital_6
			$fetch_digital6 = $mysqli->prepare("SELECT count(*) as digital6count FROM `digital6` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_digital6->execute();
			$fetch_digital6->bind_result($digital6count);
			while($fetch_digital6->fetch())
			{
				array_push($digital_6_Array,$digital6count);
			}
			$fetch_digital6->close();
			
			//wow
			$fetch_wow = $mysqli->prepare("SELECT count(*) as wowcount FROM `wow` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_wow->execute();
			$fetch_wow->bind_result($wowcount);
			while($fetch_wow->fetch())
			{
				array_push($wowArray,$wowcount);
			}
			$fetch_wow->close();
			
			//facebook
			$fetch_facebook = $mysqli->prepare("SELECT count(*) as  facebookcount FROM `facebook` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_facebook->execute();
			$fetch_facebook->bind_result($facebookcount);
			while($fetch_facebook->fetch())
			{
				array_push($facebookArray,$facebookcount);
			}
			$fetch_facebook->close();
			
			//twitter
			$fetch_twitter = $mysqli->prepare("SELECT count(*) as  twittercount FROM `twitter` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_twitter->execute();
			$fetch_twitter->bind_result($twittercount);
			while($fetch_twitter->fetch())
			{
				array_push($twitterArray,$twittercount);
			}
			$fetch_twitter->close();
			
			//twitter
			$fetch_radio = $mysqli->prepare("SELECT count(*) as  radiocount FROM `radio` where DATE_FORMAT(createddate,'%Y-%m-%d') = '$date_newsletter'");
			$fetch_radio->execute();
			$fetch_radio->bind_result($radiocount);
			while($fetch_radio->fetch())
			{
				array_push($radioArray,$radiocount);
			}
			$fetch_radio->close();
			
			$dateValues=implode(',', $datesArray);
			$newletterValues=implode(',', $newletterArray);
			$enjoy50Values=implode(',', $enjoy50Array);	
			$enjoy200Values=implode(',', $enjoy200Array);
			$enjoysolarValues=implode(',', $enjoysolarArray);
			$enjoyValues=implode(',', $enjoyArray);
			$giftValues=implode(',', $giftArray);
			$communitysolaValues=implode(',', $communitysolarArray);
			$digital_1_Values=implode(',', $digital_1_Array);
			$digital_2_Values=implode(',', $digital_2_Array);
			$digital_3_Values=implode(',', $digital_3_Array);
			$digital_4_Values=implode(',', $digital_4_Array);
			$digital_5_Values=implode(',', $digital_6_Array);
			$digital_6_Values=implode(',', $digital_6_Array);
			$wowValues=implode(',', $wowArray);
			$facebookValues=implode(',', $facebookArray);
			$twitterValues=implode(',', $twitterArray);
			$radioValues=implode(',', $radioArray); 
			
		}  
		$leadsCaptureValues = array($dateValues, $newletterValues, $enjoy50Values, $enjoy200Values, $enjoysolarValues, $enjoyValues, $giftValues, $communitysolaValues, $digital_1_Values, $digital_2_Values, $digital_3_Values, $digital_4_Values, $digital_5_Values, $digital_6_Values, $wowValues, $facebookValues, $twitterValues, $radioValues,$monthshow);
					//print_r($leadsCaptureValues);
					return $leadsCaptureValues; 
		  //echo "<pre>";
			//print_r($list);
			//print_r($leadsCaptureValues);   
	}
}
$sunsharehubValues = new SunsharehubDashboard(); 	
/* $mysqli = new mysqli("localhost","root","phpils135$","sunshare_saleshub");    
$sunsharehubValues->totalleadsCaptureTrend($mysqli);  */
?>
