<?php
class Callcenter
{
	
	function fetch_dropdown($startdate,$enddate,$mysqliportal)
	{
		//echo "select distinct ms.name,ms.id from manage_salesrep ms,leads l where ms.id=salesrep_id and DATE_FORMAT(l.datetime,'%Y-%m-%d') between '$startdate' and '$enddate'";
		 $fetch_salesRep = $mysqliportal->prepare("select distinct ms.name,ms.id from manage_salesrep ms,leads l where ms.id=salesrep_id and DATE_FORMAT(l.datetime,'%Y-%m-%d') between '$startdate' and '$enddate' ORDER BY ms.name ASC");
		$fetch_salesRep->execute();
		$fetch_salesRep->bind_result($salesrepname,$salesrepid);
		$salesRepnameArray = array();
		$salesRepidArray = array();
		while($fetch_salesRep->fetch())
		{
			array_push($salesRepnameArray,$salesrepname);
			array_push($salesRepidArray,$salesrepid);
		}
		$fetch_salesRep->close();
		$repshow = '<option value="all">All</option>';
		for($i=0; $i<count($salesRepidArray); $i++)
		{
			$repid = $salesRepidArray[$i];
			$repbname = $salesRepnameArray[$i];
			$repshow .= '<option value="'.$repid.'">'.$repbname.'</option>';
		}	
		//echo $repshow;
		
		$fetch_callcenter = $mysqliportal->prepare("select distinct ms.org_name from manage_salesrep ms where ms.org_name in ('RESULTS','PCCW') ORDER BY ms.org_name ASC");
		$fetch_callcenter->execute();
		$fetch_callcenter->bind_result($orgname);
		$show_callcenter = '<option value="all">All</option>';
		while($fetch_callcenter->fetch())
		{
			$show_callcenter .= '<option value="'.$orgname.'">'.$orgname.'</option>';
		}
		$fetch_callcenter->close();
		//echo $show_callcenter;
		
		
		 $fetch_city = $mysqliportal->prepare("select distinct TRIM(l.city) AS cityname from  leads l where l.city!=' ' and l.city not in ('0','00','000','00000','.','....','?') and DATE_FORMAT(l.datetime,'%Y-%m-%d') between '$startdate' and '$enddate' ORDER BY l.city ASC");
		$fetch_city->execute();
		$fetch_city->bind_result($cityname);
		$show_city = '<option value="all">All</option>';
		while($fetch_city->fetch())
		{
			$show_city .= '<option value="'.$cityname.'">'.$cityname.'</option>';
		}
		$fetch_city->close();
		
		//echo $show_city;
		
		 $fetch_state = $mysqliportal->prepare("select distinct TRIM(l.state) AS statename from  leads l where l.state!=' ' and l.state not in ('0','00','000','00000','.','....','?') and DATE_FORMAT(l.datetime,'%Y-%m-%d') between '$startdate' and '$enddate' ORDER BY l.state ASC");
		$fetch_state->execute();
		$fetch_state->bind_result($statename);
		$show_state = '<option value="all">All</option>';
		while($fetch_state->fetch())
		{
			$show_state .= '<option value="'.$statename.'">'.$statename.'</option>';
		}
		$fetch_state->close();
		
		//echo $show_state;
		
		$fetch_country = $mysqliportal->prepare("select distinct TRIM(l.country) AS countryname from  leads l where l.country!=' ' and l.country not in ('0','00','000','00000','.','....','?') and DATE_FORMAT(l.datetime,'%Y-%m-%d') between '$startdate' and '$enddate' ORDER BY l.country ASC");
		$fetch_country->execute();
		$fetch_country->bind_result($countryname);
		$show_country = '<option value="all">All</option>';
		while($fetch_country->fetch())
		{
			$show_country .= '<option value="'.$countryname.'">'.$countryname.'</option>';
		}
		$fetch_country->close();
		//echo $show_country;
		
		$fetch_leadstatus = $mysqliportal->prepare("select distinct TRIM(l.lstatus) AS statusid from  leads l where l.lstatus!=' ' and DATE_FORMAT(l.datetime,'%Y-%m-%d') between '$startdate' and '$enddate' ORDER BY l.lstatus ASC");
		$fetch_leadstatus->execute();
		$fetch_leadstatus->bind_result($statusid);
		$show_leadstatus = '<option value="all">All</option>';
		while($fetch_leadstatus->fetch())
		{
			if($statusid==1)
			{
				$leadid = 1;
				$leadstatus = 'Leads Saved';
			}
			else if($statusid==3)
			{
				$leadid = 3;
				$leadstatus = 'Contract Generated';
			}
			else if($statusid==4)
			{
				$leadid = 4;
				$leadstatus = 'Contract Signed';
			}
			else if($statusid==5)
			{
				$leadid = 5;
				$leadstatus = 'Declined';
			}
			$show_leadstatus .= '<option value="'.$leadid.'">'.$leadstatus.'</option>';
		}
		$fetch_leadstatus->close();
		//echo $show_leadstatus;
		$lead_diff_status = array($repshow,$show_callcenter,$show_city,$show_state,$show_country,$show_leadstatus);
		// echo "<pre>";
		 return $lead_diff_status;
		// print_r($lead_diff_status);
	}
	
	
	
}
$callcenterValues = new Callcenter();

?>