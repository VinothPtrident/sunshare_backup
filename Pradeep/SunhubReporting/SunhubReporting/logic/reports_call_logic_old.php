<?php
//include '../config/database.php';
class Report
{
	function reports_call($mysqli)
	{
		$salesrepidArray = array();
		$salesrepnameArray = array();
		$loginidArray = array();
		$callcenterArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$xcelaccountnoArray = array();
		$campaigntypefromArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		
		 $fetch_salesid = $mysqli->prepare("select lf.callcenter_id as salesrepid,ms.name as salesrepname,ms.email as loginid ,ms.org_name as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld,manage_salesrep ms  where lf.leadid=ld.lid and lf.callcenter_id=ms.id and lf.calltype ='outbound'");
		$fetch_salesid->execute();
		$fetch_salesid->bind_result($salesrepid,$salesrepname,$loginid,$callcenter,$firstname,$lastname,$emailid,$address1,$xcelaccountno,$campaigntypefrom,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus);
		$fetch_salesid->store_result();
		$rows = $fetch_salesid->num_rows;
		 while($fetch_salesid->fetch())
		{
			array_push($salesrepidArray,$salesrepid);
			array_push($salesrepnameArray,$salesrepname);
			array_push($loginidArray,$loginid);
			array_push($callcenterArray,$callcenter);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($xcelaccountnoArray,$xcelaccountno);
			array_push($campaigntypefromArray,$campaigntypefrom);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			
			
		}
		$fetch_salesid->close();
		
		
		$v=$rows;
		$tablereport = '';
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			$campaigntype_from = $campaigntypefromArray[$i];
			if($campaigntype_from == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaigntype_from == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaigntype_from == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else
			{
				$campaigntypefromstatus = 'NA';
			}
			$lead_status = $leadstatusArray[$i];
			if($lead_status == 1)
			{
				$leadstatus_show = 'Lead Saved';
			}
			else if($lead_status == 3)
			{
				$leadstatus_show = 'Contract Generated';
			}
			$emailcheck = $emailidArray[$i];
			//select count(si.id) as countemail from saleshub_import_pra si,leads le where si.email='$emailcheck' and le.lstatus=3
			$fetch_check = $mysqli->prepare("select count(id) as countemail from saleshub_import_pra si where si.email='$emailcheck'");
			$fetch_check->execute();
			$fetch_check->bind_result($countemail);
			$fetch_check->fetch();
			$fetch_check->close(); 
			$tablereport .= '<tr><td>'.$v.'</td>
			<td>'.$salesrepidArray[$i].'</td>
			 <td>'.ucwords(strtolower($salesrepnameArray[$i])).'</td>
			 <td>'.$loginidArray[$i].'</td>
			 <td>'.$callcenterArray[$i].'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			 <td>'.$xcelaccountnoArray[$i].'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$cityArray[$i].'</td>
			 <td>'.$stateArray[$i].'</td>
			 <td>'.$countryArray[$i].'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.$createdleaddateArray[$i].'</td>
			 <td>'.$leadstatus_show.'</td>
			 <td>'.$countemail.'</td>
			 </tr>';
			 $v--;
			
		}	
		//$tablereport .= '<table>';
		echo $tablereport; 
		//$tablereport = '<table>';
		
		
		
		
		
	}
	function reports_webenroll($mysqli)
	{
		//echo "select lf.calltype as source,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as excelaccountno,lf.userprefer as campaign,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld where lf.leadid=ld.lid and lf.calltype ='website' and callcenter_id=0";
		// $fetch_webenroll = $mysqli->prepare("select lf.calltype as source,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as excelaccountno,lf.userprefer as campaign,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld,manage_salesrep ms  where lf.leadid=ld.lid  and lf.calltype ='website'");
		
		$sourceArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$excelaccountnoArray = array();
		$campaignArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		$fetch_webenroll = $mysqli->prepare("select lf.calltype as source,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as excelaccountno,lf.userprefer as campaign,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld where lf.leadid=ld.lid and lf.calltype ='website' and lf.callcenter_id=0");
		$fetch_webenroll->execute();
		$fetch_webenroll->bind_result($source,$firstname,$lastname,$emailid,$address1,$excelaccountno,$campaign,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus);
		$fetch_webenroll->store_result();
		$rows = $fetch_webenroll->num_rows;
		
		//$tablereport = '<table>';
		 while($fetch_webenroll->fetch())
		{
			array_push($sourceArray,$source);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($excelaccountnoArray,$excelaccountno);
			array_push($campaignArray,$campaign);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			
			
		}
		$fetch_webenroll->close();
		
		$v=$rows;
		$tablereport = '';
		for($i=0; $i<count($sourceArray); $i++)
		{
			
			$campaignstatus =  $campaignArray[$i];	
			if($campaignstatus == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaignstatus == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaignstatus == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else
			{
				$campaigntypefromstatus = 'NA';
			}
			
			$leadstatus =  $leadstatusArray[$i];	
			if($leadstatus == 1)
			{
				$leadstatus_show = 'Lead Saved';
			}
			else if($leadstatus == 3)
			{
				$leadstatus_show = 'Contract Generated';
			}
			$emailcheck = $emailidArray[$i];
			//select count(si.id) as countemail from saleshub_import_pra si,leads le where si.email='$emailcheck' and le.lstatus=3
			$fetch_check = $mysqli->prepare("select count(id) as countemail from saleshub_import_pra si where si.email='$emailcheck'");
			$fetch_check->execute();
			$fetch_check->bind_result($countemail);
			$fetch_check->fetch();
			$fetch_check->close(); 
			
			$tablereport .= '<tr><td >'.$v.'</td>
			 <td>'.ucwords(strtolower($sourceArray[$i])).'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			 <td>'.$excelaccountnoArray[$i].'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$cityArray[$i].'</td>
			 <td>'.$stateArray[$i].'</td>
			 <td>'.$countryArray[$i].'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.$createdleaddateArray[$i].'</td>
			 <td>'.$leadstatus_show.'</td>
			 <td>'.$countemail.'</td>
			 </tr>';
			 $v--;
			
		}
		//$tablereport .= '<table>';
		echo $tablereport; 
		
		
	}	
	
}
$reportValues = new Report();
//$mysqli = new mysqli("localhost","root","phpils135$","sunshare_saleshub");
//$reportValues->reports_call($mysqli); 
//$reportValues->reports_webenroll($mysqli); 
?>