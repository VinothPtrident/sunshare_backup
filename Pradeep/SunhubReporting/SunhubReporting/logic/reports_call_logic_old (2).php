<?php
//include '../config/database.php';
class Report
{
	function reports_call($mysqliportal)
	{
		$salesrepidArray = array();
		$salesrepnameArray = array();
		$loginidArray = array();
		$callcenterArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$xcelaccountnoArray = array();
		$campaigntypefromArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		
		 /* $fetch_salesid = $mysqliportal->prepare("select lf.callcenter_id as salesrepid,ms.name as salesrepname,ms.email as loginid ,ms.org_name as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld,manage_salesrep ms  where lf.leadid=ld.lid and lf.callcenter_id=ms.id and lf.calltype ='outbound'"); */
		$fetch_salesid = $mysqliportal->prepare("select ld.salesrep_id as salesrepid,ms.name as salesrepname,ms.email as loginid ,ms.org_name as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld,manage_salesrep ms where lf.leadid=ld.lid and ld.salesrep_id=ms.id and lf.calltype ='outbound' and ld.createdlead_time!='0000-00-00 00:00:00'");
		$fetch_salesid->execute();
		$fetch_salesid->bind_result($salesrepid,$salesrepname,$loginid,$callcenter,$firstname,$lastname,$emailid,$address1,$xcelaccountno,$campaigntypefrom,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus);
		$fetch_salesid->store_result();
		$rows = $fetch_salesid->num_rows;
		 while($fetch_salesid->fetch())
		{
			array_push($salesrepidArray,$salesrepid);
			array_push($salesrepnameArray,$salesrepname);
			array_push($loginidArray,$loginid);
			array_push($callcenterArray,$callcenter);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($xcelaccountnoArray,$xcelaccountno);
			array_push($campaigntypefromArray,$campaigntypefrom);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			
			
		}
		$fetch_salesid->close();
		
		
		$v=$rows;
		$tablereport = '';
		for($i=0; $i<count($salesrepidArray); $i++)
  {
   $city_sh = $cityArray[$i];
   if($city_sh=='.' || $city_sh=='....' || $city_sh=='0' || $city_sh=='00' || $city_sh=='000' || $city_sh=='0000' || $city_sh=='?')
   {
    $city_show = 'NA';
   } 
   else
   {
    $city_show = ucwords($city_sh);
   } 
   $state_sh = $stateArray[$i]; 
   if($state_sh=='0')
   {
    $state_show = 'NA';
   } 
   else
   {
    $state_show = ucwords($state_sh);
   } 
   $country_sh = $countryArray[$i]; 
   if($country_sh=='0')
   {
    $country_show = 'NA';
   } 
   else
   {
    $country_show = ucwords($country_sh);
   } 
   $campaigntype_from = $campaigntypefromArray[$i];
   if($campaigntype_from == 1)
   {
    $campaigntypefromstatus = '$200 Restaurant.com';
   }
   else if($campaigntype_from == 2)
   {
    $campaigntypefromstatus = '$50 Visa';
   }
   else if($campaigntype_from == 3)
   {
    $campaigntypefromstatus = 'Other';
   }
   else
   {
    $campaigntypefromstatus = 'NA';
   }
   $lead_status = $leadstatusArray[$i];
   if($lead_status == 1)
   {
    $leadstatus_show = 'Lead Saved';
   }
   else if($lead_status == 3)
   {
    $leadstatus_show = 'Contract Generated';
   }
   else if($lead_status == 4)
   {
    $leadstatus_show = 'Contract Signed';
   }
   else if($lead_status == 5)
   {
    $leadstatus_show = 'Contract Declined';
   }
   $emailcheck = $emailidArray[$i];
   //select count(si.id) as countemail from saleshub_import_pra si,leads le where si.email='$emailcheck' and le.lstatus=3
   //$fetch_check = $mysqli->prepare("select count(id) as countemail from saleshub_import_pra si where si.email='$emailcheck'");
   /* $fetch_check = $mysqliportal->prepare("select l.lstatus as contractsignedcount from leads l,leadinfo lf where l.lid=lf.leadid and l.lstatus=4");
   $fetch_check->execute();
   $fetch_check->bind_result($contractsignedcount);
   $fetch_check->fetch();
   $fetch_check->close();  */
   $tablereport .= '<tr><td>'.$v.'</td>
   <td>'.$salesrepidArray[$i].'</td>
    <td>'.ucwords(strtolower($salesrepnameArray[$i])).'</td>
    <td>'.$loginidArray[$i].'</td>
    <td>'.$callcenterArray[$i].'</td>
    <td>'.$firstnameArray[$i].'</td>
    <td>'.$lastnameArray[$i].'</td>
    <td>'.$emailidArray[$i].'</td>
    <td>'.$address1Array[$i].'</td>
    <td>'.$xcelaccountnoArray[$i].'</td>
    <td>'.$campaigntypefromstatus.'</td>
    <td>'.$mobilenoArray[$i].'</td>
    <td>'.$city_show.'</td>
    <td>'.$state_show.'</td>
    <td>'.$country_show.'</td>
    <td>'.$zipcodeArray[$i].'</td>
    <td>'.date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i])).'</td>
    <td>'.$leadstatus_show.'</td>
   </tr>';
    $v--;
   
  }	
		//$tablereport .= '<table>';
		echo $tablereport; 
		//$tablereport = '<table>';
	}
	function reports_start_end_dates($mysqliportal,$startdate,$enddate){
		$salesrepidArray = array();
		$salesrepnameArray = array();
		$loginidArray = array();
		$callcenterArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$xcelaccountnoArray = array();
		$campaigntypefromArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		
		 $fetch_salesid = $mysqliportal->prepare("select ld.salesrep_id as salesrepid,ms.name as salesrepname,ms.email as loginid ,ms.org_name as callcenter,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as xcelaccountno,lf.userprefer as campaigntypefrom,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld,manage_salesrep ms  where lf.leadid=ld.lid and ld.salesrep_id=ms.id and lf.calltype ='outbound' and ld.createdlead_time BETWEEN '$startdate' AND  '$enddate' and ld.createdlead_time!='0000-00-00 00:00:00'");
		$fetch_salesid->execute();
		$fetch_salesid->bind_result($salesrepid,$salesrepname,$loginid,$callcenter,$firstname,$lastname,$emailid,$address1,$xcelaccountno,$campaigntypefrom,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus);
		$fetch_salesid->store_result();
		$rows = $fetch_salesid->num_rows;
		 while($fetch_salesid->fetch())
		{
			array_push($salesrepidArray,$salesrepid);
			array_push($salesrepnameArray,$salesrepname);
			array_push($loginidArray,$loginid);
			array_push($callcenterArray,$callcenter);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($xcelaccountnoArray,$xcelaccountno);
			array_push($campaigntypefromArray,$campaigntypefrom);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			
			
		}
		$fetch_salesid->close();
		
		
		$v=$rows;
		$tablereport = '';
		for($i=0; $i<count($salesrepidArray); $i++)
		{
			$campaigntype_from = $campaigntypefromArray[$i];
			if($campaigntype_from == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaigntype_from == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaigntype_from == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else
			{
				$campaigntypefromstatus = 'NA';
			}
			$lead_status = $leadstatusArray[$i];
			if($lead_status == 1)
			{
				$leadstatus_show = 'Lead Saved';
			}
			else if($lead_status == 3)
			{
				$leadstatus_show = 'Contract Generated';
			}
			else if($lead_status == 4)
			{
				$leadstatus_show = 'Contract Signed';
			}
			else if($lead_status == 5)
			{
				$leadstatus_show = 'Contract Declined';
			}
			$emailcheck = $emailidArray[$i];
			//select count(si.id) as countemail from saleshub_import_pra si,leads le where si.email='$emailcheck' and le.lstatus=3
			/* $fetch_check = $mysqliportal->prepare("select count(id) as countemail from saleshub_import_pra si where si.email='$emailcheck'");
			$fetch_check->execute();
			$fetch_check->bind_result($countemail);
			$fetch_check->fetch();
			$fetch_check->close();  */
			$tablereport .= '<tr><td>'.$v.'</td>
			<td>'.$salesrepidArray[$i].'</td>
			 <td>'.ucwords(strtolower($salesrepnameArray[$i])).'</td>
			 <td>'.$loginidArray[$i].'</td>
			 <td>'.$callcenterArray[$i].'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			 <td>'.$xcelaccountnoArray[$i].'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$cityArray[$i].'</td>
			 <td>'.$stateArray[$i].'</td>
			 <td>'.$countryArray[$i].'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i])).'</td>
			 <td>'.$leadstatus_show.'</td>
			 </tr>';
			 $v--;
			
		}	
		//$tablereport .= '<table>';
		echo $tablereport; 
		//$tablereport = '<table>';
		
		
		
	}
	
	function reports_webenroll($mysqliportal)
	{
		//echo "select lf.calltype as source,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as excelaccountno,lf.userprefer as campaign,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld where lf.leadid=ld.lid and lf.calltype ='website' and callcenter_id=0";
		
		
		//$sourceArray = array();
		$firstnameArray = array();
		$lastnameArray = array();
		$emailidArray = array();
		$address1Array = array();
		$excelaccountnoArray = array();
		$campaignArray = array();
		$mobilenoArray = array();
		$cityArray = array();
		$stateArray = array();
		$countryArray = array();
		$zipcodeArray = array();
		$createdleaddateArray = array();
		$leadstatusArray = array();
		//$fetch_webenroll = $mysqliportal->prepare("select lf.calltype as source,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as excelaccountno,lf.userprefer as campaign,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld where lf.leadid=ld.lid and lf.calltype ='website' and lf.callcenter_id=0 and ld.createdlead_time!='0000-00-00 00:00:00'");
		/* $fetch_webenroll = $mysqliportal->prepare("select lf.calltype as source,lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as excelaccountno,lf.userprefer as campaign,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld,manage_salesrep ms  where lf.leadid=ld.lid  and lf.calltype ='website' and ld.createdlead_time!='0000-00-00 00:00:00'"); */
		$fetch_webenroll = $mysqliportal->prepare("select lf.firstname as firstname,lf.lastname as lastname,lf.email as emailid,lf.address1 as address1,lf.utilityacc_num as excelaccountno,lf.userprefer as campaign,lf.phno as mobileno,lf.city as city,lf.state as state,lf.country as country,lf.zipcode as zipcode,ld.createdlead_time as createdleaddate,ld.lstatus as leadstatus from leadinfo lf,leads ld where lf.leadid=ld.lid and lf.calltype ='website'");
		$fetch_webenroll->execute();
		$fetch_webenroll->bind_result($firstname,$lastname,$emailid,$address1,$excelaccountno,$campaign,$mobileno,$city,$state,$country,$zipcode,$createdleaddate,$leadstatus);
		$fetch_webenroll->store_result();
		$rows = $fetch_webenroll->num_rows;
		
		//$tablereport = '<table>';
		 while($fetch_webenroll->fetch())
		{
			//array_push($sourceArray,$source);
			array_push($firstnameArray,$firstname);
			array_push($lastnameArray,$lastname);
			array_push($emailidArray,$emailid);
			array_push($address1Array,$address1);
			array_push($excelaccountnoArray,$excelaccountno);
			array_push($campaignArray,$campaign);
			array_push($mobilenoArray,$mobileno);
			array_push($cityArray,$city);
			array_push($stateArray,$state);
			array_push($countryArray,$country);
			array_push($zipcodeArray,$zipcode);
			array_push($createdleaddateArray,$createdleaddate);
			array_push($leadstatusArray,$leadstatus);
			
			
		}
		$fetch_webenroll->close();
		
		$v=$rows;
		$tablereport = '';
		for($i=0; $i<count($firstnameArray); $i++)
		{
			
			$campaignstatus =  $campaignArray[$i];	
			if($campaignstatus == 1)
			{
				$campaigntypefromstatus = '$200 Restaurant.com';
			}
			else if($campaignstatus == 2)
			{
				$campaigntypefromstatus = '$50 Visa';
			}
			else if($campaignstatus == 3)
			{
				$campaigntypefromstatus = 'Other';
			}
			else
			{
				$campaigntypefromstatus = 'NA';
			}
			
			$lead_status = $leadstatusArray[$i];
			if($lead_status == 1)
			{
				$leadstatus_show = 'Lead Saved';
			}
			else if($lead_status == 3)
			{
				$leadstatus_show = 'Contract Generated';
			}
			else if($lead_status == 4)
			{
				$leadstatus_show = 'Contract Signed';
			}
			else if($lead_status == 5)
			{
				$leadstatus_show = 'Contract Declined';
			}
			$emailcheck = $emailidArray[$i];
			/* select count(si.id) as countemail from saleshub_import_pra si,leads le where si.email='$emailcheck' and le.lstatus=3
			$fetch_check = $mysqli->prepare("select count(id) as countemail from saleshub_import_pra si where si.email='$emailcheck'");
			$fetch_check->execute();
			$fetch_check->bind_result($countemail);
			$fetch_check->fetch();
			$fetch_check->close();  */
			
			$tablereport .= '<tr><td >'.$v.'</td>
			 <td>'.$firstnameArray[$i].'</td>
			 <td>'.$lastnameArray[$i].'</td>
			 <td>'.$emailidArray[$i].'</td>
			 <td>'.$address1Array[$i].'</td>
			 <td>'.$excelaccountnoArray[$i].'</td>
			 <td>'.$campaigntypefromstatus.'</td>
			 <td>'.$mobilenoArray[$i].'</td>
			 <td>'.$cityArray[$i].'</td>
			 <td>'.$stateArray[$i].'</td>
			 <td>'.$countryArray[$i].'</td>
			 <td>'.$zipcodeArray[$i].'</td>
			 <td>'.date('m/d/Y H:i:s',strtotime($createdleaddateArray[$i])).'</td>
			 <td>'.$leadstatus_show.'</td>
			 </tr>';
			 $v--;
			
		}
		//$tablereport .= '<table>';
		echo $tablereport; 
		
	}	
	
}
$reportValues = new Report();
//$mysqli = new mysqli("localhost","root","phpils135$","sunshare_saleshub");
//$reportValues->reports_call($mysqli); 
//$reportValues->reports_webenroll($mysqli); 
?>