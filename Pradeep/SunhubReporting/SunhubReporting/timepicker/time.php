<!DOCTYPE html>
<html>
<head>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

  <script type="text/javascript" src="jquery.timepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />

  <script type="text/javascript" src="lib/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker.css" />

  <script type="text/javascript" src="lib/site.js"></script>
  <link rel="stylesheet" type="text/css" href="lib/site.css" />

</head>

<body>
    

    <section>
        <p class="body-text">Use this plugin to unobtrusively add a timepicker dropdown to your forms. It's lightweight (2.7kb minified and gzipped) and easy to customize.</p>
    </section>

    <section id="examples">
        <article>
            <div class="demo">
                <h2>Basic Example</h2>
                <p><input id="basicExample" type="text" class="time" /></p>
            </div>

            <script>
                $(function() {
                    $('#basicExample').timepicker();
                });
            </script>

            <pre class="code" data-language="javascript">$('#basicExample').timepicker();</pre>
        </article>


       
</body>
</html>
