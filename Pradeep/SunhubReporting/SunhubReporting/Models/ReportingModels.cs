﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SendGridMail.Transport;
using System.Net;
using SendGridMail;
using System.Net.Mail;


namespace SunhubReporting.Models
{
    public class ReportingModels
    {
        public DataTable dtLogin { get; set; }
        public DataTable dtCampaigns { get; set; }
        public DataTable dtWebCampaigns { get; set; }
        public DataTable dtWebEnrollment { get; set; }
        public ContractSummary contract_summary { get; set; }
        public UserDetails user_details { get; set; }
        public string MonthValue { get; set; }
        public string UserName { get; set; }
        public int WebCampaignCount { get; set; }
        public List<ContractSummary> list_contract_summary { get; set; }
        public List<String> Channel1 { get; set; }
        public List<String> Channel2 { get; set; }
        public List<String> Channel3 { get; set; }
        public List<String> Channel4 { get; set; }
        public List<WebCampaignTrend> templist { get; set; }
        public List<WebCampaignTrend> list_webcampaign { get; set; }
        public WebCampaignTrend web_campaign { get; set; }
        public List<String> list_dates { get; set; }
        public List<String> campaignNames { get; set; }
        public ContractSummary channel1Summary { get; set; }
        public ContractSummary channel2Summary { get; set; }
        public ContractSummary channel3Summary { get; set; }
        public ContractSummary channel4Summary { get; set; }
        public List<String> ChannelDaily1 { get; set; }
        public List<String> ChannelDaily2 { get; set; }
        public List<String> DateList { get; set; }
        public DataTable dtContract { get; set; }
        public DataTable dtAllReports { get; set; }

        // Login Process
        public DataTable LoginMethod(string name,string pass,string partner)
        {
             dtLogin = new DataTable();
            try
            {
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_login  '"+name+"','"+pass+"','"+partner+"'";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtLogin);
                    Connection.Close();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                name = null;
                pass = null;
                partner = null;
            }
            return dtLogin;
        }

        //Get Contract Summary Chart
        public ContractSummary GetContractSummaryChart(string startDate,string endDate)
        {
            contract_summary = new ContractSummary();
            dtContract = new DataTable();
            string name = "";
            try
            {
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_ContractsSummaryChart  '" + startDate + "','" + endDate + "'";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtContract);
                    Connection.Close();
                }
                for (int i = 0; i < dtContract.Rows.Count; i++)
                {
                    name = dtContract.Rows[i]["Name"].ToString();
                    if (name == "Contract Signed")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.ContractSigned = "0";
                        }
                        else
                        {
                            contract_summary.ContractSigned = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                    else if (name == "Contract Declined")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.ContractDeclined = "0";
                        }
                        else
                        {
                            contract_summary.ContractDeclined = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                    else if (name == "Contract Sent")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.ContractUnsigned = "0";
                        }
                        else
                        {
                            contract_summary.ContractUnsigned = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                    else if (name == "Lead Created")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.LeadSaved = "0";
                        }
                        else
                        {
                            contract_summary.LeadSaved = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                    if (string.IsNullOrEmpty(contract_summary.ContractSigned))
                    {
                        contract_summary.ContractSigned = "0";
                    }
                    if (string.IsNullOrEmpty(contract_summary.ContractDeclined))
                    {
                        contract_summary.ContractDeclined = "0";
                    }
                    if (string.IsNullOrEmpty(contract_summary.ContractUnsigned))
                    {
                        contract_summary.ContractUnsigned = "0";
                    }
                    if (string.IsNullOrEmpty(contract_summary.LeadSaved))
                    {
                        contract_summary.LeadSaved = "0";
                    }
                }
                if (dtContract.Rows.Count <= 0)
                {
                    contract_summary.ContractSigned = "0";
                    contract_summary.ContractDeclined = "0";
                    contract_summary.ContractUnsigned = "0";
                    contract_summary.LeadSaved = "0";

                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dtContract = null;
                name = null;
                startDate = null;
                endDate = null;
            }
            return contract_summary;
        }

        //Get Contract Summary Table 
        public ContractSummary GetContractSummaryTable(string startDate,string endDate,string channelName)
        {
            contract_summary = new ContractSummary();
            dtContract = new DataTable();
            string name = "";
            try
            {
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_ContractsSummaryTable  '" + startDate + "','" + endDate + "','" + channelName + "'";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtContract);
                    Connection.Close();
                }
                for (int i = 0; i < dtContract.Rows.Count; i++)
                {
                    name = dtContract.Rows[i]["Name"].ToString();
                    if (name == "Contract Signed")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.ContractSigned = "0";
                        }
                        else
                        {
                            contract_summary.ContractSigned = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                    else if (name == "Contract Declined")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.ContractDeclined = "0";
                        }
                        else
                        {
                            contract_summary.ContractDeclined = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                    else if (name == "Contract Sent")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.ContractUnsigned = "0";
                        }
                        else
                        {
                            contract_summary.ContractUnsigned = dtContract.Rows[i]["Value"].ToString();
                        }

                    }
                    else if (name == "Lead Created")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.LeadSaved = "0";
                        }
                        else
                        {
                            contract_summary.LeadSaved = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                }
                if (string.IsNullOrEmpty(contract_summary.ContractSigned))
                {
                    contract_summary.ContractSigned = "0";
                }
                if (string.IsNullOrEmpty(contract_summary.ContractDeclined))
                {
                    contract_summary.ContractDeclined = "0";
                }
                if (string.IsNullOrEmpty(contract_summary.ContractUnsigned))
                {
                    contract_summary.ContractUnsigned = "0";
                }
                if (string.IsNullOrEmpty(contract_summary.LeadSaved))
                {
                    contract_summary.LeadSaved = "0";
                }
                if (dtContract.Rows.Count <= 0)
                {
                    contract_summary.ContractSigned = "0";
                    contract_summary.ContractDeclined = "0";
                    contract_summary.ContractUnsigned = "0";
                    contract_summary.LeadSaved = "0";
                    contract_summary.Conversion = "0";
                }
                else
                {
                    if ((Decimal.Parse(contract_summary.ContractUnsigned) + Decimal.Parse(contract_summary.ContractDeclined)) != 0)
                    {
                        decimal val = (Decimal.Parse(contract_summary.ContractSigned) / (Decimal.Parse(contract_summary.ContractUnsigned) + Decimal.Parse(contract_summary.ContractDeclined))) * 100;
                        contract_summary.Conversion = Convert.ToString(Math.Truncate(val * 100) / 100);
                    }
                    if (string.IsNullOrEmpty(contract_summary.Conversion))
                    {
                        contract_summary.Conversion = "0";
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dtContract = null;
                name = null;
                startDate = null;
                endDate = null;
            }
            return contract_summary;
        }

        //Get Current Month Contract Summary
        public ContractSummary GetCurrentMonthContractSummary(string Month, string Year, string channelid)
        {
            contract_summary = new ContractSummary();
            dtContract = new DataTable();
            string name = "";
            try
            {
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_CurrentMonthSummary  " + Month + "," + Year + "," + channelid + "";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtContract);
                    Connection.Close();
                }
                for (int i = 0; i < dtContract.Rows.Count; i++)
                {
                    name = dtContract.Rows[i]["Status Name"].ToString();
                    if (name == "Contract Signed")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.ContractSigned = "0";
                        }
                        else
                        {
                            contract_summary.ContractSigned = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                    else if (name == "Contract Declined")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.ContractDeclined = "0";
                        }
                        else
                        {
                            contract_summary.ContractDeclined = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                    else if (name == "Contract Sent")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.ContractUnsigned = "0";
                        }
                        else
                        {
                            contract_summary.ContractUnsigned = dtContract.Rows[i]["Value"].ToString();
                        }

                    }
                    else if (name == "Lead Created")
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Value"].ToString()))
                        {
                            contract_summary.LeadSaved = "0";
                        }
                        else
                        {
                            contract_summary.LeadSaved = dtContract.Rows[i]["Value"].ToString();
                        }
                    }
                }
                if (string.IsNullOrEmpty(contract_summary.ContractSigned))
                {
                    contract_summary.ContractSigned = "0";
                }
                if (string.IsNullOrEmpty(contract_summary.ContractDeclined))
                {
                    contract_summary.ContractDeclined = "0";
                }
                if (string.IsNullOrEmpty(contract_summary.ContractUnsigned))
                {
                    contract_summary.ContractUnsigned = "0";
                }
                if (string.IsNullOrEmpty(contract_summary.LeadSaved))
                {
                    contract_summary.LeadSaved = "0";
                }
                if (dtContract.Rows.Count <= 0)
                {
                    contract_summary.ContractSigned = "0";
                    contract_summary.ContractDeclined = "0";
                    contract_summary.ContractUnsigned = "0";
                    contract_summary.LeadSaved = "0";
                }
                decimal val = (Decimal.Parse(contract_summary.ContractSigned) / (Decimal.Parse(contract_summary.ContractUnsigned) + Decimal.Parse(contract_summary.ContractDeclined))) * 100;
                contract_summary.Conversion = Convert.ToString(Math.Truncate(val * 100) / 100);
                if (string.IsNullOrEmpty(contract_summary.Conversion))
                {
                    contract_summary.Conversion = "0";
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dtContract = null;
                name = null;
                Month = null;
                Year = null;
                channelid = null;
            }
            return contract_summary;
        }

        //Get Monthly Summary
        public void GetMonthlySummary (string ChannelId,string Year)
        {
            dtContract = new DataTable();
            string name = "";
           
            try
            {
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_MonthlySummary  " + ChannelId + "," + Year + "";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtContract);
                    Connection.Close();
                }
                Channel1 = new List<string>();
                Channel2 = new List<string>();
                Channel3 = new List<string>();
                Channel4 = new List<string>();
                for (int i = 0; i < dtContract.Rows.Count; i++)
                {
                    name = dtContract.Rows[i]["Status Name"].ToString();
                    if (name == "Contract Signed")
                    {
                       
                        for(int k=1; k<13;k++)
                        {
                            if(string.IsNullOrEmpty(dtContract.Rows[i][k].ToString()))
                            {
                                Channel1.Add("0");
                            }
                            else
                            { 
                           Channel1.Add(dtContract.Rows[i][k].ToString());
                            }
                        }
                    }
                    else if (name == "Contract Declined")
                    {
                        
                        for (int k = 1; k < 13; k++)
                        {
                            if (string.IsNullOrEmpty(dtContract.Rows[i][k].ToString()))
                            {
                                Channel2.Add("0");
                            }
                            else
                            {
                                Channel2.Add(dtContract.Rows[i][k].ToString());
                            }
                        }
                    }
                    else if (name == "Contract Sent")
                    {
                        
                        for (int k = 1; k < 13; k++)
                        {
                            if (string.IsNullOrEmpty(dtContract.Rows[i][k].ToString()))
                            {
                                Channel3.Add("0");
                            }
                            else
                            {
                                Channel3.Add(dtContract.Rows[i][k].ToString());
                            }
                        }
                    }
                    else if (name == "Lead Created")
                    {
                       
                        for (int k = 1; k < 13; k++)
                        {
                            if (string.IsNullOrEmpty(dtContract.Rows[i][k].ToString()))
                            {
                                Channel4.Add("0");
                            }
                            else
                            {
                                Channel4.Add(dtContract.Rows[i][k].ToString());
                            }
                        }
                    }
                }
                if (dtContract.Rows.Count <= 0)
                {
                   
                    Channel1.Add("0");
                    Channel2.Add("0");
                    Channel3.Add("0");
                    Channel4.Add("0");
                }
                if (Channel1.Count == 0)
                {
                    Channel1 = new List<string>();
                    Channel1.Add("0");
                }
                if (Channel2.Count == 0)
                {
                    Channel2 = new List<string>();
                    Channel2.Add("0");
                }
                if (Channel3.Count == 0)
                {
                    Channel3 = new List<string>();
                    Channel3.Add("0");
                }
                if (Channel4.Count == 0)
                {
                    Channel4 = new List<string>();
                    Channel4.Add("0");
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dtContract = null;
                name = null;
                ChannelId = null;
                Year = null;
            }
        }

        //Get Daily Summary
        public void GetDailySummary(string month, string Year,string contract)
        {
            dtContract = new DataTable();
            
            try
            {
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_GetDailyChannelSummary  '" + Year + "','" + month + "','"+ contract + "'";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtContract);
                    Connection.Close();
                }
               
                
                DateList = new List<string>();
               
               
                if (contract == "Contract Signed")
                {
                    ChannelDaily1 = new List<string>();
                    for (int i = 0; i < dtContract.Rows.Count; i++)
                    {
                        // name = dtContract.Rows[i]["Name"].ToString();


                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Count"].ToString()))
                        {
                            ChannelDaily1.Add("0");
                        }
                        else
                        {
                            ChannelDaily1.Add(dtContract.Rows[i]["Count"].ToString());
                        }
                        DateList.Add(dtContract.Rows[i]["Date"].ToString());
                    }
                }
                 if (contract == "Contract Sent")
                {
                    ChannelDaily2 = new List<string>();
                    for (int i = 0; i < dtContract.Rows.Count; i++)
                    {
                        if (string.IsNullOrEmpty(dtContract.Rows[i]["Count"].ToString()))
                    {
                        ChannelDaily2.Add("0");
                    }
                    else
                    {
                        ChannelDaily2.Add(dtContract.Rows[i]["Count"].ToString());
                    }
                        DateList.Add(dtContract.Rows[i]["Date"].ToString());
                    }
                   


                }
                if (dtContract.Rows.Count <= 0)
                {
                    ChannelDaily1.Add("0");
                    ChannelDaily2.Add("0");
                    DateList.Add("0");
                }
                
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dtContract = null;
               
                month = null;
                Year = null;
                contract = null;
            }

        }

        //Get Web Campaign
        public List<WebCampaignTrend> GetWebCampaign(string startDate,string endDate,string landingId)
        {
            dtContract = new DataTable();
            dtCampaigns = new DataTable();
            list_webcampaign = new List<WebCampaignTrend>();
           list_dates = new List<string>();
            campaignNames = new List<string>();
            try
            {
                if (landingId == "0")
                {
                    using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                    {
                        Connection.Open();
                        string query = "exec proc_r_WebCampaignList";
                        SqlDataAdapter sqladap = new SqlDataAdapter(query, Connection);
                        sqladap.Fill(dtCampaigns);
                        for (int i = 0; i < dtCampaigns.Rows.Count; i++)
                        {
                            campaignNames.Add(dtCampaigns.Rows[i]["Name"].ToString());
                            string qry = "exec proc_r_GetWebCampaignsDaily  '" + startDate + "','" + endDate + "'," + dtCampaigns.Rows[i]["ID"].ToString() + "";
                            SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                            adap.Fill(dtContract);
                            if(i==0)
                            { 
                            for (int j = 0; j < dtContract.Rows.Count; j++)
                            {
                                list_dates.Add(dtContract.Rows[j]["Date"].ToString());
                            }
                            }
                            for (int k = 0; k < dtContract.Rows.Count; k++)
                            {
                                web_campaign = new WebCampaignTrend();
                                web_campaign.Name = dtCampaigns.Rows[i]["Name"].ToString();
                                web_campaign.Count = dtContract.Rows[k]["Count"].ToString();
                                list_webcampaign.Add(web_campaign);
                            }
                            dtContract = null;
                            dtContract = new DataTable();
                        }
                            Connection.Close();
                    }
                   
                }
                else
                {
                    using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                    {
                        Connection.Open();
                        string qry = "exec proc_r_GetWebCampaignsDaily  '" + startDate + "','" + endDate + "'," + landingId + "";
                        SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                        adap.Fill(dtContract);
                        Connection.Close();
                    }
                    for (int i = 0; i < dtContract.Rows.Count; i++)
                    {
                        if(i==0)
                        {
                            campaignNames.Add(dtContract.Rows[i]["Name"].ToString());
                        }
                        list_dates.Add(dtContract.Rows[i]["Date"].ToString());
                        web_campaign = new WebCampaignTrend();
                        web_campaign.Name = dtContract.Rows[i]["Name"].ToString();
                        web_campaign.Count = dtContract.Rows[i]["Count"].ToString();
                        list_webcampaign.Add(web_campaign);
                    }
                }
            }
            catch(Exception ex)
            {

            }
            finally
            {
                dtContract = null;
                startDate = null;
                endDate = null;
                landingId = null;
            }
            return list_webcampaign;
        }

        //Web Campaign Table
        public DataTable GetWebCampaignTable(string startDate, string endDate, string landingId)
        {
            WebCampaignCount = 0;
            dtWebCampaigns = new DataTable();
            dtCampaigns = new DataTable();
            try
            {
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_GetWebCampaignsDailyTable  '" + startDate + "','" + endDate + "'," + landingId + "";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtCampaigns);
                    Connection.Close();
                }
                for (int i = 0; i <= dtCampaigns.Rows.Count; i++)
                {
                    dtWebCampaigns.Columns.Add();
                }
                for (int i = 0; i < dtCampaigns.Columns.Count; i++)
                {
                    dtWebCampaigns.Rows.Add();
                    dtWebCampaigns.Rows[i][0] = dtCampaigns.Columns[i].ColumnName;
                }
                for (int i = 0; i < dtCampaigns.Columns.Count; i++)
                {
                    for (int j = 0; j < dtCampaigns.Rows.Count; j++)
                    {
                        if(string.IsNullOrEmpty(dtCampaigns.Rows[j][i].ToString()))
                        {
                            dtWebCampaigns.Rows[i][j + 1] = "0";
                        }
                        else
                        {
                            if (i > 0)
                            {
                                WebCampaignCount = Convert.ToInt32(dtCampaigns.Rows[j][i].ToString()) + WebCampaignCount;
                            }
                            dtWebCampaigns.Rows[i][j + 1] = dtCampaigns.Rows[j][i];
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
            finally
            {
                dtCampaigns = null;
                startDate = null;
                endDate = null;
                landingId = null;    
            }
            return dtWebCampaigns;
        }

        //All Reports
        public DataTable GetReports(string fromDate, string toDate, string salesrep, string callcenter, string city, string state, string county, string leadstatus)
        {
            dtAllReports = new DataTable();
            string query = "";
            try
            {
                query = "SELECT L.DateCreated as [CreateDate],U.FirstName+'' ''+U.LastName as [CreatedSalesRep],C.ChannelName as [CallCenter],"
                  +"L.DateModified as [UpdatedDate],L.UserModified as [UpdatedBy],P.PartnerName as [UpdatedCallCenter],L.FirstName as [First Name],L.LastName as [Last Name],L.PrimaryEmail as [Email ID],"
			        + "L.MobileNo as [Mobile],A.AddressInformation as [Address],L.UtilityAccountNo as [Utility Account No],LP.LandingPageName as [Campaign],A.City as [City],"
                    + "A.State as [State],A.County as [County],A.Zipcode as [ZipCode],LS.StatusName as [Status],LM.DateCreated as [Signed Date],"
			    +"L.EnvelopeId as [Envelope],L.LeadSourceId as [LeadSource],L.RefferedBy as [ReferredBy] FROM Lead L "
                +"INNER JOIN[User] U ON U.UserId = L.UserId INNER JOIN Channel C ON C.ChannelId = L.ChannelId INNER JOIN Address A ON "
                +"L.ServiceAddressId = A.AddressId INNER JOIN LandingPage LP ON LP.LandingPageId = L.LandingPageId INNER JOIN LeadLeadStatusMapping LM "
               + " ON LM.LeadId = L.LeadId INNER JOIN LeadStatus LS ON LS.LeadStatusId = LM.LeadStatusId INNER JOIN LeadUserMappingHistory LMH ON LMH.LeadId=L.LeadId INNER JOIN Partner P ON P.PartnerId=U.PartnerId WHERE C.ChannelName != ''Web Enrollment - Landing Page''"
                + " AND C.ChannelName != ''D2D'' AND A.City is not null and A.City != '''' AND A.County is not null and A.County != '''' AND LM.IsCurrent = 1 AND LMH.UserId=U.UserId"
                +" AND LP.LandingPageId != 1 AND LS.StatusName is not null and LS.StatusName != '''' AND A.State is not null and A.State != ''''";
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {
                    
                }
                else
                {
                    query += "AND(L.DateModified between ''" + fromDate + "'' and ''" + toDate + "'')";
                }
                if(!string.IsNullOrEmpty(salesrep))
                { 
                query += "AND  U.UserId="+salesrep;
                }
                if (!string.IsNullOrEmpty(callcenter))
                {
                    query += "AND C.ChannelId=" + callcenter;
                }
                if ((city!="All")&&(!string.IsNullOrEmpty(city)))
                {
                    query += "AND A.City=''" + city+"''";
                }
                if ((state!="All")&& (!string.IsNullOrEmpty(state)))
                {
                    query += "AND A.State=''" + state+"''";
                }
                if ((county!="All") && (!string.IsNullOrEmpty(county)))
                {
                    query += "AND A.County=''" + county+"''";
                }
                if (!string.IsNullOrEmpty(leadstatus))
                {
                    query += "AND LS.LeadStatusId=" + leadstatus;
                }
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_AllReport  '" + query + "'";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtAllReports);
                    Connection.Close();
                }
            }
            catch (Exception ex)
            {
               
            }
            finally
            {
                query = null;
                fromDate = null;
                toDate = null;
                salesrep = null;
                callcenter = null;
                city = null;
                state = null;
                county = null;
                leadstatus = null;
            }
            return dtAllReports;
        }

        //Call Center Reports
        public DataTable GetCallCenterReports(string fromDate, string toDate, string salesrep, string callcenter, string city, string state, string county, string leadstatus)
        {
            dtAllReports = new DataTable();
            string query = "";
            try
            {
                query = "SELECT L.DateCreated as [CreateDate],U.FirstName+'' ''+U.LastName as [CreatedSalesRep],C.ChannelName as [CallCenter],"
                  + "L.DateModified as [UpdatedDate],L.UserModified as [UpdatedBy],P.PartnerName as [UpdatedCallCenter],L.FirstName as [First Name],L.LastName as [Last Name],L.PrimaryEmail as [Email ID],"
                    + "L.MobileNo as [Mobile],A.AddressInformation as [Address],L.UtilityAccountNo as [Utility Account No],LP.LandingPageName as [Campaign],A.City as [City],"
                    + "A.State as [State],A.County as [County],A.Zipcode as [ZipCode],LS.StatusName as [Status],LM.DateCreated as [Signed Date],"
                + "L.EnvelopeId as [Envelope],L.LeadSourceId as [LeadSource],L.RefferedBy as [ReferredBy] FROM Lead L "
                + "INNER JOIN[User] U ON U.UserId = L.UserId INNER JOIN Channel C ON C.ChannelId = L.ChannelId INNER JOIN Address A ON "
                + "L.ServiceAddressId = A.AddressId INNER JOIN LandingPage LP ON LP.LandingPageId = L.LandingPageId INNER JOIN LeadLeadStatusMapping LM "
               + " ON LM.LeadId = L.LeadId INNER JOIN LeadStatus LS ON LS.LeadStatusId = LM.LeadStatusId INNER JOIN LeadUserMappingHistory LMH ON LMH.LeadId=L.LeadId INNER JOIN Partner P ON P.PartnerId=U.PartnerId  WHERE C.ChannelName  like''%Call Center%'' OR C.ChannelName like''%CallCenter%''"
                + "  AND A.City is not null and A.City != '''' AND A.County is not null and A.County != '''' AND LM.IsCurrent = 1 AND "
                + "   LP.LandingPageId != 1 AND LS.StatusName is not null and LS.StatusName != '''' AND A.State is not null and A.State != ''''";
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {

                }
                else
                {
                    query += "AND(L.DateModified between ''" + fromDate + "'' and ''" + toDate + "'')";
                }
                if (!string.IsNullOrEmpty(salesrep))
                {
                    query += "AND  U.UserId=" + salesrep;
                }
                if (!string.IsNullOrEmpty(callcenter))
                {
                    query += "AND L.ChannelId=" + callcenter;
                }
                if ((city != "All") && (!string.IsNullOrEmpty(city)))
                {
                    query += "AND A.City=''" + city + "''";
                }
                if ((state != "All") && (!string.IsNullOrEmpty(state)))
                {
                    query += "AND A.State=''" + state + "''";
                }
                if ((county != "All") && (!string.IsNullOrEmpty(county)))
                {
                    query += "AND A.County=''" + county + "''";
                }
                if (!string.IsNullOrEmpty(leadstatus))
                {
                    query += "AND LS.LeadStatusId=" + leadstatus;
                }
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_AllReport  '" + query + "'";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtAllReports);
                    Connection.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                query = null;
                fromDate = null;
                toDate = null;
                salesrep = null;
                callcenter = null;
                city = null;
                state = null;
                county = null;
                leadstatus = null;
            }
            return dtAllReports;
        }

        //WebEnrollment Reports
        public DataTable GetReportWebEnrollment(string fromdate,string todate, string city, string state, string county, string leadstatus)
        {
            dtWebEnrollment = new DataTable();
            string query = "";
            try
            {
                query = "SELECT L.DateCreated as [CreateDate],LP.LandingPageName as [Source],U.UserId as [SalesRepID],M.EmailId as [MemberShipID],  C.ChannelName as [CallCenter],"
                  + "L.DateModified as [UpdatedDate],L.UserModified as [UpdatedBy],"
                    + "L.FirstName as [First Name],L.LastName as [Last Name],L.PrimaryEmail as [Email ID],L.MobileNo as [Mobile],"
                    + "A.AddressInformation as [Address],L.UtilityAccountNo as [Utility Account No],LP.LandingPageName as [Campaign],"
                + "A.City as [City],A.State as [State],A.County as [County],A.Zipcode as [ZipCode],LS.StatusName as [Status],LM.DateCreated as [Signed Date]"
                + "FROM Lead L INNER JOIN[User] U ON U.UserId = L.UserId INNER JOIN Channel C ON C.ChannelId = L.ChannelId "
                + "INNER JOIN Address A ON L.ServiceAddressId = A.AddressId INNER JOIN LandingPage LP ON LP.LandingPageId = L.LandingPageId "
               + " INNER JOIN LeadLeadStatusMapping LM  ON LM.LeadId = L.LeadId INNER JOIN LeadStatus LS ON LS.LeadStatusId = LM.LeadStatusId "
                + "  INNER JOIN Membership M ON M.MembershipId=U.MembershipId WHERE  C.ChannelName != ''D2D'' AND A.City is not null and A.City != '''' AND "
                + "    A.County is not null and A.County != '''' AND LM.IsCurrent = 1  AND LP.LandingPageId != 1 AND LS.StatusName is not null and LS.StatusName != '''' AND A.State is not null and A.State != ''''  ";
                if (string.IsNullOrEmpty(fromdate) || string.IsNullOrEmpty(todate))
                {

                }
                else
                {
                    query += "AND(L.DateModified between ''" + fromdate + "'' and ''" + todate + "'')";
                }
                if ((city != "All") && (!string.IsNullOrEmpty(city)))
                {
                    query += "AND A.City=''" + city + "''";
                }
                if ((state != "All") && (!string.IsNullOrEmpty(state)))
                {
                    query += "AND A.State=''" + state + "''";
                }
                if ((county != "All") && (!string.IsNullOrEmpty(county)))
                {
                    query += "AND A.County=''" + county + "''";
                }
                if (!string.IsNullOrEmpty(leadstatus))
                {
                    query += "AND LS.LeadStatusId=" + leadstatus;
                }
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_AllReport  '" + query + "'";
                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtWebEnrollment);
                    Connection.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                query = null;
                fromdate = null;
                todate = null;
               
                city = null;
                state = null;
                county = null;
                leadstatus = null;
            }
            return dtWebEnrollment;
        }

        //WebCampaign Reports
        public DataTable GetReportWebCampaigns(string name)
        {
            dtWebCampaigns = new DataTable();
            try
            { 
            using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
            {
                Connection.Open();
                string qry = "exec proc_r_GetWebCampaigns  '" + name + "'";
                SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                adap.Fill(dtWebCampaigns);
                Connection.Close();
            }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                name = null;
            }
            return dtWebCampaigns;
        }

        public UserDetails ForgetPassword(string Email)
        {
            dtLogin = new DataTable();
            user_details = new UserDetails();
            try
            {
                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_ForgotPassword  '" + Email + "'";

                    SqlDataAdapter adap = new SqlDataAdapter(qry, Connection);
                    adap.Fill(dtLogin);
                    Connection.Close();
                }
                for (int i = 0; i < dtLogin.Rows.Count; i++)
                {
                    user_details.UserId = dtLogin.Rows[i]["ID"].ToString();
                    user_details.UserName = dtLogin.Rows[i]["UserName"].ToString();
                }
            }
            catch (Exception ex)
            {
                user_details.UserId = "0";
                user_details.UserName = "Error";
                return user_details;
            }
            finally
            {
                Email = null;
                dtLogin = null;
            }
            return user_details;
        }

        public bool SendGridSendEmail(string toEmail, string msg, string subject)
        {
            try
            {

                var username = "jems2013";
                var password = "mobility2013";
                var transportInstance = SMTP.GetInstance(new NetworkCredential(username, password));
                var message = SendGrid.GetInstance(); 

                message.Subject = subject;
                message.From = new MailAddress("telesalessupport@sunsharecorp.com");
                message.AddTo(toEmail);
                message.Html = msg;
                
                //send the mail
                transportInstance.Deliver(message);
                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string ChangePassword(string Email, string Password)
        {
            string returnValue = "";
            try
            {

                using (SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString))
                {
                    Connection.Open();
                    string qry = "exec proc_r_ChangePassword  '" + Email + "','" + Password + "'";
                    SqlCommand adap = new SqlCommand(qry, Connection);
                    int val = adap.ExecuteNonQuery();
                    Connection.Close();
                    if (val == 1)
                    {
                        returnValue = "Success";
                    }
                    else
                    {
                        returnValue = "Failure";
                    }
                }

            }
            catch (Exception ex)
            {
                returnValue = "Error";
            }
            finally
            {
                Email = null;
                Password = null;
            }
            return returnValue;
        }

    }
    
    public class ContractSummary
    {
        public string ContractDeclined { get; set; }
        public string ContractUnsigned { get; set; }
        public string ContractSigned { get; set; }
        public string LeadSaved { get; set; }
        public string Conversion { get; set; }
        public string Month { get; set; }
    }

    public class WebCampaignTrend
    {
        public string Name { get; set; }
        public string Count { get; set; }
    }


    public class UserDetails
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Status { get; set; }
    }
}