<?php
include "../header.php";
include "../logic/individualCharts_logic.php";
$partnermonthvalue = 'all';
list($total_currentmonth_leadcount,$total_currentmonth_contractUnsignedCount, $total_currentmonth_contractSignedCount,$total_currentmonth_contractDeclinedCount) = $chartValues->currentmonthContracts($partnermonthvalue,$mysqliportal);


?>

<head>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 
 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  
 <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<!--<script src="https://code.highcharts.com/themes/dark-unica.js"></script>-->
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />

<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	


	<style>
	h3, .h3 {
	font-size: 24px;
	font-weight : 700;
	color : #3071A9;
	} 
	
	</style>
<script>
 $(function() {
	 total_currentmonth_leadcount = " <?php echo $total_currentmonth_leadcount; ?>";
	total_currentmonth_contractUnsignedCount  = " <?php echo $total_currentmonth_contractUnsignedCount ; ?>"; 
	total_currentmonth_contractSignedCount  = " <?php echo $total_currentmonth_contractSignedCount ; ?>"; 
	total_currentmonth_contractDeclinedCount= " <?php echo $total_currentmonth_contractDeclinedCount ; ?>"; 
	 currentmonth_chart(total_currentmonth_leadcount,total_currentmonth_contractUnsignedCount,total_currentmonth_contractSignedCount,total_currentmonth_contractDeclinedCount);	
	 $("#programCurrentMonth").change(function(){
		var partnerCurrentmonthValue = $("#programCurrentMonth").val();
		$.post('reports/ajax/partnerIndividualAjax.php', {partnerCurrentmonthValue:partnerCurrentmonthValue}, function(data){
			
			res_currentmonth = data.split("_"); 
			total_currentmonth_leadcount = res_currentmonth[0]; 
			total_currentmonth_contractUnsignedCount = res_currentmonth[1];
			total_currentmonth_contractSignedCount = res_currentmonth[2];
			total_currentmonth_contractDeclinedCount=res_currentmonth[3];
			currentmonth_chart(total_currentmonth_leadcount,total_currentmonth_contractUnsignedCount,total_currentmonth_contractSignedCount,total_currentmonth_contractDeclinedCount);	
		});
	});	
 }); 
 
</script>
 
<script>
//No of Contracts Signed/Unsigned & Leads Saved Current Month Summary of Contracts
	function currentmonth_chart(total_currentmonth_leadcount,total_currentmonth_contractUnsignedCount,total_currentmonth_contractSignedCount,total_currentmonth_contractDeclinedCount)
	{
		
		var c1leadcount=total_currentmonth_leadcount.split(",");
		var len_c1=c1leadcount.length;
		var totalcurrentleadcount=new Array();
		for(i=0; i<len_c1; i++)
			totalcurrentleadcount.push(parseInt(c1leadcount[i]));  
		 
		var c1contractUnsignedCount=total_currentmonth_contractUnsignedCount.split(",");
		var len_c2=c1contractUnsignedCount.length;
		var totalCurrentcontractUnsignedCount=new Array();
		for(i=0; i<len_c2; i++)
			totalCurrentcontractUnsignedCount.push(parseInt(c1contractUnsignedCount[i]));  
		
		var c1contractSignedCount=total_currentmonth_contractSignedCount.split(",");
		var len_c3=c1contractSignedCount.length;
		var totalCurrentcontractSignedCount=new Array();
		for(i=0; i<len_c3; i++)
			totalCurrentcontractSignedCount.push(parseInt(c1contractSignedCount[i])); 
		
		var c1contractDeclinedCount=total_currentmonth_contractDeclinedCount.split(",");
		var len_c4=c1contractDeclinedCount.length;
		var totalCurrentcontractDeclinedCount=new Array();
		for(i=0; i<len_c4; i++)
			totalCurrentcontractDeclinedCount.push(parseInt(c1contractDeclinedCount[i])); 
		
		//alert(totalCurrentcontractSignedCount);
		// Radialize the colors
		Highcharts.setOptions({
          colors: ['#483D8B','#ffad33','#37ADB4','#DF435A']
        }); 
       Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
         };
		});
		 
		   chart = new Highcharts.Chart({
                chart: {
					plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
					renderTo: 'chart_pie',
                    //backgroundColor: 'rgba(0,0,0,0)',
                    y:100

                },
                title: {
                    text: 'Current Month Summary of Contracts Signed/Unsigned/Declined & Leads Saved'
                },
                yAxis: {
                    title: {
                        text: ' '
                    }
                },
				credits: {
                    enabled: false
                 },
                plotOptions: {
                    pie: {
						// y:1,
                        shadow: false,
						//center: ['50%', '50%'],
                        borderWidth: 0,
						allowPointSelect: true,
                        showInLegend: false,
                        size: '90%',
                        innerSize: '0%',
						cursor: 'pointer',
						 
                        data: [
								{
								name: 'Contracts Declined',
								y: parseInt(totalCurrentcontractDeclinedCount),
							   // sliced: true
								},{
								name: 'Contracts Signed',
								y: parseInt(totalCurrentcontractSignedCount),
							   // sliced: true
								}, {
								name: 'Contracts Unsigned',
								y:parseInt(totalCurrentcontractUnsignedCount),
							   
								},
								{
								name: 'Leads Saved',
								y: parseInt(totalcurrentleadcount),//Math.round(+amt_process_var),
								}
							]
                    }
                },
                tooltip: {
                    //valueSuffix: '%'
                    valueSuffix: ''
                },
                series: [
                    {
                        type: 'pie',
                        name: 'Total',
						showInLegend: true,		
                        dataLabels: {
                            color:'black',
                            distance: -30,
                            formatter: function () {
                                if(this.percentage!=0)  return Math.round(this.percentage)  + '%';

                            }
                        }
                    }
                    
                ]
            });
	}
	
</script>
</head>
<body>

<section id="container" >
     
      <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> Current Month Summary</h3>
				<div class="container" align="left">
				    <form  role="form">
								<div class="row">
									<div  class="col-md-4" >
									</div>
									<div  class="col-md-6" style="margin-left:-90px;">
										<span style="font-size:20px;"></span>
									</div>		
									<!--</form>-->
									<div class="col-sm-2" style="margin-left:80px;">
									<select name="programCurrentMonth" id="programCurrentMonth" class="form-control col-lg-2">
										<option value="all">All</option>
										<option value="10001">The Results</option>
										<option value="10002">PCCW</option>
										<option value="10003">SunShare Sales</option>
										<option value="10007">Web</option>
									</select>
									</div>
								</div>
					</form>
				</div>
              <!-- page start-->
			 
                <div class="tab-pane" id="chartjs" style="margin-top:-10px;">
                    <div class="row mt">
						<div class="col-lg-12">
                            <div class="content-panel" id="linechart_all">
								<div class="panel-body text-center">
							        <div id="chart_pie" class="panel-body text-center" style="height:450px">
									
							        </div>
									
                                </div>
                            </div>
						   
                        </div>
                    </div>
				 </div>
                 
              <!-- page end-->
		</section>          
	</section><!-- /MAIN CONTENT -->
</section>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css" />
<?php
include "../footer.php";
?>
