<?php
include ("../header.php");
include("../logic/reports_call_logic.php");
?>

 <script src="datatablefilter_column.js"></script>
 
 <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
<script src="http://code.jquery.com/jquery-1.12.0.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <!--<script type="text/javascript"  src="http://jquery-datatables-column-filter.googlecode.com/svn/trunk/media/js/jquery.dataTables.columnFilter.js"></script>-->
 
    <script src="jquery.dataTables.columnFilter.js"></script>
  
  <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/dt/dt-1.10.10/datatables.min.css"/>-->
   <link rel="stylesheet" href="jquery.dataTables.min.css">
    <!--<link href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" rel="stylesheet">-->
 
     <link rel="stylesheet" href="<?php echo CSS_FOLDER_PATH ?>datatable-buttons.css" rel="stylesheet">
	 
  <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
   <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
   <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
   <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
   <script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
<style>
h3, .h3 {
	font-size: 24px;
	font-weight : 700;
	color : #3071A9;
}
.color-line {
    background: #f7f9fa linear-gradient(to right, #34495e, #34495e 25%, #9b59b6 25%, #9b59b6 35%, #3498db 35%, #3498db 45%, #62cb31 45%, #62cb31 55%, #ffb606 55%, #ffb606 65%, #e67e22 65%, #e67e22 75%, #e74c3c 85%, #e74c3c 85%, #c0392b 85%, #c0392b 100%) no-repeat scroll 50% 100% / 100% 6px;
    height: 6px;
}

</style>

<script>

/* $(document).ready(function() {
	$('#seconddiv1').hide();
    $('#example').dataTable({ 
          "sScrollY" : "200",
          "sScrollX" : "100%",
          "bScrollCollapse" : true,
          "bSort" : false,
		  "iDisplayLength": 10
         // "sPaginationType" : "full_numbers",
         // "iDisplayLength" : 25,
         // "bLengthChange" : false
	}).columnFilter({ 	sPlaceHolder: "head:after",
					aoColumns: [null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					{ type: "select"},
				    	 		{ type: "select" },
								{ type: "select" },
								null,
                                { type: "date-range" },
								{ type: "select" }
						    ]

		});;
	
});  */
$(document).ready(function() {
	$('#seconddiv1').hide();
	//$('#city').addClass('form-control');
    $('#example').dataTable({ 
	 dom: 'Bfrtip',
        buttons: [
             { extend: 'excelHtml5', text: 'Export' }
        ],
          "sScrollY" : "200",
          "sScrollX" : "100%",
          "bScrollCollapse" : true,
          "bSort" : false,
		  "iDisplayLength": 10
         // "sPaginationType" : "full_numbers",
         // "iDisplayLength" : 25,
         // "bLengthChange" : false
	}).columnFilter({ sPlaceHolder: "head:before",	
		aoColumns: [
		        { sSelector: "#sno", type:"null"  },
				{ sSelector: "#fname",type:"null" },
				
				{ sSelector: "#lname",type:"null"},
				{ sSelector: "#emailid",type:"null"},
				{ sSelector: "#address",type:"null"},
				{ sSelector: "#excelacc",type:"null"},
				{ sSelector: "#campaign",type:"null"},
				{ sSelector: "#mobileno",type:"null"},
		        { sSelector: "#city",type:"select"},
				{ sSelector: "#state",type:"select"},
				{ sSelector: "#country",type:"select"},
				{ sSelector: "#zipcode",type:"null"},
				{ sSelector: "#startdate",type:"date-range"},
                { sSelector: "#leadstatus",type:"select"}
		]

	});
	
	/* $("#example_range_from_13").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			maxDate: new Date,
			onClose: function( selectedDate ) {
				$( "#example_range_to_13" ).datepicker( "option", "minDate", selectedDate );
			}
	    });
	  
	  $("#example_range_to_13").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			maxDate: new Date,
			//onClose: function( selectedDate ) {
				//	$( "#from" ).datepicker( "option", "minDate", selectedDate );
				//}
		}); */
	
});
</script>
<!--main content start-->
 <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Web Enrollment</h3>
          	<div class="row mt" style="margin-top:-18px;">
				<div class="col-lg-12">
					<div class="form-panel">
						 <div style="width:100%;border:1.5px solid black;width:100%;text-align:center;padding:30px;margin-left:2px;margin-right:2px;margin-top:1px;" align="center">
						   <div class="row">
								<label id="sno" ></label>
								<label id="fname"></label>
								  <label id="lname"></label>
								<label id="emailid"></label>
								<label id="address"></label>
							   <label id="excelacc"></label>
								 <label id="campaign"></label>
								 <label id="mobileno"></label>
								 <div class="row">
								 <div class="col-sm-offset-3 col-sm-6">
									<span class="select-date" >Select Date Range: </span> 
											<label id="startdate"></label>
								  </div>
								 <div class="col-sm-12">
									<div class="col-sm-3">
								   <label >Select City: </label> <br><label id="city" class="col-sm-12"></label>
								    </div>
									<div class="col-sm-3">
								  <label class="">Select State:</label> <label id="state" class="col-sm-12"></label>
								  </div>
								  <div class="col-sm-3">
									<label class="">Select County :</label> <label id="country" class="col-sm-12"></label>
								  </div>
								  <div class="col-sm-3">
									<label class="">Select Lead Status : </label> <label id="leadstatus" class="col-sm-12" ></label>
								  </div>
								   <label id="zipcode"></label>
								   </div>
							   </div>
							   
							    
					  </div>
					  </div>
				</div>	
			</div>
		</div>	
          	<div class="row mt" style="margin-top:-18px;">
          		<div class="col-lg-12">
                  <div class="form-panel">
				    <div id="firstdiv1">
				 
                      <table id="example" class="table table-striped table-bordered display" cellspacing="0" width="100%">  
					<thead>
						<tr>
							<th style="font-size:13px;">Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">Country</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Date</th>
							<th style="font-size:13px;">Lead Status</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Last Modified</th>
							<th style="font-size:13px;">Last Modified Date</th>
						</tr>
					</thead>
					
       
				<tbody>
				<?php 
				//echo $reportValues->reports_webenroll($mysqliportal);
				echo $reportValues->reports_webenroll_testing($mysqliportal);
				?>
				
				</tbody>
				</table>
                  </div>
				   <div id="seconddiv1" style="disply:none" >
				 <form method="post">
					
					   <div  style="margin-left:920px;margin-top:0px"><a class="fa fa-lg btn btn-success btn-sm" name="excelbtn" id="excelbtn"  style="color:white">Get Excel</a> &nbsp;<button class="fa fa-arrow-circle-left fa-lg  btn btn-primary btn-sm"  name="backbtn" id="backbtn"  style="color:white"> All</button> <span></span></div>
					   <br/>
				  <div id="datatable1">
				 
							  </div>
				 </div>
				  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
			
			
			<!-- SORTABLE TO DO LIST -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

<?php include "../footer.php" ?>
