<?php
include "../header.php";
//include ("../config/database.php");
include "../logic/dashboard_logic.php";

list($total_leadcount, $total_contractUnsignedCount, $total_contractSignedCount,$total_contractDeclinedCount,$totalmaxdate,$totalmindate) = $dashboardValues->overallSummary_contractsign_Unsign_LeadSaved($mysqliportal);
$totalmaxdates = date('F - d -Y',strtotime($totalmaxdate)); 
$totalmindates = date('F - d -Y',strtotime($totalmindate)); 
/* print_r($total_leadcount);
exit; */
list($re_results_leadsavedsum, $re_results_contractUnsignedsum, $re_results_contractSignedsum,$re_results_contractDeclinedsum, $pccw_results_leadsavedsum, $pccw_results_contractUnsignedsum, $pccw_results_contractSignedsum,$pccw_results_contractDeclinedsum,$sun_results_leadsavedsum,$sun_results_contractUnsignedsum,$sun_results_contractSignedsum,$sun_results_contractDeclinedsum,$web_results_leadsavedsum,$web_results_contractUnsignedsum,$web_results_contractSignedsum,$web_results_contractDeclinedsum) = $dashboardValues->overallSummary_Contracts($mysqliportal);

$coversion_per_results =  (($re_results_contractSignedsum)/($re_results_contractUnsignedsum+$re_results_contractDeclinedsum))*100;
$coversion_per_pccw =  (($pccw_results_contractSignedsum)/($pccw_results_contractUnsignedsum+$pccw_results_contractDeclinedsum))*100;
$coversion_per_sun =  (($sun_results_contractSignedsum)/($sun_results_contractUnsignedsum+$sun_results_contractDeclinedsum))*100;
$coversion_per_web =  (($web_results_contractSignedsum)/($web_results_contractUnsignedsum+$web_results_contractDeclinedsum))*100;

$partnermonthlyvalue = 'all';
list($total_monthly_months,$total_monthly_leadcount, $total_monthly_contractUnsignedCount, $total_monthly_contractSignedCount,$total_monthly_contractDeclinedCount) = $dashboardValues->monthlySummary_yearContracts($partnermonthlyvalue,$mysqliportal);

$partnermonthvalue = 'all';
list($total_currentmonth_leadcount,$total_currentmonth_contractUnsignedCount, $total_currentmonth_contractSignedCount,$total_currentmonth_contractDeclinedCount) = $dashboardValues->currentmonthContracts($partnermonthvalue,$mysqliportal);

$cmonth = date('F Y'); 
list($total_monthDates, $total_daily_contractUnsignedCount, $total_daily_contractSignedCount, $showMonth) = $dashboardValues->dailyContracts($cmonth,$mysqliportal);
/* print_r($total_monthDates);
exit; */
?>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<!--<script src="https://code.highcharts.com/themes/dark-unica.js"></script>-->
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

 <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	 
 
	
	
	
<style>
h3, .h3 {
	font-size: 24px;
	font-weight : 700;
	color : #3071A9;
} 
 .ui-datepicker-calendar {
    display: none ;
 }
 td {
	 color:black;
 }
.ui-datepicker-trigger {
   position: absolute;
   z-index: 1000;
}
</style>

<script>

$(function() {
	total_leadcount = " <?php echo $total_leadcount; ?>";
	total_contractUnsignedCount = " <?php echo $total_contractUnsignedCount; ?>";
	total_contractSignedCount = " <?php echo $total_contractSignedCount; ?>";
    total_contractDeclinedCount=" <?php echo $total_contractDeclinedCount; ?>";
   	totalmaxdates=" <?php echo $totalmaxdates; ?>";
	totalmindates=" <?php echo $totalmindates; ?>";
	
	/* re_results_leadsavedsum = " <?php echo $re_results_leadsavedsum; ?>";
	re_results_contractUnsignedsum = " <?php echo $re_results_contractUnsignedsum; ?>";
	re_results_contractSignedsum  = " <?php echo $re_results_contractSignedsum ; ?>"; 
	pccw_results_leadsavedsum = " <?php echo $pccw_results_leadsavedsum; ?>";
	pccw_results_contractUnsignedsum = " <?php echo $pccw_results_contractUnsignedsum; ?>";
	pccw_results_contractSignedsum = " <?php echo $pccw_results_contractSignedsum; ?>";
    sun_results_leadsavedsum=$sun_results_leadsavedsum
	sun_results_contractUnsignedsum=$sun_results_contractUnsignedsum
	sun_results_contractSignedsum=$sun_results_contractSignedsum
	$sun_results_contractDeclinedsum,$web_results_leadsavedsum,$web_results_contractUnsignedsum,$web_results_contractSignedsum,$web_results_contractDeclinedsum	 */
	
	total_monthly_months = " <?php echo $total_monthly_months; ?>";
	total_monthly_leadcount = " <?php echo $total_monthly_leadcount; ?>";
	total_monthly_contractUnsignedCount  = " <?php echo $total_monthly_contractUnsignedCount ; ?>"; 
	total_monthly_contractSignedCount  = " <?php echo $total_monthly_contractSignedCount ; ?>";
    total_monthly_contractDeclinedCount  = " <?php echo $total_monthly_contractDeclinedCount ; ?>";	
	
	
	total_currentmonth_leadcount = " <?php echo $total_currentmonth_leadcount; ?>";
	total_currentmonth_contractUnsignedCount  = " <?php echo $total_currentmonth_contractUnsignedCount ; ?>"; 
	total_currentmonth_contractSignedCount = " <?php echo $total_currentmonth_contractSignedCount ; ?>"; 
	total_currentmonth_contractDeclinedCount = "<?php echo $total_currentmonth_contractDeclinedCount; ?>";
	
	total_monthDates = " <?php echo $total_monthDates; ?>";
	total_daily_contractUnsignedCount  = " <?php echo $total_daily_contractUnsignedCount; ?>"; 
	total_daily_contractSignedCount  = " <?php echo $total_daily_contractSignedCount; ?>"; 
	showMonth  = " <?php echo $showMonth; ?>"; 
	
	overal_chart1(total_monthly_months,total_monthly_leadcount,total_monthly_contractUnsignedCount,total_monthly_contractSignedCount,total_monthly_contractDeclinedCount);
	firstChart1(total_leadcount, total_contractUnsignedCount, total_contractSignedCount,total_contractDeclinedCount,totalmaxdates,totalmindates);
	//firstChart2();
	daily_chart(total_monthDates,total_daily_contractUnsignedCount,total_daily_contractSignedCount,showMonth);
	currentmonth_chart(total_currentmonth_leadcount,total_currentmonth_contractUnsignedCount,total_currentmonth_contractSignedCount,total_currentmonth_contractDeclinedCount);	
	//firstChart2();
	
	
  $('#monthpick').datepicker({
        changeMonth: true,
        changeYear: true,
		showOn: 'button', 
		//buttonImage: 'img/cal.gif',
		buttonImage: 'http://tools.infinitelinked.com/saleshubdashboard_new/includes/image/calendar.gif',
      buttonImageOnly: true, 
	  buttonText: 'Open calendar',
        dateFormat: 'MM yy',
         onClose: function(dateText, inst) { 
           var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
           var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
			var month_select = $("#monthpick").val();
			//alert(month_select);
			 $.post('reports/ajax/partnerMonthlyAjax.php', {month_select:month_select}, function(data){
				res_daily = data.split("_"); 
				total_monthDates = res_daily[0]; 
				total_daily_contractUnsignedCount = res_daily[1]; 
				total_daily_contractSignedCount = res_daily[2];
				showMonth = res_daily[3];
				daily_chart(total_monthDates,total_daily_contractUnsignedCount,total_daily_contractSignedCount,showMonth)
				//document.writeln(data);
			});	
			
		}
    });  
	
	$('#pick1').datepicker( {
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
         onClose: function(dateText, inst) { 
           var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
           var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
	 }
    }); 
	$("#partnermonthly").change(function(){
		var partnermonthlyvalue = $("#partnermonthly").val();
		//alert(partnermonthlyvalue);
		$.post('reports/ajax/partnerMonthlyAjax.php', {partnermonthlyvalue:partnermonthlyvalue}, function(data){
			//alert(data);
		res_monthly = data.split("_"); 
		total_monthly_months = res_monthly[0]; 
		total_monthly_leadcount = res_monthly[1]; 
		total_monthly_contractUnsignedCount = res_monthly[2];
		total_monthly_contractSignedCount = res_monthly[3];
		total_monthly_contractDeclinedCount = res_monthly[4];
		 
		overal_chart1(total_monthly_months,total_monthly_leadcount,total_monthly_contractUnsignedCount,total_monthly_contractSignedCount,total_monthly_contractDeclinedCount);
		
		});
	});
	$("#programCurrentMonth").change(function(){
		var partnerCurrentmonthValue = $("#programCurrentMonth").val();
		$.post('reports/ajax/partnerMonthlyAjax.php', {partnerCurrentmonthValue:partnerCurrentmonthValue}, function(data){
			res_currentmonth = data.split("_"); 
			total_currentmonth_leadcount = res_currentmonth[0]; 
			total_currentmonth_contractUnsignedCount = res_currentmonth[1];
			total_currentmonth_contractSignedCount = res_currentmonth[2];
			total_currentmonth_contractDeclinedCount=res_currentmonth[3];
			currentmonth_chart(total_currentmonth_leadcount,total_currentmonth_contractUnsignedCount,total_currentmonth_contractSignedCount,total_currentmonth_contractDeclinedCount);	
		});
	});	
	/* $('#pick').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		dateFormat: 'MM yy'
	}).focus(function() {
		var thisCalendar = $(this);
		$('.ui-datepicker-calendar').detach();
		$('.ui-datepicker-close').click(function() {
var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
thisCalendar.datepicker('setDate', new Date(year, month, 1));
		});
	});
	
	$('#pick1').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		dateFormat: 'MM yy'
	}).focus(function() {
		var thisCalendar = $(this);
		$('.ui-datepicker-calendar').detach();
		$('.ui-datepicker-close').click(function() {
var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
thisCalendar.datepicker('setDate', new Date(year, month, 1));
		});
	}); */
});



</script>
<script>

	function daily_chart(total_monthDates,total_daily_contractUnsignedCount,total_daily_contractSignedCount,showMonth)
	{
		var pro=total_monthDates.split(",");
		var len=pro.length;
		var totalmonthdates=new Array();
		for(i=0; i<len; i++)
		totalmonthdates.push(pro[i]);
        //alert(total_monthDates);	
		
		var pro1=total_daily_contractUnsignedCount.split(",");
		var len1=pro1.length;
		var totalDailyContractUnsignedCount=new Array();
		for(i=0; i<len1; i++)
		totalDailyContractUnsignedCount.push(parseInt(pro1[i])); 
			
		var pro2=total_daily_contractSignedCount.split(",");
		var len2=pro2.length;
		var totaldDailyContractSignedCount=new Array();
		for(i=0; i<len2; i++)
		totaldDailyContractSignedCount.push(parseInt(pro2[i])); 
	
		var pro3=showMonth.split(",");
		var len3=pro3.length;
		var showMonthVal=new Array();
		for(i=0; i<len3; i++)
		showMonthVal.push(pro3[i]); 
		
		Highcharts.setOptions({
          colors: ['#F6BB6B','#7CB5EC','#ff8080']
        }); 
       Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
         };
		});
  
   
   //Contract Signed/Unsigned & Leads Saved – Daily Summary
   $('#dailychart').highcharts({
		 chart: {
                type: 'column',
            },
        title: {
            text: 'Contracts Signed/Unsigned – Daily Summary',
            x: -	20 //center
        },
        /* subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        } ,*/
        xAxis: {
            categories: totalmonthdates,
			title: {
                text: showMonthVal
            }
        },
		credits:
		{
			enabled : false
		},
        /* yAxis: {
            title: {
                text: 'Counts'
            },
			//min: 0,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        }, */
		yAxis: {
            min: 0,
            title: {
                text: 'Counts'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
		 legend: {
            align: 'center',
            x: 30,
            verticalAlign: 'bottom',
            y: 10,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            valueSuffix: ''
        },
        /* legend: {
            layout: 'horizontal',
            //align: 'top',
            verticalAlign: 'bottom',
            borderWidth: 0
        }, */
		plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            },
			row: {
				colorByPoint: true
			}
        },
		colors: [
				'#e4d354',
                '#2b908f'
				],
        series: [ {
            name: 'Contracts Unsigned',
            data: totalDailyContractUnsignedCount
        },{
            name: 'Contracts Signed',
            data: totaldDailyContractSignedCount
        } ]
    });
   

	}
	//Contract Signed/Unsigned & Leads Saved Monthly Summary
	function overal_chart1(total_monthly_months,total_monthly_leadcount,total_monthly_contractUnsignedCount,total_monthly_contractSignedCount,total_monthly_contractDeclinedCount)
	{
		
		
		var t1monthcount=total_monthly_months.split(",");
		var len_c1=t1monthcount.length;
		var totalmonthshow=new Array();
		for(i=0; i<len_c1; i++)
			totalmonthshow.push(t1monthcount[i]); 
		
		var c1monthlyleadcount=total_monthly_leadcount.split(",");
		var len_c1=c1monthlyleadcount.length;
		var totalmonthlyleadcount=new Array();
		for(i=0; i<len_c1; i++)
			totalmonthlyleadcount.push(parseInt(c1monthlyleadcount[i])); 
		
		
		var c1contractMonthlyUnsignedCount=total_monthly_contractUnsignedCount.split(",");
		var len_c2=c1contractMonthlyUnsignedCount.length;
		var total_monthly_contractUnsignedCount=new Array();
		for(i=0; i<len_c2; i++)
			total_monthly_contractUnsignedCount.push(parseInt(c1contractMonthlyUnsignedCount[i])); 
	
		var c1contract_monthly_SignedCount=total_monthly_contractSignedCount.split(",");
		var len_c3=c1contract_monthly_SignedCount.length;
		var total_monthly_contractSignedCount=new Array();
		for(i=0; i<len_c3; i++)
			total_monthly_contractSignedCount.push(parseInt(c1contract_monthly_SignedCount[i]));

        var c1contract_monthly_DeclinedCount=total_monthly_contractDeclinedCount.split(",");
		var len_c4=c1contract_monthly_DeclinedCount.length;
		var total_monthly_contractDeclinedCount=new Array();
		for(i=0; i<len_c4; i++)
			total_monthly_contractDeclinedCount.push(parseInt(c1contract_monthly_DeclinedCount[i]));
		
			
		Highcharts.setOptions({
          colors: ['#483D8B', '#FFB400', '#568FC6', '#86b300']
      });
		$('#container5').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Contracts Signed/Unsigned/Declined & Leads Saved Monthly Summary'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
			categories: totalmonthshow,
            crosshair: true,
			title: {
                text: 'Months -'+<?php echo date('Y'); ?>
            }
        },
			credits: {
                enabled: false
            },
        yAxis: {
            min: 0,
            title: {
                text: 'Counts'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            //pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                //'<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                //pointPadding: 0.2,
				pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Contracts Declined',
            data: total_monthly_contractDeclinedCount

        }, {
            name: 'Contracts Signed',
            data: total_monthly_contractSignedCount

        }, {
            name: 'Contracts Unsigned',
            data: total_monthly_contractUnsignedCount

        }, {
            name: 'Leads Saved',
            data: totalmonthlyleadcount

        }]
    }); 	
	
	}	
	
		//Overall Summary of Contract Signed/Unsigned & Leads Saved
	function firstChart1(total_leadcount, total_contractUnsignedCount, total_contractSignedCount,total_contractDeclinedCount,totalmaxdates,totalmindates)
	{
		var c1leadcount=total_leadcount.split(",");
		var len_c1=c1leadcount.length;
		var totalleadcount=new Array();
		for(i=0; i<len_c1; i++)
			totalleadcount.push(parseInt(c1leadcount[i])); 
		
		var c1contractUnsignedCount=total_contractUnsignedCount.split(",");
		var len_c2=c1contractUnsignedCount.length;
		var totalcontractUnsignedCount=new Array();
		for(i=0; i<len_c2; i++)
			totalcontractUnsignedCount.push(parseInt(c1contractUnsignedCount[i])); 
		
		var c1contractSignedCount=total_contractSignedCount.split(",");
		var len_c3=c1contractSignedCount.length;
		var totalcontractSignedCount=new Array();
		for(i=0; i<len_c3; i++)
			totalcontractSignedCount.push(parseInt(c1contractSignedCount[i]));

        var c1contractDeclinedCount=total_contractDeclinedCount.split(",");
		var len_c4=c1contractDeclinedCount.length;
		var totalcontractDeclinedCount=new Array();
		for(i=0; i<len_c4; i++)
			totalcontractDeclinedCount.push(parseInt(c1contractDeclinedCount[i]));		
		
		/* Highcharts.setOptions({
			lang: {
			thousandsSep: ','
		   },
          colors: ['#000000','#FBA438','#7CB5EC','#C0C0C0']
        });  */
       Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
         };
		});
		 
		$('#chart3').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Summary of Contracts Signed/Unsigned/Declined & Leads Saved'
        },
		subtitle: {
                text: totalmindates+' To '+totalmaxdates
            },
        xAxis: {
            categories: ['Contracts Declined','Contracts Signed', 'Contracts Unsigned', 'Leads Saved']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Counts'
            }
        },
        legend: {
            reversed: true
        },
		credits: {
			enabled : false
		},
        plotOptions: {
			series: {
				colorByPoint: true
			}
        },
		colors: [
		        '#483D8B',
                '#2b908f',
				'#e4d354',
                '#568FC6'
			],
        series: [{
			showInLegend: false,
            name: 'Total',
            data: [totalcontractDeclinedCount,totalcontractSignedCount,totalcontractUnsignedCount,totalleadcount]
			}]
		});
	}
	
	//No of Contracts Signed/Unsigned & Leads Saved Current Month Summary of Contracts
	function currentmonth_chart(total_currentmonth_leadcount,total_currentmonth_contractUnsignedCount,total_currentmonth_contractSignedCount,total_currentmonth_contractDeclinedCount)
	{
		
		var c1leadcount=total_currentmonth_leadcount.split(",");
		var len_c1=c1leadcount.length;
		var totalcurrentleadcount=new Array();
		for(i=0; i<len_c1; i++)
			totalcurrentleadcount.push(parseInt(c1leadcount[i]));  
		 
		var c1contractUnsignedCount=total_currentmonth_contractUnsignedCount.split(",");
		var len_c2=c1contractUnsignedCount.length;
		var totalCurrentcontractUnsignedCount=new Array();
		for(i=0; i<len_c2; i++)
			totalCurrentcontractUnsignedCount.push(parseInt(c1contractUnsignedCount[i]));  
		
		var c1contractSignedCount=total_currentmonth_contractSignedCount.split(",");
		var len_c3=c1contractSignedCount.length;
		var totalCurrentcontractSignedCount=new Array();
		for(i=0; i<len_c3; i++)
			totalCurrentcontractSignedCount.push(parseInt(c1contractSignedCount[i])); 
		
		var c1contractDeclinedCount=total_currentmonth_contractDeclinedCount.split(",");
		var len_c4=c1contractDeclinedCount.length;
		var totalCurrentcontractDeclinedCount=new Array();
		for(i=0; i<len_c4; i++)
			totalCurrentcontractDeclinedCount.push(parseInt(c1contractDeclinedCount[i]));
		//alert(totalCurrentcontractSignedCount);
		// Radialize the colors
		Highcharts.setOptions({
          colors: ['#483D8B','#ffad33','#37ADB4','#DF435A']
        }); 
       Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
         };
		});
		 
		   chart = new Highcharts.Chart({
                chart: {
					plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
					renderTo: 'chart_pie',
                    //backgroundColor: 'rgba(0,0,0,0)',
                    y:100

                },
                title: {
                    text: 'Current Month Summary of Contracts Signed/Unsigned/Declined & Lead Saved'
                },
                yAxis: {
                    title: {
                        text: ' '
                    }
                },
				credits: {
                    enabled: false
                 },
                plotOptions: {
                    pie: {
						// y:1,
                        shadow: false,
						//center: ['50%', '50%'],
                        borderWidth: 0,
						allowPointSelect: true,
                        showInLegend: false,
                        size: '90%',
                        innerSize: '0%',
						cursor: 'pointer',
						 
                        data: [
								{
								name: 'Contracts Declined',
								y: parseInt(totalCurrentcontractDeclinedCount),
							   // sliced: true
								},{
								name: 'Contracts Signed',
								y: parseInt(totalCurrentcontractSignedCount),
							   // sliced: true
								},{
								name: 'Contracts Unsigned',
								y:parseInt(totalCurrentcontractUnsignedCount),
							   
								},
								{
								name: 'Leads Saved',
								y: parseInt(totalcurrentleadcount),//Math.round(+amt_process_var),
								}
							]
                    }
                },
                tooltip: {
                    //valueSuffix: '%'
                    valueSuffix: ''
                },
                series: [
                    {
                        type: 'pie',
                        name: 'Total',
						showInLegend: true,		
                        dataLabels: {
                            color:'black',
                            distance: -30,
                            formatter: function () {
                                if(this.percentage!=0)  return Math.round(this.percentage)  + '%';
                                 //return Math.round(this.percentage)  + '%';

                            }
                        }
                    }
                    
                ]
            });
	}
	
	</script>
  </head>
	
  <body>

  <section id="container" >
     
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> Dashboard</h3>
				
              <!-- page start-->
              <div class="tab-pane" id="chartjs">
			  <div class="row mt">
			  <div class="col-lg-6">
                          <div class="content-panel">
							 
                              <div class="panel-body text-center">
                                  <!--<canvas id="polarArea" height="300" width="400"></canvas>-->
								
								 <div class="carousel-inner">
								<div class="item active">
									<div id='chart3' style="height:350px"></div>
								</div>
								
							</div>
							 
                             </div>
                          </div>
                      </div>
			  
                      <div class="col-lg-6">
                          <div class="content-panel">
							 <h4 align="center" style="color:black">Summary of Contracts    <span style="font-size:15px;">(<?php echo $totalmindates.' To '.$totalmaxdates; ?>)</span></h4>
							    <div class="panel-body text-center" style="height:340px">
                                
									<table style="height:95%;" class="table" border="2">
										<thead style="font-size:15px;" >
										  <tr style="background-color:#D4D4D4;color:black;border: 2px solid #000;">
											<th>Name</th>
											<th>Leads Saved</th>
											<th>Contracts Sent</th>
											<th>Contracts Signed</th>
											<th>Contracts Declined</th>
											<th>Conversion Percentage</th>	
										  </tr>
										</thead>
										<tbody align="left" >
										<tr style="background-color:#FBFBFB;font-family:bold;font-size:15px;border: 2px solid #000;">
											<td>THE RESULTS</td>
											<td style="padding-left:20px;"><?php echo number_format($re_results_leadsavedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($re_results_contractUnsignedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($re_results_contractSignedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($re_results_contractDeclinedsum); ?></td>
											<td style="padding-left:20px;"><?php echo round($coversion_per_results,2); ?> %</td>
											
										  </tr>
										  <tr style="background-color:#FBFBFB;font-family:bold;font-size:15px;border: 2px solid #000;">
											<td>PCCW</td>
											<td style="padding-left:20px;"><?php echo number_format($pccw_results_leadsavedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($pccw_results_contractUnsignedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($pccw_results_contractSignedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($pccw_results_contractDeclinedsum); ?></td>
											<td style="padding-left:20px;"><?php echo round($coversion_per_pccw,2); ?> %</td>
											
										  </tr>
										  
										   <tr style="background-color:#FBFBFB;font-family:bold;font-size:15px;border: 2px solid #000;">
											<td>SUNSHARE SALES</td>
											<td style="padding-left:20px;"><?php echo number_format($sun_results_leadsavedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($sun_results_contractUnsignedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($sun_results_contractSignedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($sun_results_contractDeclinedsum); ?></td>
											<td style="padding-left:20px;"><?php echo round($coversion_per_sun,2); ?> %</td>
											
										  </tr>
										   <tr style="background-color:#FBFBFB;font-family:bold;font-size:15px;border: 2px solid #000;">
											<td>WEB</td>
											<td style="padding-left:20px;"><?php echo number_format($web_results_leadsavedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($web_results_contractUnsignedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($web_results_contractSignedsum); ?></td>
											<td style="padding-left:20px;"><?php echo number_format($web_results_contractDeclinedsum); ?></td>
											<td style="padding-left:20px;"><?php echo round($coversion_per_web,2); ?> %</td>
											
										  </tr>
										   
										  
										</tbody>
									 </table>
									</div>
                          </div>
                      </div>
                      
                  </div>
			  
			  
					
                    <div class="row mt">
                      <div class="col-lg-6">
                          <div class="content-panel">
							  <div class="col-lg-4"></div>
							  <div align="right" class="col-lg-8">
							    <div class="col-lg-6" style="font-size:15px;"> <!-- <b>Select Program</b>--></div>
								  <div class="col-lg-6">
									<!--<select name="program" class="form-control col-lg-2">
										<option value="">All</option>
										<option value="results">Results</option>
										<option value="pccw">PCCW</option>
									</select>-->
									<select name="partnermonthly" id="partnermonthly"class="form-control col-lg-2">
										<option value="all">All</option>
										<option value="10001">The Results</option>
										<option value="10002">PCCW</option>
										<option value="10003">SunShare Sales</option>
										<option value="10007">Web</option>
									</select>
								  </div>	
							  </div>
                              <div class="panel-body text-center">
                                  <!--<canvas id="radar" height="70px" width="300"></canvas>-->
								  <div id="container5" class="panel-body text-center" style="height:400px">
							  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="content-panel">
							 <div class="col-lg-4"></div>
							  <div align="right" class="col-lg-8">
							    <div class="col-lg-6" style="font-size:15px;"> <!-- <b>Select Program</b>--></div>
								  <div class="col-lg-6">
									<!--<input type="text" name="pick1" id="pick1" class="form-control col-lg-2">-->
									<select name="programCurrentMonth" id="programCurrentMonth" class="form-control col-lg-2">
										<option value="all">All</option>
										<option value="10001">The Results</option>
										<option value="10002">PCCW</option>
										<option value="10003">SunShare Sales</option>
										<option value="10007">Web</option>
									</select>
								  </div>	
							  </div>
                              <div class="panel-body text-center">
                                 
								
								 <div class="carousel-inner">
								<div class="item active">
									<div id='chart_pie' style="height:380px"></div>
								</div>
								
							</div>
							 
                             </div>
                          </div>
                      </div>
                  </div>
					<div class="row mt">
                      <div class="col-lg-12">
                          <div class="content-panel" style="height:430px">
							  <div class="col-lg-4"></div>
							  <div align="right" class="col-lg-8">
							    <div class="col-lg-8" style="font-size:15px;"> <!-- <b>Select Program</b>--></div>
								  <div class="col-lg-4">
									 <input type="text" name="monthpick" id="monthpick" class="form-control col-lg-2" value="<?php echo $cmonth;?>">
								  </div>	
							  </div>
                              <!--<div class="panel-body text-center">
                                  <canvas id="radar" height="70px" width="300"></canvas>-->
								  <div  id="dailychart" class="panel-body text-center" style="height:400px;margin-bottom:70px;">
							  </div>
                            <!-- </div>-->
                          </div>
                      </div>
                      <div class="col-lg-3">
                          
                      </div>
                  </div>
              </div>
              <!-- page end-->
          </section>          
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      <!--footer end-->


    <!-- js placed at the end of the document so the pages load faster -->
   <!-- <script src="Theme/assets/js/jquery.js"></script>
    <script src="Theme/assets/js/jquery-1.8.3.min.js"></script>
    <script src="Theme/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="Theme/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="Theme/assets/js/jquery.scrollTo.min.js"></script>
    <script src="Theme/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="Theme/assets/js/jquery.sparkline.js"></script>-->


    <!--common script for all pages-->
    <!--<script src="Theme/assets/js/common-scripts.js"></script>-->
    
    <!-- <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script> -->
    <!--<script type="text/javascript" src="Theme/assets/js/gritter-conf.js"></script>-->

    <!--script for this page-->
    <!--<script src="Theme/assets/js/sparkline-chart.js"></script>    
	<script src="Theme/assets/js/zabuto_calendar.js"></script>	-->
	
	<script type="text/javascript">
         /* $(document).ready(function () {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome to Dashgum!',
            // (string | mandatory) the text inside the notification
            text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
            // (string | optional) the image to display on the left
            image: 'assets/img/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
        });

        return false;
        });  */
	</script>
	
	<script type="application/javascript">
         /* $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }  */
    </script>
  <?php include "../footer.php" ?>

