<?php
include ("../header.php");
include("../logic/reports_call_logic.php");
?>
 <script src="datatablefilter_column.js"></script>
 <link rel="stylesheet" href="jquery.dataTables.min.css">
<script src="http://code.jquery.com/jquery-1.12.0.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <!--<script type="text/javascript"  src="http://jquery-datatables-column-filter.googlecode.com/svn/trunk/media/js/jquery.dataTables.columnFilter.js"></script>-->
 
    <script src="jquery.dataTables.columnFilter.js"></script>
<!-- For datatable excel button--> 
<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>

<link rel="stylesheet" href="<?php echo CSS_FOLDER_PATH ?>datatable-buttons.css" rel="stylesheet">
 <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css"> -->
<style>
h3, .h3 {
	font-size: 24px;
	font-weight : 700;
	color : #3071A9;
} 
.color-line {
    background: #f7f9fa linear-gradient(to right, #34495e, #34495e 25%, #9b59b6 25%, #9b59b6 35%, #3498db 35%, #3498db 45%, #62cb31 45%, #62cb31 55%, #ffb606 55%, #ffb606 65%, #e67e22 65%, #e67e22 75%, #e74c3c 85%, #e74c3c 85%, #c0392b 85%, #c0392b 100%) no-repeat scroll 50% 100% / 100% 6px;
    height: 6px;
}


</style>
 <script>
  $(function() {
    $( "#startdate" ).datepicker();
	$( "#enddate" ).datepicker();
  });
  </script>
<script>
$(document).ready(function() {
	
    /* $('#example').DataTable({
		  "sScrollY" : "200",
          "sScrollX" : "100%",
          "bScrollCollapse" : true,
          "bSort" : false,
		  "iDisplayLength": 10,
		  dom: 'Bfrtip',
          buttons: [
            'excel'
        ]
         // "sPaginationType" : "full_numbers",
         // "iDisplayLength" : 25,
         // "bLengthChange" : false
	}); */
	
	$('#example').dataTable({ 
	 dom: 'Bfrtip',
        buttons: [
             { extend: 'excelHtml5', text: 'Export' }
        ],
          "sScrollY" : "200",
          "sScrollX" : "100%",
          "bScrollCollapse" : true,
          "bSort" : false,
		  "iDisplayLength": 10
         // "sPaginationType" : "full_numbers",
         // "iDisplayLength" : 25,
         // "bLengthChange" : false
	}).columnFilter({ sPlaceHolder: "head:before",	
		aoColumns: [
				{ sSelector: "#salesrep",type:"select"},
				{ sSelector: "#callcenter",type:"select"},
				{ sSelector: "#fname",type:"null" },
				
				{ sSelector: "#lname",type:"null"},
				{ sSelector: "#emailid",type:"null"},
				{ sSelector: "#address",type:"null"},
				
				{ sSelector: "#campaign",type:"null"},
				{ sSelector: "#mobileno",type:"null"},
		        
		       
		        { sSelector: "#city",type:"select"},
				{ sSelector: "#state",type:"select"},
				{ sSelector: "#country",type:"select"},
				{ sSelector: "#zipcode",type:"null"},
				{ sSelector: "#startdate",type:"date-range"},
                { sSelector: "#leadstatus",type:"select"}
		]

	});
	
	
});
</script>

	 
	 <style>
 option:empty
{
 display:none;
}
 </style>
<!--main content start-->
 <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Call Center</h3>
          	
          	<!-- SIMPLE TO DO LIST -->
			<div class="row mt" style="margin-top:-18px;">
				<div class="col-lg-12">
					<div class="form-panel">
						<div style="width:100%;border:1.5px solid black;width:100%;text-align:center;padding:30px;margin-left:2px;margin-right:2px;margin-top:1px;" align="center">
							<!--<div class="row">
								<label id="sno" ></label>
								 <label id="salesrepid"></label>
							   <label id="loginid"></label>
								  <label id="fname"></label>
								<label id="lname"></label>
								<label id="emailid"></label>
								<label id="address"></label>
							   <label id="excelacc"></label>
								 <label id="campaign"></label>
								 <label id="mobileno"></label>
								 <div class="row">
								 <div class="col-sm-12">
									<div class="col-sm-3">
									<label class="" >Select Date Range: </label> 
											<label id="startdate" class="col-sm-12"></label>
								  </div>
									<div class="col-sm-3">
								   <label >Select Salesrep: </label> <br><label id="salesrep" class="col-sm-12"></label>
								    </div>
									<div class="col-sm-3">
								  <label class="">Select Call center:</label> <label id="callcenter" class="col-sm-12"></label>
								  </div>
								  <div class="col-sm-3">
									<label class="">Select City:</label> <label id="city" class="col-sm-12"></label>
								  </div>
								  
								   
								  
								   <label id="zipcode"></label>
								   </div>
							   </div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									
								  </div>
									<div class="col-sm-3">
									<label class="">Select State: </label> <label id="state" class="col-sm-12" ></label>
								  </div>
									<div class="col-sm-2">
								   <label >Select Country: </label> <br><label id="country" class="col-sm-12"></label>
								    </div>
									<div class="col-sm-2">
								  <label class="">Select Leads Status:</label> <label id="leadstatus" class="col-sm-12"></label>
								  </div>
								  
								  <div class="col-sm-3">
									
								  </div>
							</div>-->
							 <div class="row">
								  <label id="fname"></label>
								<label id="lname"></label>
								<label id="emailid"></label>
								<label id="address"></label>
							  
								 <label id="campaign"></label>
								 <label id="mobileno"></label>
								 <div class="row">
								 <div class="col-sm-offset-3 col-sm-6">
									<span class="select-date" >Select Date Range: </span> 
											<label id="startdate"></label>
								  </div>
								 <div class="col-sm-12">
									<div class="col-sm-1">
									</div>
									<div class="col-sm-3">
								    <label >Select Sales Rep: </label> <br><label id="salesrep" class="col-sm-12"></label>
								    </div>
									<div class="col-sm-3">
								  <label >Select Call Center:</label> <label id="callcenter" class="col-sm-12"></label>
								  </div>
								  <div class="col-sm-3">
									<label class="">Select City:</label> <label id="city" class="col-sm-12"></label>
								  </div>
								  
								   <label id="zipcode"></label>
								   </div>
							   </div>
							   <br/>
							   <div class="row">
								 
								 <div class="col-sm-12">
								 <div class="col-sm-1">
									</div>
									<div class="col-sm-3">
								    <label >Select State: </label> <label id="state" class="col-sm-12" ></label>
								    </div>
									<div class="col-sm-3">
								  <label >Select County: </label> <br><label id="country" class="col-sm-12"></label>
								  </div>
								  <div class="col-sm-3">
									 <label class="">Select Leads Status:</label> <label id="leadstatus" class="col-sm-12"></label>
								  </div>
								  
								   <label id="zipcode"></label>
								   </div>
							   </div>
							    
					  </div>
						</div>
					</div>
				</div>
			</div>
				<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt" style="margin-top:-18px;">
          		<div class="col-lg-12">
                  <div class="form-panel">
				<div id="firstdiv">
					  <!--<form method="post" action="report_call_excel.php">
						<input type="submit" name="btn_excel" id="btn_excel" class="fa fa-lg btn btn-success btn-sm pull-right" value="Get Excel"  /> 
						<br><br>
					  </form>-->
                      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">  
					<thead>
						<tr>
							
							<th style="font-size:13px;">Sales Rep</th>
							<th style="font-size:13px;">Call Center</th>
							<th style="font-size:13px;">First Name</th>
							<th style="font-size:13px;">Last Name</th>
							<th style="font-size:13px;">Email ID</th>
							<th style="font-size:13px;">Address</th>
							<th style="font-size:13px;">Campaign Type/Preferences</th>
							<th style="font-size:13px;">Mobile No</th>
							<th style="font-size:13px;">City</th>
							<th style="font-size:13px;">State</th>
							<th style="font-size:13px;">Country</th>
							<th style="font-size:13px;">Zipcode</th>
							<th style="font-size:13px;">Date</th>
							<th style="font-size:13px;">Lead Status</th>
							<th style="font-size:13px;">Lead Source</th>
							<th style="font-size:13px;">Last Modified</th>
							<th style="font-size:13px;">Last Modified Date</th>
							<!--<th style="font-size:13px;">Contract Signed</th>-->
						</tr>
					</thead>
       
				<tbody>
				<?php 
				//echo $reportValues->reports_call($mysqliportal);
				echo $reportValues->reports_all($mysqliportal);
				?>
				
				</tbody>
				</table>
				 </div>
				 
				 
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
			<!-- SORTABLE TO DO LIST -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

<?php include "../footer.php" ?>
