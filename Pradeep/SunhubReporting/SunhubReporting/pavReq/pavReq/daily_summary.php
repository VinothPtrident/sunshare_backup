<?php
include "../header.php";
include "../logic/individualCharts_logic.php";
$cmonth = date('F Y'); 
list($total_monthDates, $total_daily_contractUnsignedCount, $total_daily_contractSignedCount, $showMonth) = $chartValues->dailyContracts($cmonth,$mysqliportal);



?>

<head>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 
 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  
 <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<!--<script src="https://code.highcharts.com/themes/dark-unica.js"></script>-->
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />

<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	


	
	<style>
h3, .h3 {
	font-size: 24px;
	font-weight : 700;
	color : #3071A9;
} 
 .ui-datepicker-calendar {
    display: none ;
 }
 td {
	 color:black;
 }

</style>
<script>
 $(function() {
	total_monthDates = " <?php echo $total_monthDates; ?>";
	total_daily_contractUnsignedCount  = " <?php echo $total_daily_contractUnsignedCount; ?>"; 
	total_daily_contractSignedCount  = " <?php echo $total_daily_contractSignedCount; ?>"; 
	showMonth  = " <?php echo $showMonth; ?>"; 
	daily_chart(total_monthDates,total_daily_contractUnsignedCount,total_daily_contractSignedCount,showMonth);
	 $('#monthpick').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
         onClose: function(dateText, inst) { 
           var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
           var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
			var month_select = $("#monthpick").val();
			 $.post('reports/ajax/partnerIndividualAjax.php', {month_select:month_select}, function(data){
				res_daily = data.split("_"); 
				total_monthDates = res_daily[0]; 
				total_daily_contractUnsignedCount = res_daily[1]; 
				total_daily_contractSignedCount = res_daily[2];
				showMonth = res_daily[3];
				daily_chart(total_monthDates,total_daily_contractUnsignedCount,total_daily_contractSignedCount,showMonth)
				//document.writeln(data);
			});	
			
		}
    });  
 }); 
 
</script>
 
<script>
//No of Contracts Signed/Unsigned & Leads Saved Current Month Summary of Contracts
	function daily_chart(total_monthDates,total_daily_contractUnsignedCount,total_daily_contractSignedCount,showMonth)
	{
		var pro=total_monthDates.split(",");
		var len=pro.length;
		var totalmonthdates=new Array();
		for(i=0; i<len; i++)
		totalmonthdates.push(pro[i]); 
		
		var pro1=total_daily_contractUnsignedCount.split(",");
		var len1=pro1.length;
		var totalDailyContractUnsignedCount=new Array();
		for(i=0; i<len1; i++)
		totalDailyContractUnsignedCount.push(parseInt(pro1[i])); 
			
		var pro2=total_daily_contractSignedCount.split(",");
		var len2=pro2.length;
		var totaldDailyContractSignedCount=new Array();
		for(i=0; i<len2; i++)
		totaldDailyContractSignedCount.push(parseInt(pro2[i])); 
	
		var pro3=showMonth.split(",");
		var len3=pro3.length;
		var showMonthVal=new Array();
		for(i=0; i<len3; i++)
		showMonthVal.push(pro3[i]); 
		
		Highcharts.setOptions({
          colors: ['#F6BB6B','#7CB5EC','#ff8080']
        }); 
       Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
         };
		});
  
   
   //Contract Signed/Unsigned & Leads Saved – Daily Summary
   $('#dailychart').highcharts({
		 chart: {
                type: 'column',
            },
        title: {
            text: 'Contracts Signed/Unsigned – Daily Summary',
            x: -	20 //center
        },
        /* subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        } ,*/
        xAxis: {
            categories: totalmonthdates,
			title: {
                text: showMonthVal
            }
        },
		credits:
		{
			enabled : false
		},
        /* yAxis: {
            title: {
                text: 'Counts'
            },
			//min: 0,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        }, */
		yAxis: {
            min: 0,
            title: {
                text: 'Counts'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
		 legend: {
            align: 'center',
            x: 30,
            verticalAlign: 'bottom',
            y: 10,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            valueSuffix: ''
        },
        /* legend: {
            layout: 'horizontal',
            //align: 'top',
            verticalAlign: 'bottom',
            borderWidth: 0
        }, */
		plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            },
			row: {
				colorByPoint: true
			}
        },
		colors: [
				'#e4d354',
                '#2b908f'
				],
        series: [ {
            name: 'Contracts Unsigned',
            data: totalDailyContractUnsignedCount
        },{
            name: 'Contracts Signed',
            data: totaldDailyContractSignedCount
        } ]
    });
   

	}
	</script>
</head>
<body>

<section id="container" >
     
      <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> Daily Summary</h3>
				<div class="container" align="left">
				    <form  role="form">
								<div class="row">
									<div  class="col-md-4" >
									</div>
									<div  class="col-md-6" style="margin-left:-90px;">
										
									</div>		
									<!--</form>-->
									<div class="col-sm-2" style="margin-left:80px;">
									 <input type="text" name="monthpick" id="monthpick" class="form-control col-lg-2" value="<?php echo $cmonth;?>">
									</div>
								</div>
					</form>
				</div>
              <!-- page start-->
			 
                <div class="tab-pane" id="chartjs" style="margin-top:-10px;">
                    <div class="row mt">
						<div class="col-lg-12">
                            <div class="content-panel" id="linechart_all">
								<div class="panel-body text-center">
							        <div id="dailychart" class="panel-body text-center" style="height:450px">
									
							        </div>
									
                                </div>
                            </div>
						   
                        </div>
                    </div>
				 </div>
                 
              <!-- page end-->
		</section>          
	</section><!-- /MAIN CONTENT -->
</section>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css" />
<?php
include "../footer.php";
?>
