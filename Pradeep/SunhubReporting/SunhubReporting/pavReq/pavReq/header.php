<?php 
//include("../config/database.php");
include("config/database.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Saleshub Dashboard</title>
	 <!--external css-->
    <link href="<?php echo FONT_FOLDER_PATH ?>font-awesome.css" rel="stylesheet" />
    <!-- Bootstrap core CSS -->
    <link href="<?php echo CSS_FOLDER_PATH ?>bootstrap.css" rel="stylesheet">
    <link href="<?php echo CSS_FOLDER_PATH ?>bootstrap.min.css" rel="stylesheet">
   
    
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo CSS_FOLDER_PATH ?>style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo CSS_FOLDER_PATH ?>style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo CSS_FOLDER_PATH ?>style-conquer.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo CSS_FOLDER_PATH ?>to-do.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	.site-footer {
	  position: fixed;
	  bottom: 0;
	  width: 100%;
	  /* Set the fixed height of the footer here */
	  height: 30px;
	 
	}
	</style>
  </head>
  <body>
  <section id="container" >	
	<!--header start-->
      <header class="header black-bg">
	  <div>
			 <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="left" data-original-title="Toggle Navigation"></div>
              </div>
            <div class="page-logo">
				<a  class="logo"><img src="<?php echo IMG_FOLDER_PATH ?>elite.png" height="60px;" /></a>
			</div>
			<!--<div style="padding-top:0.4cm;font-size: 34px;color:black;font-family: Times New Roman, Times, serif;text-align: center;font-weight: 600;"> Saleshub Dashboard
			</div>-->
			<div style="padding-top:0.4cm;font-size: 34px;color:black;font-family: Times New Roman, Times, serif;text-align: center;font-weight: 600;"> 
			</div>
			 <div class="top-menu" style="margin-top:-60px;">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="http://tools.infinitelinked.com/saleshubdashboard_new/">Logout</a></li>
            	</ul>
            </div>
		</div> 
        </header>
      <!--header end-->
	   <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a><img src="<?php echo IMG1_FOLDER_PATH ?>ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">ADMIN</h5>
              	  	<?php 
$url = explode("/",$_SERVER['REQUEST_URI']);
$filename = $url[count($url)-1];

//if($filename=="dashboard_update.php")
if($filename=="dashboard_sunsharehub.php")
{

$dashboard='active';
}
else
{

$dashboard='';
}

 if($filename=="import.php")
{

$import='active';
}
else
{

$import='';
}

if($filename=="linechart.php" || $filename=="current_month_summary.php" || $filename=="daily_summary.php" || $filename=="monthly_summary.php")
{
 $reports='active';
						}
else
{

$reports='';
}
if($filename == 'linechart.php' )
{
  $report_location='active';
}
else
{
$report_location='';
} 
if($filename == 'current_month_summary.php' )
{
  $report_pie='active';
}
else
{
$report_pie='';
} 
if($filename == 'daily_summary.php' )
{
  $report_dailychart='active';
}
else
{
$report_dailychart='';
} 
if($filename == 'monthly_summary.php' )
{
  $report_barchart='active';
}
else
{
$report_barchart='';
} 
//if($filename == 'reports_call.php' || $filename == 'report_enrollment.php' )
if($filename == 'reports_call_with_filters.php' || $filename == 'report_enrollment.php' )
{
  $report_web='active';
}
else
{
$report_web='';
} 
//if($filename == 'reports_call.php' )
if($filename == 'reports_call_with_filters.php' )
{
  $report_call='active';
}
else
{
$report_call='';
} 
if($filename == 'report_enrollment.php' )
{
  $report_entrol='active';
}
else
{
$report_entrol='';
}
if($filename == 'reports_all.php' )
{
  $report_all='active';
}
else
{
$report_all='';
}
/*
if($filename=="manage_access.php")
{

$manage_access='active';
} 
else
{
$manage_access='';
}  */
?>
                  <li class="mt">
                      <a class="<?php echo $dashboard;?>" href="<?php echo BASE_PATH?>/design/dashboard_sunsharehub.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

					<!--<li class="sub-menu">
                      <a class="<?php echo $import;?>" href="<?php echo BASE_PATH?>/design/import.php" >
                          <i class="fa fa-cogs"></i>
                          <span>Import</span>
                      </a>
                  </li>-->
 
                  <li class="sub-menu <?php echo $reports ?>">
                     <a href="<?php echo BASE_PATH ?>/design/linechart.php" class="<?php echo $reports ?>">
                          <i class="fa fa-th"></i>
                          <span>Charts</span>
						  
                      </a>
					  
                      <ul class="sub">
                          <li class="<?php echo $report_location ?>"><a  href="<?php echo BASE_PATH?>/design/linechart.php" >Web Lead Trend</a></li>
						  <li class="<?php echo $report_pie ?>"><a  href="<?php echo BASE_PATH?>/design/current_month_summary.php" >Current Month Summary</a></li>
						   <li class="<?php echo $report_barchart ?>"><a  href="<?php echo BASE_PATH?>/design/monthly_summary.php" >Monthly Summary</a></li>
						    <li class="<?php echo $report_dailychart ?>"><a  href="<?php echo BASE_PATH?>/design/daily_summary.php" >Daily Summary</a></li>
                         
                      </ul>
                  </li>
				    <li class="sub-menu <?php echo $report_web ?>">
                      <!--<a href="<?php echo BASE_PATH ?>/design/reports_call.php" class="<?php echo $report_web ?>">
                          <i class="fa fa-th"></i>
                          <span>Reports</span>
                      </a>-->
					  <a href="<?php echo BASE_PATH ?>/design/reports_call_with_filters.php" class="<?php echo $report_web ?>">
                          <i class="fa fa-th"></i>
                          <span>Reports</span>
                      </a>
                      <ul class="sub">
                         <!-- <li class="<?php echo $report_call ?>"><a  href="<?php echo BASE_PATH?>/design/reports_call.php" >Call Center</a></li>-->
						 <li class="<?php echo $report_call ?>"><a  href="<?php echo BASE_PATH?>/design/reports_call_with_filters.php" >Call Center</a></li>
                          <li class="<?php echo $report_entrol ?>"><a  href="<?php echo BASE_PATH?>/design/report_enrollment.php" >Web Enrollment</a></li>
						  <li class="<?php echo $report_all ?>"><a  href="<?php echo BASE_PATH?>/design/reports_all.php" >All</a></li>
                         
                      </ul>
                  </li>
				  <!--
                  <li class="sub-menu">
                      <a class="<?php echo $manage_access;?>" href="<?php echo BASE_PATH?>/design/manage_access.php" >
                          <i class="fa fa-book"></i>
                          <span>Manage</span>
                      </a>
                  </li>-->
                </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
	   <!--main content start-->
     <!-- /MAIN CONTENT -->
	  
	   <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo JS_FOLDER_PATH ?>jquery.js"></script>
    <script src="<?php echo JS_FOLDER_PATH ?>bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo JS_FOLDER_PATH ?>jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo JS_FOLDER_PATH ?>jquery.scrollTo.min.js"></script>
    <script src="<?php echo JS_FOLDER_PATH ?>jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="<?php echo JS_FOLDER_PATH ?>common-scripts.js"></script>

    <!--script for this page-->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="<?php echo JS_FOLDER_PATH ?>tasks.js" type="text/javascript"></script>
	<script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>
  </body>
</html>
      