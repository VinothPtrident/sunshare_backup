<?php
include "../header.php";
include "../logic/individualCharts_logic.php";
$partnermonthlyvalue = 'all';
list($total_monthly_months,$total_monthly_leadcount, $total_monthly_contractUnsignedCount, $total_monthly_contractSignedCount,$total_monthly_contractDeclinedCount) = $chartValues->monthlySummary_yearContracts($partnermonthlyvalue,$mysqliportal);


?>

<head>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 
 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  
 <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<!--<script src="https://code.highcharts.com/themes/dark-unica.js"></script>-->
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />

<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	


	<style>
	h3, .h3 {
	font-size: 24px;
	font-weight : 700;
	color : #3071A9;
	} 
	
	</style>
<script>
 $(function() {
	 total_monthly_months = " <?php echo $total_monthly_months; ?>";
	total_monthly_leadcount = " <?php echo $total_monthly_leadcount; ?>";
	total_monthly_contractUnsignedCount  = " <?php echo $total_monthly_contractUnsignedCount ; ?>"; 
	total_monthly_contractSignedCount  = " <?php echo $total_monthly_contractSignedCount ; ?>"; 
	total_monthly_contractDeclinedCount=" <?php echo $total_monthly_contractDeclinedCount ; ?>";
	overal_chart1(total_monthly_months,total_monthly_leadcount,total_monthly_contractUnsignedCount,total_monthly_contractSignedCount,total_monthly_contractDeclinedCount);	
	
	$("#partnermonthly").change(function(){
		var partnermonthlyvalue = $("#partnermonthly").val();
		$.post('reports/ajax/partnerIndividualAjax.php', {partnermonthlyvalue:partnermonthlyvalue}, function(data){
		res_monthly = data.split("_"); 
		total_monthly_months = res_monthly[0]; 
		total_monthly_leadcount = res_monthly[1]; 
		total_monthly_contractUnsignedCount = res_monthly[2];
		total_monthly_contractSignedCount = res_monthly[3];
		total_monthly_contractDeclinedCount=res_monthly[4];
		overal_chart1(total_monthly_months,total_monthly_leadcount,total_monthly_contractUnsignedCount,total_monthly_contractSignedCount,total_monthly_contractDeclinedCount);
		
		});
	});
	
 }); 
 
</script>
 
<script>
//Contract Signed/Unsigned & Leads Saved Monthly Summary
	function overal_chart1(total_monthly_months,total_monthly_leadcount,total_monthly_contractUnsignedCount,total_monthly_contractSignedCount,total_monthly_contractDeclinedCount)
	{
		
		
		var t1monthcount=total_monthly_months.split(",");
		var len_c1=t1monthcount.length;
		var totalmonthshow=new Array();
		for(i=0; i<len_c1; i++)
			totalmonthshow.push(t1monthcount[i]); 
		
		var c1monthlyleadcount=total_monthly_leadcount.split(",");
		var len_c1=c1monthlyleadcount.length;
		var totalmonthlyleadcount=new Array();
		for(i=0; i<len_c1; i++)
			totalmonthlyleadcount.push(parseInt(c1monthlyleadcount[i])); 
		
		
		var c1contractMonthlyUnsignedCount=total_monthly_contractUnsignedCount.split(",");
		var len_c2=c1contractMonthlyUnsignedCount.length;
		var total_monthly_contractUnsignedCount=new Array();
		for(i=0; i<len_c2; i++)
			total_monthly_contractUnsignedCount.push(parseInt(c1contractMonthlyUnsignedCount[i])); 
	
		
		var c1contract_monthly_SignedCount=total_monthly_contractSignedCount.split(",");
		var len_c3=c1contract_monthly_SignedCount.length;
		var total_monthly_contractSignedCount=new Array();
		for(i=0; i<len_c3; i++)
			total_monthly_contractSignedCount.push(parseInt(c1contract_monthly_SignedCount[i]));

        var c1contract_monthly_DeclinedCount=total_monthly_contractDeclinedCount.split(",");
		var len_c3=c1contract_monthly_DeclinedCount.length;
		var total_monthly_contractDeclinedCount=new Array();
		for(i=0; i<len_c3; i++)
			total_monthly_contractDeclinedCount.push(parseInt(c1contract_monthly_DeclinedCount[i]));		
			
		Highcharts.setOptions({
          colors: ['#483D8B','#FFB400', '#568FC6', '#86b300']
      });
		$('#container5').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Contracts Signed/Unsigned/Declined & Leads Saved Monthly Summary'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
			categories: totalmonthshow,
            crosshair: true,
			title: {
                text: 'Months -'+<?php echo date('Y'); ?>
            }
        },
			credits: {
                enabled: false
            },
        yAxis: {
            min: 0,
            title: {
                text: 'Counts'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            //pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                //'<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                //pointPadding: 0.2,
				pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Contracts Declined',
            data: total_monthly_contractDeclinedCount

        },{
            name: 'Contracts Signed',
            data: total_monthly_contractSignedCount

        }, {
            name: 'Contracts Unsigned',
            data: total_monthly_contractUnsignedCount

        }, {
            name: 'Leads Saved',
            data: totalmonthlyleadcount

        }]
    }); 	
	
	}	
	
</script>
</head>
<body>

<section id="container" >
     
      <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> Monthly Summary</h3>
				<div class="container" align="left">
				    <form  role="form">
								<div class="row">
									<div  class="col-md-4" >
									</div>
									<div  class="col-md-6" style="margin-left:-90px;">
										
									</div>		
									<!--</form>-->
									<div class="col-sm-2" style="margin-left:80px;">
									<select name="partnermonthly" id="partnermonthly"class="form-control col-lg-2">
										<option value="all">All</option>
										<option value="10001">The Results</option>
										<option value="10002">PCCW</option>
										<option value="10003">SunShare Sales</option>
										<option value="10007">Web</option>
									</select>
									</div>
								</div>
					</form>
				</div>
              <!-- page start-->
			 
                <div class="tab-pane" id="chartjs" style="margin-top:-10px;">
                    <div class="row mt">
						<div class="col-lg-12">
                            <div class="content-panel" id="linechart_all">
								<div class="panel-body text-center">
							        <div id="container5" class="panel-body text-center" style="height:450px">
									
							        </div>
									
                                </div>
                            </div>
						   
                        </div>
                    </div>
				 </div>
                 
              <!-- page end-->
		</section>          
	</section><!-- /MAIN CONTENT -->
</section>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css" />
<?php
include "../footer.php";
?>
