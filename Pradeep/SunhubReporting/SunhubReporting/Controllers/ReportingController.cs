﻿using SunhubReporting.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace SunhubReporting.Controllers
{
    public class ReportingController : Controller
    {
      
        ReportingModels reportModel = new ReportingModels();
        public List<SelectListItem> Channel { get; set; }

        public List<SelectListItem> ChannelNew { get; set; }

        public List<SelectListItem> WebCampaign { get; set; }

        public List<SelectListItem> SalesRep { get; set; }

        public List<SelectListItem> City { get; set; }

        public List<SelectListItem> State { get; set; }

        public List<SelectListItem> County { get; set; }

        public List<SelectListItem> LeadStatus { get; set; }

        public List<SelectListItem> CallCenter { get; set; }

        [HttpPost]
        public ActionResult LoginPost(string username, string password, string partnerid)
        {
            string pageval = "";
            try
            {
                reportModel.dtLogin = reportModel.LoginMethod(username, password, partnerid);
                for (int i = 0; i < reportModel.dtLogin.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(reportModel.dtLogin.Rows[i]["ID"].ToString()))
                    {
                        Session["MemberId"] = reportModel.dtLogin.Rows[i]["ID"].ToString();
                        System.Web.HttpContext.Current.Session["sessionString"] = reportModel.dtLogin.Rows[i]["Name"].ToString();
                    }
                    else
                    {
                        Session["MemberId"] = "";
                        System.Web.HttpContext.Current.Session["sessionString"] = "";
                    }
                   
                }
                string check = Session["MemberId"].ToString();
                if (!string.IsNullOrEmpty(check))
                {
                    pageval = "Home";
                }
                else
                {
                    pageval = "Login";
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "Error", url = Url.Action("Login", "Reporting") });
            }
            finally
            {
                username = null;
                password = null;
                partnerid = null;
            }
            return Json(new { result = "Redirect", url = Url.Action(pageval, "Reporting") });
        }

        //[ChildActionOnly]
        //public ActionResult Menu()
        //{
        //    try
        //    {
               

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {

        //    }
        //    return PartialView("_PartialMenu");
        //}

        public void ddlChannel(string selectval)
        {

            string query = string.Format("exec proc_r_ChannelList");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Channel =
                    new List<SelectListItem>();
                while (reader.Read())
                {
                    Channel.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt32(0).ToString()
                        });
                }
                con.Close();
            }
            List<SelectListItem> list = Channel;
            if (selectval != "")
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    if (list[i].Text == selectval)
                    {
                        var item = list[i];
                        list[i] = list[0];
                        list[0] = item;
                    }
                }
            }
            ViewBag.Channel = list;
        }

        public void ddlChannelNew(string selectval)
        {

            string query = string.Format("exec proc_r_ChannelList");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                ChannelNew =
                    new List<SelectListItem>();
                while (reader.Read())
                {
                    ChannelNew.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt32(0).ToString()
                        });
                }
                con.Close();
            }
            List<SelectListItem> list = ChannelNew;
            if (selectval != "")
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    if (list[i].Text == selectval)
                    {
                        var item = list[i];
                        list[i] = list[0];
                        list[0] = item;
                    }
                }
            }
            ViewBag.ChannelNew = list;
        }

        public void ddlWebCampaign(string selectval)
        {

            string query = string.Format("exec proc_r_WebCampaignList");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                WebCampaign =
                    new List<SelectListItem>();
                while (reader.Read())
                {
                    WebCampaign.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt32(0).ToString()
                        });
                }
                con.Close();
            }
            List<SelectListItem> list = WebCampaign;
            if (selectval != "")
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    if (list[i].Text == selectval)
                    {
                        var item = list[i];
                        list[i] = list[0];
                        list[0] = item;
                    }
                }
            }
            ViewBag.WebCampaign = list;
        }

        public void ddlSalesRep(string selectval)
        {
            try { 

            string query = string.Format("exec proc_r_SalesRepList");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                SalesRep =
                    new List<SelectListItem>();
                while (reader.Read())
                {
                    SalesRep.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt64(0).ToString()
                        });
                }
                    con.Close();
                }
            List<SelectListItem> list = SalesRep;
            if (selectval != "")
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    if (list[i].Text == selectval)
                    {
                        var item = list[i];
                        list[i] = list[0];
                        list[0] = item;
                    }
                }
            }
            ViewBag.SalesRep = list;
            }
            catch(Exception ex)
            {

            }
        }

        public void ddlCity(string selectval)
        {
            try
            {
                string query = string.Format("exec proc_r_CityList");

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString);

                SqlCommand cmd = new SqlCommand(query, con);
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    City =
                        new List<SelectListItem>();
                    while (reader.Read())
                    {
                        City.Add(
                            new SelectListItem
                            {
                                Text = reader.GetString(0),
                                Value = reader.GetString(0)
                            });
                    }
                    con.Close();
                }
                List<SelectListItem> list = City;
                if (selectval != "")
                {
                    for (int i = 0; i < list.Count(); i++)
                    {
                        if (list[i].Text == selectval)
                        {
                            var item = list[i];
                            list[i] = list[0];
                            list[0] = item;
                        }
                    }
                }
                ViewBag.City = list;
            }
            catch (Exception ex)
            {

            }
        }

        public void ddlState(string selectval)
        {
            try
            {
                string query = string.Format("exec proc_r_StateList");

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString);

                SqlCommand cmd = new SqlCommand(query, con);
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    State =
                        new List<SelectListItem>();
                    while (reader.Read())
                    {
                        State.Add(
                            new SelectListItem
                            {
                                Text = reader.GetString(0),
                                Value = reader.GetString(0)
                            });
                    }
                    con.Close();
                }
                List<SelectListItem> list = State;
                if (selectval != "")
                {
                    for (int i = 0; i < list.Count(); i++)
                    {
                        if (list[i].Text == selectval)
                        {
                            var item = list[i];
                            list[i] = list[0];
                            list[0] = item;
                        }
                    }
                }
                ViewBag.State = list;
            }
            catch (Exception ex)
            {

            }
        }

        public void ddlCounty(string selectval)
        {
            try
            {
                string query = string.Format("exec proc_r_CountyList");

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString);

                SqlCommand cmd = new SqlCommand(query, con);
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    County =
                        new List<SelectListItem>();
                    while (reader.Read())
                    {
                        County.Add(
                            new SelectListItem
                            {
                                Text = reader.GetString(0),
                                Value = reader.GetString(0)
                            });
                    }
                    con.Close();
                }
                List<SelectListItem> list = County;
                if (selectval != "")
                {
                    for (int i = 0; i < list.Count(); i++)
                    {
                        if (list[i].Text == selectval)
                        {
                            var item = list[i];
                            list[i] = list[0];
                            list[0] = item;
                        }
                    }
                }
                ViewBag.County = list;
            }
            catch (Exception ex)
            {

            }
        }

        public void ddlLeadStatus(string selectval)
        {
            try
            {
                string query = string.Format("exec proc_r_LeadStatusList");

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString);

                SqlCommand cmd = new SqlCommand(query, con);
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    LeadStatus =
                        new List<SelectListItem>();
                    while (reader.Read())
                    {
                        LeadStatus.Add(
                            new SelectListItem
                            {
                                Text = reader.GetString(1),
                                Value = reader.GetInt32(0).ToString()
                            });
                    }
                    con.Close();
                }
                List<SelectListItem> list = LeadStatus;
                if (selectval != "")
                {
                    for (int i = 0; i < list.Count(); i++)
                    {
                        if (list[i].Text == selectval)
                        {
                            var item = list[i];
                            list[i] = list[0];
                            list[0] = item;
                        }
                    }
                }
                ViewBag.LeadStatus = list;
            }
            catch (Exception ex)
            {

            }
        }

        public void ddlCallCenter(string selectval)
        {
            try
            {
                string query = string.Format("exec proc_r_CallCenterList");

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SunHubReporting"].ConnectionString);

                SqlCommand cmd = new SqlCommand(query, con);
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    CallCenter =
                        new List<SelectListItem>();
                    while (reader.Read())
                    {
                        CallCenter.Add(
                            new SelectListItem
                            {
                                Text = reader.GetString(1),
                                Value = reader.GetInt32(0).ToString()
                            });
                    }
                    con.Close();
                }
                List<SelectListItem> list = CallCenter;
                if (selectval != "")
                {
                    for (int i = 0; i < list.Count(); i++)
                    {
                        if (list[i].Text == selectval)
                        {
                            var item = list[i];
                            list[i] = list[0];
                            list[0] = item;
                        }
                    }
                }
                ViewBag.CallCenter = list;
            }
            catch (Exception ex)
            {

            }
        }

        [HttpPost]
        public ActionResult ForgetPassword(string EmailId)
        {
            bool k;
            string fp = "";
            try
            {
                if (!string.IsNullOrEmpty(EmailId))
                {
                    reportModel.user_details = reportModel.ForgetPassword(EmailId);
                    if (reportModel.user_details != null)
                    {
                        var code = Guid.NewGuid();
                        var callbackUrl = Url.Action("ChangePassword", "Reporting", new { userId = reportModel.user_details.UserId, username = reportModel.user_details.UserName, code }, protocol: Request.Url.Scheme);
                        var filePath = HostingEnvironment.MapPath("~/Emailer/PasswordResetTemplate.html");
                        if (filePath == null) return RedirectToAction("ForgotPassword", "Reporting");
                        var emailContent = System.IO.File.ReadAllText(filePath)
                            .Replace("{0}", $"{reportModel.user_details.UserName}")
                            .Replace("{1}", callbackUrl);
                        k = reportModel.SendGridSendEmail(EmailId, emailContent, "Reset Password");
                        if (k == true)
                        {
                            fp = "Success";
                        }
                        else
                        {
                            fp = "failure";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "Error", url = Url.Action("ForgotPassword", "Reporting") });
            }
            finally
            {
                reportModel = null;
                EmailId = null;
            }
            return Json(new { result = fp, url = Url.Action("ForgotPassword", "Reporting") });
        }

        [HttpPost]
        public ActionResult ChangePasswordMethod(string email, string password)
        {
            try
            {

                if ((!string.IsNullOrEmpty(email)) && (!string.IsNullOrEmpty(password)))
                {
                    reportModel.user_details.Status = reportModel.ChangePassword(email, password);
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "Error", url = Url.Action("ChangePassword", "Reporting") });
            }
            finally
            {
                email = null;
                password = null;
            }
            return Json(new { result = reportModel.user_details.Status, url = Url.Action("ChangePassword", "Reporting") });
        }

        public ActionResult ChangePassword(string code, string username)
        {
            return code == null ? View("Error") : View();
        }

        public ActionResult Index()
        {
            return View();
        }

        //Daily Summary Page (Charts) - _PartialDailySummary 
        public PartialViewResult DailyChannelSummary(string Month,string Year)
        {
            try
            {
                reportModel.MonthValue = Month;
                reportModel.GetDailySummary(Month,Year, "Contract Signed");
                reportModel.GetDailySummary(Month, Year, "Contract Sent");
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return PartialView("_PartialDailySummary", reportModel);
        }

        //Monthly Summary Page (Charts) -  _PartialMonthlySummary
        public PartialViewResult MonthlyChannelSummary (string ChannelId)
        {
            try
            {
                if(string.IsNullOrEmpty(ChannelId))
                {
                    ChannelId = "0";
                }
                reportModel.GetMonthlySummary(ChannelId, DateTime.Now.Year.ToString());
              
            }
            catch(Exception ex)
            {

            }
            finally
            {

            }
            return PartialView("_PartialMonthlySummary", reportModel);
        }

        // Current Month Page (Charts) -  _PartialCurrentMonth 
        public PartialViewResult CurrentMonth(string ChannelId)
        {
            try
            {
                if (string.IsNullOrEmpty(ChannelId))
                {
                    ChannelId = "0";
                }
           
                reportModel.contract_summary = reportModel.GetCurrentMonthContractSummary(DateTime.Now.Month.ToString(), DateTime.Now.Year.ToString(), ChannelId);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return PartialView("_PartialCurrentMonth", reportModel);
        }

        // WebLeadTrend Page (Charts) -  _PartialWebLeadTrend 
        public PartialViewResult WebLeadTrendChart(string start,string end,string landid)
        {
            try
            {
                if(string.IsNullOrEmpty(landid))
                {
                    landid = "0";
                }
                reportModel.list_webcampaign = reportModel.GetWebCampaign(start, end, landid);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return PartialView("_PartialWebLeadTrend", reportModel);
        }

        // WebLeadTrend Page (Charts) - _PartialWebLeadTrendTable 
        public PartialViewResult WebLeadTrendTable(string start, string end, string landid)
        {
            try
            {
                if (string.IsNullOrEmpty(landid))
                {
                    landid = "0";
                }
                reportModel.dtWebCampaigns = reportModel.GetWebCampaignTable(start, end, landid);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return PartialView("_PartialWebLeadTrendTable", reportModel);
        }

        // AllReports (Reports) - _PartialAllReports 
        public PartialViewResult AllReports(string fromDate,string toDate,string salesrep,string callcenter,string city,string state,string county,string leadstatus)
        {
            try
            {
                reportModel.dtAllReports = reportModel.GetReports(fromDate, toDate, salesrep, callcenter, city, state, county, leadstatus);
            }
            catch(Exception ex)
            {
            }
            return PartialView("_PartialAllReports", reportModel);
        }

        // Call Center Page (Reports) - _PartialAllReports 
        public PartialViewResult ReportsCallCenter(string fromDate, string toDate, string salesrep, string callcenter, string city, string state, string county, string leadstatus)
        {
            try
            {
                reportModel.dtAllReports = reportModel.GetCallCenterReports(fromDate, toDate, salesrep, callcenter, city, state, county, leadstatus);
            }
            catch (Exception ex)
            {
            }
            return PartialView("_PartialAllReports", reportModel);
        }

        //Web Campaigns Page (Reports) - _PartialWebCampaigns 
        public PartialViewResult WebCampaignsPartial(string name)
        {
            try
            {
                if (string.IsNullOrEmpty(name))
                {
                    name = "0";
                }
                reportModel.dtWebCampaigns = reportModel.GetReportWebCampaigns(name);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return PartialView("_PartialWebCampaigns", reportModel);
        }

        //Web Enrollment Page (Reports) - _PartialWebEnrollment  
        public PartialViewResult WebEnrollmentPartial(string fromDate,string toDate,string city, string state, string county, string leadstatus)
        {
            try
            {
                reportModel.dtWebEnrollment = reportModel.GetReportWebEnrollment(fromDate, toDate, city, state, county, leadstatus);
            }
            catch (Exception ex)
            {
            }
            return PartialView("_PartialWebEnrollment", reportModel);
        }

        //Dashboard Page
        [SessionAuthorize]
        public ActionResult Home()
        {
            try
            {
               // ViewData["sessionString"] = System.Web.HttpContext.Current.Session["sessionString"] as String;
                ddlChannel("");
                ddlChannelNew("");
                string startDate =  DateTime.Now.Year+"-01-01";
                string endDate = DateTime.Now.Year+"-"+DateTime.Now.Month+"-"+DateTime.Now.Day;
                reportModel.channel1Summary = reportModel.GetContractSummaryTable(startDate, endDate, "Results - Call Center");
                reportModel.channel2Summary = reportModel.GetContractSummaryTable(startDate, endDate, "PCCW - CallCenter");
                reportModel.channel3Summary = reportModel.GetContractSummaryTable(startDate, endDate, "Web Enrollment - Auto");
                reportModel.channel4Summary = reportModel.GetContractSummaryTable(startDate, endDate, "Sunshare");
                reportModel.contract_summary = reportModel.GetContractSummaryChart(startDate, endDate);
            }
            catch(Exception ex)
            {

            }
            finally
            {
            }
            return View(reportModel);
        }

        //Login Page
        public ActionResult Login()
        {
            Session.Clear();
            return View();
        }

        //Current Month Summary - Charts
        [SessionAuthorize]
        public ActionResult CurrentMonthSummary()
        {
            try
            {
               // ViewData["sessionString"] = System.Web.HttpContext.Current.Session["sessionString"] as String;
                ddlChannelNew("");
            }
            catch(Exception ex)
            {

            }
            finally
            {

            }
            return View();
        }

        //Monthly Summary - Charts
        [SessionAuthorize]
        public ActionResult MonthlySummary()
        {
            try
            {
                // ViewData["sessionString"] = System.Web.HttpContext.Current.Session["sessionString"] as String;
                ddlChannel("");
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return View();
        }

        //Daily Summary - Charts
        [SessionAuthorize]
        public ActionResult DailySummary()
        {
            try
            {
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return View();
        }

        //WebLead Trend - Charts
        [SessionAuthorize]
        public ActionResult WebLeadTrend()
        {
            try
            {
                ddlWebCampaign("");
               // reportModel.WebCampaignCount = 0;
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return View();
        }

        // All Channels - Reports
        [SessionAuthorize]
        public ActionResult ReportsAll()
        {
            try
            {
                ddlSalesRep("");
                ddlChannel("");
                ddlCity("");
                ddlState("");
                ddlCounty("");
                ddlLeadStatus("");
            }
            catch(Exception ex)
            {

            }
            finally
            {

            }
            return View();
        }

        //Call Center - Reports
        [SessionAuthorize]
        public ActionResult CallCenterReports()
        {
            try
            {
                ddlSalesRep("");
                ddlCallCenter("");
                ddlCity("");
                ddlState("");
                ddlCounty("");
                ddlLeadStatus("");
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return View();
        }

        //Web Enrollment - Reports
        [SessionAuthorize]
        public ActionResult WebEnrollment()
        {
            try
            {
                ddlCity("");
                ddlState("");
                ddlCounty("");
                ddlLeadStatus("");
            }
            catch (Exception ex)
            {
            }
            finally
            { 
            }
            return View();
        }

        //Web Campaigns - Reports
        [SessionAuthorize]
        public ActionResult WebCampaigns()
        {
            try
            {
                ddlWebCampaign("");
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
            return View();
        }

        public ActionResult ForgotPassword()
        {

            return View();
        }

        public class SessionAuthorizeAttribute : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                return httpContext.Session["sessionString"] != null;
            }

            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                filterContext.Result = new RedirectResult("~/Reporting/Login");
            }
        }

    }
}