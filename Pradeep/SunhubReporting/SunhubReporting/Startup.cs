﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SunhubReporting.Startup))]
namespace SunhubReporting
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
