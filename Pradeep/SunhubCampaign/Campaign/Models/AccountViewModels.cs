﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System;
using System.Linq;

namespace Campaign.Models
{
   
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
       
    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class campaigndata
    {
        public DataSet StoreAllData { get; set; }
        public DataSet filterdata { get; set; }
        public DataSet futuredata { get; set; }
        public DataSet completeddata { get; set; }
        public DataSet datasetpiechart { get; set; }
        public DataSet getdata()
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                cmd.Connection = CN;
                cmd.CommandText = "proc_c_campaigndetails";
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                CN.Open();
                adap.Fill(ds);
                CN.Close();
                return ds;
            }

        }

        public List<campaignfields> list_campFields;

        public campaignfields campfields;
        public DataSet getfilterdata(string st,string ed)
        {           
            DataSet filterdata = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_getfilteredcampaigndetails '" + st + "','" + ed + "'", CN);
                adap.Fill(filterdata);
                CN.Close();
                return filterdata;
            }

        }

        public DataSet getfuturedata()
        {
            DataSet futuredata = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_getfuturecampaigndetails", CN);
                adap.Fill(futuredata);
                CN.Close();
                return futuredata;
            }

        }

        public DataSet getcompleteddata()
        {
            DataSet completeddata = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_getcompletedcampaigndetails ", CN);
                adap.Fill(completeddata);
                CN.Close();
                return completeddata;
            }

        }

        public DataSet gettypecount()
        {
            DataSet dsgettypecount = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_gettypescount ", CN);
                adap.Fill(dsgettypecount);
                CN.Close();
                return dsgettypecount;
            }

        }

        public bool UpdateCreativeCampaignDetails(string CreativeID, string Name, string StartDate, string EndDate, string filesavedpath, string Alterby, string filename)
        {
            try
            {
                using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    CN.Open();
                    SqlCommand adap = new SqlCommand("exec proc_c_UpdateCreative '" + CreativeID + "','" + Name + "','" + StartDate + "','" + EndDate + "','" + filesavedpath + "','" + Alterby + "','" + filename + "'", CN);
                    int val = adap.ExecuteNonQuery();
                    CN.Close();
                }

            }
            catch
            { }
            return true;
        }

        public bool SaveCreativeData(string Name, string StartDate, string EndDate, string filesavedpath, string createdby, string filename)
        {
            try
            {

                using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    CN.Open();
                    SqlCommand adap = new SqlCommand("exec proc_c_AddCreative '" + Name + "','" + StartDate + "','" + EndDate + "','" + filesavedpath + "','" + createdby + "','" + filename + "'", CN);
                    int val = adap.ExecuteNonQuery();
                    CN.Close();
                }
            }
            catch
            { }
            return true;
        }

        public DataSet geteditdata(string id)
        {
            DataSet dsedit = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_geteditcampaigndetails '"+ id + "'",CN);            
                adap.Fill(dsedit);
                CN.Close();
                return dsedit;
            }

        }
        public void UpdateCampaignData(List<campaignfields> lst_camp, string Name, string StartDate, string EndDate, string PlanBudget, string createdby)
        {
            DataTable dtUpdate = new DataTable();
            dtUpdate.Columns.AddRange(new DataColumn[14] { new DataColumn("CampaignID", typeof(int)),
                    new DataColumn("ParentType", typeof(string)),
                     new DataColumn("SubType", typeof(string)),
                      new DataColumn("Channel", typeof(string)),
                       new DataColumn("Incentive", typeof(string)),
                        new DataColumn("Creative", typeof(string)),
                      new DataColumn("PhoneNo", typeof(string)),
                    new DataColumn("LandingPage",typeof(string)),
            new DataColumn("Budget", typeof(string)),
            new DataColumn("State",typeof(string)),
            new DataColumn("County",typeof(string)),
            new DataColumn("PlannedCost",typeof(string)),
            new DataColumn("ActualCost",typeof(string)),
            new DataColumn("OfferCode",typeof(string)),});
            for (int i = 0; i < lst_camp.Count; i++)
            {
                dtUpdate.Rows.Add(Convert.ToInt32(lst_camp[i].CampaignID), lst_camp[i].ParentType, lst_camp[i].SubType, lst_camp[i].Channel, lst_camp[i].Incentive, lst_camp[i].Creative, lst_camp[i].PhoneNo, lst_camp[i].LandingPageURL, lst_camp[i].Budget, lst_camp[i].State, lst_camp[i].County, lst_camp[i].PlannedCost, lst_camp[i].ActualCost, lst_camp[i].OfferCode);
            }
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("proc_c_UpdateCampaignDatas"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = CN;
                    cmd.Parameters.AddWithValue("@tblCampaign", dtUpdate);
                    cmd.Parameters.AddWithValue("@name", Name);
                    cmd.Parameters.AddWithValue("@startDate", StartDate);
                    cmd.Parameters.AddWithValue("@endDate", EndDate);
                    cmd.Parameters.AddWithValue("@PlanBudget", PlanBudget);
                    cmd.Parameters.AddWithValue("@createdby", createdby);
                    CN.Open();
                    cmd.ExecuteNonQuery();
                    CN.Close();
                }
            }

        }

        public bool getdeletedata(string id)
        {
           
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlCommand adap = new SqlCommand("exec proc_c_campaigndelete '" + id + "'", CN);
                int val = adap.ExecuteNonQuery();
                CN.Close();
                if (val > 0)
                {
                    return true;
                }
                else { return false; }
            }

        }

        public bool updatecampaigndata(string id, string CampaignName, string Sdate, string Edate, string PlannedBudget, string Type, string Channel, string SubType, string Incentive, string Creative, string PhoneNo, string Landing, string Budget, string alterby, string State, string County, string PlannedCost, string ActualCost, string OfferCode)
        {
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                string qry = "exec proc_c_campaignupdate " + id + ",'" + CampaignName + "','" + Sdate + "','" + Edate + "','" + PlannedBudget + "','" + Type + "','" + Channel + "','" + SubType + "','" + Incentive + "','" + Creative + "','" + PhoneNo + "','" + Landing + "','" + Budget + "','" + alterby + "','" + State + "','" + County + "','" + PlannedCost + "','" + ActualCost + "','" + OfferCode + "'";
                SqlCommand adap = new SqlCommand(qry, CN);
                int val = adap.ExecuteNonQuery();
                CN.Close();
                if (val > 0)
                {
                    return true;
                }
                else { return false; }
            }
        }

        public bool insertcampaigndata(string Name, string Sdate, string Edate, string PlannedBudget, string Type, string Channel, string SubType, string Incentive, string Creative, string PhoneNo, string Landing, string Budget, string createdby, string State, string County, string PlannedCost, string ActualCost, string OfferCode)
        {
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                string qry = "exec proc_c_campaigninsert '" + Name + "','" + Sdate + "','" + Edate + "','" + PlannedBudget + "','" + Type + "','" + Channel + "','" + SubType + "','" + Incentive + "','" + Creative + "','" + PhoneNo + "','" + Landing + "'," + Budget + ",'" + createdby + "','" + State + "','" + County + "','" + PlannedCost + "','" + ActualCost + "','" + OfferCode + "'";
                SqlCommand adap = new SqlCommand(qry, CN);
                int val = adap.ExecuteNonQuery();
                CN.Close();
                if (val > 0)
                {
                    return true;
                }
                else { return false; }

            }
        }

        public void modelActiveCampaign(string id,string id2)
        {
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlCommand adap = new SqlCommand("exec proc_c_activecampaign '" + id + "','"+ id2 +"' ", CN);
                adap.ExecuteNonQuery();
                CN.Close();
            }
        }

        public DataSet piedata()
        {
            DataSet dspiechart = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_getpiechartdata ", CN);
                adap.Fill(dspiechart);
                CN.Close();

            }
            return dspiechart;
        }

        public DataSet piedatadrill(string parname, string chname)
        {
            DataSet dspiechartdrill = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_getpiechartdrilldata '" + parname + "','" + chname + "' ", CN);
                adap.Fill(dspiechartdrill);
                CN.Close();

            }
            return dspiechartdrill;
        }

        public DataSet barchartbymonth()
        {
            DataSet dsbarchart = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_getdatabymonth ", CN);
                adap.Fill(dsbarchart);
                CN.Close();

            }
            return dsbarchart;
        }

        public DataSet barchartbyleadcount()
        {
            DataSet dsleadbarchart = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_getleadcount ", CN);
                adap.Fill(dsleadbarchart);
                CN.Close();

            }
            return dsleadbarchart;
        }

        public DataSet barchartbyleadchannelcount()
        {
            DataSet dsleadbarchannelchart = new DataSet();
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlDataAdapter adap = new SqlDataAdapter("exec proc_c_getleadchannelcount ", CN);
                adap.Fill(dsleadbarchannelchart);
                CN.Close();

            }
            return dsleadbarchannelchart;
        }
        
        public void InsertType(string id, string name)
        {

            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlCommand adap = new SqlCommand("exec proc_c_InsertType " + id + ",'" + name + "' ", CN);
                adap.ExecuteNonQuery();
                CN.Close();

            }

        }

        public void InsertfromtreeType(string t1, string t2, string t3)
        {

            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlCommand adap = new SqlCommand("exec proc_c_InsertfromtreeType '" + t1 + "','" + t2 + "','" + t3 + "'", CN);
                adap.ExecuteNonQuery();
                CN.Close();

            }

        }

        public void modelinsertparent(string Parentname)
        {
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlCommand adap = new SqlCommand("exec proc_c_saveparenttype '" + Parentname + "' ", CN);
                adap.ExecuteNonQuery();
                CN.Close();
            }
        }

        public void deletetreenode(string val1, string val2, string val3, string val4)
        {
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlCommand adap = new SqlCommand("exec proc_c_campaigntreenodedelete '" + val1 + "','" + val2 + "','" + val3 + "','" + val4 + "'", CN);
                int val = adap.ExecuteNonQuery();
                CN.Close();
            }

        }

        public int renametreenode(string val0, string val1, string val2, string val3, string val4)
        {
            int val = 0;
            using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                CN.Open();
                SqlCommand adap = new SqlCommand("exec proc_c_campaigntreenoderename '" + val0 + "','" + val1 + "','" + val2 + "','" + val3 + "','" + val4 + "'", CN);
                val = adap.ExecuteNonQuery();
                CN.Close();
            }
            return val;
        }

        //For Add Creative Methods
        #region Add Creative Methods
        public List<AddCreative> GetAddCreativeData()
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            List<AddCreative> LstSignedContracts = new List<AddCreative>();
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetAddCreative";
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            CN.Open();
            da.Fill(dt);
            CN.Close();
            //Bind SignedContractsModel generic list using LINQ 
            LstSignedContracts = (from DataRow dr in dt.Rows

                                  select new AddCreative()
                                  {
                                      CreativeID = Convert.ToInt32(dr["CreativeID"]),
                                      Name = dr["Name"].ToString(),
                                      StartDate = dr["StartDate"].ToString(),
                                      EndDate = dr["EndDate"].ToString(),
                                      Active = dr["Active"].ToString(),
                                      Filepath = dr["filenames"].ToString(),
                                      ImageURL= dr["FilePath"].ToString()
                                  }).ToList();
            return LstSignedContracts;


        }

        //For Update Creative
        public bool UpdateCreativeCampaignDetails(string CreativeID, string Name, string StartDate, string EndDate)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetUpdateCreative";
            com.Parameters.AddWithValue("@CreativeID", CreativeID);
            com.Parameters.AddWithValue("@Name", Name);
            com.Parameters.AddWithValue("@StartDate", StartDate);
            com.Parameters.AddWithValue("@EndDate", EndDate);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }

        //For Delete Creative
        public bool DeleteCreativeCampaignDetails(string CreativeID)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetDeleteCreative";
            com.Parameters.AddWithValue("@CreativeID", CreativeID);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }

        //For Active/Deactive
        public bool Active(string CreativeID, string Active)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetActiveDeactiveCreatives";
            com.Parameters.AddWithValue("@CreativeID", CreativeID);
            com.Parameters.AddWithValue("@Active", Active);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }

        //For Upload Image
        public bool UpdatePath(string CreativeID, string filename)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_UpdateUploadFile";
            com.Parameters.AddWithValue("@CreativeID", CreativeID);
            com.Parameters.AddWithValue("@FilePath", filename);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }
        #endregion

        //For Add Incentive Methods
        #region Add Incentive Methods

        public List<AddIncentive> GetAddIncentiveData()
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            List<AddIncentive> AddIncentive = new List<AddIncentive>();
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetAddIncentive";
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            CN.Open();
            da.Fill(dt);
            CN.Close();

            AddIncentive = (from DataRow dr in dt.Rows

                            select new AddIncentive()
                            {
                                IncentiveID = Convert.ToInt32(dr["IncentiveID"]),
                                Name = dr["Name"].ToString(),
                                StartDate = dr["StartDate"].ToString(),
                                EndDate = dr["EndDate"].ToString(),
                                Active = dr["Active"].ToString()

                            }).ToList();
            return AddIncentive;


        }

        //For Update Incentive
        public bool UpdateIncentiveCampaignDetails(string IncentiveID, string Name, string StartDate, string EndDate)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetUpdateIncentive";
            com.Parameters.AddWithValue("@IncentiveID", IncentiveID);
            com.Parameters.AddWithValue("@Name", Name);
            com.Parameters.AddWithValue("@StartDate", StartDate);
            com.Parameters.AddWithValue("@EndDate", EndDate);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }

        ////For Delete Incentive
        public bool DeleteIncentiveCampaignDetails(string IncentiveID)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetDeleteIncentive";
            com.Parameters.AddWithValue("@IncentiveID", IncentiveID);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }

        ////For Active/Deactive
        public bool IncentiveActive(string IncentiveID, string Active)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetActiveDeactiveIncentives";
            com.Parameters.AddWithValue("@IncentiveID", IncentiveID);
            com.Parameters.AddWithValue("@Active", Active);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }
        #endregion

        ////For Add OfferCode Methods
        //#region Offer Code Methods
        //public List<OfferCode> GetOfferCodeData()
        //{
        //    SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //    List<OfferCode> offercode = new List<OfferCode>();
        //    SqlCommand com = new SqlCommand();
        //    com.Connection = CN;
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.CommandText = "proc_c_GetAddOffercode";
        //    SqlDataAdapter da = new SqlDataAdapter(com);
        //    DataTable dt = new DataTable();
        //    CN.Open();
        //    da.Fill(dt);
        //    CN.Close();

        //    offercode = (from DataRow dr in dt.Rows

        //                 select new OfferCode()
        //                 {
        //                     OfferCodeID = Convert.ToInt32(dr["OffercodeID"]),
        //                     Name = dr["Name"].ToString(),
        //                     StartDate = dr["StartDate"].ToString(),
        //                     EndDate = dr["EndDate"].ToString(),
        //                     Active = dr["Active"].ToString()

        //                 }).ToList();
        //    return offercode;
        //}

        ////For Update OfferCode
        //public bool UpdateOfferCodeCampaignDetails(string OfferCodeID, string Name, string StartDate, string EndDate)
        //{
        //    SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //    SqlCommand com = new SqlCommand();
        //    com.Connection = CN;
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.CommandText = "proc_c_GetUpdateOffercode";
        //    com.Parameters.AddWithValue("@OfferCodeID", OfferCodeID);
        //    com.Parameters.AddWithValue("@Name", Name);
        //    com.Parameters.AddWithValue("@StartDate", StartDate);
        //    com.Parameters.AddWithValue("@EndDate", EndDate);
        //    CN.Open();
        //    com.ExecuteNonQuery();
        //    CN.Close();
        //    return true;
        //}

        //////For Delete Offer Code
        //public bool DeleteOfferCodeCampaignDetails(string OfferCodeID)
        //{
        //    SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //    SqlCommand com = new SqlCommand();
        //    com.Connection = CN;
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.CommandText = "proc_c_GetDeleteOffercode";
        //    com.Parameters.AddWithValue("@OfferCodeID", OfferCodeID);
        //    CN.Open();
        //    com.ExecuteNonQuery();
        //    CN.Close();
        //    return true;
        //}

        //////For Active/Deactive Offer Code
        //public bool OfferCodeActive(string OfferCodeID, string Active)
        //{
        //    SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //    SqlCommand com = new SqlCommand();
        //    com.Connection = CN;
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.CommandText = "proc_c_GetActiveDeactiveOffercode";
        //    com.Parameters.AddWithValue("@OfferCodeID", OfferCodeID);
        //    com.Parameters.AddWithValue("@Active", Active);
        //    CN.Open();
        //    com.ExecuteNonQuery();
        //    CN.Close();
        //    return true;
        //}
        //#endregion


        #region Phone Number Page Methods
        public List<PhoneNo> GetPhoneNoData()
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            List<PhoneNo> phno = new List<PhoneNo>();
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetAddPhoneNo";
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            CN.Open();
            da.Fill(dt);
            CN.Close();

            phno = (from DataRow dr in dt.Rows

                    select new PhoneNo()
                    {
                        PhoneNoID = Convert.ToInt32(dr["PhoneNoID"]),
                        Name = dr["Name"].ToString(),
                        StartDate = dr["StartDate"].ToString(),
                        EndDate = dr["EndDate"].ToString(),
                        Active = dr["Active"].ToString()

                    }).ToList();
            return phno;
        }

        //For Update PhoneNo
        public bool UpdatePhoneNoCampaignDetails(string PhoneNoID, string Name, string StartDate, string EndDate)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetUpdatePhoneNo";
            com.Parameters.AddWithValue("@PhoneNoID", PhoneNoID);
            com.Parameters.AddWithValue("@Name", Name);
            com.Parameters.AddWithValue("@StartDate", StartDate);
            com.Parameters.AddWithValue("@EndDate", EndDate);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }

        ////For Delete Offer Code
        public bool DeletePhoneNoCampaignDetails(string PhoneNoID)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetDeletePhoneNo";
            com.Parameters.AddWithValue("@PhoneNoID", PhoneNoID);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }

        ////For Active/Deactive Offer Code
        public bool PhoneNoActive(string PhoneNoID, string Active)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetActiveDeactivePhoneNo";
            com.Parameters.AddWithValue("@PhoneNoID", PhoneNoID);
            com.Parameters.AddWithValue("@Active", Active);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }


        #endregion
        
        //For Add LandingPage Methods
        #region Landing Page Methods
        public List<LandingPage> GetLandingData()
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            List<LandingPage> LandingPageURL = new List<LandingPage>();
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetLandingpage";
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            CN.Open();
            da.Fill(dt);
            CN.Close();

            LandingPageURL = (from DataRow dr in dt.Rows

                              select new LandingPage()
                              {
                                  LandingID = Convert.ToInt32(dr["LandingpageID"]),
                                  Name = dr["Name"].ToString(),
                                  LandingURL = dr["URL"].ToString(),
                                  StartDate = dr["StartDate"].ToString(),
                                  EndDate = dr["EndDate"].ToString(),
                                  Active = dr["Active"].ToString()

                              }).ToList();

            return LandingPageURL;
        }

        //For Update OfferCode
        public bool UpdateLandingCampaignDetails(string LandingID, string URL, string Name, string StartDate, string EndDate)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetUpdateLandingPage";
            com.Parameters.AddWithValue("@LandingID", LandingID);
            com.Parameters.AddWithValue("@Name", Name);
            com.Parameters.AddWithValue("@URL", URL);
            com.Parameters.AddWithValue("@StartDate", StartDate);
            com.Parameters.AddWithValue("@EndDate", EndDate);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }

        ////For Delete Offer Code
        public bool DeleteLandingCampaignDetails(string LandingID)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetDeleteLandingpage";
            com.Parameters.AddWithValue("@LandingID", LandingID);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }

        ////For Active/Deactive Offer Code
        public bool LandingActive(string LandingID, string Active)
        {
            SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = CN;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "proc_c_GetActiveDeactiveLandingURL";
            com.Parameters.AddWithValue("@LandingID", LandingID);
            com.Parameters.AddWithValue("@Active", Active);
            CN.Open();
            com.ExecuteNonQuery();
            CN.Close();
            return true;
        }
        #endregion
        

    }


    //For Add Creative Class
    #region Add Creative Class   
    public class AddCreative
    {
        public int CreativeID { get; set; }
        public string Name { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Active { get; set; }
        public string Filepath { get; set; }
        public string ImageURL { get; set; }
    }
    #endregion

    //For Add Incentive Class
    #region Add Incentive Class    
    public class AddIncentive
    {
        public int IncentiveID { get; set; }
        public string Name { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Active { get; set; }
    }
    #endregion

    //For Offer Code Class
    #region Add Offer Code
    public class OfferCode
    {
        public int OfferCodeID { get; set; }
        public string Name { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Active { get; set; }
    }
    #endregion

    //For Landing Page URL
    #region Landing Page URL
    public class LandingPage
    {
        public int LandingID { get; set; }
        public string Name { get; set; }
        public string LandingURL { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Active { get; set; }
    }
    #endregion


    public class PhoneNo
    {
        public int PhoneNoID { get; set; }
        public string Name { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Active { get; set; }
    }

    public class piechart
    {
        public string PlanName { get; set; }
        public int PaymentAmount { get; set; }
    }

    public class chartbymonth
    {
        public string Name { get; set; }
        public int count { get; set; }
    }

    public class chartbyleadcount
    {
        public string Name { get; set; }
        public int count { get; set; }
    }

    public class chartbyleadchannelcount
    {
        public string Name { get; set; }
        public int count { get; set; }
    }

    public class ddlvalue 
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }

    public class campaignfields
    {
        public string CampaignID { get; set; }
        public string CampaignName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PlannedBudget { get; set; }
        public string ParentType { get; set; }        
        public string SubType { get; set; }
        public string Channel { get; set; }
        public string Incentive { get; set; }
        public string Creative { get; set; }
        public string PhoneNo { get; set; }
        public string LandingPageURL { get; set; }
        public string Budget { get; set; }
        public string OfferCode { get; set; }
        public string State { get; set; }
        public string County { get; set; }
        public string PlannedCost { get; set; }
        public string ActualCost { get; set; }
    }

    public class TreeViewItemModel
    {
        public string text { get; set; } //text do be displayed in the node
        public string icon { get; set; } //remove this property if not wanting to customize node icon     
        public List<TreeViewItemModel> children { get; set; }
    }

}

