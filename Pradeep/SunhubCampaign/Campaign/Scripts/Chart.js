﻿var json2 = {};
var rows = '';

function getData(paname, chname) {

    var result = null;
    $.ajax({
        url: "Home/PiechartdrillList",
        type: 'get',
        data: { parentname: paname, channelname:chname },
        dataType: 'json',
        async: false,
        success: function (data) {
            var items = '';
            //alert(paname);
            //alert(chname);
            var aData = data;
            if (!chname) {
                btname = paname;
            }else{
                btname = chname;
            }
           //var marr = [];
            var obj = {
                name: btname,
                //name: paname,

                data:[]
            };
            $.map(aData, function (item, index) {
                var i = [item.PlanName, item.PaymentAmount];
               
                
                var arrdata = {};
               
                //obj['id'] = nameeee;

                //marr.push(arr);
                arrdata['name'] = item.PlanName;
                arrdata['y'] = item.PaymentAmount;

               
                if (!chname) {
                    arrdata['drilldown'] = true;
                }else{
                    //arrdata['drilldown'] = true;
                }
              
                // obj['data'] = arrdata;
                obj.data.push(arrdata);
                //marr.push(obj);

                rows += "['" + item.PlanName + "'," + item.PaymentAmount + "],"
            });
            //marr.push(obj);
            var C_Cart = (rows.substring(0, rows.length - 1))
            var jsonArray = JSON.parse(JSON.stringify(obj));
            result = jsonArray;
        }
    });

    
    return result;
}

function dd(ser) {
    //alert(ser);
    var chart = {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        events: {
            drilldown: function (e) {
                if (!e.seriesOptions) {
                    //alert(e.point.name);
                    //alert(e.point.series.name);
                    var chart = this;
                    if (e.point.series.name == 'Parent') {
                        data = getData(e.point.name);
                    }
                    else {
                        data = getData(e.point.series.name,e.point.name);
                    }
                    
                    //alert(data);
                    //console.log(data);
                    //highcharts.addSeriesAsDrilldown(e.point, data);
                    chart.addSeriesAsDrilldown(e.point, data);
                }

            }
        }
    };
    var title = {
        text: ''
    };

    // Radialize the colors
    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });

    var tooltip = {
        formatter: function () {
            return '<b>' + this.point.name + '</b>:<br/>Percentage: <b>' + Highcharts.numberFormat(this.percentage, 2) + ' %</b><br/>Count:<b>' + this.y+' </b>';
        }
    };

    //var tooltip = {
    //    pointFormat: ' Percentage: <b>{point.percentage:.1f}%</b><br/> Count: <b>{point.y} </b>'
        
    //};
    var plotOptions = {
        pie: {
            allowPointSelect: true,            
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name} </b>: {point.percentage:.1f}%',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    };

    //need to check with integer value.
    var series = [{
        //type: 'pie',       
        name: 'Parent',
        //colors: ['#4EE157', '#808080'],
        data: ser
    }];
    var drilldown = {
        series: []
    };
    var credits = {
        enabled:false
    };
    json2.chart = chart;
    json2.title = title;
    json2.tooltip = tooltip;
    json2.series = series;
    json2.drilldown = drilldown;
    json2.plotOptions = plotOptions;
    json2.credits = credits;
    $('#chartdiv').highcharts(json2);
}
var json2 = {};
var rows = '';

$.ajax({
    type: "GET",
    url: "Home/PiechartList",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
        var items = '';

        var aData = data;
        
        var marr = []
        
        $.map(aData, function (item, index) {
            var i = [item.PlanName, item.PaymentAmount];
            var obj = {};
            var arr = [String, Number];
            arr[0] = item.PlanName;
            arr[1] = item.PaymentAmount;
            //marr.push(arr);
            obj['name'] = item.PlanName;
            obj['y'] = item.PaymentAmount;
            obj['drilldown'] = true;
            marr.push(obj);
           
           rows += "['" + item.PlanName + "'," + item.PaymentAmount + "],"
        });      
        var C_Cart = (rows.substring(0, rows.length - 1))      
        var jsonArray = JSON.parse(JSON.stringify(marr));
        dd(jsonArray);

    },
    error: function (ex) {
        //var r = jQuery.parseJSON(ex.responseText);
        //alert("Message: " + r.Message);
        //alert("StackTrace: " + r.StackTrace);
        //alert("ExceptionType: " + r.ExceptionType);
    }

});


function bar(value) {
    //alert(value);
    var barjson = {};
    var chart = {
        type: 'column'
    };
    var title = {
        text: ''
    };
    var tooltip = {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    };
    var x = {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true,
        title: {
            text: 'Months'
        }
    };

    var y = {
        min: 0,
        title: {
            text: 'Counts'
        }
    };
    var plotOptions = {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    };

    //need to check with integer value.
    var series = [  
    {
        name: 'Campaign',
        data: value

    }];
    var credits = {
        enabled:false
    };
    barjson.chart = chart;
    barjson.title = title;
    barjson.tooltip = tooltip;
    barjson.xAxis = x;
    barjson.yAxis = y;
    barjson.series = series;
    barjson.plotOptions = plotOptions;
    barjson.credits = credits;
    $('#Monthbarchart').highcharts(barjson);

}

$.ajax({
    type: "GET",
    url: "Home/barchartList",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
        var baritems = '';

        var baraData = data;

        var barmarr = []
        // alert(aData);
        $.map(baraData, function (baritem, index) {
            var bari = [baritems.Name, baritems.Count];
            var barobj = {};           
            var bararr = [Number];          
            bararr[0] = baritem.count;
            barmarr.push(bararr);          
        });
      
        var barjsonArray = JSON.parse(JSON.stringify(barmarr));      
        bar(barjsonArray);


    },
    error: function (ex) {
        //var r = jQuery.parseJSON(ex.responseText);
        //alert("Message: " + r.Message);
        //alert("StackTrace: " + r.StackTrace);
        //alert("ExceptionType: " + r.ExceptionType);
    }

});



function barchartlead(value, value2) {
    //alert(value);
    var barleadjson = {};
    var chart = {
        type: 'column'
    };
    var title = {
        text: ''
    };
    var tooltip = {
        valueSuffix: ' '
    };
    var x = {
        categories: value,
        title: {
            text: 'Campaigns'
        }
    };

    var y = {
        min: 0,
        title: {
            text: 'Counts',
            //align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    };
    var plotOptions = {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    };
   
    //need to check with integer value.
    var series = [
    {        name: 'Leads',
    data: value2,
    color: '#E88F04'
    }];
    var credits = {
        enabled: false
    };
    barleadjson.chart = chart;
    barleadjson.title = title;
    barleadjson.tooltip = tooltip;
    barleadjson.xAxis = x;
    barleadjson.yAxis = y;
    
    barleadjson.series = series;
    barleadjson.plotOptions = plotOptions;
    barleadjson.credits = credits;
    $('#barchartleadcount').highcharts(barleadjson);

}

$.ajax({
    type: "GET",
    url: "Home/barchartleadList",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
        var baritems = '';

        var baraData = data;

        var barmarr = []
        var barmarr2 = []
        // alert(aData);
        $.map(baraData, function (baritem, index) {
            //var bari = [baritem.Name, baritem.count];
            //var barobj = {};
            var bararr = [String];
            bararr[0] = baritem.Name;            
            barmarr.push(bararr);

            var bararr2 = [Number];            
            bararr2[0] = baritem.count;
            barmarr2.push(bararr2);
        });

        var barjsonArray = JSON.parse(JSON.stringify(barmarr));
        var barjsonArray2 = JSON.parse(JSON.stringify(barmarr2));
        barchartlead(barjsonArray, barjsonArray2);


    },
    error: function (ex) {
        //var r = jQuery.parseJSON(ex.responseText);
        //alert("Message: " + r.Message);
        //alert("StackTrace: " + r.StackTrace);
        //alert("ExceptionType: " + r.ExceptionType);
    }

});


function barchartChannellead(value, value2) {
    //alert(value);
    var barleadjson = {};
    var chart = {
        type: 'spline'
    };
    var title = {
        text: ''
    };
    var tooltip = {
        valueSuffix: ' '
    };
    var x = {
        categories: value,
        title: {
            text: 'Channels'
        }
    };

    var y = {
        min: 0,
        title: {
            text: 'Counts',
            //align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    };
    var plotOptions = {
        spline: {
            lineWidth: 4,
            dataLabels: {
                enabled: true
            }
        }
    };

    //need to check with integer value.
    var series = [
    {
        name: 'Leads',
        data: value2,
        //color: '#E88F04'
    }];
    var credits = {
        enabled: false
    };
    barleadjson.chart = chart;
    barleadjson.title = title;
    barleadjson.tooltip = tooltip;
    barleadjson.xAxis = x;
    barleadjson.yAxis = y;

    barleadjson.series = series;
    barleadjson.plotOptions = plotOptions;
    barleadjson.credits = credits;
    $('#barchartChannelleadcount').highcharts(barleadjson);

}

$.ajax({
    type: "GET",
    url: "Home/barchartchannelleadList",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
        var baritems = '';

        var baraData = data;

        var barmarr = []
        var barmarr2 = []
        // alert(aData);
        $.map(baraData, function (baritem, index) {
            //var bari = [baritem.Name, baritem.count];
            //var barobj = {};
            var bararr = [String];
            bararr[0] = baritem.Name;
            barmarr.push(bararr);

            var bararr2 = [Number];
            bararr2[0] = baritem.count;
            barmarr2.push(bararr2);
        });

        var barjsonArray = JSON.parse(JSON.stringify(barmarr));
        var barjsonArray2 = JSON.parse(JSON.stringify(barmarr2));
        barchartChannellead(barjsonArray, barjsonArray2);


    },
    error: function (ex) {
        //var r = jQuery.parseJSON(ex.responseText);
        //alert("Message: " + r.Message);
        //alert("StackTrace: " + r.StackTrace);
        //alert("ExceptionType: " + r.ExceptionType);
    }

});

//var modalId = '';
//$('#enablecampaign').on('show.bs.modal', function (e) {

//    modalId = $(e.relatedTarget).data('id');

//})

var modalId = '';
var activetype = '';
function values(v1, v2) {
    modalId = v1;
    activetype = v2;
}

$("#btnenable").on("click", function (eve) {

    $.ajax({
        url: "../Home/Isactive",
        type: "POST",
        dataType: "text",
        //dataType: 'application/json; charset=utf-8',
        data: { id: modalId, id2: activetype },
        success: function (data, status, jqXHR) {

            //alert(status);

        },
        error: function (jqXHR, status, err) {

        }

    });
    setTimeout(function () {
        window.location.reload();
    }, 1500);
});





var val = '';
var val2 = '';

function typeone() {
    val = 1;
}
function typetwo() {
    val = 2;
}
function typethree() {
    val = 3;
}
function typefour() {
    val = 4;
}
$("#btnsubmit").on("click", function (eve) {
    val2 = $("#txtdata").val();
    $.ajax({
        url: "../Home/typeinsert",
        type: "POST",
        dataType: "text",
        //dataType: 'application/json; charset=utf-8',
        data: { id: val, name: val2 },
        success: function (data, status, jqXHR) {
            setTimeout(function () {
                window.location.reload();
            }, 1500);
            // alert("Added Successfuly");

        },
        error: function (jqXHR, status, err) {

        }
    });

});
