
var CreativeTableEditable = function () {


    var handleTable = function () {

        var nEditing = null;
        var nNew = false;

        var tableWrapper = $("#sample_editable_1_wrapper");

        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false //hide search box with special css class
        }); // initialize select2 dropdown


        //For Add Creative Page

        var table = $('#sample_editable_1').dataTable({
            "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }
            ]
        });

        var returnvalpath = '';
        var fileNameIndex = '';
        var filename = '';
        $('#sbmtupld').click(function () {
            var addval = 1;
            var indupdate = $("#RowIndex").val();
            var finalval = parseInt(indupdate) + parseInt(addval);
            // Checking whether FormData is available in browser  
            if (window.FormData !== undefined) {
                var fileUpload = $("#flupd").get(0);
                var files = fileUpload.files;

                var fileData = new FormData();

                for (var i = 0; i < files.length; i++) {
                    fileData.append(files[i].name, files[i]);
                }

                $.ajax({
                    url: '/Home/getUploadFilespath',
                    type: "POST",
                    contentType: false,
                    processData: false,
                    data: fileData,
                    success: function (result) {
                        returnvalpath = result[0];

                        fileNameIndex = returnvalpath.lastIndexOf('/') + 1;
                        filename = returnvalpath.substr(fileNameIndex);

                        $("#sample_editable_1").children().children()[finalval].children[3].innerHTML = filename;
                        $("#sample_editable_1").val(returnvalpath);
                    },
                    error: function (err) {
                        //  alert(err.statusText);
                    }
                });
            } else {
                alert("Data is not supported.");
            }
        });

        $("#sample_editable_1").on("click", "tr", function (e) {
            var index = $(this).closest("tr").index();
            var position = table.fnGetPosition(this);
            hiddenColumnValue = table.fnGetData(position)[0];
            $("#CreativeID").val(hiddenColumnValue.trim());
            $("#RowIndex").val(index);

            $.ajax({
                url: '../Home/UpdateID',
                type: 'POST',
                datatype: "TEXT",
                data: { 'CreativeID': hiddenColumnValue },

                success: function (data) {

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });
        });



        $('#sample_editable_1_new').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(table, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;


                } else {

                    table.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = table.fnAddData(['', '', '', '', '', '', '', '']);
            var nRow = table.fnGetNodes(aiNew[0]);
            editRow(table, nRow);
            nEditing = nRow;
            nNew = true;
            $("#sample_editable_1_new").removeClass("btn btn-success");
            $("#sample_editable_1_new").addClass("btn btn-default");
            $("#sample_editable_1_new").addClass("disableClick");
            $("#RowIndex").val('');
        });


        $('#sample_editable_1 tbody').on('click', 'tr td:nth-child(2) input', function () {
            $(this).datepicker('show').on('change', function () {
                var enddate = new Date($("#EndDate").val());
                var sdate = new Date($("#StartDate").val());
                if (enddate < sdate) {
                    $('.datepicker').hide();
                    $('#FromDateRestriction').modal({
                        show: 'True'
                    });
                    $("#StartDate").val('');
                }
                else {
                    $('.datepicker').hide();
                }
            });
        });


        $('#sample_editable_1 tbody').on('click', 'tr td:nth-child(3) input', function () {
            $(this).datepicker('show').on('change', function () {
                var enddate = new Date($("#EndDate").val());
                var sdate = new Date($("#StartDate").val());
                if (enddate < sdate) {
                    $('.datepicker').hide();
                    $('#ToDateRestriction').modal({
                        show: 'True'
                    });
                    $("#EndDate").val('');
                }
                else {
                    $('.datepicker').hide();
                }
            });
        });


        function load() {
            $.blockUI({
                message: "<img src='/assets/img/Loading_img.gif' alt='Please Wait...'></img> <h3>Please wait...</h3>",
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                }
            });
        }


        table.on('click', '.edit', function (e) {
            e.preventDefault();
            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                // nEditing = null;               
                restoreRow(table, nEditing);    //modified
                editRow(table, nRow);
                nEditing = nRow;
                $("#sample_editable_1_new").removeClass("btn btn-success");
                $("#sample_editable_1_new").addClass("btn btn-default");
                $("#sample_editable_1_new").addClass("disableClick");
                $("#RowIndex").val('');

            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                //$.blockUI();
                load();
                saveRow(table, nEditing);
                nEditing = null;

                //alert("Updated! Do not forget to do some ajax to sync with backend :)");

                var oTable = document.getElementById('sample_editable_1');
                var ind = $("#RowIndex").val();
                var oCells = oTable.rows.item(parseInt(ind) + 1).cells;
                var _name = oCells.item(0).innerHTML.trim();
                var _Sdate = oCells.item(1).innerHTML.trim();
                var _Edate = oCells.item(2).innerHTML.trim();

                if (_name != "Name" && _name != "") {

                    if (isNaN(hiddenColumnValue)) {
                        hiddenColumnValue = 0;
                    }


                    $.ajax({
                        url: '../Home/SaveCreative',
                        type: 'POST',
                        datatype: "TEXT",
                        data: { 'Name': _name, 'StartDate': _Sdate, 'EndDate': _Edate, 'filesavedpath': returnvalpath, 'Filename': filename },

                        success: function (data) {
                            swal({ title: "Saved Successfully", type: "success", text: "", timer: 1000, showConfirmButton: false },
                                function () {
                                    location.reload();
                                    data.hiddenColumnValue = null;

                                });

                        },
                        error: function () {
                            swal("Failure", "Something went wrong", "error");
                        },
                    });
                    $("#RowIndex").val('');
                }
                else {
                    swal({
                        title: "Please Fill Name Field!",
                        type: "warning"
                    },
                           function () {
                               location.reload();
                               $("#RowIndex").val('');
                           });
                }

            } else {
                /* No edit in progress - let's start one */

                //unload();

                editRow(table, nRow);
                nEditing = nRow;

                $("#sample_editable_1_new").removeClass("btn btn-success");
                $("#sample_editable_1_new").addClass("btn btn-default");
                $("#sample_editable_1_new").addClass("disableClick");      //modified

                $("#RowIndex").val('');
            }
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();

            if (nNew) {
                table.fnDeleteRow(nEditing);
                nNew = false;
                $("#sample_editable_1_new").removeClass("disableClick");
                $("#sample_editable_1_new").addClass("btn btn-success");

            } else {
                restoreRow(table, nEditing);
                nEditing = null;
                $("#sample_editable_1_new").removeClass("disableClick");
                $("#sample_editable_1_new").addClass("btn btn-success");   //modified
            }
        });


        $('#DeleteYes').click(function () {
            var s = $("#CreativeID").val();
            load();
            $.ajax({
                url: '../Home/DeleteCreative',
                type: 'POST',
                datatype: "TEXT",
                data: { 'CreativeID': s },
                success: function (data) {
                    swal({ title: "Deleted Successfully", type: "success", text: "", timer: 1000, showConfirmButton: false },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });

        });

        $("#Activate").click(function () {
            var Act = $("#CreativeID").val();
            load();
            $.ajax({
                url: '../Home/Activate',
                type: 'POST',
                datatype: "TEXT",
                data: { 'CreativeID': Act, 'Active': '0' },
                success: function (data) {
                    swal({ title: "Disabled Successfully", type: "success", text: "", timer: 1000, showConfirmButton: false },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });
        });

        $("#Deactivate").click(function () {
            var Deact = $("#CreativeID").val();
            load();
            $.ajax({
                url: '../Home/Activate',
                type: 'POST',
                datatype: "TEXT",
                data: { 'CreativeID': Deact, 'Active': '1' },
                success: function (data) {
                    swal({ title: "Enabled Successfully", type: "success", text: "", timer: 1000, showConfirmButton: false },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });
        });

        function restoreRow(table, nRow) {
            var aData = table.fnGetData(nRow);
            if (aData != null && aData[0] != "") {
                var jqTds = $('>td', nRow);
                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.fnUpdate(aData[i], nRow, i, false);
                }
                table.fnDraw();
            }
            else {
                table.fnDeleteRow(nEditing);
                nNew = false;
            }
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[1] + '">';
            jqTds[1].innerHTML = '<input type="text" id="StartDate" class="form-control small" value="' + aData[2] + '" readonly>';
            jqTds[2].innerHTML = '<input type="text" id="EndDate" class="form-control small" value="' + aData[3] + '" readonly>';
            //if (aData[0] == "") {
            //    jqTds[3].innerHTML = '<a class="btn btn-default modal-link disableClick" data-toggle="modal" data-target="#uploadImage" id="Browsebtn">Browse</a> ';
            //}
            //else {
            jqTds[3].innerHTML = '<a class="btn btn-default" data-toggle="modal" data-target="#uploadImage" id="Browsebtn">Browse</a> ';
            //}          
            jqTds[5].innerHTML = '<center><a class="edit" href="">Save</a></center>';
            jqTds[6].innerHTML = '<center><a class="cancel" href="">Cancel</a></center>';
        }

        function saveRow(table, nRow) {
            var jqInputs = $('input', nRow);
            table.fnUpdate(jqInputs[0].value, nRow, 1, false);
            table.fnUpdate(jqInputs[1].value, nRow, 2, false);
            table.fnUpdate(jqInputs[2].value, nRow, 3, false);
            table.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 6, false);
            table.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 7, false);
            table.fnDraw();
        }

        function cancelEditRow(table, nRow) {
            var jqInputs = $('input', nRow);
            table.fnUpdate(jqInputs[0].value, nRow, 0, false);
            table.fnUpdate(jqInputs[1].value, nRow, 1, false);
            table.fnUpdate(jqInputs[2].value, nRow, 2, false);
            table.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
            table.fnDraw();
        }

    }


    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

























