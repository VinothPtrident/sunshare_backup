
var TableEditable = function () {

    
    var handleTable = function () {

        var tableWrapper = $("#sample_editable_1_wrapper");

        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false //hide search box with special css class
        }); // initialize select2 dropdown

        //For Add Incentive Page

        function IncentiverestoreRow(ITable, nRow) {

            var aData = ITable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                ITable.fnUpdate(aData[i], nRow, i, false);
            }

            ITable.fnDraw();
        }

        function IncentiveeditRow(ITable, nRow) {
            var aData = ITable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
            //jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[4] + '">';
            jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
            jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
        }

        function IncentivesaveRow(ITable, nRow) {
            var jqInputs = $('input', nRow);
            ITable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            ITable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            ITable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            //ITable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            ITable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
            ITable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 6, false);
            ITable.fnDraw();
        }

        function IncentivecancelEditRow(ITable, nRow) {
            var jqInputs = $('input', nRow);
            ITable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            ITable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            ITable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            ITable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
            ITable.fnDraw();
        }

        var IncentiveTable = $('#sample_editableIncentive_1');

        var ITable = IncentiveTable.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0],
                'visible': false

            }, {
                "searchable": true,
                "targets": [0]
            }
            ],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc

        });

        $("#sample_editableIncentive_1").on("click", "tr", function (e) {
            var position = IncentiveTable.fnGetPosition(this);
            var hiddenColumnValue = IncentiveTable.fnGetData(position)[0];
            $("#IncentiveID").val(hiddenColumnValue.trim());
        });

        var nIncentiveEditing = null;
        var nIncentiveNew = false;

        $('#sample_editableIncentive_1_new').click(function (e) {
            e.preventDefault();

            if (nIncentiveNew && nIncentiveEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    IncentivesaveRow(ITable, nIncentiveEditing); // save
                    $(nIncentiveEditing).find("td:first").html("Untitled");
                    nIncentiveEditing = null;
                    nIncentiveNew = false;

                } else {

                    ITable.fnDeleteRow(nIncentiveEditing); // cancel
                    nIncentiveEditing = null;
                    nIncentiveNew = false;
                    return;
                }
            }
            
            var aiNew = ITable.fnAddData(['', '', '', '', '', '', '', '']);
            var nRow = ITable.fnGetNodes(aiNew[0]);
            IncentiveeditRow(ITable, nRow);
            nIncentiveEditing = nRow;
            nIncentiveNew = true;
        });

        $('#sample_editableIncentive_1 tbody').on('click', 'tr td:nth-child(2) input', function () {
            $(this).datepicker('show').on('change', function () {
                $('.datepicker').hide();
            });
        });

        $('#sample_editableIncentive_1 tbody').on('click', 'tr td:nth-child(3) input', function () {
            $(this).datepicker('show').on('change', function () {
                $('.datepicker').hide();
            });
        });

        $('#sample_editableIncentive_1 tbody').on('click', 'tr td:nth-child(5)', function () {

            var value = $(this).closest('tr').find('td:eq(4)').text().trim();
         
            var position = IncentiveTable.fnGetPosition(this);

            var hiddenColumnValue = IncentiveTable.fnGetData(position)[0];

            if (hiddenColumnValue.trim() == "") {
                hiddenColumnValue = "0";
                //alert(hiddenColumnValue);
            }

            var name = $(this).closest('tr').find('td:eq(0) input').val();
            var Sdate = $(this).closest('tr').find('td:eq(1) input').val();
            var Edate = $(this).closest('tr').find('td:eq(2) input').val();

            if (value == 'Save') {

                $.ajax({
                    url: '../Home/SaveIncentive',
                    type: 'POST',
                    datatype: "TEXT",
                    data: { 'IncentiveID': hiddenColumnValue.trim(),'Name':name, 'StartDate': Sdate, 'EndDate': Edate },
                    success: function (data) {

                        swal({
                            title: "Success",
                            text: "Saved Successfully",
                            type: "success"
                        },
                            function () {
                                location.reload();
                            });

                    },
                    error: function () {
                        swal("Failure", "Something went wrong", "error");
                    }
                });
            }
        });

        $('#sample_editableIncentive_1 tbody').on('click', 'tr td:nth-child(6)', function () {

            var value = $(this).closest('tr').find('td:eq(5)').text().trim();

            var position = IncentiveTable.fnGetPosition(this);

            var hiddenColumnValue = IncentiveTable.fnGetData(position)[0];

            if (value == 'Delete') {

                $.ajax({
                    url: '../Home/DeleteIncentive',
                    type: 'POST',
                    datatype: "TEXT",
                    data: { 'IncentiveID': hiddenColumnValue.trim() },
                    success: function (data) {

                        swal({
                            title: "Success",
                            text: "Deleted Successfully",
                            type: "success"
                        },
                            function () {
                                location.reload();
                            });

                    },
                    error: function () {
                        swal("Failure", "Something went wrong", "error");
                    }
                });
            }
        });

        $("#IncentiveActivate").on("click", "tr", function () {

            var s = $('#IncentiveID').val();

            $.ajax({
                url: '../Home/IncentiveActivate',
                type: 'POST',
                datatype: "TEXT",
                data: { 'IncentiveID': s, 'Active': '0' },
                success: function (data) {

                    swal({
                        title: "Success",
                        text: "Disabled Successfully",
                        type: "success"
                    },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });
        });

        $("#IncentiveDeactivate").on('click', 'tr', function () {
            var s = $("#IncentiveID").val();
            $.ajax({
                url: '../Home/IncentiveActivate',
                type: 'POST',
                datatype: "TEXT",
                data: { 'IncentiveID': s, 'Active': '1' },
                success: function (data) {

                    swal({
                        title: "Success",
                        text: "Enabled Successfully",
                        type: "success"
                    },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });
        });


        IncentiveTable.on('click', '.delete', function (e) {
            e.preventDefault();

            //if (confirm("Are you sure to delete this row ?") == false) {
            //    return;
            //}
            var nRow = $(this).parents('tr')[0];
            ITable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        IncentiveTable.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nIncentiveNew) {
                ITable.fnDeleteRow(nIncentiveEditing);
                nIncentiveNew = false;
            } else {
                IncentiverestoreRow(ITable, nIncentiveEditing);
                nIncentiveEditing = null;
            }
        });

        IncentiveTable.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nIncentiveEditing !== null && nIncentiveEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                IncentiverestoreRow(ITable, nIncentiveEditing);
                IncentiveeditRow(ITable, nRow);
                nIncentiveEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                //alert('1');
                IncentivesaveRow(ITable, nIncentiveEditing);
                nIncentiveEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                IncentiveeditRow(ITable, nRow);
                nIncentiveEditing = nRow;
            }
        });

        //----------------------------------------------------
        //----------------------------------------------------
        //----------------------------------------------------

        //For Add Creative Page

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
            jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
            jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
            oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 6, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1');

        var oTable = table.dataTable({
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0],
                'visible': false
            }, {
                "searchable": true,
                "targets": [0]
            }
            ],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc

        });

        

        $("#sample_editable_1").on("click", "tr", function (e) {
            var position = table.fnGetPosition(this);
            var hiddenColumnValue = table.fnGetData(position)[0];
            $("#CreativeID").val(hiddenColumnValue.trim());
        });


        $('#sample_editable_1 tbody').on('click', 'tr td:nth-child(2) input', function () {
            $(this).datepicker('show').on('change', function () {
                $('.datepicker').hide();
            });
        });

        $('#sample_editable_1 tbody').on('click', 'tr td:nth-child(3) input', function () {
            $(this).datepicker('show').on('change', function () {
                $('.datepicker').hide();
            });
        });


        $('#sample_editable_1 tbody').on('click', 'tr td:nth-child(6)', function () {

            var value = $(this).closest('tr').find('td:eq(5)').text().trim();

            var position = table.fnGetPosition(this);

            var hiddenColumnValue = table.fnGetData(position)[0];

            alert(hiddenColumnValue.trim());

            if (value == 'Delete') {
                $.ajax({
                    url: '../Home/DeleteCreative',
                    type: 'POST',
                    datatype: "TEXT",
                    data: { 'CreativeID': hiddenColumnValue.trim() },
                    success: function (data) {

                        swal({
                            title: "Success",
                            text: "Deleted Successfully",
                            type: "success"
                        },
                            function () {
                                location.reload();
                            });

                    },
                    error: function () {
                        swal("Failure", "Something went wrong", "error");
                    }
                });
            }

        });

        $('#sample_editable_1 tbody').on('click', 'tr td:nth-child(5)', function () {

            var value = $(this).closest('tr').find('td:eq(4)').text().trim();


            var position = table.fnGetPosition(this);

            var hiddenColumnValue = table.fnGetData(position)[0];


            if (hiddenColumnValue.trim() == "") {
                hiddenColumnValue = "0";
            }

            var name = $(this).closest('tr').find('td:eq(0) input').val();
            var Sdate = $(this).closest('tr').find('td:eq(1) input').val();
            var Edate = $(this).closest('tr').find('td:eq(2) input').val();

            if (value == 'Save') {

                $.ajax({
                    url: '../Home/SaveCreative',
                    type: 'POST',
                    datatype: "TEXT",
                    data: { 'CreativeID': hiddenColumnValue.trim(), 'Name': name, 'StartDate': Sdate, 'EndDate': Edate },

                    success: function (data) {

                        swal({
                            title: "Success",
                            text: "Saved Successfully",
                            type: "success"
                        },
                            function () {
                                location.reload();
                            });

                    },
                    error: function () {
                        swal("Failure", "Something went wrong", "error");
                    }
                });
            }
        });

        $("#Activate").click(function () {
            var s = $("#CreativeID").val();
            $.ajax({
                url: '../Home/Activate',
                type: 'POST',
                datatype: "TEXT",
                data: { 'CreativeID': s, 'Active': '0' },
                success: function (data) {

                    swal({
                        title: "Success",
                        text: "Disabled Successfully",
                        type: "success"
                    },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });
        });

        $("#Deactivate").click(function () {
            var s = $("#CreativeID").val();
            $.ajax({
                url: '../Home/Activate',
                type: 'POST',
                datatype: "TEXT",
                data: { 'CreativeID': s, 'Active': '1' },
                success: function (data) {

                    swal({
                        title: "Success",
                        text: "Enabled Successfully",
                        type: "success"
                    },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });
        });

        var nEditing = null;
        var nNew = false;

        $('#sample_editable_1_new').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;


                } else {

                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            //if (confirm("Are you sure to delete this row ?") == false) {
            //    return;
            //}

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();

            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                //alert('1');
                saveRow(oTable, nEditing);
                nEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });

        //----------------------------------------------------------------
        //----------------------------------------------------------------
        //----------------------------------------------------------------
    }

    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();












