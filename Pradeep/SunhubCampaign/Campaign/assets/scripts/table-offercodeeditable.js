

var TableOfferCodeEditable = function () {


    var handleTable = function () {

        var nEditing = null;
        var nNew = false;

        var tableWrapper = $("#sample_editable_1_wrapper");

        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false //hide search box with special css class
        }); // initialize select2 dropdown


        //For Add Creative Page

        var table = $('#sample_OfferCodeeditable_1').dataTable({
            "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }
            ]
        });

        $("#sample_OfferCodeeditable_1").on("click", "tr", function (e) {
            var index = $(this).closest("tr").index();
            var position = table.fnGetPosition(this);
            hiddenColumnValue = table.fnGetData(position)[0];
            $("#OfferCodeID").val(hiddenColumnValue.trim());
            $("#OfferRowIndex").val(index);
        });

       

        $('#sample_Offercodeeditable_1_new').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(table, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;
                   

                } else {

                    table.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = table.fnAddData(['', '', '', '', '', '', '', '']);
            var nRow = table.fnGetNodes(aiNew[0]);
            editRow(table, nRow);
            nEditing = nRow;
            nNew = true;
            $("#sample_Offercodeeditable_1_new").removeClass("btn btn-success");
            $("#sample_Offercodeeditable_1_new").addClass("btn btn-default");
            $("#sample_Offercodeeditable_1_new").addClass("disableClick");
            $("#OfferRowIndex").val('');
        });

        function load() {
            $.blockUI({
                message: "<img src='/assets/img/Loading_img.gif' alt='Please Wait...'></img> <h3>Please wait...</h3>",
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                }
            });
        }

        $('#sample_OfferCodeeditable_1 tbody').on('click', 'tr td:nth-child(2) input', function () {
            $(this).datepicker('show').on('change', function () {
                var enddate = new Date($("#EndDate").val());
                var sdate = new Date($("#StartDate").val());
                if (enddate < sdate) {
                    $('.datepicker').hide();
                    $('#FromDateRestriction').modal({
                        show: 'True'
                    });
                    $("#StartDate").val('');
                }
                else {
                    $('.datepicker').hide();
                }
            });
        });


        $('#sample_OfferCodeeditable_1 tbody').on('click', 'tr td:nth-child(3) input', function () {
            $(this).datepicker('show').on('change', function () {
                var enddate = new Date($("#EndDate").val());
                var sdate = new Date($("#StartDate").val());
                if (enddate < sdate) {
                    $('.datepicker').hide();
                    $('#ToDateRestriction').modal({
                        show: 'True'
                    });
                    $("#EndDate").val('');
                }
                else {
                    $('.datepicker').hide();
                }
            });
        });


        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                //nEditing = null;
                restoreRow(table, nEditing);
                editRow(table, nRow);
                nEditing = nRow;

                $("#sample_Offercodeeditable_1_new").removeClass("btn btn-success");
                $("#sample_Offercodeeditable_1_new").addClass("btn btn-default");
                $("#sample_Offercodeeditable_1_new").addClass("disableClick");
                $("#OfferRowIndex").val('');

            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                load();

                saveRow(table, nEditing);
                nEditing = null;

                //alert("Updated! Do not forget to do some ajax to sync with backend :)");

                var oTable = document.getElementById('sample_OfferCodeeditable_1');
                var ind = $("#OfferRowIndex").val();
                var oCells = oTable.rows.item(parseInt(ind) + 1).cells;
                var _name = oCells.item(0).innerHTML.trim();
                var _Sdate = oCells.item(1).innerHTML.trim();
                var _Edate = oCells.item(2).innerHTML.trim();
                if (_name != "Name" && _name != "") {
                    if (isNaN(hiddenColumnValue)) {
                        hiddenColumnValue = 0;
                    }

                    $.ajax({
                        url: '../Home/SaveOfferCode',
                        type: 'POST',
                        datatype: "TEXT",
                        data: { 'OfferCodeID': hiddenColumnValue, 'Name': _name, 'StartDate': _Sdate, 'EndDate': _Edate },

                        success: function (data) {
                            swal({ title: "Saved Successfully", type: "success", text: "", timer: 1000, showConfirmButton: false },
                                function () {
                                    location.reload();
                                    data.hiddenColumnValue = null;
                                });

                        },
                        error: function () {
                            swal("Failure", "Something went wrong", "error");
                        }

                    });

                    $("#OfferRowIndex").val('');
                }
                else {
                    swal({
                        title: "Please Fill Name Field!",
                        type: "warning"
                    },
                           function () {
                               location.reload();
                               $("#OfferRowIndex").val('');
                           });
                }

            } else {
                /* No edit in progress - let's start one */
                editRow(table, nRow);
                nEditing = nRow;

                $("#sample_Offercodeeditable_1_new").removeClass("btn btn-success");
                $("#sample_Offercodeeditable_1_new").addClass("btn btn-default");
                $("#sample_Offercodeeditable_1_new").addClass("disableClick");      //modified

                $("#OfferRowIndex").val('');
            }
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();

            if (nNew) {
                table.fnDeleteRow(nEditing);
                nNew = false;
                $("#sample_Offercodeeditable_1_new").removeClass("disableClick");
                $("#sample_Offercodeeditable_1_new").addClass("btn btn-success");
            } else {
                restoreRow(table, nEditing);
                nEditing = null;
                $("#sample_Offercodeeditable_1_new").removeClass("disableClick");
                $("#sample_Offercodeeditable_1_new").addClass("btn btn-success");
            }
        });

       
        $('#OfferCodeDeleteYes').click(function () {
            var s = $("#OfferCodeID").val();
            load();
            $.ajax({
                url: '../Home/DeleteOfferCode',
                type: 'POST',
                datatype: "TEXT",
                data: { 'OfferCodeID': s },
                success: function (data) {
                    swal({ title: "Deleted Successfully", type: "success", text: "", timer: 1000, showConfirmButton: false },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });

        });

        $("#OffercodeActivate").click(function () {
            var Act = $("#OfferCodeID").val();
            load();
            $.ajax({
                url: '../Home/OfferCodeActivate',
                type: 'POST',
                datatype: "TEXT",
                data: { 'OfferCodeID': Act, 'Active': '0' },
                success: function (data) {
                    swal({ title: "Disabled Successfully", type: "success", text: "", timer: 1000, showConfirmButton: false },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });
        });

        $("#OfferCodeDeactivate").click(function () {
            var Deact = $("#OfferCodeID").val();
            load();
            $.ajax({
                url: '../Home/OfferCodeActivate',
                type: 'POST',
                datatype: "TEXT",
                data: { 'OfferCodeID': Deact, 'Active': '1' },
                success: function (data) {
                    swal({ title: "Enabled Successfully", type: "success", text: "", timer: 1000, showConfirmButton: false },
                        function () {
                            location.reload();
                        });

                },
                error: function () {
                    swal("Failure", "Something went wrong", "error");
                }
            });
        });

        function restoreRow(table, nRow) {
            var aData = table.fnGetData(nRow);
            if (aData != null && aData[0] != "") {
                var jqTds = $('>td', nRow);
                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.fnUpdate(aData[i], nRow, i, false);
                }
                table.fnDraw();
            }
            else
            {
                table.fnDeleteRow(nEditing);
                nNew = false;
            }
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);          
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[1] + '">';
            jqTds[1].innerHTML = '<input type="text" id="StartDate" class="form-control small" value="' + aData[2] + '" readonly>';
            jqTds[2].innerHTML = '<input type="text" id="EndDate" class="form-control small" value="' + aData[3] + '" readonly>';
            jqTds[4].innerHTML = '<center><a class="edit" href="">Save</a></center>';
            jqTds[5].innerHTML = '<center><a class="cancel" href="">Cancel</a></center>';
        }

        function saveRow(table, nRow) {
            var jqInputs = $('input', nRow);
            table.fnUpdate(jqInputs[0].value, nRow, 1, false);
            table.fnUpdate(jqInputs[1].value, nRow, 2, false);
            table.fnUpdate(jqInputs[2].value, nRow, 3, false);
            table.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
            table.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 6, false);
            table.fnDraw();
        }

        function cancelEditRow(table, nRow) {
            var jqInputs = $('input', nRow);
            table.fnUpdate(jqInputs[0].value, nRow, 0, false);
            table.fnUpdate(jqInputs[1].value, nRow, 1, false);
            table.fnUpdate(jqInputs[2].value, nRow, 2, false);
            table.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
            table.fnDraw();
        }

    }

    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

























