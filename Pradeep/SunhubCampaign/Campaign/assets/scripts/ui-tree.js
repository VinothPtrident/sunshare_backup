var barjsonArray = "";
var UITree = function () {


    var contextualMenuSample = function() {
             
      
        $("#tree_3").jstree({
            "core": {
                "themes": {
                    "responsive": false
                },
                // so that create works
                "check_callback": true,
                'data': barjsonArray               
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state": { "key": "demo2" },
            "plugins": ["contextmenu", "dnd", "state", "types"]
        });

    }

    
    return {
        //main function to initiate the module
        init: function () {
            contextualMenuSample();
         
        }

    };

}();