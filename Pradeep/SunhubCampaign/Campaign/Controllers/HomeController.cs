﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Campaign.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;

namespace Campaign.Controllers
{
    public class HomeController : Controller
    {       
        campaigndata data = new campaigndata();
        List<campaignfields> list_camp = new List<campaignfields>();
        campaignfields camp_field = new campaignfields();
        public ActionResult Index()
        {
      
            return View();
        }

        //public ActionResult Dashboard()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [HttpGet]
        public JsonResult PiechartList()
        {
           // System.Threading.Thread.Sleep(5000);
            List<piechart> lstchart = new List<piechart>();
            piechart cls;
            data.datasetpiechart = data.piedata();
            for (int i = 0; i < data.datasetpiechart.Tables[0].Rows.Count; i++)
            {
                cls = new piechart();
                cls.PlanName = data.datasetpiechart.Tables[0].Rows[i]["Name"].ToString();
                cls.PaymentAmount = Convert.ToInt32(data.datasetpiechart.Tables[0].Rows[i]["count"].ToString());
                lstchart.Add(cls);
            }
            return Json(lstchart, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult PiechartdrillList(string parentname, string channelname)
        {
            // System.Threading.Thread.Sleep(5000);
            List<piechart> lstchart = new List<piechart>();
            piechart cls;
            data.datasetpiechart = data.piedatadrill(parentname, channelname);
            for (int i = 0; i < data.datasetpiechart.Tables[0].Rows.Count; i++)
            {
                cls = new piechart();
                cls.PlanName = data.datasetpiechart.Tables[0].Rows[i]["Name"].ToString();
                cls.PaymentAmount = Convert.ToInt32(data.datasetpiechart.Tables[0].Rows[i]["count"].ToString());
                lstchart.Add(cls);
            }
            return Json(lstchart, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult barchartList()
        {
            // System.Threading.Thread.Sleep(5000);
            List<chartbymonth> lstchart = new List<chartbymonth>();
            chartbymonth clsmonth;           
            for (int i = 0; i < data.barchartbymonth().Tables[0].Rows.Count; i++)
            {
                clsmonth = new chartbymonth();
                clsmonth.Name = data.barchartbymonth().Tables[0].Rows[i]["Name"].ToString();
                clsmonth.count = Convert.ToInt32(data.barchartbymonth().Tables[0].Rows[i]["Count"].ToString());
                lstchart.Add(clsmonth);
            }
            return Json(lstchart, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult barchartleadList()
        {
            // System.Threading.Thread.Sleep(5000);
            List<chartbyleadcount> lstleadchart = new List<chartbyleadcount>();
            chartbyleadcount clsleadcount;
            for (int i = 0; i < data.barchartbyleadcount().Tables[0].Rows.Count; i++)
            {
                clsleadcount = new chartbyleadcount();
                clsleadcount.Name = data.barchartbyleadcount().Tables[0].Rows[i]["Name"].ToString();
                clsleadcount.count = Convert.ToInt32(data.barchartbyleadcount().Tables[0].Rows[i]["count"].ToString());
                lstleadchart.Add(clsleadcount);
            }
            return Json(lstleadchart, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult barchartchannelleadList()
        {
            // System.Threading.Thread.Sleep(5000);
            List<chartbyleadchannelcount> lstleadchannelchart = new List<chartbyleadchannelcount>();
            chartbyleadchannelcount clsleadchannelcount;
            for (int i = 0; i < data.barchartbyleadchannelcount().Tables[0].Rows.Count; i++)
            {
                clsleadchannelcount = new chartbyleadchannelcount();
                clsleadchannelcount.Name = data.barchartbyleadchannelcount().Tables[0].Rows[i]["Channel"].ToString();
                clsleadchannelcount.count = Convert.ToInt32(data.barchartbyleadchannelcount().Tables[0].Rows[i]["count"].ToString());
                lstleadchannelchart.Add(clsleadchannelcount);
            }
            return Json(lstleadchannelchart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveParents(string name)
        {
            try
            {
                data.modelinsertparent(name);
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("Hierarchy", "home");
        }

        //[HttpPost]
        //public ActionResult CreateCampaignload(FormCollection collection)
        //{
        //    try
        //    {

        //        ViewBag.insermsg = null;
        //        string Name = collection["Name"];
        //        string Sdate = collection["Sdate"];
        //        string Edate = collection["Edate"];
        //        string PlannedBudget = collection["Planned Budget"];


        //        string parent = collection["parent"];
        //        string Channel = collection["Channel"];
        //        string subtype = collection["subtype"];
        //        string Incentive = collection["Incentive"];               
        //        string Creative = collection["Creative"];               
        //        string Offercode = collection["Offercode"];
        //        string LandingPage = collection["Landing Page"];
        //        string Budget = collection["Budget"];

        //        collection = null;

        //        if (data.insertcampaigndata(Name, parent, subtype, Incentive, Creative, Sdate, Edate, LandingPage, Budget, "", null, false))
        //        {
        //            //ViewBag.insermsg = "Inserted Successfully.";
        //            TempData["Success"] = "Inserted Successfully.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //     return RedirectToAction("CreateCampaign", "Home");
        //}

        [HttpPost]
        public JsonResult UPDATEEDITCAMPAIGNDATA(string id2)
        {
            List<string> lstrnval = new List<string>();
            string[] vals = id2.Split('|');
            try
            {

                string Name = vals[0];
                string Sdate = vals[1];
                string Edate = vals[2];
                string PlannedBudget = vals[3];
                string Type = vals[4];
                string Channel = vals[5];
                string SubType = vals[6];
                string Incentive = vals[7];
                string Creative = vals[8];
                string PhoneNo = vals[9];
                string Landing = vals[10];
                string Budget = vals[11];
                string campid = vals[12];
                string OfferCode = vals[13];
                string State = vals[14];
                string County = vals[15];
                string PlannedCost = vals[16];
                string ActualCost = vals[17];

                if (data.updatecampaigndata(campid, Name, Sdate, Edate, PlannedBudget, Type, Channel, SubType, Incentive, Creative, PhoneNo, Landing, Budget, "", State, County, PlannedCost, ActualCost, OfferCode))
                {
                    lstrnval.Add("Success");
                }
            }
            catch (Exception ex) { }

            return Json(lstrnval, JsonRequestBehavior.AllowGet);
        }


        //[HttpGet]
        //public JsonResult SaveTypes(string name)
        //{
        //    string[] vals = name.Split(','); List<string> lstreturnval = new List<string>();
        //    Array.Reverse(vals);
        //    string query = string.Format("exec proc_c_savetypes '"+ vals[0] + "','"+ vals[1] + "','"+ vals[2] + "'");
        //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

        //    SqlCommand cmd = new SqlCommand(query, con);
        //    con.Open();
        //    int val = cmd.ExecuteNonQuery();
        //    con.Close();
        //    if (val > 0)
        //    {
        //        lstreturnval.Add("Success ");
        //    }
        //    else {
        //        lstreturnval.Add("Failure");
        //    }
        //    lstreturnval.Add("Success ");
        //    return Json(lstreturnval, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult SpecificCampaignDetails(FormCollection collection)
        {
            try
            {
                ViewBag.start = "";
                ViewBag.end = "";
                string Sdate = collection["Sdate"];
                string Edate = collection["Edate"];
                data.filterdata = data.getfilterdata(Sdate, Edate);
                ViewBag.start = Sdate;
                ViewBag.end = Edate;
            }
            catch (Exception ex) { }
            return View(data);
        }


        [HttpPost]
        public ActionResult Isactive(string id,string id2)
        {
            try
            {
                //data.modelActiveCampaign(Convert.ToInt32(id),id2);
                data.modelActiveCampaign(id, id2);
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("CampaignDetails", "home");
        }

        [HttpPost]
        public ActionResult typeinsert(string id, string name)
        {
            try
            {
                data.InsertType(id, name);
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("CreateCampaign", "home");
        }

        [HttpPost]
        public JsonResult LoadTypes()
        {

            DataTable dttreetypes = new DataTable();
            string query = string.Format("select * from tbl_m_Types_CM");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlDataAdapter cmd = new SqlDataAdapter(query, con);
            cmd.Fill(dttreetypes);
            con.Close();

            List<TreeViewItemModel> Childrenlist = new List<TreeViewItemModel>();
            List<TreeViewItemModel> aChildrenlist = null;
            List<TreeViewItemModel> aaChildrenlist = null;
            TreeViewItemModel aChild = null;
            TreeViewItemModel aaChild = null;
            DataTable dtparent = new DataTable();
            dtparent.Columns.Add("PType");
            List<string> liPType = new List<string>();

            for (int ti = 0; ti < dttreetypes.Rows.Count; ti++)
            {

                if (!liPType.Contains(dttreetypes.Rows[ti][1].ToString()))
                {
                    liPType.Add(dttreetypes.Rows[ti][1].ToString());
                    DataRow dr1 = dtparent.NewRow();
                    dr1[0] = dttreetypes.Rows[ti][1].ToString();
                    dtparent.Rows.Add(dr1);
                }
            }


            for (int ti = 0; ti < dtparent.Rows.Count; ti++)
            {
                TreeViewItemModel aItem = new TreeViewItemModel();
                aItem.text = dtparent.Rows[ti][0].ToString();
                aItem.icon = "flaticon-shapes";
                DataRow[] result = null;
                result = dttreetypes.Select("ParentType = '" + dtparent.Rows[ti][0].ToString() + "'");
                List<string> li2 = new List<string>();

                aChildrenlist = new List<TreeViewItemModel>();
                foreach (DataRow row in result)
                {
                    aChild = new TreeViewItemModel();
                    DataRow[] result2 = null;
                    result2 = dttreetypes.Select("Channel = '" + row[3].ToString() + "' and ParentType = '" + dtparent.Rows[ti][0].ToString() + "'");
                    if (!li2.Contains(row[3].ToString()))
                    {
                        if (row[3].ToString() != "")
                        {
                            aChild.text = row[3].ToString();
                            aChild.icon = "flaticon-travel";
                            li2.Add(row[3].ToString());
                            aaChildrenlist = new List<TreeViewItemModel>();
                            foreach (DataRow row2 in result2)
                            {
                                if (row2[2].ToString() != "")
                                {
                                    aaChild = new TreeViewItemModel();
                                    aaChild.text = row2[2].ToString();
                                    aaChild.icon = "fa fa-file icon-state-warning";
                                    aaChildrenlist.Add(aaChild);
                                    aChild.children = aaChildrenlist;
                                }
                            }
                            aChildrenlist.Add(aChild);
                        }
                    }

                }
                aItem.children = aChildrenlist;
                Childrenlist.Add(aItem);

            }

            return Json(Childrenlist, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveTypes(string valjson)
        {
            string ParentID1 = "";
            bool isParent1 = false;

            string ParentID2 = "";
            bool isParent2 = false;

            string ParentID3 = "";
            bool isParent3 = false;

            Dictionary<string, string> JsonValue = new Dictionary<string, string>();
            JArray a = JArray.Parse(valjson);

            foreach (JObject o in a.Children<JObject>())
            {
                isParent1 = true;
                foreach (JProperty p in o.Properties())
                {
                    string name = p.Name;
                    if (name.IndexOf("children") >= 0)
                    {
                        int idx = name.IndexOf("\"children\":");
                        int length = ("\"children\":").Length;
                        string x = p.ToString().Substring(length);

                        if (x.Trim().Length > 2)
                        {
                            JArray Level2Array = JArray.Parse(x);
                            foreach (JObject Level2o in Level2Array.Children<JObject>())
                            {
                                isParent2 = true;
                                foreach (JProperty Level2p in Level2o.Properties())
                                {
                                    string Level2name = Level2p.Name;
                                    if (Level2name.IndexOf("children") >= 0)
                                    {
                                        int Level2idx = Level2name.IndexOf("\"children\":");
                                        int Level2length = ("\"children\":").Length;
                                        string Level2x = Level2p.ToString().Substring(Level2length);

                                        if (Level2x.Trim().Length > 2)
                                        {
                                            JArray Level3Array = JArray.Parse(Level2x);
                                            foreach (JObject Level3o in Level3Array.Children<JObject>())
                                            {
                                                isParent3 = true;
                                                foreach (JProperty Level3p in Level3o.Properties())
                                                {
                                                    string Level3name = Level3p.Name;
                                                    if (Level3name.IndexOf("children") >= 0)
                                                    {
                                                        int Level3idx = Level3name.IndexOf("\"children\":");
                                                        int Level3length = ("\"children\":").Length;
                                                        string Level3x = Level3p.ToString().Substring(Level3length);

                                                        if (Level3x.Trim().Length > 2)
                                                        {

                                                        }
                                                    }
                                                    else if (Level3p.Name.IndexOf("li_attr") == -1 && Level3p.Name.IndexOf("a_attr") == -1 && Level3p.Name.IndexOf("state") == -1 && Level3p.Name.IndexOf("text") >= 0)
                                                    {
                                                        string value = (string)Level3p.Value;

                                                        if (!JsonValue.ContainsKey(value))
                                                        {
                                                            JsonValue.Add(value, ParentID2);
                                                        }

                                                        //  System.Diagnostics.Debug.Print(ParentID2 + " -- " + value);

                                                        if (isParent3)
                                                        {
                                                            ParentID3 = value;
                                                            isParent3 = false;

                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (Level2p.Name.IndexOf("li_attr") == -1 && Level2p.Name.IndexOf("a_attr") == -1 && Level2p.Name.IndexOf("state") == -1 && Level2p.Name.IndexOf("text") >= 0)
                                    {
                                        string value = (string)Level2p.Value;

                                        if (!JsonValue.ContainsKey(value))
                                        {
                                            JsonValue.Add(value, ParentID1);
                                        }

                                        //  System.Diagnostics.Debug.Print(ParentID1 + " -- " + value);

                                        if (isParent2)
                                        {
                                            ParentID2 = value;
                                            isParent2 = false;

                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (p.Name.IndexOf("li_attr") == -1 && p.Name.IndexOf("a_attr") == -1 && p.Name.IndexOf("state") == -1 && p.Name.IndexOf("text") >= 0)
                    {
                        string value = (string)p.Value;

                        if (isParent1)
                        {
                            ParentID1 = value;
                        }
                        if (!JsonValue.ContainsKey(value))
                        {
                            JsonValue.Add(value, ParentID1);
                        }

                        // System.Diagnostics.Debug.Print(ParentID1 + " -- " + value);

                        if (isParent1)
                        {
                            ParentID1 = value;
                            isParent1 = false;
                        }
                    }
                }
            }

            foreach (string key in JsonValue.Keys)
            {
                string value = "";
                string parent = "";
                string rootParent = "";
                value = key;
                parent = JsonValue[key].ToString();
                rootParent = JsonValue[parent].ToString();

                if (value == parent && value == rootParent)
                {
                    rootParent = "";
                    parent = "";
                }
                else if (rootParent == parent)
                {
                    rootParent = "";
                }
                else if (value == parent)
                {
                    value = "";
                }

                //System.Diagnostics.Debug.Print(rootParent + " -- " + parent + " -- " + value);
                System.Diagnostics.Debug.Print(value + " -- " + parent + " -- " + rootParent);
                if ((value == "" && parent == "" && rootParent != "") || (value != "" && parent == "" && rootParent == ""))
                {

                }
                else
                {

                    data.InsertfromtreeType(value, parent, rootParent);
                }
            }
            
            return View();
        }

        [HttpPost]
        public JsonResult Deletenode(string nodetype, string nodetext, string parenttext, string parenttext2)
        {
            List<string> lstrndeleteval = new List<string>();
            try
            {
                data.deletetreenode(nodetype, nodetext, parenttext, parenttext2);
                lstrndeleteval.Add("Success");
            }
            catch { }

            return Json(lstrndeleteval, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Renametreenodest(string nodeID, string nodeold, string nodetext, string Parentnodetxt, string Rootnodetxt)
        {
            int val = 0;
            try
            {
                val = data.renametreenode(nodeID, nodeold, nodetext, Parentnodetxt, Rootnodetxt);
            }
            catch { }

            return Json(val, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult home()
        {
            try
            {
                data.filterdata = data.getfilterdata("", "");
                data.futuredata = data.getfuturedata();
                data.completeddata = data.getcompleteddata();
                sharecount();

            }
            catch (Exception ex) { }
            return View(data);
        }

        public void sharecount()
        {
            DataSet dscount = new DataSet();
            dscount = data.gettypecount();

            if (dscount.Tables[0].Rows.Count > 0)
            {
                DataRow[] dr = dscount.Tables[0].Select("TypeName = 'TV'");
                DataRow[] dr2 = dscount.Tables[0].Select("TypeName = 'Radio'");
                DataRow[] dr3 = dscount.Tables[0].Select("TypeName = 'Direct Mail'");
                DataRow[] dr4 = dscount.Tables[0].Select("TypeName = 'Newspaper'");
                DataRow[] dr5 = dscount.Tables[0].Select("TypeName = 'Paid Social'");
                DataRow[] dr6 = dscount.Tables[0].Select("TypeName = 'Paid Search'");
                DataRow[] dr7 = dscount.Tables[0].Select("TypeName = 'Others'");
                foreach (DataRow row in dr)
                {
                    ViewBag.TVcount = row[0].ToString();

                }
                foreach (DataRow row in dr2)
                {
                    ViewBag.Radiocount = row[0].ToString();

                }
                foreach (DataRow row in dr3)
                {
                    ViewBag.Directcount = row[0].ToString();

                }
                foreach (DataRow row in dr4)
                {
                    ViewBag.Newspapercount = row[0].ToString();

                }
                foreach (DataRow row in dr5)
                {
                    ViewBag.PaidSocialcount = row[0].ToString();

                }
                foreach (DataRow row in dr6)
                {
                    ViewBag.PaidSearchcount = row[0].ToString();

                }
                foreach (DataRow row in dr7)
                {
                    ViewBag.Otherscount = row[0].ToString();

                }
            }


        }

        public ActionResult CreateCampaign()
        {
            try
            {
                ddlparenttype("");
                ddlsubtypeval("");
                ddlchannelval("");
                ddlIncentive("");
                ddlCreative("");
                ddlLandingpage("");
                ddlPhoneNo("");
                ddlState("");
                ddlCounty("", "0");
                // ViewBag.insermsg = null;

            }
            catch(Exception ex)
            {

            }

            return View();
        }       

        [HttpGet]
        public JsonResult DDLChannel(string name)
        {
            string query = string.Format("select distinct dense_rank() OVER(ORDER BY Channel) AS id,Channel from tbl_m_Types_CM where channel is not null and ParentType = '" + name.Trim() +"'");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            List<ddlvalue> lstrnval = new List<ddlvalue>();
            ddlvalue clsddlvalue;
            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();               
                while (reader.Read())
                {
                    clsddlvalue = new ddlvalue();
                    clsddlvalue.Name = reader.GetString(1);
                    clsddlvalue.ID = reader.GetInt64(0).ToString();
                    lstrnval.Add(clsddlvalue);
                }
                con.Close();
            }            
            return Json(lstrnval, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DDLSubType(string name)
        {
            string query = string.Format("select distinct dense_rank() OVER(ORDER BY SubType) AS id,SubType from tbl_m_Types_CM where SubType is not null and Channel = '" + name.Trim() + "'");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            List<ddlvalue> lstrnval = new List<ddlvalue>();
            ddlvalue clsddlvalue;
            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    clsddlvalue = new ddlvalue();
                    clsddlvalue.Name = reader.GetString(1);
                    clsddlvalue.ID = reader.GetInt64(0).ToString();
                    lstrnval.Add(clsddlvalue);
                }
                con.Close();
            }
            return Json(lstrnval, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DDLCounty(string name)
        {
            string query = "";
            if ((name == "All") || (string.IsNullOrEmpty(name)))
            {
                query = string.Format("select IDENTITY(INT, 1,1) as [Id],a.Name as [Name] INTO #temp from(SELECT Distinct(CountyName ) as [Name] FROM County  ) a order by a.Name asc  select * from #temp");
            }
            else
            {
                query = string.Format("SELECT CountyId as [Id],CountyName as [Name] FROM County WHERE StateId=(select StateId from State where StateName= '" + name.Trim() + "') order by CountyName asc ");
            }
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            List<ddlvalue> lstrnval = new List<ddlvalue>();
            ddlvalue clsddlvalue;
            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    clsddlvalue = new ddlvalue();
                    clsddlvalue.Name = reader.GetString(1);
                    clsddlvalue.ID = reader.GetInt32(0).ToString();
                    lstrnval.Add(clsddlvalue);
                }
                con.Close();
            }
            return Json(lstrnval, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditCampaignLoad(List<campaignfields> objects, string name, string sdate, string edate, string pBudget)
        {
            campaignfields obj_campFields;
            List<campaignfields> list_camp_field = new List<campaignfields>();
            try
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    obj_campFields = new campaignfields();
                    obj_campFields.CampaignID = objects[i].CampaignID;
                    obj_campFields.ParentType = objects[i].ParentType;
                    obj_campFields.SubType = objects[i].SubType;
                    obj_campFields.Channel = objects[i].Channel;
                    obj_campFields.Incentive = objects[i].Incentive;
                    obj_campFields.Creative = objects[i].Creative;
                    obj_campFields.PhoneNo = objects[i].PhoneNo;
                    obj_campFields.LandingPageURL = objects[i].LandingPageURL;
                    obj_campFields.Budget = objects[i].Budget;
                    obj_campFields.State = objects[i].State;
                    obj_campFields.County = objects[i].County;
                    obj_campFields.PlannedCost = objects[i].PlannedCost;
                    obj_campFields.ActualCost = objects[i].ActualCost;
                    obj_campFields.OfferCode = objects[i].OfferCode;
                    list_camp_field.Add(obj_campFields);
                }
                data.UpdateCampaignData(list_camp_field, name, sdate, edate, pBudget, "");
                
            }
            catch (Exception ex)
            {
                return Json(new { title = "Error", typeval = "error" }, JsonRequestBehavior.AllowGet);
            }
            //return RedirectToAction("CreateCampaign", "Home");
            return Json(new { title = "Saved Successfully", typeval = "success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateCampaignload(string id)
        {
            try
            {
                string[] vals = id.Split('|');
                string Name = vals[0];
                string Sdate = vals[1];
                string Edate = vals[2];
                string PlannedBudget = vals[3];
                string Type = vals[4];
                string Channel = vals[5];
                string SubType = vals[6];
                string Incentive = vals[7];
                string Creative = vals[8];
                string PhoneNo = vals[9];
                string Landing = vals[10];
                string Budget = vals[11];
                string OfferCode = vals[12];
                string State = vals[13];
                string County = vals[14];
                string PlannedCost = vals[15];
                string ActualCost = vals[16];

                if (data.insertcampaigndata(Name, Sdate, Edate, PlannedBudget, Type, Channel, SubType, Incentive, Creative, PhoneNo, Landing, Budget, "", State, County, PlannedCost, ActualCost, OfferCode))
                {

                }

            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("CreateCampaign", "Home");
            //  return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Route("Home/EDITCAMPAIGNDATA/{id}")]
        public ActionResult EDITCAMPAIGNDATA(string id)
        {
            id = id.Replace("~", "/");
            // id = System.Net.WebUtility.UrlDecode(id);
            DataSet StoreeditData = new DataSet();
            campaigndata cf = new campaigndata();
            cf.list_campFields = new List<campaignfields>();
            try
            {

                ViewBag.updatemsg = null;
                StoreeditData = data.geteditdata(id);

                for (int i = 0; i < StoreeditData.Tables[0].Rows.Count; i++)
                {
                    cf.campfields = new campaignfields();
                    cf.campfields.CampaignID = StoreeditData.Tables[0].Rows[i]["CampaignID"].ToString();
                    cf.campfields.Budget = StoreeditData.Tables[0].Rows[i]["Budget"].ToString();
                    cf.campfields.ParentType = StoreeditData.Tables[0].Rows[i]["ParentType"].ToString();
                    cf.campfields.SubType = StoreeditData.Tables[0].Rows[i]["SubType"].ToString();
                    cf.campfields.Incentive = StoreeditData.Tables[0].Rows[i]["Incentive"].ToString();
                    cf.campfields.Creative = StoreeditData.Tables[0].Rows[i]["Creative"].ToString();
                    cf.campfields.PhoneNo = StoreeditData.Tables[0].Rows[i]["PhoneNo"].ToString();
                    cf.campfields.LandingPageURL = StoreeditData.Tables[0].Rows[i]["LandingPageURL"].ToString();
                    cf.campfields.Channel = StoreeditData.Tables[0].Rows[i]["Channel"].ToString();
                    cf.campfields.State = StoreeditData.Tables[0].Rows[i]["State"].ToString();
                    cf.campfields.County = StoreeditData.Tables[0].Rows[i]["County"].ToString();
                    cf.campfields.PlannedCost = StoreeditData.Tables[0].Rows[i]["PlannedCost"].ToString();
                    cf.campfields.ActualCost = StoreeditData.Tables[0].Rows[i]["ActualCost"].ToString();
                    cf.campfields.OfferCode = StoreeditData.Tables[0].Rows[i]["OfferCode"].ToString();
                    cf.list_campFields.Add(cf.campfields);
                }
                cf.campfields = new campaignfields();
                cf.campfields.CampaignID = StoreeditData.Tables[0].Rows[0]["CampaignID"].ToString();
                cf.campfields.CampaignName = StoreeditData.Tables[0].Rows[0]["CampaignName"].ToString();
                cf.campfields.StartDate = StoreeditData.Tables[0].Rows[0]["StartDate"].ToString();
                cf.campfields.EndDate = StoreeditData.Tables[0].Rows[0]["EndDate"].ToString();
                cf.campfields.PlannedBudget = StoreeditData.Tables[0].Rows[0]["PlannedBudget"].ToString();
                ddlparenttype("");
                ddlsubtypeval("");
                ddlchannelval("");
                ddlIncentive("");
                ddlCreative("");
                ddlLandingpage("");
                ddlPhoneNo("");
                ddlState("");
                ddlCounty("", "0");
            }
            catch (Exception ex) { }
            return View(cf);
        }


        public ActionResult DELETECAMPAIGNDATA(string id)
        {
            try {
            data.getdeletedata(id);
            TempData["Success"] = "Data has been deleted successfully.";
            } catch (Exception ex) { }
            return RedirectToAction("CampaignDetails", "Home");
        }

        
        public ActionResult CampaignDetails()
        {
            try { 
            data.StoreAllData = data.getdata();
          
            } catch (Exception ex) { }
            return View(data);
        }

        public ActionResult ExportData(string st, string ed)
        {
            try
            {
                DataTable dt = new DataTable();
                if (st != null)
                {
                    dt = data.getfilterdata(st, ed).Tables[0];
                    System.Web.UI.WebControls.GridView gv = new System.Web.UI.WebControls.GridView();
                    gv.DataSource = dt;
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=CampaignData.xls");
                    Response.ContentType = "application/ms-excel";
                    Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    dt = data.getfilterdata("", "").Tables[0];
                    System.Web.UI.WebControls.GridView gv = new System.Web.UI.WebControls.GridView();
                    gv.DataSource = dt;
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=CampaignData.xls");
                    Response.ContentType = "application/ms-excel";
                    Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("SpecificCampaignDetails", "Home");
        }


        //public ActionResult SpecificCampaignDetails()
        //{
        //    try { 
        //    data.filterdata = data.getfilterdata("","");

        //    } catch (Exception ex) { }
        //    return View(data);
        //}

        public ActionResult SpecificCampaignDetails()
        {
            try
            {
                ViewBag.start = "";
                ViewBag.end = "";
                data.filterdata = data.getfilterdata("", "");

            }
            catch (Exception ex) { }
            return View(data);
        }

        //[HttpPost]
        //public ActionResult SpecificCampaignDetails(FormCollection collection)
        //{
        //    try { 
        //    string Sdate = collection["Sdate"];
        //    string Edate = collection["Edate"];
        //    data.filterdata = data.getfilterdata(Sdate, Edate);
        //    } catch (Exception ex) { }
        //    return View(data);
        //}

        public List<SelectListItem> parenttype { get; set; }

        public void ddlparenttype(string selectval)
        {

            string query = string.Format("select distinct dense_rank() OVER(ORDER BY ParentType) AS id,ParentType from tbl_m_Types_CM");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
           
                SqlCommand cmd = new SqlCommand(query, con);
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    parenttype =
                        new List<SelectListItem>();
                    while (reader.Read())
                    {
                        parenttype.Add(
                            new SelectListItem
                            {
                                Text = reader.GetString(1),
                                Value = reader.GetInt64(0).ToString()
                            });
                    }
                    con.Close();
                }
                List<SelectListItem> listparenttype = parenttype;


                if (selectval != "")
                {
                    for (int i = 0; i < listparenttype.Count(); i++)
                    {
                        if (listparenttype[i].Text == selectval)
                        {
                            var item = listparenttype[i];
                            listparenttype[i] = listparenttype[0];
                            listparenttype[0] = item;
                        }
                    }
                }
                ViewBag.parentType = listparenttype;
      
            
          }
        
        public List<SelectListItem> subtype { get; set; }

        public void ddlsubtypeval(string selectval)
        {
            string query = string.Format("select distinct dense_rank() OVER(ORDER BY SubType) AS id,[SubType] from tbl_m_Types_CM where [SubType] is not null");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            if (selectval != "")
            {
                SqlCommand cmd = new SqlCommand(query, con);
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    subtype =
                        new List<SelectListItem>();

                    while (reader.Read())
                    {
                        subtype.Add(
                            new SelectListItem
                            {
                                Text = reader.GetString(1),
                                Value = reader.GetInt64(0).ToString()
                            });
                    }
                    con.Close();
                }
                List<SelectListItem> listsubtype = subtype;
                if (selectval != "")
                {
                    for (int i = 0; i < listsubtype.Count(); i++)
                    {
                        if (listsubtype[i].Text == selectval)
                        {
                            var index = listsubtype.FindIndex(x => x.Text == selectval);
                            var item = listsubtype[index];
                            listsubtype[index] = listsubtype[0];
                            listsubtype[0] = item;
                        }
                    }
                }
                ViewBag.subtype = listsubtype;
            }
            else
            {
                var firstvalue = new SelectList(new[] { "Select" });
                ViewBag.subtype = firstvalue;

            }
        }

        public List<SelectListItem> channel { get; set; }
        public void ddlchannelval(string selectval)
        {

            string query = string.Format("select distinct dense_rank() OVER(ORDER BY Channel) AS id,[Channel] from tbl_m_Types_CM where [Channel] is not null");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            if (selectval != "")
            {
                SqlCommand cmd = new SqlCommand(query, con);
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    channel =
                        new List<SelectListItem>();

                    while (reader.Read())
                    {
                        channel.Add(
                            new SelectListItem
                            {
                                Text = reader.GetString(1),
                                Value = reader.GetInt64(0).ToString()
                            });
                    }
                    con.Close();
                }
                List<SelectListItem> listchannel = channel;
                if (selectval != "")
                {
                    for (int i = 0; i < listchannel.Count(); i++)
                    {
                        if (listchannel[i].Text == selectval)
                        {
                            var index = listchannel.FindIndex(x => x.Text == selectval);
                            var item = listchannel[index];
                            listchannel[index] = listchannel[0];
                            listchannel[0] = item;
                        }
                    }
                }
                ViewBag.channel = listchannel;
            }
            else
            {
                var firstvalue = new SelectList(new[] { "Select" });
                ViewBag.channel = firstvalue;
            }
        }

        public List<SelectListItem> Incentive { get; set; }
        public void ddlIncentive(string selectval)
        {

            string query = string.Format("select IncentiveID,Name from tbl_m_Incentive_CM order by name");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Incentive =
                    new List<SelectListItem>();

                while (reader.Read())
                {
                    Incentive.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt32(0).ToString()
                        });
                }
                con.Close();
            }
            List<SelectListItem> listincentive = Incentive;
            if (selectval != "")
            {
                for (int i = 0; i < listincentive.Count(); i++)
                {
                    if (listincentive[i].Text == selectval)
                    {
                        var index = listincentive.FindIndex(x => x.Text == selectval);
                        var item = listincentive[index];
                        listincentive[index] = listincentive[0];
                        listincentive[0] = item;
                    }
                }
            }
            ViewBag.Incentive = listincentive;
        }

        public List<SelectListItem> Creative { get; set; }
        public void ddlCreative(string selectval)
        {

            string query = string.Format("select CreativeID,Name from tbl_m_Creative_CM order by name");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Creative =
                    new List<SelectListItem>();

                while (reader.Read())
                {
                    Creative.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt32(0).ToString()
                        });
                }
                con.Close();
            }
            List<SelectListItem> listCreative = Creative;
            if (selectval != "")
            {
                for (int i = 0; i < listCreative.Count(); i++)
                {
                    if (listCreative[i].Text == selectval)
                    {
                        var index = listCreative.FindIndex(x => x.Text == selectval);
                        var item = listCreative[index];
                        listCreative[index] = listCreative[0];
                        listCreative[0] = item;
                    }
                }
            }
            ViewBag.Creative = listCreative;
        }

        public List<SelectListItem> PhoneNo { get; set; }
        public void ddlPhoneNo(string selectval)
        {

            string query = string.Format("select PhoneNoID,Name from tbl_m_PhoneNo_CM order by name");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                PhoneNo =
                    new List<SelectListItem>();

                while (reader.Read())
                {
                    PhoneNo.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt32(0).ToString()
                        });
                }
                con.Close();
            }
            List<SelectListItem> listPhoneNo = PhoneNo;
            if (selectval != "")
            {
                for (int i = 0; i < listPhoneNo.Count(); i++)
                {
                    if (listPhoneNo[i].Text == selectval)
                    {
                        var index = listPhoneNo.FindIndex(x => x.Text == selectval);
                        var item = listPhoneNo[index];
                        listPhoneNo[index] = listPhoneNo[0];
                        listPhoneNo[0] = item;
                    }
                }
            }
            ViewBag.PhoneNo = listPhoneNo;
        }
        
        public List<SelectListItem> Landingpage { get; set; }
        public void ddlLandingpage(string selectval)
        {

            string query = string.Format("select LandingpageID,Name from tbl_m_Landingpage_CM order by name");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Landingpage =
                    new List<SelectListItem>();

                while (reader.Read())
                {
                    Landingpage.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt32(0).ToString()
                        });
                }
                con.Close();
            }
            List<SelectListItem> listLandingpage = Landingpage;
            if (selectval != "")
            {
                for (int i = 0; i < listLandingpage.Count(); i++)
                {
                    if (listLandingpage[i].Text == selectval)
                    {
                        var index = listLandingpage.FindIndex(x => x.Text == selectval);
                        var item = listLandingpage[index];
                        listLandingpage[index] = listLandingpage[0];
                        listLandingpage[0] = item;
                    }
                }
            }
            ViewBag.Landingpage = listLandingpage;
        }

        public List<SelectListItem> State { get; set; }

        public void ddlState(string selectval)
        {

            string query = string.Format("exec  proc_c_StateList");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                State =
                    new List<SelectListItem>();

                while (reader.Read())
                {
                    State.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt32(0).ToString()
                        });
                }
                con.Close();
            }
            List<SelectListItem> listState = State;
            if (selectval != "")
            {
                for (int i = 0; i < listState.Count(); i++)
                {
                    if (listState[i].Text == selectval)
                    {
                        var index = listState.FindIndex(x => x.Text == selectval);
                        var item = listState[index];
                        listState[index] = listState[0];
                        listState[0] = item;
                    }
                }
            }
            ViewBag.State = listState;
        }

        public List<SelectListItem> County { get; set; }

        public void ddlCounty(string selectval, string id)
        {

            string query = string.Format("exec  proc_c_CountyList  " + id);

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                County =
                    new List<SelectListItem>();

                while (reader.Read())
                {
                    County.Add(
                        new SelectListItem
                        {
                            Text = reader.GetString(1),
                            Value = reader.GetInt32(0).ToString()
                        });
                }
                con.Close();
            }
            List<SelectListItem> listCounty = County;
            if (selectval != "")
            {
                for (int i = 0; i < listCounty.Count(); i++)
                {
                    if (listCounty[i].Text == selectval)
                    {
                        var index = listCounty.FindIndex(x => x.Text == selectval);
                        var item = listCounty[index];
                        listCounty[index] = listCounty[0];
                        listCounty[0] = item;
                    }
                }
            }
            ViewBag.County = listCounty;
        }
        
        public ActionResult Hierarchy()
        {
            try
            {


            }
            catch (Exception ex) { }
            return View();
        }

        //For Add Incentive Action Methods
        #region Add incentive Action Methods
        public ActionResult AddIncentive()
        {
            ModelState.Clear();
            var model = data.GetAddIncentiveData();
            return View("AddIncentive", model);
        }

        public ActionResult SaveIncentive(string IncentiveID, string Name, string StartDate, string EndDate)
        {
            data.UpdateIncentiveCampaignDetails(IncentiveID, Name, StartDate, EndDate);
            ModelState.Clear();
            var model = data.GetAddIncentiveData();
            return View("AddIncentive",model);
        }

        public ActionResult DeleteIncentive(string IncentiveID)
        {

            if (!string.IsNullOrEmpty(IncentiveID))
            {
                data.DeleteIncentiveCampaignDetails(IncentiveID);
            }
            ModelState.Clear();
            var model = data.GetAddIncentiveData();
            return View("AddIncentive", model);
        }

        public ActionResult IncentiveActivate(string IncentiveID, string Active)
        {
            data.IncentiveActive(IncentiveID, Active);
            ModelState.Clear();
            var model = data.GetAddIncentiveData();
            return View("AddIncentive", model);
        }
        #endregion


        //For Add Creative Action Methods
        #region Add Creatives Action Methods

        [HttpPost]
        public JsonResult SaveCreative(string Name, string StartDate, string EndDate, string filesavedpath, string filename)
        {
            string CID = GlobalVariables.CreativeID;
            if (CID == "")
            {
                data.SaveCreativeData(Name, StartDate, EndDate, filesavedpath, null, filename);
            }
            else
            {
                data.UpdateCreativeCampaignDetails(CID, Name, StartDate, EndDate, filesavedpath, null, filename);
            }
            return Json("Success");
        }

        [HttpPost]
        public ActionResult getUploadFilespath()
        {

            string fname = null;
            List<string> filename = new List<string>();
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {

                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        string path = Path.Combine(Server.MapPath("~/UploadFiles/"), fname);
                        fname = Request.Url.Host + "/UploadFiles/" + fname;
                        filename.Add(fname);
                        file.SaveAs(path);
                    }
                    // Returns message that successfully uploaded  
                    return Json(filename);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        public ActionResult AddCreative()
        {
            ModelState.Clear();
            var model = data.GetAddCreativeData();
            return View("AddCreative", model);

        }

        //public ActionResult SaveCreative(string CreativeID, string Name, string StartDate, string EndDate)
        //{
        //    data.UpdateCreativeCampaignDetails(CreativeID, Name, StartDate, EndDate);
        //    ModelState.Clear();
        //    var model = data.GetAddCreativeData();
        //    return View("AddCreative", model);
        //    //return RedirectToAction("AddCreative");
        //}

        public ActionResult DeleteCreative(string CreativeID)
        {

            if (!string.IsNullOrEmpty(CreativeID))
            {
                data.DeleteCreativeCampaignDetails(CreativeID);
            }
            ModelState.Clear();
            var model = data.GetAddCreativeData();
            return View("AddCreative", model);
        }

        public ActionResult Activate(string CreativeID, string Active)
        {
            data.Active(CreativeID, Active);
            ModelState.Clear();
            var model = data.GetAddCreativeData();
            return View("AddCreative", model);
        }


        public ActionResult UpdateID(string CreativeID)
        {
            GlobalVariables.CreativeID = CreativeID;
            return null;
        }

        #endregion


        //For Phone No Code Action Methods
        #region Add Phone No Code Methods
        public ActionResult AddOffercode()
        {
            ModelState.Clear();
            var model = data.GetPhoneNoData();
            return View("AddOffercode", model);
        }

        public ActionResult SaveOfferCode(string OfferCodeID, string Name, string StartDate, string EndDate)
        {
            data.UpdatePhoneNoCampaignDetails(OfferCodeID, Name, StartDate, EndDate);
            ModelState.Clear();
            var model = data.GetPhoneNoData();
            return View("AddOffercode", model);
        }

        public ActionResult DeleteOfferCode(string OfferCodeID)
        {

            if (!string.IsNullOrEmpty(OfferCodeID))
            {
                data.DeletePhoneNoCampaignDetails(OfferCodeID);
            }
            ModelState.Clear();
            var model = data.GetPhoneNoData();
            return View("AddOffercode", model);
        }

        public ActionResult OfferCodeActivate(string OfferCodeID, string Active)
        {
            data.PhoneNoActive(OfferCodeID, Active);
            ModelState.Clear();
            var model = data.GetPhoneNoData();
            return View("AddOffercode", model);
        }
        #endregion
        

        //For Add Landing Page Methods
        #region Landing Page Methods
        public ActionResult AddLandingpage()
        {
            ModelState.Clear();
            var model = data.GetLandingData();
            return View("AddLandingpage", model);
        }

        public ActionResult SaveLanding(string LandingID, string URL, string Name, string StartDate, string EndDate)
        {
            data.UpdateLandingCampaignDetails(LandingID, URL, Name, StartDate, EndDate);
            ModelState.Clear();
            var model = data.GetLandingData();
            return View("AddLandingpage", model);
        }

        public ActionResult DeleteLanding(string LandingID)
        {
            if (!string.IsNullOrEmpty(LandingID))
            {
                data.DeleteLandingCampaignDetails(LandingID);
            }
            ModelState.Clear();
            var model = data.GetLandingData();
            return View("AddLandingpage", model);
        }

        public ActionResult LandingActivate(string LandingID, string Active)
        {
            data.LandingActive(LandingID, Active);
            ModelState.Clear();
            var model = data.GetLandingData();
            return View("AddLandingpage", model);
        }

        #endregion

        

    }


    public static class GlobalVariables
    {
        // read-write variable
        public static string CreativeID
        {
            get
            {
                return HttpContext.Current.Application["CreativeID"] as string;
            }
            set
            {
                HttpContext.Current.Application["CreativeID"] = value;
            }
        }

    }



}